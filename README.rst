1) starting script prepare_data_original_dialog_summary.py
Inputfile:    
      This script calls 3 scripts
           author_manipulation
           json_pdtb_corenlp
	   pdtbresult_tosent_dict
    output:Dialog_File_midrange_authorformat_corenlp_corenlp_pdtb.json

1) Author manipulation: change S1 to Author, remove HTML formatting tags
2)json_pdtb_corenlp
Most of the processing done here.
 Calls stanford_parser_arg using py4j
You should have the corresponding corenlp_arg.java running as a server.
Gets the annotations and store them as a dictionary
3) Calls pdtb annotations
4)output file
     

Mechanical Turk
1)PrepareinputcontributorMT_v6
2)split_mtcontrib, this merges all the 5 batches and splits into train and test.

3)Run pdtb_coref.py thrice.
First with section 
pdtb
core
 split 
giving train and test with a new column with pdtb and coref replaced.
4)Now always use train and test file with core.
5)run build features. This creates creates the features and also gives a balanced files with features.
6) run classification

Plots
1) Run utterance_number_dialog.py 
2)run histogram_turn

Check Readme_argument_quality