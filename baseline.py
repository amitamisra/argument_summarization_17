from normalize import *
#max_words = 40
#depth = 300
#no_features = 20
#train_length = 4000

"""Sentence is df["sentence"]"""

def getEmbedding(sentence, model):
    
    global max_words, depth, no_features, train_length
#    model = model[0]
    list = np.array([])
    for word in sentence:
        if word in model.wv.vocab:
             list = np.append(list, model.wv[word])
    
    #print list.size
    if(list.size > depth*max_words):
        list = list[0:depth*max_words]
    #print sentence
    pad = np.zeros(depth*max_words - list.size)
    list = np.append(list, pad)
    #print list.shape
    return list
    
"""Word2vec is X_train"""
def run_rnn(word2vec, y_train, flag='reg'):


    """Right now, we don't have additional features, so no merging. Using only word2vec"""
#        Word model
    global max_words, depth, no_features, train_length, history, loss_function, model_flag, validation_ratio
    model_flag = flag
    model_word = Sequential()
    model_word.add(Bidirectional(LSTM(max_words), input_shape=(max_words, depth)))
    model_word.add(Dropout(0.2))
    
    model = model_word
#    model_features = Sequential()
#    model_features.add(Bidirectional(LSTM(max_words, return_sequences=True), input_shape=(no_features, 1)))
#    model_features.add(Dropout(0.2))

#    merged2 = Merge([model_word, model_features], mode='concat')
##        merged = Concatenate([model_word, model_features], axis=-1)

#    model = Sequential()
#    model.add(merged2)
#    model.add(Dropout(0.2))
#    model.add(Bidirectional(LSTM(max_words)))#, return_sequences=True)))
#    model.add(Dropout(0.2))

    model.add(Dense(1, activation='linear'))
#        model.add(Dense(5, activation="softmax"))
#        optimizer = RMSprop(lr=0.01)
#        model.compile(loss='categorical_crossentropy', optimizer=optimizer)
    adam = Adam(lr=0.00055, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=3e-6)
    if flag == 'reg':
        model.compile(loss='mean_squared_error', optimizer=adam)            
#    else:
#        model.compile(loss='binary_cross_entropy', optimizer=adam, batch_size=32)
#    model.fit(word2vec, y_train, batch_size=32, epochs=20, validation_split = 0.05, callbacks=[history])
    model.fit(word2vec, y_train, batch_size=32, epochs=10, validation_split = validation_ratio, callbacks=[history])
    model_json = model.to_json()
    with open("model_rnn.json", "w") as json_file:
        json_file.write(model_json)
    model.save_weights("model_rnn.h5")
    print("Saved model to disk")
#        print(history.History)

    return model

def main():

    global max_words, depth, no_features, train_length, filename

    logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s',\
    level=logging.INFO)

    try:
        [X, y, df] = pickle.load(open("data_rnn"), "rb")
   
    except:

        [df, test] = readData()

        model = wv.Word2Vec(df["tokenized_sents"], size=depth, window=5, min_count=5, workers=4)
        print("length of df is", len(df))
        print(df.size)
        
        df['sentence'] = df['sentence'].map(lambda x: x.encode('unicode-escape').decode('utf-8'))

#        df['tokenized_sents'] = df.apply(lambda row: nltk.word_tokenize(row['sentence'].decode('utf-8')), axis=1)
        
        test['sentence'] = test['sentence'].map(lambda x: x.encode('unicode-escape').decode('utf-8'))

        test['tokenized_sents'] = test.apply(lambda row: nltk.word_tokenize(row['sentence'].decode('utf-8')), axis=1)
        #X.shape[0]#7349
        
        try:
            model = wv.Word2Vec.load("word2vec")
            #model.similarity("this", "is")
#            model.init_sims(replace=True)
            print("loaded")
        except:
            model = wv.Word2Vec(df["tokenized_sents"], size=depth, window=5, min_count=5, workers=4)
            model.save("word2vec")

        df['embedding'] = df['tokenized_sents'].apply(getEmbedding, args=(model,))
        test['embedding'] = test['tokenized_sents'].apply(getEmbedding, args=(model,))
        X_train = list(df['embedding'])
        X_train = np.reshape(np.ravel(X_train), (len(X_train), max_words, depth))
        
        X_test = list(test['embedding'])
        X_test = np.reshape(np.ravel(X_test), (len(X_test), max_words, depth))
        
        y_train = np.array(df['Prediction'])
        y_test = np.array(test['Prediction']) 
#        print y.mean()
#        print y.std()
        y = np.array(df['Prediction']) 
        print(y.mean())
        print(y.std())
#        pickle.dump([X, y, df], open("data_rnn", "wb"))
    

#    features = np.zeros(len(X))
#    X_train = X[0:train_length]
#    X_test = X[train_length:]
#    y_train = y[0:train_length]
#    y_test = y[train_length:]
#    features_train = feature[0:train_length]
#    features_test = feature[train_length:]
    
    try:
        loadmodel
    except:
        model = run_rnn(X_train, y_train)
    pred = model.predict(X_test)  
    print(pred.shape)
    y_test = np.reshape(y_test, (len(y_test), 1))
#    print abs(pred - y_test)
#    print sum(sum(abs(pred - y_test))), float(len(pred))   
    print("MAE = ", sum(sum(abs(pred - y_test))) / float(len(pred)))
#    pred = pred.astype('int16') + 1
#    print sum(sum(pred == y_test)) / float(len(pred))
    print("predictions 1 - 10\n",pred[1:10])
    print("True labels 1 - 10\n",y_test[1:10])
    
main()
