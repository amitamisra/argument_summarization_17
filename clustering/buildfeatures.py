'''
Created on Mar 20, 2017
build feature files
Input : File obtained after running prepare_data_original_dialog_summary 
Corenlp column: Acces corenlp information


@author: amita
'''
import time
from collections import ChainMap
import os
import pandas as pd
import configparser, logging
#from collections import ChainMap
from word2vec_extractor import Word2vecExtractor
from timeit import default_timer as timer
from src import file_utilities
from soft_clustering import FuzzyCmeans
from pdtb_extractor import PDTBExtractor
#import skip_thought_vectors_py3.addskipthought as addskipthought
logging.basicConfig(level=logging.CRITICAL)

logger = logging.getLogger(__name__)

features=["w2vec"]
cluster_features=["Word2VecfeatureGoogle"]
numclusterrange=(2,9)
def addw2vecfeatures(sentence,W2vecextractor):
        w2vec_scores=W2vecextractor.extract(sentence)
        return w2vec_scores

def addpdtbfeatures(row,pdtbcol):
    if "pdtb" in features:
        pdtb_vec=PDTBextractor.extract(row)
        return pdtb_vec
           
def applyfeatures(row,tokencol,W2vecextractor):
    
    if "w2vec" in features:
        sentence=" ".join(row[tokencol])
        w2vec_scores=addw2vecfeatures(sentence,W2vecextractor)
        z= dict(ChainMap(w2vec_scores))
        return pd.Series(z)

def createfuzzyclusters(df,feature_df,cluster_features,numclusterrange):
    fuzzy=FuzzyCmeans(df,feature_df,cluster_features,numclusterrange)
    filtered_df=fuzzy.select_clusterfeatures()
    fuzzy.callcmeans(filtered_df,numclusterrange)
    
    
    
def cluster_sent_dialog(sentencesdf,tokencol,W2vecextractor,features_file):  
    #sentence_list=row[tokencol].values
    
    start = timer()
    feature_df=sentencesdf.apply(applyfeatures, args=(tokencol,W2vecextractor),axis=1)
    createfuzzyclusters(sentencesdf,feature_df,cluster_features,numclusterrange)
    feature_df.fillna(0,inplace=True)
    merged_df= sentencesdf.join(feature_df)
    end = timer()
    print("feature generation for {0}took".format(str(features)))
    print(end - start)      
    merged_df.to_csv(features_file)


    
def cluster_list_dialogs(row,dfcolumn_list,tokencol,W2vecextractor,features_file):
    """
    col_corenlp, we have features from corenlp here, process them to create features for the task
    """
    col=row[dfcolumn_list[0]]
    corenlp_info=col[dfcolumn_list[1]]
    sentencesdf=pd.DataFrame(corenlp_info)
    
    key=row["key"]
 
    logger.info("clustering dialog {0}".format(key)) 
    cluster_sent_dialog(sentencesdf,tokencol,W2vecextractor,features_file)
    
        
def clusterdocs(input_file,features_file,dfcolumn_list,sentcolumn,w2vec_loc,srparser_loc,parse_model):
    if "w2vec" in features:
        W2vecextractor=Word2vecExtractor(w2vec_loc)   
    else:
        W2vecextractor=""                                       
    logger.info("inputfile with all the documents{0}".format(input_file))   
    df=pd.read_json(input_file+".json")
    df.apply(cluster_list_dialogs,args=(dfcolumn_list,sentcolumn,W2vecextractor,features_file),axis=1)


    
def run(section):
    args= readCongigFile(section)
    inputfile=args[0]
    features_file=args[1]
    dfcolumn_list=args[2]
    sentcolumn=args[3]
    w2vec_loc=args[4]
    srparser_loc=args[5]
    parse_model=args[6]
    clusterdocs(inputfile,features_file,dfcolumn_list,sentcolumn,w2vec_loc,srparser_loc,parse_model)   
      

def readCongigFile(section):
    config = configparser.ConfigParser()
    config_file=file_utilities.get_absolutepath_data("config","buildfeatures_Config.ini")
    config.read(config_file)
    
    dfcolumn_list=config.get(section,'datacolumn').split(",")
    sentcolumn=config.get(section,'tokencolumn')
    srparser_loc=config.get(section,'srparser_loc')
    parse_model=config.get(section,'parse_model')
    w2vec_loc=config.get(section,'w2vec_loc')
    input_file=config.get(section,"input_file")
    feature_file=config.get(section,'feature_file')
   
    #input_file=file_utilities.get_absolutepath_data(input_file_config)
    #feature_file=file_utilities.get_absolutepath_data(feature_file_config) 
    arguments=(input_file,feature_file,dfcolumn_list,sentcolumn,w2vec_loc,srparser_loc,parse_model)

    return  (arguments)
    
if __name__ == '__main__':
    section="dialog_clustering"
    run(section)
    
