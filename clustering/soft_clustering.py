'''
Created on Mar 20, 2017

@author: amita
'''
import skfuzzy
import numpy as np
import matplotlib
matplotlib.use('PS')
import matplotlib.pyplot as plt
import numpy as np
from sklearn.decomposition import PCA



class Pca_class():
    def __init__(self,num):
        self.num=num
    def createpca(self):
        pcaobj=PCA(n_components=self.num)
        return pcaobj


    
class FuzzyCmeans():
    '''
    df: consists of sentences in a dialog
    
    df_allfeatures:all features obtained through applyfeatures in buildfeatures
    
    clusterfeaturelist: slelected features you want for clustering
    num_cluster_range: TO DO, for now it uses a fixed range between 2-9
    '''
        
    def select_clusterfeatures(self):
        colnames=self.df_allfeatures.columns.values.tolist()
        feature_cols=[col for col in colnames if str(col).startswith(tuple(self.clusterfeaturelist))]
        filtered_df=self.df_allfeatures.filter(feature_cols)
        return filtered_df
    
    def callcmeans(self,filtereddf,numclustersrange):
        colors = ['b', 'orange', 'g', 'r', 'c', 'm', 'y', 'k', 'Brown', 'ForestGreen']
        fig1, axes1 = plt.subplots(3, 3, figsize=(8, 8))
        fpcs = []
        #pca=Pca_class(10)
        #pcaobj=pca.createpca()
        #reduceddf=pcaobj.fit_transform(filtereddf)
        #alldata=filtereddf.values
        for ncenters, ax in enumerate(axes1.reshape(-1), 2):
            cntr, u, u0, d, jm, p, fpc = skfuzzy.cluster.cmeans(reduceddf.transpose(), ncenters, 2, error=0.005, maxiter=1000, init=None)
        
            # Store fpc values for later
            fpcs.append(fpc)
        
            # Plot assigned clusters, for each data point in training set
            #cluster_membership = np.argmax(u, axis=0)
            #print(cluster_membership)
            labels=[np.argmax(elem) for elem in u.transpose()]
            print(labels)
            self.df["tokens"].values
            print (u)
            #----------------------------------------- for j in range(ncenters):
                #------------------------ ax.plot(xpts[cluster_membership == j],
                        #-- ypts[cluster_membership == j], '.', color=colors[j])
        
            # Mark the center of each fuzzy cluster
            #--------------------------------------------------- for pt in cntr:
                #----------------------------------- ax.plot(pt[0], pt[1], 'rs')
#------------------------------------------------------------------------------ 
            # ax.set_title('Centers = {0}; FPC = {1:.2f}'.format(ncenters, fpc))
            #---------------------------------------------------- ax.axis('off')
    # Plot assigned clusters, for each data point in training set
            cluster_membership = np.argmax(u, axis=0)
            print(fpc)
        pass
            
    def __init__(self,df,df_allfeatures,clusterfeaturelist,num_cluster_range):
        '''
       
        '''
        self.df=df
        self.clusterfeaturelist=clusterfeaturelist
        self.df_allfeatures=df_allfeatures
        self.num_cluster_range=num_cluster_range
        
        