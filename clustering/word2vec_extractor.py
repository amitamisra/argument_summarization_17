'''
Created on Oct 11, 2016

@author: amita
'''
import numpy as np
import nltk
import string
from nltk.corpus import stopwords
import gensim

stops = set(stopwords.words("english"))
punct = set(string.punctuation)

class Word2vecExtractor:

    def __init__(self, w2vecmodel):
            # w2v = gensim.models.Word2Vec.load_word2vec_format("nlp/tools/word2vec/trunk/GoogleNews-vectors-negative300.bin", binary=True)
        self.w2vecmodel=gensim.models.Word2Vec.load_word2vec_format(w2vecmodel,binary=True)
    
    def sen2vec(self,sentence):    
        words = [word for word in nltk.word_tokenize(sentence) if word not in stops and word not in punct]
        res = np.zeros(self.w2vecmodel.vector_size)
        count = 0
        for word in words:
            if word in self.w2vecmodel:
                count += 1
                res += self.w2vecmodel[word]

        if count != 0:
            res /= count

        return res 
    
    def features_todict(self,vector):    
        number_w2vec=vector.size   
          
        columnnames=["Word2VecfeatureGoogle_"+ str(i) for i in range(0, number_w2vec)]  
        feature_dict=dict(zip(columnnames,vector)) 
        return feature_dict
    
    def extract(self,sentence):
        v1=self.sen2vec(sentence)
        w2vec_dict=self.features_todict(v1)
        return w2vec_dict
                                
        