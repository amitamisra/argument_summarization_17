from utils import utils
import dialogue_systems.dialogue_systems.config as config
import os

def parse_swda(SWDA_DATA):
    swda_utt_dirs = os.listdir(SWDA_DATA)
    utterances = []
    for utt_dir in swda_utt_dirs:
        curr_dir = SWDA_DATA + utt_dir
        curr_dir_files = os.listdir(curr_dir)
        for utt_file in curr_dir_files:
            rows, fieldnames = utils.read_csv(curr_dir+"/"+utt_file)
            for row in rows:
                id = "_".join([row["swda_filename"], row["ptb_basename"], row["conversation_no"],
                              row["transcript_index"]])
                text = row["text"]
                act_tag = row["act_tag"]
                utterances += [[id, act_tag, text]]
    return utterances

if __name__=="__main__":
    utterances = parse_swda(config.SWDA_DATA)
    utils.write_csv_py3(config.SWDA_SINGLE_FILE, utterances, ["id", "act_tag", "text"])