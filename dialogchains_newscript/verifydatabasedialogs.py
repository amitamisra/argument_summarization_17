'''
Created on Feb 19, 2017
I am writing this script to verify the dialogs i get from database map correctly to what I used for summaries
@author: amita
'''
import pandas as pd
from bs4 import BeautifulSoup

def createhtml(row, summcol,databasecol,keycol):
    text=""
    summary=row[summcol]
    database=row[databasecol]
    keycol=row[keycol]
    text=text+ "<br>" + "<br><b>Key</b>" + str(keycol)+ "<b>Dialogtext_summary</b>" + "<br>" + summary + "<br>" + "<b>Dialogtext_database</b>" + "<br>" + database 
    return text

def writehtml(input_database,summcol,databasecol,keycol,out_file):
    df=pd.read_csv(input_database)
    df["database_summary"]=df.apply(createhtml, args=(summcol,databasecol,keycol),axis=1)
    lines=df["database_summary"].values  
    print(lines[0])  
    all_lines=" ".join(lines)
    all_lines= """<html><head></head><body>""" + all_lines +"""</body> </html>"""  
    with open(out_file, "w", encoding="utf-8") as f:
        f.write(all_lines)    
if __name__ == '__main__':
    summcol="Dialog_shownfor_summary"
    databasecol="Dialog_from_database"
    input_database="/Users/amita/git/argument_summarization_17/data/data_database/gun-control/NaturalSummaryCSV_database.csv"
    keycol="key"
    out_file="/Users/amita/git/argument_summarization_17/data/data_database/gun-control/NaturalSummaryCSV_database_html"
    writehtml(input_database,summcol,databasecol,keycol,out_file)