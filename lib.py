"""Config file. All constants, hyperparameters and linraries included here"""

##############################################################################################################################################

"""Libraries"""

import pandas as pd
import logging
from pandas import DataFrame
from keras.callbacks import History 
history = History()
import os
from nltk.corpus import stopwords
import re
#import enchant
from nltk.stem.porter import *
import numpy as np
import cPickle as pickle
from collections import Counter
from keras.models import model_from_json
import math
import signal
import h5py
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten, LSTM, Bidirectional
from keras.layers import Convolution1D, MaxPooling1D, Convolution2D, MaxPooling2D
from keras.optimizers import SGD
from keras.optimizers import Adam
from keras.constraints import maxnorm
from keras import backend as K
from scipy.sparse import csr_matrix
from sklearn.manifold import TSNE
import codecs
import pylab as plot
import random
from sklearn.utils import shuffle
import nltk
import gensim.models.word2vec as wv
from keras.layers import Merge
import copy
from collections import Counter
from sklearn.metrics import confusion_matrix

###############################################################################################################################################

"""Constants and hyper parameters"""

K.set_image_dim_ordering('th')
max_words = 40 #number of word in text
depth = 100#300 #length of embedding of a word of Word2vec. Can be set to anything.
no_features = 20 # Number of additional features. Currently, this variable does nothing.
data_length = 12000 #Total number of samples
ratio = 0.8 #Percentage of training samples
train_length = int(ratio * data_length) #Number of training samples
filename_train = "train.csv"#gc_all_sentences_with_predictions_filtered.csv" #Names of the file is the data folder
filename_val = "val.csv"
filename_test = "test.csv"
#Takes only regression now
model_flag = "reg" # reg or classification, currently takes in only reg.
validation_ratio = 0.05 #ratio of validation samples to training samples
###################################################################################################################################################

