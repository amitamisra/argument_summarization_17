'''
Created on Nov 15, 2016

@author: amita
'''
import os
import numpy as np
np.random.seed(1337)

from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils.np_utils import to_categorical
from keras.layers import Dense, Input, Flatten
from keras.layers import Conv1D, MaxPooling1D, Embedding, Bidirectional
from keras.models import Model
from keras.models import Sequential
from keras.layers import LSTM
from keras.layers.embeddings import Embedding
from keras.preprocessing import sequence
from keras.layers.convolutional import Convolution1D
import sys
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from keras.layers import Dropout
from sklearn.model_selection import GridSearchCV
from keras.wrappers.scikit_learn import KerasClassifier
import codecs
from pprint import pprint
from keras.callbacks import ModelCheckpoint
import time
GLOVE_DIR = "/Users/amita/software/" + '/glove.6B/'
MAX_SEQUENCE_LENGTH = 50
MAX_NB_WORDS = 20000
EMBEDDING_DIM = 100
VALIDATION_SPLIT = 0.2
TRUNC='post'
PADDING="post"
from keras.callbacks import ModelCheckpoint
word_index={}
embeddings_index = {}
f = open(os.path.join(GLOVE_DIR, 'glove.6B.100d.txt'), "r", encoding='utf-8')
for line in f:
    values = line.split()
    word = values[0]
    coefs = np.asarray(values[1:], dtype='float32')
    embeddings_index[word] = coefs
f.close()

print('Found %s word vectors.' % len(embeddings_index))

# second, prepare text samples and their labels
print('Processing text dataset')

def vectorize_text (input_train,input_test,textcolumn,labelcol):
    """ vectorize the text samples into a 2D integer tensor
    """
    global word_index
    df_train=pd.read_csv(input_train)
    train_texts=df_train[textcolumn].values
    labels_train=df_train[labelcol].values
    tokenizer = Tokenizer(nb_words=MAX_NB_WORDS)
    tokenizer.fit_on_texts(train_texts)
    sequences_train = tokenizer.texts_to_sequences(train_texts)
    word_index = tokenizer.word_index
    print('Found %s unique tokens.' % len(word_index))

    # encode class values as integers
    encoder = LabelEncoder()
    encoder.fit(labels_train)
    encoded_labels_train = encoder.transform(labels_train)
    # convert integers to dummy variables (i.e. one hot encoded)
    labels_train= to_categorical(encoded_labels_train)
    data_train = pad_sequences(sequences_train, maxlen=MAX_SEQUENCE_LENGTH,truncating=TRUNC,padding=PADDING)
    
    df_test=pd.read_csv(input_test)
    test_texts=df_test[textcolumn].values
    labels_test=df_test[labelcol].values
    sequences_test = tokenizer.texts_to_sequences(test_texts)
    data_test= pad_sequences(sequences_test, maxlen=MAX_SEQUENCE_LENGTH,truncating=TRUNC,padding=PADDING)
    labels_test = to_categorical(encoder.transform(labels_test))
    
    print('Shape of data tensor:', data_train.shape)
    print('Shape of label tensor:', labels_train.shape)
    return(data_train,labels_train,data_test,labels_test)


def create_embedding_matrix(word_index):    
    nb_words = min(MAX_NB_WORDS, len(word_index))
    embedding_matrix = np.zeros((nb_words + 1, EMBEDDING_DIM))
    for word, i in word_index.items():
        if i > MAX_NB_WORDS:
            continue
        embedding_vector = embeddings_index.get(word)
        if embedding_vector is not None:
            # words not found in embedding index will be all-zeros.
            embedding_matrix[i] = embedding_vector

    # load pre-trained word embeddings into an Embedding layer
    # note that we set trainable = False so as to keep the embeddings fixed
    embedding_layer = Embedding(nb_words + 1,
                                EMBEDDING_DIM,
                                weights=[embedding_matrix],
                                input_length=MAX_SEQUENCE_LENGTH,
                                trainable=True,dropout=0.2)
    return embedding_layer 

def split_train_val(data,labels):
    indices = np.arange(data.shape[0])
    np.random.shuffle(indices)
    data = data[indices]
    labels = labels[indices]
    nb_validation_samples = int(VALIDATION_SPLIT * data.shape[0])
    x_train = data[:-nb_validation_samples]
    y_train = labels[:-nb_validation_samples]
    x_val = data[-nb_validation_samples:]
    y_val = labels[-nb_validation_samples:]
    return(x_train,y_train,x_val,y_val)
    

def create_model(optimizer='rmsprop'):
    model = Sequential()
    embedding_layer= create_embedding_matrix(word_index)
    model.add(embedding_layer)
    model.add(Convolution1D(nb_filter=32, filter_length=3, border_mode='same', activation='relu'))
    model.add(MaxPooling1D(pool_length=2))
    model.add(Dropout(0.2)) 
    model.add(Bidirectional(LSTM(200)))
    model.add(Dense(2, activation='sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['accuracy']) 
    return model
    


def run(input_train,input_test,text_col,label_col,resfile): 
    data_train,labels_train, data_test,labels_test=vectorize_text(input_train,input_test,text_col,label_col) 
    x_train,y_train= data_train,labels_train
    x_test, y_test= data_test,labels_test
    filepath="weights.best.hdf5"
    checkpoint = ModelCheckpoint(filepath, monitor='val_acc', verbose=1, save_best_only=True, mode='max')
    callbacks_list = [checkpoint]
# Fit the model
    model=create_model()
    model.fit(x_train, y_train, validation_split=0.2, nb_epoch=3, batch_size=100, callbacks=callbacks_list, verbose=0)
    scores = model.evaluate(x_test, y_test, verbose=0)
    time.sleep(0.2)
    print("Accuracy: %.2f%%" % (scores[1]*100))
    
       
if __name__ == '__main__':
    input_train="/Users/amita/git/tweet_politics/data/tweets/splits/train_80_no_hashtags.csv"
    input_test="/Users/amita/git/tweet_politics/data/tweets/splits/test_no_hashtags_balanced.csv"
    text_col="text"
    label_col="class"
    resfile="/Users/amita/git/deeplearn/data/train_80_no_hashtags_progress"
    run(input_train,input_test,text_col,label_col,resfile)