from lib import *
def handler(signum, frame):
    print 'Ctrl+Z pressed'
    assert False
signal.signal(signal.SIGTSTP, handler)

def readData():
    
    global filename_train, filename_val, filename_test, data_length
    cwd = os.getcwd()
    path = cwd + "/data/NNdata/" + filename_train
    df = pd.read_csv(path, sep=',', encoding='utf-8');
    df['Prediction'] = pd.to_numeric(df['Prediction'], errors='coerce')
    df = df.dropna(subset=['Prediction', 'sentence'])
    df = df.sample(frac=1.0, random_state=23)
    df = df.reset_index()
    df = df[['sentence', 'Prediction']] 
#    df['sentence'] = df['sentence'].apply(cleanhtml).apply(cleanUrl)#.apply(removeTrailingHash);
#    df['sentence'] = tokenize_and_stopwords(df['sentence'])

    path = cwd + "/data/NNdata/" + filename_val;
    df_val = pd.read_csv(path, sep=',', encoding='utf-8');
    df_val['GoodSliderMean'] = pd.to_numeric(df_val['GoodSliderMean'], errors='coerce')
    df_val = df_val.dropna(subset=['GoodSliderMean', 'Phrase.x'])
    df_val = df_val.sample(frac=1.0, random_state=23)
    df_val = df_val.reset_index()
    df_val = df_val[['Phrase.x', 'GoodSliderMean']] 
    df_val = df_val.rename(columns={'GoodSliderMean': 'Prediction', 'Phrase.x':'sentence'})    

    frame = [df, df_val]
    train = pd.concat(frame)
    train['tokenized_sents'] = train.apply(lambda row: nltk.word_tokenize(row['sentence']), axis=1)
        
    path = cwd + "/data/NNdata/" + filename_test;
    df_test = pd.read_csv(path, sep=',', encoding='utf-8');
    df_test['GoodSliderMean'] = pd.to_numeric(df_test['GoodSliderMean'], errors='coerce')
    df_test = df_test.dropna(subset=['GoodSliderMean', 'Phrase.x'])
    df_test = df_test.sample(frac=1.0, random_state=23)
    df_test = df_test.reset_index()
    df_test = df_test[['Phrase.x', 'GoodSliderMean']] 
    test = df_test.rename(columns={'GoodSliderMean': 'Prediction', 'Phrase.x':'sentence'})   
    
    test['tokenized_sents'] = test.apply(lambda row: nltk.word_tokenize(row['sentence']), axis=1)
    print len(train), len(test)
    return [train, test]

def tokenize_and_stopwords(data_sample):

    print type(data_sample)
    print len(data_sample)
    #Get all english stopwords
    try:
        words = open("common_words.txt", "r").readlines()
        for i in range(len(words)):
            words[i] = words[i].strip()
    except: 
        words = []
    
    print "words", words
#    abb_dict = pickle.load(open("abbreviations", "r"))
    stop = stopwords.words('english') + words #list(string.punctuation) + ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    #Use only characters from reviews
    data_sample = data_sample.str.replace("[^a-zA-Z ]", " ")#, " ")
    data_sample = data_sample.str.lower()
                
    return [(" ").join([i for i in sentence.split() if i not in stop]) for sentence in data_sample]

def cleanhtml(tweet):
      cleanr = re.compile('<.*?>')
      cleansentence = re.sub(cleanr, '', tweet)
      return cleansentence

def cleanUrl(tweet):
    tweet= re.sub(r"http\S+", "",  tweet)
    return tweet;

def removeMention(tweet):
    tweet = tweet.replace("rt@","").rstrip()
    tweet = tweet.replace("rt ","").rstrip()
    tweet = tweet.replace("@","").rstrip()
    return tweet;

def stemmer(preprocessed_data_sample):
    print "stemming "
    #Create a new Porter stemmer.
    stemmer = PorterStemmer()
    #try:
    for i in range(len(preprocessed_data_sample)):
        #Stemming
        try:
            preprocessed_data_sample[i] = preprocessed_data_sample[i].replace(preprocessed_data_sample[i], " ".join([stemmer.stem(str(word)) for word in preprocessed_data_sample[i].split()]))
        except:
        #No stemming
            preprocessed_data_sample[i] = preprocessed_data_sample[i].replace(preprocessed_data_sample[i], " ".join([str(word) for word in preprocessed_data_sample[i].split()]))
    return preprocessed_data_sample
    
def load_embeddings(file_name):
 
    with codecs.open(file_name, 'r', 'utf-8') as f_in:
        vocabulary, wv = zip(*[line.strip().split(' ', 1) for line in 
f_in])
    wv = np.loadtxt(wv)
    return wv, vocabulary
    
#feature extraction - TFIDF and unigrams
def vectorize(preprocessed_data_sample):
    from sklearn.feature_extraction.sentence import TfidfVectorizer

    # Initialize the "CountVectorizer" object, which is scikit-learn's
    # bag of words tool.
#    no_features = 1000#500#806#150#800#600#350

    #ngram_range=(1, 1)
#    input=u'content', encoding=u'utf-8', decode_error=u'strict', strip_accents=None, lowercase=True, preprocessor=None, tokenizer=None, analyzer=u'word', stop_words=None, token_pattern=u'(?u)\b\w\w+\b', ngram_range=(1, 1), max_df=1.0, min_df=1, max_features=None, vocabulary=None, binary=False, dtype=<type 'numpy.int64'>, norm=u'l2', use_idf=True, smooth_idf=True, sublinear_tf=False

    vectorizer = TfidfVectorizer(analyzer = "word", tokenizer = None, preprocessor = None, stop_words = None, vocabulary = None, ngram_range=(1,1), strip_accents=None)#, max_features = no_features)#, ngram_range=(2,2))
    #vectorizer = CountVectorizer(analyzer = "word", tokenizer = None, preprocessor = None, stop_words = None, ngram_range=(2,2), max_features = no_features)
    # fit_transform() does two functions: First, it fits the model
    # and learns the vocabulary; second, it transforms our training data
    # into feature vectors. The input to fit_transform should be a list of
    # strings.
    train_data_features = vectorizer.fit_transform(preprocessed_data_sample)

    # Numpy arrays are easy to work with, so convert the result to an
    # array
    train_data_features = train_data_features.toarray()
    return [train_data_features, vectorizer, no_features]
