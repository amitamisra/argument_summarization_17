'''
Created on Apr 2, 2016
run various regression algorithms, 
parameter tuning and feature selection can be done by pipeline and parameter grid 
There are a few default parameter setting for each regressor. 
These can be changed using a csv input
The function run reads a csv containing a sequence of train,test and regression models to run . parameter can be set in this csv
The final output contains actual and predicted scores written to a csv, alongwith regression metrics.
Fields in Csv
ModelType,
Parameters,
data_length,
ratio,
train_file_name,
val_file_name,
test_file_name,
val_ratio,
embeddings,
type,
dropout,
savedModel,
done
        
( These parameters for the deepnet)
scoring: scoring critertia for nested CV
Featureselection: by default I am using auto for all the features, You can give a list of features like[10,20,40]
featurelist: is the specific feature names you want before any feature selection is done. They should be separated by comma.
you can just specify initial distinguishing characters for features. 
For example to keep unigram_i,bigram_i_am  as features, the feature list should be unigram, bigram
It then creates  a directory baseoutput/featurelist/classifier/featurelist_CV.csv for nested cross validation
It then creates  a directory baseoutput/featurelist/classifier/featurelist_test.csv for evaluation on nested test set
CV follds=10, can be changed in the script, See default settings below 
@author: sharath
'''
import configparser

import pandas as pd, os
#import regression
#import  file_utilities
import logging
import gensim
from scipy import spatial

##############################################################################################################################################

"""Libraries"""

import pandas as pd
import logging
from pandas import DataFrame
from keras.callbacks import History 
history = History()
import os
from nltk.corpus import stopwords
import re
#import enchant
from nltk.stem.porter import *
import numpy as np
import cPickle as pickle
from collections import Counter
from keras.models import model_from_json
import math
import signal
import h5py
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten, LSTM, Bidirectional
from keras.layers import Convolution1D, MaxPooling1D, Convolution2D, MaxPooling2D
from keras.optimizers import SGD
from keras.optimizers import Adam
from keras.constraints import maxnorm
from keras import backend as K
from scipy.sparse import csr_matrix
from sklearn.manifold import TSNE
import codecs
import pylab as plot
import random
from sklearn.utils import shuffle
import nltk
import gensim.models.word2vec as wv
from keras.layers import Merge
import copy
from collections import Counter
from sklearn.metrics import confusion_matrix
#from normalize import *

###############################################################################################################################################

"""Default Constants and hyper parameters"""

K.set_image_dim_ordering('th')
default_max_words = 40 #number of word in text
default_depth = 100#300 #length of embedding of a word of Word2vec. Can be set to anything.
default_features = 20 # Number of additional features. Currently, this variable does nothing.
default_data_length = 12000 #Total number of samples
default_ratio = 0.8 #Percentage of training samples
default_train_length = int(default_ratio * default_data_length) #Number of training samples
default_filename_train = "train.csv"#gc_all_sentences_with_predictions_filtered.csv" #Names of the file is the data folder
default_filename_val = "val.csv"
default_filename_test = "test.csv"
#Takes only regression now
default_model_flag = "reg" # reg or classification, currently takes in only reg.
default_validation_ratio = 0.05 #ratio of validation samples to training samples
###################################################################################################################################################


##logging.basicConfig(level=logging.INFO)
##logger = logging.getLogger(__name__)

##PROJECT_DIR = os.path.dirname(os.path.abspath((os.path.join(os.path.dirname(__file__), ".."))))
##RESOURCE_DIR = os.path.abspath(os.path.join(PROJECT_DIR, "resources"))
##defaultalphastart=1
##defaultalphaend=10
##defaultalphacount=10
##defaultgammastart=1
##defaultgammaend=30
##defaultCstart=1
##defaultCend=60
##defaultgammacount=10
##defaultCcount=10
##defaultscoring="mean_squared_error"
##defaultfolds=10

def handler(signum, frame):
    print 'Ctrl+Z pressed'
    assert False
signal.signal(signal.SIGTSTP, handler)

def readData(filename_train, filename_val, filename_test, data_length):
    
#    cwd = os.getcwd()
#    path = cwd + "/data/NNdata/" + filename_train
    df = pd.read_csv(filename_train, sep=',', encoding='utf-8');
    df['Prediction'] = pd.to_numeric(df['Prediction'], errors='coerce')
    df = df.dropna(subset=['Prediction', 'sentence'])
    df = df.sample(frac=1.0, random_state=23)
    df = df.reset_index()
    df = df[['sentence', 'Prediction']] 
#    df['sentence'] = df['sentence'].apply(cleanhtml).apply(cleanUrl)#.apply(removeTrailingHash);
#    df['sentence'] = tokenize_and_stopwords(df['sentence'])

#    path = cwd + "/data/NNdata/" + filename_val;
    df_val = pd.read_csv(filename_val, sep=',', encoding='utf-8');
    df_val['GoodSliderMean'] = pd.to_numeric(df_val['GoodSliderMean'], errors='coerce')
    df_val = df_val.dropna(subset=['GoodSliderMean', 'Phrase.x'])
    df_val = df_val.sample(frac=1.0, random_state=23)
    df_val = df_val.reset_index()
    df_val = df_val[['Phrase.x', 'GoodSliderMean']] 
    df_val = df_val.rename(columns={'GoodSliderMean': 'Prediction', 'Phrase.x':'sentence'})    

    frame = [df, df_val]
    train = pd.concat(frame)
    train['tokenized_sents'] = train.apply(lambda row: nltk.word_tokenize(row['sentence']), axis=1)
        
#    path = cwd + "/data/NNdata/" + filename_test;
    df_test = pd.read_csv(filename_test, sep=',', encoding='utf-8');
    df_test['GoodSliderMean'] = pd.to_numeric(df_test['GoodSliderMean'], errors='coerce')
    df_test = df_test.dropna(subset=['GoodSliderMean', 'Phrase.x'])
    df_test = df_test.sample(frac=1.0, random_state=23)
    df_test = df_test.reset_index()
    df_test = df_test[['Phrase.x', 'GoodSliderMean']] 
    test = df_test.rename(columns={'GoodSliderMean': 'Prediction', 'Phrase.x':'sentence'})   
    
    test['tokenized_sents'] = test.apply(lambda row: nltk.word_tokenize(row['sentence']), axis=1)
    print len(train), len(test)
    return [train, test]

def tokenize_and_stopwords(data_sample):

    print type(data_sample)
    print len(data_sample)
    #Get all english stopwords
    try:
        words = open("common_words.txt", "r").readlines()
        for i in range(len(words)):
            words[i] = words[i].strip()
    except: 
        words = []
    
    print "words", words
#    abb_dict = pickle.load(open("abbreviations", "r"))
    stop = stopwords.words('english') + words #list(string.punctuation) + ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    #Use only characters from reviews
    data_sample = data_sample.str.replace("[^a-zA-Z ]", " ")#, " ")
    data_sample = data_sample.str.lower()
                
    return [(" ").join([i for i in sentence.split() if i not in stop]) for sentence in data_sample]

def cleanhtml(tweet):
      cleanr = re.compile('<.*?>')
      cleansentence = re.sub(cleanr, '', tweet)
      return cleansentence

def cleanUrl(tweet):
    tweet= re.sub(r"http\S+", "",  tweet)
    return tweet;

def removeMention(tweet):
    tweet = tweet.replace("rt@","").rstrip()
    tweet = tweet.replace("rt ","").rstrip()
    tweet = tweet.replace("@","").rstrip()
    return tweet


"""Sentence is df["sentence"]"""

def getEmbedding(sentence, model, max_words, depth):
    
#    global max_words, depth#, no_features, train_length
#    model = model[0]
    list = np.array([])
    for word in sentence:
        if word in model.wv.vocab:
             list = np.append(list, model.wv[word])
    
    #print list.size
    if(list.size > depth*max_words):
        list = list[0:depth*max_words]
    #print sentence
    pad = np.zeros(depth*max_words - list.size)
    list = np.append(list, pad)
    #print list.shape
    return list
    
"""Word2vec is X_train"""
def run_rnn(X_train, y_train, flag, max_words, depth, train_length, validation_ratio, saved_model, dropout):

    """Right now, we don't have additional features, so no merging. Using only word2vec"""
#        Word model
    global history#
    model_flag = flag
    model_word = Sequential()
    model_word.add(Bidirectional(LSTM(max_words), input_shape=(max_words, depth)))
    model_word.add(Dropout(dropout))
    
    model = model_word
#    model_features = Sequential()
#    model_features.add(Bidirectional(LSTM(max_words, return_sequences=True), input_shape=(no_features, 1)))
#    model_features.add(Dropout(dropout))

#    merged2 = Merge([model_word, model_features], mode='concat')
##        merged = Concatenate([model_word, model_features], axis=-1)

#    model = Sequential()
#    model.add(merged2)
#    model.add(Dropout(dropout))
#    model.add(Bidirectional(LSTM(max_words)))#, return_sequences=True)))
#    model.add(Dropout(dropout))
#        model.add(Dense(5, activation="softmax"))
#        optimizer = RMSprop(lr=0.01)
#        model.compile(loss='categorical_crossentropy', optimizer=optimizer)
    adam = Adam(lr=0.00055, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=3e-6)
    
    if flag == 'regression':
        model.add(Dense(1, activation='linear'))
        model.compile(loss='mean_squared_error', optimizer=adam)            
    elif flag == 'classification':
        model.add(Dense(1, activation='softmax'))
        model.compile(loss='binary_cross_entropy', optimizer=adam)
        
#    model.fit(X_train, y_train, batch_size=64, epochs=20, validation_split = 0.05, callbacks=[history])
    model.fit(X_train, y_train, batch_size=64, epochs=10, validation_split = validation_ratio, callbacks=[history])
    model_json = model.to_json()
    with open(str(saved_model) + ".json", "w") as json_file:
        json_file.write(model_json)
    model.save_weights(str(saved_model) + ".h5")
    print("Saved model to disk")
#        print(history.History)

    get_last_hidden_output = K.function([model.layers[0].input, K.learning_phase()],
                                  [model.layers[0].output])

#    get_last_hidden_output = K.function([model.layers[0].input],
#                                  [model.layers[4].output])

# output in train mode = 0
#    hidden_output = np.array(get_last_hidden_output([X_train[0:1200], 0])[0])
    
# output in train mode = 0


    start = 0
    increment = 100
    flag = 1
    while start+increment <= len(X_train):
        if flag:
            hidden_output = get_last_hidden_output([X_train[start:start+increment], 0])[0]
            flag = 0
        else:
            hidden_output = np.concatenate((hidden_output, get_last_hidden_output([X_train[start:start+increment], 0])[0]))
        start += increment
    if start != len(X_train):
        hidden_output = np.concatenate((hidden_output, get_last_hidden_output([X_train[start:len(X_train)], 0])[0]))

    return [model, hidden_output]
    
    
def splifeatures_label(inputcsv,class_label,feature_list):
    df=pd.read_csv(inputcsv,na_filter=False)
    colnames=list(df.columns.values)
    feature_cols=[col for col in colnames if str(col).startswith(tuple(feature_list))]
    Y_values= df[class_label].values
    X_values= df[list(feature_cols)].values
    return(X_values,Y_values)





def executesvm(X_train,Y_train,eval_type_list,scoringcriteria,gammastart,gammaend,gammacount,Cstart,Cend,Ccount,folds,num_features,param_dict_regression):
    logger.info("exceuting svm")
    logger.info(param_dict_regression["featureList"])
    svmregressor= regression.SVMRegression(num_features,gammastart,gammaend,gammacount,Cstart,Cend,Ccount)
    svmregressor.setpipeline()
    svmregressor.setparam_grid()
    result_dict_Test={}
    result_dict_CV={}
    if "CV" in eval_type_list:
        result_dict_CV=regression.gridsearchnestedCV(X_train,Y_train,svmregressor.pipeline,svmregressor.param_grid,scoringcriteria, folds,param_dict_regression)
        
    if "test" in eval_type_list:
        inputtest=param_dict_regression["inputTest"]
        feature_list=param_dict_regression["featureList"]
        class_label=param_dict_regression["class_label"]
        X_test,Y_test=splifeatures_label(inputtest,class_label,feature_list)
        result_dict_Test=regression.gridSearchTestSet(X_train, Y_train,X_test,Y_test, svmregressor.pipeline,svmregressor.param_grid, scoringcriteria, folds,param_dict_regression)
    
    if "scorefolds" in eval_type_list:
        regression.gridsearchnestedCVfoldsTTest(X_train,Y_train,svmregressor.pipeline,svmregressor.param_grid,scoringcriteria,folds,param_dict_regression)
        
    logger.info("Done executing svm :  "+ str(param_dict_regression["featureList"]))
    return(result_dict_CV,result_dict_Test)





def executeRidgeregression(X_train,Y_train,eval_type_list,scoringcriteria,alphastart,alphaend, alphacount,num_features,folds,param_dict_regression):
    logger.info("exceuting ridgeregression")
    logger.info(param_dict_regression["featureList"])
    ridgeregressor=regression.RidgeRegression(num_features,alphastart,alphaend, alphacount)
    ridgeregressor.setpipeline()
    ridgeregressor.setparam_grid()
    result_dict_Test={}
    result_dict_CV={}
    if "CV" in eval_type_list:
        result_dict_CV=regression.gridsearchnestedCV(X_train,Y_train,ridgeregressor.pipeline,ridgeregressor.param_grid,scoringcriteria,folds,param_dict_regression)
    if "test" in eval_type_list:
        inputtest=param_dict_regression["inputTest"]
        feature_list=param_dict_regression["featureList"]
        class_label=param_dict_regression["class_label"]
        X_test,Y_test=splifeatures_label(inputtest,class_label,feature_list)
        result_dict_Test=regression.gridSearchTestSet(X_train, Y_train,X_test, Y_test, ridgeregressor.pipeline,ridgeregressor.param_grid,scoringcriteria,folds,param_dict_regression)
    logger.info("Done executing Ridge:  "+ str(param_dict_regression["featureList"]))
    
    if "scorefolds" in eval_type_list:
        regression.gridsearchnestedCVfoldsTTest(X_train,Y_train,ridgeregressor.pipeline,ridgeregressor.param_grid,scoringcriteria,folds,param_dict_regression)
        
    return(result_dict_CV,result_dict_Test)


def executeGBRregression(X_train,Y_train,eval_type_list,scoringcriteria,alphastart,alphaend, alphacount,num_features,defaultfolds,param_dict_regression):
    logger.info("exceuting GBRregression")
    logger.info(param_dict_regression["featureList"])
    gradientRegressor=regression.GradientBoostingRegression(num_features,alphastart,alphaend, alphacount)
    gradientRegressor.setpipeline()
    gradientRegressor.setparam_grid()
    result_dict_Test={}
    result_dict_CV={}
    if "CV" in eval_type_list:
        result_dict_CV=regression.gridsearchnestedCV(X_train,Y_train,gradientRegressor.pipeline,gradientRegressor.param_grid,scoringcriteria,defaultfolds,param_dict_regression)
    if "test" in eval_type_list:
        inputtest=param_dict_regression["inputTest"]
        feature_list=param_dict_regression["featureList"]
        class_label=param_dict_regression["class_label"]
        X_test,Y_test=splifeatures_label(inputtest,class_label,feature_list)
        result_dict_Test=regression.gridSearchTestSet(X_train, Y_train,X_test, Y_test, gradientRegressor.pipeline,gradientRegressor.param_grid,scoringcriteria,defaultfolds,param_dict_regression)
    return(result_dict_CV,result_dict_Test)

def execute(param_dict_regression, path):

    model_type = param_dict_regression["ModelType"]
    parameters = param_dict_regression["Parameters"]
    data_length = param_dict_regression["data_length"]
   
    ratio = param_dict_regression["ratio"]
    embeddings = param_dict_regression["embeddings"]
    type_ = param_dict_regression["type"]
   
    dropout = param_dict_regression["dropout"]
    saved_model = param_dict_regression["savedModel"]
    
    train_file_name = param_dict_regression["train_file_name"]
    val_file_name = param_dict_regression["val_file_name"]
    test_file_name = param_dict_regression["test_file_name"]
#        
    val_ratio = param_dict_regression["val_ratio"]
    depth = param_dict_regression["depth"]
    max_words = param_dict_regression["max_words"]
    no_features = param_dict_regression["no_features"]
    train_length = int(ratio * data_length)
    
    
#    Train+val and test data    
    [df, test] = readData(path+train_file_name, path+val_file_name, path+test_file_name, data_length)
    
    df['sentence'] = df['sentence'].map(lambda x: x.encode('unicode-escape').decode('utf-8'))
    df['tokenized_sents'] = df.apply(lambda row: nltk.word_tokenize(row['sentence'].decode('utf-8')), axis=1)
    test['sentence'] = test['sentence'].map(lambda x: x.encode('unicode-escape').decode('utf-8'))
    test['tokenized_sents'] = test.apply(lambda row: nltk.word_tokenize(row['sentence'].decode('utf-8')), axis=1)

    if embeddings == 'word2vec':

        try:
            model = wv.Word2Vec.load("word2vec")
            #model.similarity("this", "is")
#            model.init_sims(replace=True)
            print "loaded"
        except:
            model = wv.Word2Vec(df["tokenized_sents"], size=depth, window=5, min_count=5, workers=4)
            model.save("word2vec")

        df['embedding'] = df['tokenized_sents'].apply(getEmbedding, args=(model, max_words, depth,))
        test['embedding'] = test['tokenized_sents'].apply(getEmbedding, args=(model, max_words, depth))
        X_train = list(df['embedding'])
        X_train = np.reshape(np.ravel(X_train), (len(X_train), max_words, depth))
        
        X_test = list(test['embedding'])
        
    elif embeddings == "glove":
        junk = 0

    if type_ == "regression":
        y_train = np.array(df['Prediction'])
        y_test = np.array(test['Prediction'])
    
    elif type_ == "classification":
#        <.5 = 0 and  >= 0.5 = 1                
        junk = 0

    X_test = np.reshape(np.ravel(X_test), (len(X_test), max_words, depth))
#        print y.mean()
#        print y.std()
#        pickle.dump([X, y, df], open("data_rnn", "wb"))

#Additional features
#    features = np.zeros(len(X))
#    X_train = X[0:train_length]
#    X_test = X[train_length:]
#    y_train = y[0:train_length]
#    y_test = y[train_length:]
#    features_train = feature[0:train_length]
#    features_test = feature[train_length:]
    
    try:
        loadmodel
    except:
        [model, hidden_output] = run_rnn(X_train, y_train, type_, max_words, depth, train_length, val_ratio, saved_model, dropout)
        
    pred = model.predict(X_test)

#    Evaluation
    if type_ == "regression":
        y_test = np.reshape(y_test, (len(y_test), 1))
        print "MAE = ", sum(sum(abs(pred - y_test))) / float(len(pred))
        print "predictions 1 - 10\n",pred[1:10]
        print "True labels 1 - 10\n",y_test[1:10]
    
    if type_ == "classification":
        junk = 0    

####        result_dict_CV,result_dict_Test=executeRidgeregression(X_train,Y_train,eval_type_list,scoringcriteria,alphastart,alphaend, alphacount,num_features,defaultfolds,param_dict_regression)
####    return(result_dict_CV,result_dict_Test) 

def createresultdict(param_dict_regression,result_dict,eval_type):   
    newdict={}
    newdict["inputTrain"]=param_dict_regression["inputTrain"] 
    newdict["inputTest"]=param_dict_regression["inputTest"]    
    newdict["reg_type"]=param_dict_regression["reg_type"]
    newdict["feature_cols_included"]=param_dict_regression["feature_cols_included"]
    newdict["feature_list"]=str(param_dict_regression["featureList"])
    newdict["rmse"]=result_dict["rmse"]
    newdict["rrse"]=result_dict["rrse"]
    newdict["rsquare"]=result_dict["rsquare"]
    newdict["r"]=result_dict["r"]
    newdict["Eval_type"]=eval_type
    newdict["param_grid"]=result_dict["param_grid"]
    newdict["best_param"]=result_dict["best_param"]
    return newdict
    
def addresult(result_dict_CV,result_dict_Test,param_dict_regression,all_list):
    
  
    if result_dict_CV:
        newdict=createresultdict(param_dict_regression,result_dict_CV,"CV")
        all_list.append(newdict)
    if result_dict_Test:
        newdicttest=createresultdict(param_dict_regression,result_dict_Test,"test")
        all_list.append(newdicttest)
        

   
def run(section, input_file):
#    config = configparser.ConfigParser()
#    regressioninput=file_utilities.get_absolutepath_data("config","regression.ini")
#    config.read(regressioninput)
#    input_file_config=config.get(section,"csv_with_file_list")
#    resultCSVFile_config=config.get(section,'resultCSVFile')
#    input_file=file_utilities.get_absolutepath_data(input_file_config)
#    resultCSVFile=file_utilities.get_absolutepath_data(resultCSVFile_config) 
    cwd = os.getcwd()
    temp_path = cwd + "/data/NNdata/" 
    path = cwd + "/data/NNdata/" + input_file
    inputdata_df=pd.read_csv(path)
    inputdata_df.fillna(0)
    
#    all_list=[]
#    if os.path.exists(resultCSVFile):
#            os.remove(resultCSVFile)
    for index, row in inputdata_df.iterrows():
        param_dict_regression={}
        done=row["done"]
        if int(done)==1:
            continue
        #addcosine(row["inputTrain"])
        param_dict_regression["ModelType"]=row["ModelType"]
        param_dict_regression["Parameters"]=row["Parameters"]
        param_dict_regression["data_length"]=row["data_length"]
       
        param_dict_regression["ratio"]=row["ratio"]
        param_dict_regression["embeddings"]=row["embeddings"]
        param_dict_regression["type"]=row["type"]
       
        param_dict_regression["dropout"]=row["dropout"]
        param_dict_regression["savedModel"]=row["savedModel"]
        
        param_dict_regression["train_file_name"]=row["train_file_name"]
        param_dict_regression["val_file_name"]=row["val_file_name"]
        param_dict_regression["test_file_name"]=row["test_file_name"]
#        
        param_dict_regression["val_ratio"]=row["val_ratio"]
        param_dict_regression["depth"]=row["depth"]
        param_dict_regression["max_words"]=row["max_words"]
        param_dict_regression["no_features"]=row["no_features"]
        
        execute(param_dict_regression, temp_path)
#     
#        param_dict_regression["reg_type"]=row["reg_type"]
#        param_dict_regression["featureList"]=list(row["featureList"].split(","))
#        param_dict_regression["eval_type_list"]=row["eval_type_list"]
#        param_dict_regression["class_label"]=row["class_label"]
#        param_dict_regression["Num_features_selection"]=list(str(row["Featureselection"]).split(","))# A list of possible  feature count  for feature selection, by default no feature selection
#        
#        param_dict_regression["inputTrain"]=file_utilities.get_absolutepath_data(row["inputTrain"])
#        param_dict_regression["inputTest"]=file_utilities.get_absolutepath_data(row["inputTest"])
#        outBaseDir=file_utilities.get_absolutepath_data(row["outputBaseDir"])
#        if not os.path.exists(outBaseDir):
#            os.mkdir(outBaseDir)
#        param_dict_regression["outputBaseDir"]= outBaseDir   
#        result_dict_CV,result_dict_Test=execute(param_dict_regression)  
#        addresult(result_dict_CV,result_dict_Test,param_dict_regression,all_list)
    
    
#    if all_list:
#        df=pd.DataFrame(all_list)
#        df.to_csv(resultCSVFile,index=False)           
    
if __name__ == '__main__':
   
    section="afs"
    filename = "nn_config.csv"
    run(section, filename)
   
