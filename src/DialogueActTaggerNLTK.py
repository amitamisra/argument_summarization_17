import nltk
import dialogutils
import dialogconfig
import os

class DialogueActTaggerNLTK:

    def __init__(self, tagger_name=None, pickled_model=None):
        self.tagger_name = tagger_name
        self.dialogue_acts_nps = ["Accept", "Bye", "Clarify", "Continuer", "Emotion", "Emphasis", "Greet",
                                  "No Answer", "Other", "Reject", "Statement", "System", "Wh-Question",
                                  "Yes Answer", "Yes/No Question"]
        self.dialogue_acts_swda = ['sd', 'b', 'sv', 'qy', 'x', 'ny', 'qw', 'nn', 'h', 'qy^d', 'fa', 'ft']
                                   # 'aa', 'qw^d',
        self.featureKeys = {
            "wh_question": ['who', 'which', 'where', 'when', 'what', 'how'],
            "i_dont_know": ["i don't know", "i dont know", "dunno", "donno"],
            "no_words": ["no", "nah", "nope"],
            "yes_words": ["yes", "yeah", "yup"],
            "do_words": ["do", "did", "does"],
            "thanking_words": ['thank', 'thanks', 'thank you'],
            "apology_words": ['sorry', 'apology', "apologize"]
        }
        self.pickled_model_dir = dialogconfig.NLTK_DAC_MODEL
        self.pickled_model = pickled_model
        self.model = self.get_model()


    def get_model(self):
        """Retrieves the classifer model if it exists as a pickle,
           if not: creates, pickles, and saves it

        Args:
            None

        Returns:
            None
        """
        if os.path.isdir(self.pickled_model_dir):
            self.pickled_model = dialogutils.load_model_from_pickle(self.pickled_model_dir)
        else:
            # TODO: update how we select the tagger training data here
            self.pickled_model = self.train_tagger_nps()
            dialogutils.save_model_to_pickle(self.pickled_model, self.pickled_model_dir)


    def train_tagger_swda(self):
        """Trains the NLTK dialogue act classifier on all of the available SWDA data

        Args:
            None

        Returns:
            Model for classification
        """
        rows, _ = dialogutils.read_csv(dialogutils.SWDA_DATA)
        featuresets = [(self.dialogue_act_features(row["text"]), row["act_tag"]) for row in rows]
        size = int(len(featuresets) * 0.1)
        size = int(len(featuresets))
        train_set, test_set = featuresets[size:], featuresets[:size]
        classifier = nltk.NaiveBayesClassifier.train(featuresets)
        print(nltk.classify.accuracy(classifier, test_set))
        return classifier


    def train_tagger_nps(self):
        """Trains the NLTK dialogue act classifier on all of the available NPS Chat data

        Args:
            None

        Returns:
            Model for classification
        """
        posts = nltk.corpus.nps_chat.xml_posts()#[:10000]
        featuresets = [(self.dialogue_act_features(post.text), post.get('class')) for post in posts]
        #size = int(len(featuresets) * 0.1)
        #size = int(len(featuresets))
        #train_set, test_set = featuresets[size:], featuresets[:size]
        classifier = nltk.NaiveBayesClassifier.train(featuresets)
        #print(nltk.classify.accuracy(classifier, test_set))
        return classifier

    def has_lexical_feature(self, post, act):
        words = set(post.lower().split())
        for word in self.featureKeys[act]:
            if word in words:
                return word


    def dialogue_act_features(self, post):
        """Finds features for dialogue act classification

        Args:
            post: text to get features for

        Returns:
            List of word features
        """
        features = {}
        post = post.lower()
        for feature_key in self.featureKeys:
            if feature_key in ["wh_question", "no_words", "yes_words", "do_words"]:
                post_words = post.split()
                num_words = int(len(post_words)/2)
                post_half = " ".join(post_words[:num_words]) if num_words>1 else "".join(post_words)
                has_feature = self.has_lexical_feature(post_half, feature_key)
            elif feature_key in ["i_dont_know", "thanking_words", "apology_words"]:
                has_feature = self.has_lexical_feature(post, feature_key)
            if has_feature:
                features['lexical_feature({})'.format(feature_key)] = has_feature
        for word in nltk.word_tokenize(post):
            features['contains({})'.format(word.lower())] = True
        return features


    def run_dialogue_act_tagger(self, text):
        """Runs the NLTK Dialogue Act Tagger
           See: http://www.nltk.org/book/ch06.html
           Trained on the NPSChat dataset: http://faculty.nps.edu/cmartell/NPSChat.htm

        Args:
            text: string text to tag

        Returns:
            One of 15 possible dialogue act tags (i.e. "Statement", "Emotion", "ynQuestion", etc.)
        """
        text_features = self.dialogue_act_features(text)
        dialogue_act = self.pickled_model.classify(text_features)
        return dialogue_act