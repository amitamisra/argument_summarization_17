'''
Created on Mar 13, 2017

@author: amita
'''
import pandas as pd
import sklearn.metrics
import math
from numpy.ma import corrcoef

def class_label(row,col):
    newdict={}
    if row[col] >=3:
        newdict[col+"class"]=1
    else:
        newdict[col+"class"]=0
    return newdict         

def pairwisecorr(inputcsv):
    df=pd.read_csv(inputcsv+".csv")
    dftopic_gp=df.groupby("topic")
    allrows=[]
    for topic,dftopic in dftopic_gp:
        for  index, row in dftopic.iterrows():
            responsecsv=row["inputcsv"]
            responsedf=pd.read_csv(responsecsv)
            worker_list=row["worker_list"].split(",")
            df1 = responsedf[worker_list]
            corr=df1.corr()
#             workercol1_values=[int(x) for x in responsedf[worker_list[0]].tolist()]
#             workercol2_values=[int(y) for y in responsedf[worker_list[1]].tolist()]
#             corr=corrcoef(workercol1_values,workercol2_values)
            row["corr"]=corr
            allrows.append(row)
    df_coref=pd.DataFrame(allrows)
    outcsv=inputcsv+"_withcorref.csv"
    df_coref.to_csv(outcsv)
            
    
    
    
def kappa(inputcsv,workeridcolumntuples):
    df=pd.read_csv(inputcsv)
    for worker_tup in workeridcolumntuples:
            workercol1=worker_tup[0]
            workercol2=worker_tup[1]
            commonwoker1=[]
            commonwoker2=[]
            workercol1_values=df[ workercol1].tolist()
            workercol2_values=df[ workercol2].tolist()
            worker_both=zip(workercol1_values,workercol2_values)
            for tup in  worker_both:
                if not math.isnan(tup[0]) and not math.isnan(tup[1]):
                    #print(tup[0] ,tup[1])
                    commonwoker1.append(0 if int(tup[0])==0 else 1)
                    commonwoker2.append( 0 if int(tup[1])==0 else 1)
            print(sklearn.metrics.cohen_kappa_score(commonwoker1,commonwoker2), len(commonwoker1))            
    
if __name__ == '__main__':
    inputcsv="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/IAA"  
    pairwisecorr(inputcsv)