'''
Created on Mar 13, 2017

@author: amita
'''
import pandas as pd
import sklearn.metrics
import math
from numpy.ma import corrcoef
from itertools import combinations
import sklearn.metrics
import math,sys,numpy
ignore_list=["workerAZJGPGWLIX9Zweight","workerA1HMMAPNUG5A8Mweight","workerA13PN6UCIP0FS9weight"]
def class_label(row,work):

        if row[work]!=None:
            if not math.isnan(row[work]) :
                if int(row[work]) >=3:
                    return 1
                else:
                    if int(row[work]) ==2:
                        return 0   
          
            
            
def createworkerkey(workerpair0,workerpair1):
    """
    some rows were missing. Since MT does not let  a worker do a HIT again, to use same 
    worker, i created  a new account. check here and if it was the case, use original worker id by replacing else do not replace
    """
    
#     if workerpair0=="workerA13PN6UCIP0FS9weight" or  workerpair1=="workerA13PN6UCIP0FS9weight":
#         print()
    if workerpair0=="workerA1HMMAPNUG5A8Mweight"  or workerpair0=="workerA13PN6UCIP0FS9weight":
        workerpair0="workerA2SDALQ5QDCM0weight"
    
    if workerpair1=="workerA1HMMAPNUG5A8Mweight" or workerpair1=="workerA13PN6UCIP0FS9weight":
            workerpair1="workerA2SDALQ5QDCM0weight"  
     
    if workerpair0=="workerAZJGPGWLIX9Zweight":
        workerpair0="workerAK5KE6MAYPM4Vweight"
    else:
        if workerpair1=="workerAZJGPGWLIX9Zweight":
            workerpair1="workerAK5KE6MAYPM4Vweight"  
          
           
    newkey=(workerpair0,workerpair1)  
   
    return newkey      
        

def kappa(inputcsv):
    df=pd.read_csv(inputcsv+".csv")
    dftopic_gp=df.groupby("topic")
    allrows=[]
    lines=[]  
    for topic,dftopic in dftopic_gp:
        allworker_dict={}
        for  index, row in dftopic.iterrows():
            #print(row["inputcsv"])
            responsecsv=row["inputcsv"]
            responsedf=pd.read_csv(responsecsv)
            colnames_responsedf=responsedf.columns.values.tolist()
            worker_list_manual=[col for col in colnames_responsedf if str(col).startswith("worker") and str(col).endswith("weight")]
            worker_list=row["worker_list"].split(",")
            if sorted(worker_list) != sorted(worker_list_manual):
                print("terminating error in worker_list" + str(responsecsv))
                sys.exit()
            for worker in worker_list:
                responsedf[worker+"_class"]= responsedf.apply(class_label, args=(worker,),axis=1)
                
            #responsedf.dropna(subset=[worker_list[i] for i in range(0,len(worker_list))])
            for workerpair in combinations(worker_list,2):
                newdf=responsedf[ [workerpair[0],workerpair[1], workerpair[0]+"_class", workerpair[1]+"_class"] ]
                newdf=newdf.dropna()
                #print(worker)
                list1=newdf[workerpair[0]+"_class"].tolist()
                list2=newdf[workerpair[1]+"_class"].tolist()
                if list1 and list2:
                #key=workerpair[0]+"_"+workerpair[1]
                    key=createworkerkey(workerpair[0],workerpair[1])
                    #if key=="workerA2SDALQ5QDCM0weight_workerAK5KE6MAYPM4Vweight":
                    #    print("stop")
                    
                    key1=key[0]
                    key2=key[1]
                    
                    if key1!=key2:
                        if key in allworker_dict.keys():
                            prev_values=allworker_dict[key]
                            prevkey1values=prev_values[key1]
                            prevkey2values=prev_values[key2]
                            newkey1values=prevkey1values+list1
                            newkey2values=prevkey2values+list2
                            keydict={}
                            keydict[key1]=newkey1values
                            keydict[key2]=newkey2values
                            allworker_dict[key]=keydict
                        else:
                                if (key2,key1) in allworker_dict:
                                    prev_values=allworker_dict[(key2,key1)]
                                    prevkey2values=prev_values[key2]
                                    prevkey1values=prev_values[key1]
                                    
                                    newkey2values=prevkey2values+list2
                                    newkey1values=prevkey1values+list1
                                    keydict={}
                                    
                                    keydict[key2]=newkey2values
                                    keydict[key1]=newkey1values
                                    allworker_dict[(key2,key1)]=keydict
                                    
                                else:
                                    keydict={}
                                    keydict[key1]=list1
                                    keydict[key2]=list2
                                    allworker_dict[key]=keydict

    
                    
        totalkappa=0
        count=0            
        print(topic)    
        for key, values in  allworker_dict.items():
                    workers=list(values.keys())
                    if workers[0]==workers[1]:
                        continue
                    
                    kappa=sklearn.metrics.cohen_kappa_score(values[workers[0]],values[workers[1]])
                    print("kappa for workers")
                    print(key)
                    print("kappa"+str(kappa))
                    if kappa > 0:
                        totalkappa=totalkappa+kappa
                        count=count+1
                
        average=float(totalkappa)/count
       
        print(topic+ " average")
        print(average)   
        lines.append("topic {0}".format(topic))
        lines.append("avg kappa {0}".format(average))
    
    out_txt=inputcsv+"_withkappa.txt"
    with open(out_txt,"w",encoding="utf-8") as f:
            f.writelines(lines)
    
    
def isNaN(num):
    return num != num            
def testkappa(inputcsv,topic,lines):
    df=pd.read_csv(inputcsv)
    worker_list=[col for col in df if str(col).startswith("worker") and str(col).endswith("weight")]
    iaa_df=df[worker_list] 
    for index, row in iaa_df.iterrows():

            if "workerA13PN6UCIP0FS9weight" in worker_list:
                if isNaN(row["workerA2SDALQ5QDCM0weight"]) and not isNaN(row["workerA13PN6UCIP0FS9weight"]) :
                    row["workerA2SDALQ5QDCM0weight"]=int(row["workerA13PN6UCIP0FS9weight"] ) 
    
            if "workerA1HMMAPNUG5A8Mweight" in worker_list:
                if isNaN(row["workerA2SDALQ5QDCM0weight"]) and not isNaN(row["workerA1HMMAPNUG5A8Mweight"]) :
                    row["workerA2SDALQ5QDCM0weight"]=int(row["workerA1HMMAPNUG5A8Mweight"] ) 
         
            if "workerAZJGPGWLIX9Zweight" in worker_list:
                if isNaN(row["workerAK5KE6MAYPM4Vweight"]):
                        if not isNaN(row["workerAZJGPGWLIX9Zweight"]):
                            row["workerAK5KE6MAYPM4Vweight"]=int(row["workerAZJGPGWLIX9Zweight"] )           
                
    for worker in worker_list:
                iaa_df[worker+"_class"]= iaa_df.apply(class_label, args=(worker,),axis=1)
    
    newworker_list=[work for work in worker_list if work not in ignore_list]
    print(newworker_list)           
    iaa_df.to_csv(inputcsv[:-4]+"_class.csv")            
    
    totalkappa=0
    count=0 
    
    for workerpair in combinations(newworker_list,2):
        #print("workerpair")
        reduced_df=iaa_df[[workerpair[0]+"_class",workerpair[1]+"_class"]]
        reduced_df=reduced_df.dropna()
        list1=reduced_df[workerpair[0]+"_class"].tolist()
        list2=reduced_df[workerpair[1]+"_class"].tolist()
        kappa=sklearn.metrics.cohen_kappa_score(list1,list2)
      
        
        if kappa > 0:
            totalkappa=totalkappa+kappa
            count=count+1
    average=float(totalkappa)/count
   
   
    print(topic+ " average")
    print(average)  
    lines.append("topic {0}".format(topic))
    lines.append("avg kappa {0}".format(average)) 
    return average
    
        
    
    

if __name__ == '__main__':
    #inputcsv="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/IAA"  
    
    input_gc="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gun-control/Mturk/mturkcontributorresults2/all_mtbatch_gun_corenlp_pdtb.csv"
    input_abortion="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/abortion/Mturk/mturkcontributorresults2/all_mtbatch_abortion_corenlp_pdtb.csv"
    input_gm="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gay-rights-debates/Mturk/mturkcontributorresults2/all_mtbatch_gay_corenlp_pdtb.csv"
    
    lines=[]
    abortion_kappa=testkappa(input_abortion,"abortion",lines)
    gm_kappa=testkappa(input_gm,"gm",lines)
    gc_kappa=testkappa(input_gc,"gc",lines)
    
    out_txt="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/IAA_withkappa.txt"
    with open(out_txt,"w",encoding="utf-8") as f:
            f.writelines(lines)
    