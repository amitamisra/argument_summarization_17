#!/usr/bin/python3
#coding: utf8 
#
# Script used to prefilter a list of sentences within a topic before
# they are made into pairs for a mechanical turk HIT.
#
# Used for the facet similarity HITs with sentences scored by the argument
# quality regressor.
#
#
from itertools import combinations

import configparser
import csv
import sys
import random
import re
import string
import os
import nltk
import enchant
import codecs

class FilteredCombinator:
    """ Apply a list of filter objects, defined as a function
    with a __call__ method that takes a combination as a tuple
    and then performs the filtering
    """
    def __init__(self, items: list, filters: list):
        print("Inniting combinator")
        combs = combinations(items, 2)
        if filters is None or len(filters) == 0:
            self.combinations = combs
        else:
            self.combinations = (comb for comb in combs if all(f(comb) for f in filters))
        # combs = list(combinations(items, 2))
        # random.shuffle(combs)
        #
        # if filters is None or len(filters) == 0:
        #     self.combinations = (comb for comb in combs)
        # else:
        #     self.combinations = (comb for comb in combs if all(f(comb) for f in filters))
        #
        # del combs
        print("done inniting combinator")

    def __iter__(self):
        return self

    # Return the flattened next combination
    def __next__(self):
        return sum(next(self.combinations), [])


class SpecialExclusionSetFilter:
    """ Filter to exclude test file """
    def __init__(self, exclusion_set: set, comb_header: list):
        self.exclusion_set = exclusion_set
        self.comb_header = comb_header

    def __call__(self, comb: tuple) -> bool:
        row = comb[0] + comb[1]
        key = (row[self.comb_header.index("datasetId_1")],
               row[self.comb_header.index("discussionId_1")],
               row[self.comb_header.index("postId_1")],
               row[self.comb_header.index("sentenceId_1")],
               row[self.comb_header.index("datasetId_2")],
               row[self.comb_header.index("discussionId_2")],
               row[self.comb_header.index("postId_2")],
               row[self.comb_header.index("sentenceId_2")])

        return key not in self.exclusion_set


class MaxPairingFilter:
    """ Filter to exclude combinations based on a maximum
    amount of allowed pairings for single sentences
    """
    def __init__(self, n_pairs: int, keys: list):
        self.n_pairs = n_pairs
        self.keys = keys
        self.seen = dict()

    def make_key(self, row: list) -> str:
        key = ""
        for col in self.keys:
            key += row[col]
            key += "_"

        return key

    def seen_too_much(self, key: str) -> bool:
        if key not in self.seen:
            self.seen[key] = 0
        self.seen[key] += 1
        return self.seen[key] - 1 > self.n_pairs

    def __call__(self, comb: tuple) -> bool:
        # Make sure neither author has been paired too many times already
        key1 = self.make_key(comb[0])
        key2 = self.make_key(comb[1])
        return not self.seen_too_much(key1) and not self.seen_too_much(key2)


class WithinXPercentFilter:
    """ Filter to exclude sentences that are not within
    X Percent of the same length.
    """
    def __init__(self, x_percent: float, text_index: int):
        self.x_percent = x_percent
        self.text_index = text_index
        self.len_cache = dict()

    def __call__(self, comb: tuple) -> bool:
        text1 = comb[0][self.text_index]
        text2 = comb[1][self.text_index]

        if text1 in self.len_cache:
            len1 = self.len_cache[text1]
        else:
            len1 = len(nltk.word_tokenize(text1))
            self.len_cache[text1] = len1

        if text2 in self.len_cache:
            len2 = self.len_cache[text2]
        else:
            len2 = len(nltk.word_tokenize(text2))
            self.len_cache[text2] = len2

        if len1 >= len2:
            longer = len1
            shorter = len2
        else:
            longer = len2
            shorter = len1

        return shorter / longer >= self.x_percent


class SentenceFilterRunner:
    # Check for broken QUOTE="name" or quote="name" literals
    quote_re = re.compile("[qQ][uU][oO][tT][eE] ?(=|])")

    @staticmethod
    def get_quote_pairs(text):
        pairs = list()
        is_open = False
        begin = None
        pair_type = None
        # pair types:
        # 0 : "
        # 1 : “ ”
        for i in range(0, len(text)):

            if text[i] is '"' and not is_open:
                is_open = True
                begin = i
                pair_type = 0
                continue
            if text[i] is '"' and is_open and pair_type == 0:
                is_open = False
                end = i
                pairs.append((begin, end))
            if text[i] == '“' and not is_open:
                is_open = True
                begin = i
                pair_type = 1
                continue
            if text[i] == '”' and is_open and pair_type == 1:
                is_open = False
                end = i
                pairs.append((begin, end))

        if is_open:
            end = len(text) - 1
            pairs.append((begin, end))

        return pairs

    @staticmethod
    def make_combination_header(header: list) -> list:
        return ["{0}_{1}".format(header_entry, index) for index in range(1, 3) for header_entry in header]

    # Excludes numbers and periods that enchant includes as english words
    # for some reason
    @staticmethod
    def has_x_english_words(sent: list, en_dict, x, re_number):
        sent = [token for token in sent if not re.match(re_number, token) and not token == "."]
        return [en_dict.check(word.lower()) for word in sent].count(True) >= x

    @staticmethod
    def has_a_verb(sent):
        verb_tags = {"VB", "VBD", "VBG", "VBN", "VBP", "VBZ"}
        pos_tags = [pair[1] in verb_tags for pair in nltk.pos_tag(sent)]

        return any(pos_tags)

    @staticmethod
    def has_quote_artifact(text):
        return SentenceFilterRunner.quote_re.search(text) is not None

    @staticmethod
    def wc_in_range(words: list, min_words: int, max_words: int) -> bool:
        return min_words <= len(words) <= max_words

    def get_quote_percentage(self, text):
        quote_pairs = self.get_quote_pairs(text)
        total_quote = sum(map(lambda p: p[1] - p[0] + 1, quote_pairs))
        return total_quote / len(text)

    def has_x_percent_unigram_overlap(self, tokens: set, included_unigram_sets: list) -> bool:
        
        token_set = set(tokens)
        for included_set in included_unigram_sets:
            largest_set_size = len(tokens) if len(tokens) > len(included_set) else len(included_set)
            if len(tokens)==0:
                return True
            if len(token_set & included_set) / largest_set_size >= self.unigram_percentage:
                return True

        included_unigram_sets.append(token_set)
        return False

    def make_exclusion_keys(self):
        print("Creating exclusion keys")
        result = set()
        for exclusion_file in self.exclusion_files:
            print("Excluding input from exclusion file: {0}".format(exclusion_file))
            with open(exclusion_file, 'r') as f:
                reader = csv.reader(f)
                header = next(reader)
                for item in reader:
                    forward_key = (item[header.index("datasetId_1")],
                                   item[header.index("discussionId_1")],
                                   item[header.index("postId_1")],
                                   item[header.index("sentenceId_1")],
                                   item[header.index("datasetId_2")],
                                   item[header.index("discussionId_2")],
                                   item[header.index("postId_2")],
                                   item[header.index("sentenceId_2")])
                    result.add(forward_key)
                    reverse_key = (item[header.index("datasetId_2")],
                                   item[header.index("discussionId_2")],
                                   item[header.index("postId_2")],
                                   item[header.index("sentenceId_2")],
                                   item[header.index("datasetId_1")],
                                   item[header.index("discussionId_1")],
                                   item[header.index("postId_1")],
                                   item[header.index("sentenceId_1")])
                    result.add(reverse_key)
                    del forward_key
                    del reverse_key

        return result

    def _clear_removal_memory(self):
        self.removal_memory = {
            "seen": 0,
            "quote_artifact": 0,
            "quote_percentage": 0,
            "english_words": 0,
            "wc": 0,
            "verb": 0,
            "author limit": 0,
            "unigram_overlap": 0
        }

    def print_removal_memory(self):
        # Print debug statements for items removed and why
        print("Row removal statistics:")
        for key, val in self.removal_memory.items():
            print("\t{0}: {1}".format(key, val))

    def build_sentence_pool(self):
        print("Building sentence pool")
        re_number = re.compile("[0-9]+(\.[0-9]+)?")
        en_dict = enchant.Dict("en_US")

        authors_by_disc = dict()

        # Use a seed for predictable results
        random.seed(100)
        random.shuffle(self.input_rows)
        finished_count = 0
        report_at = self.report_interval

        rows = list()
        seen = set()
        included_unigram_sets = list()

        self._clear_removal_memory()

        for row in self.input_rows:
            # Whether to skip this row, previously just called continue, but
            # we want full statistics reporting for which filters are applying
            skip_row = False
            # Avoid issues with continues by just incrementing the count here
            finished_count += 1
            if finished_count >= report_at:
                report_at += self.report_interval
                print("Finished building sentence pool for {0} sentences so far".format(finished_count))

            try:
                sent = row[self.header.index("sentence")]
            except:
                print(sys.exc_info()[0])
                print(row)

            # Exclude duplicates
            if sent not in seen:
                seen.add(sent)
            else:
                self.removal_memory["seen"] += 1
                skip_row = True

            # Don't include items with quote artifacts
            if self.has_quote_artifact(sent):
                self.removal_memory["quote_artifact"] += 1
                skip_row = True

            # Don't include items where >= X% of the text is a quote
            if self.quote_percentage is not None and self.get_quote_percentage(sent) > self.quote_percentage:
                self.removal_memory["quote_percentage"] += 1
                skip_row = True

            # Parse words in sentence
            tokens = nltk.word_tokenize(sent)
            words = [x for x in tokens if x not in string.punctuation]

            # Make sure sentence has at least 6 english words
            if self.required_num_dict_words is not None and \
                    not self.has_x_english_words(words, en_dict, self.required_num_dict_words, re_number):
                self.removal_memory["english_words"] += 1
                skip_row = True

            # Don't include sentences not in the word count range
            if self.wc_range is not None and not self.wc_in_range(words, self.wc_range[0], self.wc_range[1]):
                self.removal_memory["wc"] += 1
                skip_row = True

            # Make sure sentence has a verb
            if not self.has_a_verb(words):
                self.removal_memory["verb"] += 1
                skip_row = True

            if self.unigram_percentage is not None and self.has_x_percent_unigram_overlap(words, included_unigram_sets):
                self.removal_memory["unigram_overlap"] += 1
                skip_row = True

            if self.use_one_post_per_auth_per_disc:
                # Filter down to one sentence per author per discussion
                # Make sure this is the last check so the counts don't
                # get messed up
                dataset_id = row[self.header.index("datasetId")]
                disc_id = row[self.header.index("discussionId")]
                full_id = "{0}_{1}".format(dataset_id, disc_id)

                auth_id = row[self.header.index("authorId")]

                if full_id not in authors_by_disc:
                    authors_by_disc[full_id] = set()

                if auth_id in authors_by_disc[full_id]:
                    self.removal_memory["author limit"] += 1
                    skip_row = True
                else:
                    authors_by_disc[full_id].add(auth_id)

            if not skip_row:
                # passed all checks, add the row
                yield row

    def __init__(self, input_rows, header, topic_code, config_file=None):
        # Read the config file and setup important input variables
        config = configparser.ConfigParser()
        if config_file is None:
            config.read(get_default_config_path())
        else:
            config.read(config_file)

        self.input_rows = input_rows
        self.header = header

        # Mandatory options
        self.exclusion_files = config.get(topic_code, "exclusion-files").split(",")
        self.report_interval = int(config.get(topic_code, "report-interval"))

        # Disablable features
        self.wc_range = config.get(topic_code, "wc-range")
        if self.wc_range == "disable":
            self.wc_range = None
        else:
            self.wc_range = self.wc_range.split("-")
            self.wc_range = int(self.wc_range[0]), int(self.wc_range[1])

        self.unigram_percentage = config.get(topic_code, "unigram-overlap-percentage")
        if self.unigram_percentage == "disable":
            self.unigram_percentage = None
        else:
            self.unigram_percentage = float(self.unigram_percentage)

        self.quote_percentage = config.get(topic_code, "quote-percentage")
        if self.quote_percentage == "disable":
            self.quote_percentage = None
        else:
            self.quote_percentage = float(self.quote_percentage)

        self.required_num_dict_words = config.get(topic_code, "required-num-dictionary-words")
        if self.required_num_dict_words == "disable":
            self.required_num_dict_words = None
        else:
            self.required_num_dict_words = int(self.required_num_dict_words)

        one_post_auth = config.get(topic_code, "one-post-per-author-per-discussion")
        if one_post_auth == "True":
            self.use_one_post_per_auth_per_disc = True
        elif one_post_auth == "False" or one_post_auth == "disable":
            self.use_one_post_per_auth_per_disc = False
        else:
            raise Exception("Unknown option for one-post-per-author-per-discussion")


def get_default_config_path():
    return os.path.abspath(os.path.join(os.path.dirname(__file__),
        "apply_argument_similarity_pre_filter_config.ini"))


def run():
    if len(sys.argv) == 2:
        topic_code = sys.argv[1]
    else:
        print("usage: apply_argument_similarity_pre_filter.py topic_code")
        print("example: apply_argument_similarity_pre_filter.py GC")
        sys.exit(1)
    config = configparser.ConfigParser()
    config.read(get_default_config_path())
    if topic_code in config:
        print("Reading configuration for \"{0}\"".format(topic_code))
    else:
        print("error: no configuration present for \"{0}\"".format(topic_code))
        sys.exit(1)
    input_file = config.get(topic_code, "input-file")
    sentences_output_file = config.get(topic_code, "sentences-output-file")
    combinations_output_file = config.get(topic_code, "combinations-output-file")
    max_sent_inclusions = config.get(topic_code, "max-inclusions-per-sentence")
    if sentences_output_file== "disable":
        sentences_output_file = None
    if combinations_output_file == "disable":
        combinations_output_file = None
    if max_sent_inclusions == "disable":
        max_sent_inclusions = None
    else:
        max_sent_inclusions = int(max_sent_inclusions)

    with codecs.open(input_file,'r',encoding='utf8') as f:
    #with open(input_file, 'r') as f:
        reader = csv.reader(f)
        header = next(reader)
        input_rows = [row for row in reader]

    if sentences_output_file is not None:
        print("Filtering sentences...")
        sentence_filter = SentenceFilterRunner(input_rows, header, topic_code)
        rows = [row for row in sentence_filter.build_sentence_pool()]
        sentence_filter.print_removal_memory()

        # We've filtered all the single rows, write these out to a file
        print("Done filtering sentences, writing these out to a file")
        with codecs.open(sentences_output_file, 'w',encoding='utf8') as f:
            writer = csv.writer(f)
            writer.writerow(header)
            for row in rows:
                writer.writerow(row)
    else:
        # Make combinations using the input items, skipping filtering step
        print("Not filtering sentences")
        rows = input_rows

    if combinations_output_file is not None:
        print("Creating and filtering combinations")
        # Now create all the valid combinations and write these to a file
        max_sent_inclusions_filter = MaxPairingFilter(max_sent_inclusions, [header.index("sentence")])
        combination_header = SentenceFilterRunner.make_combination_header(header)
        combination_filters = list()
        if sentences_output_file is not None:
            previously_used_data_exclusion_filter = SpecialExclusionSetFilter(
                    sentence_filter.make_exclusion_keys(), combination_header)
            combination_filters.append(previously_used_data_exclusion_filter)
        # Make sure to apply the max_sent_inclusions_filter LAST, in other words, put it as the last
        # filter in the list
        combination_filters.append(max_sent_inclusions_filter)
        combs = FilteredCombinator(rows, combination_filters)
        print("Writing out combinations to a file... this might take a while")
        with open(combinations_output_file, 'w') as f:
            writer = csv.writer(f)
            writer.writerow(combination_header)
            for row in combs:
                writer.writerow(row)

if __name__ == "__main__":
    run()

