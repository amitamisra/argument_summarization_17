'''
Created on Nov 28, 2017

@author: amita
'''
import readability_metrics
import nltk
import string

def addreadability(sentence):
    
    read_scores={'read_Kincaid':0,'read_ARI':0,'read_Coleman-Liau':0,'read_FleschReadingEase':0,"read_GunningFogIndex":0,"read_LIX":0,"read_SMOGIndex":0,"read_RIX":0}
    words=nltk.word_tokenize(sentence)
    if words[-1] in string.punctuation:
        words=words[:-1]
    if len(words) > 1:
        
        read_scores=readability_metrics.getmeasures(sentence)
    return read_scores


def create_featurename(feature_dict, prefix):
    newdict={}
    for key, value in feature_dict.items():
        newkey=prefix+"_"+key
        newdict[newkey]=value
    return newdict     
    

def generate_features_label(rows,features):
    
    allfeatures_class1={}
    allfeatures_class2={}
    feature_rows=[]
    for row in rows:
        sent_lower=row["sent_lower"]
        sent_upper=row["sent_upper"] 
        if "readability" in features:
            read_score_lower=addreadability(sent_lower)
            read_score_upper=addreadability(sent_upper)
            s1_feature_dict=create_featurename(read_score_lower, "S1")
            s2_feature_dict=create_featurename(read_score_upper, "S2")
            allfeatures_class1.update( s1_feature_dict)
            allfeatures_class1.update( s2_feature_dict)
            allfeatures_class1["label"]=0
            allfeatures_class1["S1"]=sent_lower
            allfeatures_class1["S2"]=sent_upper
            
            # do the balancing trick, reverse the features to generate the other class
            s2_feature_dict=create_featurename(read_score_lower, "S2")
            s1_feature_dict=create_featurename(read_score_upper, "S1")
            allfeatures_class2.update( s1_feature_dict)
            allfeatures_class2.update( s2_feature_dict)
            
            allfeatures_class1["S1"]=sent_upper
            allfeatures_class1["S2"]=sent_lower
            allfeatures_class2["label"]=1
            
        
        feature_rows.append(allfeatures_class1)
        feature_rows.append(allfeatures_class2)
        
    return feature_rows    
    
if __name__ == '__main__':
    pass