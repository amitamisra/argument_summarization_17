'''
Created on Feb 26, 2017
add two new columns, one without author information, one where S is replaced by author
@author: amita
'''
import re,pandas as pd
import os, string
import file_utilities


def add_punct_remformat(text):
    """
    adding missing punctuation at the end of a author turn, remove formatting
    """
    new_text_list=[]
    text=re.sub("(<br><b>)\s*", ' ',text)
    text=re.sub('(</b>)\s*', ' ',text)
    text_list=re.split("\s(?=[sS][12]:[0-9])", text)
    for post in text_list:
        if post:
            post=post.strip()
            if post[-1] in string.punctuation:
                new_text_list.append(post)
            else:
                post=post +"."
                new_text_list.append(post)
    return " ".join(new_text_list)        


def addcolumn_noformat_Sbyauthor(row, column):
    text=row[column]
    punct_text=add_punct_remformat(text)
    punct_text=re.sub("[Ss](?=[1]:[0-9]*\s*)", 'Author',punct_text)
    punct_text=re.sub("[Ss](?=[2]:[0-9]*\s*)", 'Author',punct_text)
    #noformat=re.sub("(<br><b>)\s*", ' ',nobold)
    return (punct_text)
    
def addcolumn_noauthor(row,column):
    text=row[column]
    punct_text=add_punct_remformat(text)
    punct_text=re.sub("([Ss][1-2]:[0-9]*-)\s*", ' ',punct_text)
    print( punct_text)
    return  punct_text
    
    
def run(topic,inputfile,column,output_dir):
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    data_dir=file_utilities.DATA_DIR 
    input_base=os.path.basename(inputfile)
    outputfile=os.path.join(output_dir,input_base+"_authorformat")
    inputcsv=inputfile +".csv" 
    df= pd.read_csv(inputcsv)  
    df["dialog_no_format_author"]=df.apply(addcolumn_noformat_Sbyauthor,args=(column,),axis=1)
    #df["dialog_no_format"]=df.apply(addcolumn_noauthor,args=(column,),axis=1)
    df.to_csv(outputfile +".csv",index=False) 
    df.to_pickle(outputfile +".pkl") 
    newcolumnname="dialog_no_format_author"    
    return outputfile, newcolumnname
    
if __name__ == '__main__':
    topic="gun-control"
    input_fordialog="dialogdata,CSV,gun-control,MTdata"
    textcolumn="Dialog"
    data_dir=file_utilities.DATA_DIR
    input_withdir_fordialog=os.path.join(*input_fordialog.split(","))
    input_withpath_fordialog=os.path.join(data_dir,input_withdir_fordialog)
    run(topic,input_withpath_fordialog,textcolumn)