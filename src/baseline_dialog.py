'''
Created on Nov 3, 2016
apply baselines on dialog data and create a dialog directory with one dialog per file

@author: amita
'''
import sys,os
import pandas as pd
from db_connect import setup_connection
import db_functions
import file_utilities
import file_handling
import baselines

def applytextrank(x,column,textrank,dialog_dir):
    """
    textrank: An object of class Textrank
    """
    text=x[column]
    summary=textrank.summarize_ratio(text)
    ratiopercentage=str(int(round(textrank.ratio*100))) +"_percent"
    rowdict={"textranksummary with ratio {0}".format(ratiopercentage) : summary}
    key=x["key"]
    dialog=x[column]
    filename=os.path.join(dialog_dir,key+".txt")
    file_handling.writeTextFile(filename,dialog)
    return pd.Series(rowdict)
    
def run(topic,inputfile,column):
    """
    topic: gaymarriage/guncontrol 
    inputfile:CSV with dialogs and summaries from database
    return the name of output file without extension
    """
    data_dir=file_utilities.DATA_DIR 
    textrank=baselines.Textrank(0.2,10) 
    output_dir=os.path.join(data_dir,"data_originalsummaries",topic)
    input_base=os.path.basename(inputfile)
    output=os.path.join(output_dir,input_base+"_baseline")
    dialog_dir=file_utilities.get_absolutepath_data("data_database", topic,"dialog_dir")
    if not os.path.exists(dialog_dir):
        os.mkdir(dialog_dir)
    
    inputcsv=inputfile+".csv"    
    df=pd.read_csv(inputcsv)
    summarydf=df.apply(applytextrank,args=(column,textrank,dialog_dir),axis=1 )
    mergeddf=df.join(summarydf) 
    mergeddf.to_csv(output+".csv",index=False) 
    mergeddf.to_pickle(output+".pkl") 
    return output    
    
if __name__ == '__main__':
    topic="gay-rights-debates"
    inputcsv="/Users/amita/git/argument_summarization_17/data/data_database/gay-rights-debates/AllDialogs_phase_1_2_Formatted_database"
    run(topic,inputcsv)
    topic="gun-control"
    inputcsv="/Users/amita/git/argument_summarization_17/data/data_database/gun-control/NaturalSummaryCSV_database"
    run(topic,inputcsv)
