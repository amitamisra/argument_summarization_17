'''
Created on Oct 3, 2016
add some baselines for summarization
@author: amita
'''
from gensim.summarization import keywords
import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

from gensim.summarization import summarize
class Textrank:
    def __init__(self, ratio, wordcount):
        '''
        Text rank uses ratio or wordcount to finalize output summary, default is 20%
        '''
        self.ratio=ratio
        self.wordcount=wordcount
        
    def summarize_ratio(self,text): 
        return summarize(text,ratio=self.ratio)   
        
    def summarize_worcount(self,text): 
        return summarize(text,word_count=self.wordcount)  
    def summarize_default(self,text): 
        return summarize(text)  
        
if __name__ == '__main__':
    textrank=Textrank(0.5,2)
    text=""" S1:1-  Another problem with the study is that most ( or at least more ) people in the country very well might identify as Christian... S2:1-  Jyoshu, you don't know how they conducted the study. You don't know what questions they asked, or what they screening process was, or anything. You're just making up objections, and deciding without any factual evidence that they apply. S1:2-  Yes, that's the point. I'd prefer to know the specifics. Questioning a study is a VERY appropriate thing to do in any study, particularly if it's used to vilify a group of people. S2:2-  So then question, don't just state that the study has a problem. And " vilify "? Good grief! Where did you get that from? You seem to be taking this a bit too much to heart. A study was done, and done well from all evidence that I've seen, and the results reported. No one is vilifying anyone. If you can show an actuall problem with the study, and not something hypothetical that you came up with, than please do so. It seems quite professionally done to me, but you go ahead and dig all you want. But if you're simply uncomfortable with the results, well, that ' s no reason to go around saying that people aren't doing their jobs or are out to vilify others. S1:3-  Er... that's why I pointed out the specific things I saw as a problem. Are you in a bit of a reactionary mood today? Yeah, because in my experience in these forums, here and elsewhere, an enormous amount of attention is given to trying to make Christianity look bad, through generalizations, citing irrelevant studies, and misrepresentations. See, the divorce rate of Christians has NO bearing whatsoever on the gay marriage amendment. Or did you forget that? S2:3-  No more than usual. :) You, however, are stating that some is a problem, as opposed to possibly a problem. As if you wanted to simply dismiss the results, rather than actually check their validity. That's pretty reactionary. As long as people say that they're trying to protect the " sanctity " of marriage by keeping gays out it's extremely relevant, or at least a very good refutation."""
    #print(textrank.summarize_ratio(text))
    #text="I do not support your views on the topic of evolution. Your claim is false. we should not argue."
    print (textrank.summarize_ratio(text))
    print (keywords(text))