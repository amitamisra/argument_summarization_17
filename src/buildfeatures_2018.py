'''
Created on Nov 15, 2017

@author: amita
'''

if __name__ == '__main__':
    pass


def addreadability(sentence):
    
    read_scores={'read_Kincaid':0,'read_ARI':0,'read_Coleman-Liau':0,'read_FleschReadingEase':0,"read_GunningFogIndex":0,"read_LIX":0,"read_SMOGIndex":0,"read_RIX":0}
    words=nltk.word_tokenize(sentence)
    if words[-1] in string.punctuation:
        words=words[:-1]
    if len(words) > 1:
        
        read_scores=readability_metrics.getmeasures(sentence)
    return read_scores

def addLIWCscores(row,sent_col,dialog):
    liwc_features={}
    
    prev_sent_dict={}
    liwc_extractor_object=liwc_extractor.LIWCextractor()
    sent_no=int(row["sent_no"])
    sentence=row[sent_col]
    liwc_sent_dict=liwc_extractor_object.score_text(sentence)
    #logger.info(liwc_sent_dict)
    for key,value in liwc_sent_dict.items():
        liwc_features["liwc_current"+key]=value
    key=row["Key"]
    #logger.info("adding features for liwc for key {0}".format(key))
    #logger.info(sent_no)
    if sent_no > 0:
        prev_sent_no=sent_no-1
        
        #dialog=copydf.loc[copydf["Key"]==key]
        dialog.sent_no = dialog.sent_no.astype(int)
        prev_sentence=dialog[dialog["sent_no"]==int(prev_sent_no)][sent_col].values[0]
        prev_sent_dict=liwc_extractor_object.score_text(prev_sentence)
        for key,value in prev_sent_dict.items():
            liwc_features["liwc_prev"+key]=value
    return liwc_features