'''
Created on Mar 20, 2017
build feature files and do clustering
Input : File obtained after running prepare_data_original_dialog_summary 
Corenlp column: Acces corenlp information

run pdtb_coref to add pdtb and oref features, split into train and test
@author: amita
'''

import  pdtb_helper
import numpy as np
import copy
import random
np.random.seed(1)
random.seed(1)
import time
from collections import ChainMap
import os
import pandas as pd
import configparser, logging
#from collections import ChainMap
from word2vec_extractor import Word2vecExtractor
from timeit import default_timer as timer
from src import file_utilities
from pdtb_extractor import PDTBExtractor
from  word2vec_extractor import Word2vecExtractor
import createclasslabel_balance
import ast
import readability_metrics
import nltk
import dialogue_act_classifier
import liwc_extractor
from multiprocessing import Pool
from lexRankExtractor import LexRankExtractor
from textrankExtractor import TextRankExtractor
from kldivergencesumExtractor import  KLSumExtractor
from sumbasicExtractor import SumBasicExtractor
import utterance_number_dialog
import  createbaselinefeature
logger = logging.getLogger()
import string
handler = logging.StreamHandler()
formatter = logging.Formatter(
        '%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.INFO)
#import skip_thought_vectors_py3.addskipthought as addskipthought
logging.basicConfig(level=logging.CRITICAL)

logger = logging.getLogger(__name__)

combinedpdtb=["pdtb_ind _ Contingency",    "pdtb_ind _ Cause" ,   "pdtb_ind _ Pragmatic Cause"  ,  "pdtb_ind _ Condition"  , "pdtb_ind _ Comparision",    "pdtb_ind _ Contrast" ,   "pdtb_ind _ Pragmatic Contrast",    "pdtb_ind _ Concession"]

features=["textrank","liwc","dialog_act","w2vec","lexrank","readability","syntax","position","author","kl","sumbasic"]
#features=["dialog_act"]
#features=["kl","sumbasic"]
global w2vobject
global customdoc2vec



def loadresourcesrequired(param_dict):
    pass
    
num_partitions = 10 #number of partitions to split dataframe
num_cores = 4 #number of cores on your machine

keystokeepfeatures="Key,label,sent,sent_no,sent_coref".split(",")

def addw2vecfeatures(sentence,w2vobject):
        feature_dictw2vec=w2vobject.extract(sentence)
        return feature_dictw2vec

        
def addreadability(sentence):
    
    read_scores={'read_Kincaid':0,'read_ARI':0,'read_Coleman-Liau':0,'read_FleschReadingEase':0,"read_GunningFogIndex":0,"read_LIX":0,"read_SMOGIndex":0,"read_RIX":0}
    words=nltk.word_tokenize(sentence)
    if words[-1] in string.punctuation:
        words=words[:-1]
    if len(words) > 1:
        
        read_scores=readability_metrics.getmeasures(sentence)
    return read_scores


         

def addbaseline(sentno,featuredict,name):
    score={}
    score[name]=featuredict[sentno]
    return   score   
    
               

def addLIWCscores(row,sent_col,dialog):
    liwc_features={}
    
    prev_sent_dict={}
    liwc_extractor_object=liwc_extractor.LIWCextractor()
    sent_no=int(row["sent_no"])
    sentence=row[sent_col]
    liwc_sent_dict=liwc_extractor_object.score_text(sentence)
    #logger.info(liwc_sent_dict)
    for key,value in liwc_sent_dict.items():
        liwc_features["liwc_current"+key]=value
    key=row["Key"]
    #logger.info("adding features for liwc for key {0}".format(key))
    #logger.info(sent_no)
    if sent_no > 0:
        prev_sent_no=sent_no-1
        
        #dialog=copydf.loc[copydf["Key"]==key]
        dialog.sent_no = dialog.sent_no.astype(int)
        prev_sentence=dialog[dialog["sent_no"]==int(prev_sent_no)][sent_col].values[0]
        prev_sent_dict=liwc_extractor_object.score_text(prev_sentence)
        for key,value in prev_sent_dict.items():
            liwc_features["liwc_prev"+key]=value
    return liwc_features
         
    
def addpdtb_fromrow(row):
    sum_pdtb=0
    pdtb_vec_scores={}
    for pdtb_com in combinedpdtb:
        sum_pdtb = sum_pdtb + row[pdtb_com]
    pdtb_vec_scores["sumpdtb"]=sum_pdtb
    return pdtb_vec_scores

def adddialogscores(row,sent_col,dialog,actobject):
    #start = timer()
    dialogfeatures={}
    sent_no=int(row["sent_no"])
    key=row["Key"]
    
    #logger.info("adding features for dialog act for key {0}".format(key))
    #logger.info(key)
    #logger.info(sent_no)
    if sent_no > 0:
        prev_sent_no=sent_no-1
        
        #dialog=copydf.loc[copydf["Key"]==key]
        #dialog.sent_no = dialog.sent_no.astype(int)
        #print(key)
        #print(sent_no)
        sentence=dialog[dialog["sent_no"]==int(prev_sent_no)][sent_col].values[0]
        act_type=actobject.DialogueActTaggerNLTK.run_dialogue_act_tagger(sentence)
        dialogscore=dialogue_act_classifier.createactdictionary_2(act_type)
    else:
        dialogscore=dialogue_act_classifier.createactdictionary_2("none")  
    
    for key,value in dialogscore.items():
        dialogfeatures["dialog_act"+key]=value
        
    
    #end = timer()
    #print("feature generation for {0}took".format(str("dialogact")))
    return dialogfeatures     
    

def addauthor_initiate(row):
    sent_no=row["sent_no"]
    corenlp_author_dict= ast.literal_eval(row["corelp_author_dict"])
    author_initiate=ast.literal_eval(row["corelp_author_dict"])["corenlp_info"][sent_no]['Author_turn']
    sents=len(ast.literal_eval(row["corelp_author_dict"])["corenlp_info"])
    parts=sents/4.0
    pos=sent_no%parts            
    author_info={}
    if author_initiate:
        author_info["author_turn"]=1
        author_info["part_dialog"]=pos
    else:
        author_info["author_turn"]=0
        author_info["part_dialog"]=pos
    return author_info     


def addsyntax(row):
    syn_features="SNLP_NoCoref_Sentiment    SNLP_NoCoref_Ner    SNLP_NoCoref_Adv    SNLP_NoCoref_Adj    SNLP_NoCoref_height".split()
    syntax_dict={}
    syntax_dict["SNLP_NoCoref_Sentiment"]=row["SNLP_NoCoref_Sentiment"]
    syntax_dict["SNLP_NoCoref_Ner"]=row["SNLP_NoCoref_Sentiment"]
    if row["SNLP_NoCoref_Adv"]==0:
        syntax_dict["SNLP_NoCoref_ratioAdv_Adj"]=0
    else:    
        syntax_dict["SNLP_NoCoref_ratioAdv_Adj"]=row["SNLP_NoCoref_Adj"]/float(row["SNLP_NoCoref_Adv"])
    syntax_dict["SNLP_NoCoref_height"]=row["SNLP_NoCoref_height"]
    return syntax_dict





def addpos_features(dialogdf):
    allrows=[]
    prevturn_id=""
    sentcount=0
    turns_gp=dialogdf.groupby("turn_ids").size().to_dict()
    for index,row in dialogdf.iterrows():
        
        turnid=row["turn_ids"]
        if prevturn_id=="":
            prevturn_id=turnid
            numsents=turns_gp[turnid]
            parts= numsents/3
        else: 
            if prevturn_id != turnid:
                sentcount=0  
                numsents=turns_gp[turnid]
                parts= numsents/3 
        
        sentcount=sentcount+1
        if sentcount <= parts:
                row["TurnFirst"]=1
                row["TurnMid"]=0
                row["TurnLast"]=0
        else:
                if sentcount <= 2*parts:
                    row["TurnMid"]=1
                    row["TurnLast"]=0
                    row["TurnFirst"]=0
                else: 
                    row["TurnFirst"]=0
                    row["TurnMid"]=0
                    row["TurnLast"]=1
        
        prevturn_id=turnid
        allrows.append(row)
    updateddf=pd.DataFrame(allrows)
    return updateddf
    
def addpositionscores(row,copydf):
    position_dict={}
    sent_no=row["sent_no"]
    #print(sent_no)
    copydfrow=copydf[copydf["sent_no"]==sent_no]
    position_dict["TurnMid"]=  copydfrow["TurnMid"].values[0]
    position_dict["TurnLast"]= copydfrow["TurnLast"].values[0]
    position_dict["TurnFirst"]= copydfrow["TurnFirst"].values[0]
    return position_dict
    
      
                 
def applyfeatures(row,W2vecobject,sent_col,copydf,actobject,lexdict,textdict,sumbasicdict,kldict):
    liwc_scores={} 
    read_scores={}
    w2vec_scores={}
    dialog_scores={}
    lexrank_scores={}
    position_scores={}
    syntax_features={}
    pdtb_vec_scores={}
    sumbasicscores={}
    klscores={}
    text_rank_scores={}
    sentence=row[sent_col]
    sent_no=row["sent_no"]
    
    
    
    if "w2vec" in features:
        w2vec_scores=addw2vecfeatures(sentence,W2vecobject)
        
   
    if "pdtb" in features:
             
                pdtb_vec_scores=addpdtb_fromrow(row)
            
    if "authorinfo" in features:
        authorschange=addauthor_initiate(row)
    
    if 'readability' in features: 
        read_sentence=row[sent_col]            
        read_scores=addreadability(read_sentence)
    
    if "dialog_act" in features:
        dialog_scores=adddialogscores(row,sent_col,copydf,actobject)
    
    if "liwc" in features:
        liwc_scores=addLIWCscores(row,sent_col,copydf)   
           
    if "position" in features:
        position_scores=addpositionscores(row,copydf)    
    
    if "lexrank" in features:
        lexrank_scores=addbaseline(sent_no,lexdict,"lexscore")
        
    if "textrank" in features:
        text_rank_scores=addbaseline(sent_no,textdict,"textscore")    
    if "sumbasic" in features:  
        sumbasicscores=addbaseline(sent_no,sumbasicdict,"sumscore") 
    if "kl" in features:
        klscores=addbaseline(sent_no,kldict,"klscore")
    
    if "syntax" in features:
        syntax_features=addsyntax(row)      
            
    
    
    feature_dict= dict(ChainMap(w2vec_scores,read_scores,dialog_scores,liwc_scores,lexrank_scores,position_scores,pdtb_vec_scores, syntax_features,text_rank_scores,sumbasicscores,klscores))

    return feature_dict
    

            
def extractfeatures(input_file,features_file,sentcolumn,w2vec_loc,tierlabelcol,mintierweight):
    allrows=[]
    alloriginalrows=[]
    """dialogdf : all sentences for one dialog
    "KEY: unique key for dialogue
    generate features for one dialogue, one at a time
    """
    if "w2vec" in features:
        W2vecObject=Word2vecExtractor(w2vec_loc)   
    else:
        W2vecObject=""    
    
    if "dialog_act" in features:
        actobject=dialogue_act_classifier.load_dialogue_act() 
        if actobject:
            print("succesfully loaded dialog act tagger")
    else:
        actobject=None     
        
        
        
                                     
    logger.info("inputfile with all the documents{0}".format(input_file))   
    df=pd.read_csv(input_file+".csv")
    #df["sent_with_coref"]=addcorefsent(df,input_file) 
    start = timer()
    
    
    dialog_gps=df.groupby("Key")
    for key,dialogdf in dialog_gps:
        count=0
        copydialogdf=copy.deepcopy(dialogdf)
        copydialogdf['sent_no'] = copydialogdf['sent_no'].astype(int)
        copydialogdf=copydialogdf.sort("sent_no")
        ids=utterance_number_dialog.get_utterance_number(copydialogdf)
        copydialogdf["turn_ids"]=ids
        copydialogdf["turn_ids"]=copydialogdf["turn_ids"].astype(str)
        ones=0
        lexrankobj= LexRankExtractor()
        textrankobj=TextRankExtractor()
        sumbasicobj=SumBasicExtractor()
        kldivobj=KLSumExtractor()
        sentences=copydialogdf[sentcolumn].tolist()
        labeldict=copydialogdf.groupby("label").size().to_dict()
        for key,value in labeldict.items():
            if key >=3:
                ones=ones+value
        
        dflexrank_sent_set=lexrankobj.getlexranksummary_set(sentences,int(ones))
        dftextrank_sent_set=textrankobj.gettextranksummary_set(sentences,int(ones))
        sumbasicdf_sent_set=sumbasicobj.getsumbasicsummary_set(sentences,int(ones))
        dfkl_sent_set=kldivobj.getklsummary_set(sentences,int(ones))
        
        
        lexdict=createbaselinefeature.baselinefeaturedict(dflexrank_sent_set,copydialogdf,sentcolumn)
        textdict=createbaselinefeature.baselinefeaturedict(dftextrank_sent_set,copydialogdf,sentcolumn)
        sumbasicdict=createbaselinefeature.baselinefeaturedict(sumbasicdf_sent_set,copydialogdf,sentcolumn)
        kldict=createbaselinefeature.baselinefeaturedict(dfkl_sent_set,copydialogdf,sentcolumn)
        
#         textrankincount={'textin':0}
#         lexincount={'lexin':0}
#         sumincount={"sumin":0}
#         klincount={"klin":0}
        
        if "position" in features:
            copydialogdf= addpos_features(copydialogdf)
        for  index, row in dialogdf.iterrows():
            row["turn_ids"]=ids[count]
            feature_row=applyfeatures(row,W2vecObject,sentcolumn,copydialogdf,actobject,lexdict,textdict,sumbasicdict,kldict)
            feature_row["turn_ids"]=ids[count]
            count=count+1
            for key_keep in keystokeepfeatures:
                feature_row[key_keep]=row[key_keep]
                feature_row["lexranksummary"]=dflexrank_sent_set
                feature_row["textranksummary"]=dftextrank_sent_set
                feature_row["sumbasicsummary"]=sumbasicdf_sent_set
                feature_row["KLsummary"]=dfkl_sent_set
                
            
            allrows.append(feature_row)
            alloriginalrows.append(row)
    
    
    merged_df=pd.DataFrame(allrows)
    merged_df.fillna(0,inplace=True)
    end = timer()
    print("feature generation for {0}took".format(str(features)))
    print(end - start)  
    balanced_df,classname=createclasslabel_balance.balance_classlabel(merged_df,sentcolumn,tierlabelcol,mintierweight)  
    dirname=os.path.dirname(features_file)
    bal_dir=  os.path.join(dirname,"balancedclean/")
    features_file_base=os.path.basename(features_file)
    if not  os.path.exists(bal_dir):
        os.makedirs(bal_dir)
    bal_file=os.path.join(bal_dir,features_file_base+"balance.csv")
    merged_df.to_csv(features_file+".csv")
    balanced_df.to_csv(bal_file)
    
    #outfile_pdtb=input_file+"_pdtb_wind"+pdtb_window+".csv"
    #df_pdtb=pd.DataFrame(alloriginalrows)
    #df_pdtb.to_csv( outfile_pdtb)
    
    


    
def run(section):
    args= readCongigFile(section)
    inputfile=args[0]
    features_file=args[1]
    sentcolumn=args[2]
    w2vec_loc=args[3]
    tierlabelcol=args[4]
    mintierweight=args[5]
    extractfeatures(inputfile,features_file,sentcolumn,w2vec_loc,tierlabelcol,mintierweight)   
      

def readCongigFile(section):
    config = configparser.ConfigParser()
    config_file=file_utilities.get_absolutepath_data("config","buildfeaturessemdial_Config.ini")
    config.read(config_file)
    sentcolumn=config.get(section,'text')
    tierlabelcol=config.get(section,'tierlabelcol')
    mintierweight=config.get(section,'mintierweight')
    w2vec_loc=config.get(section,'w2vec_loc')
    input_file=config.get(section,"input_file")
    feature_file=config.get(section,'feature_file')
    pdtb_window = config.get(section, 'window') 
    
   
    #input_file=file_utilities.get_absolutepath_data(input_file_config)
    #feature_file=file_utilities.get_absolutepath_data(feature_file_config) 
    arguments=(input_file,feature_file,sentcolumn,w2vec_loc,tierlabelcol,mintierweight)

    return  (arguments)


if __name__ == '__main__':
    
    
    section="GMtest"
    run(section)
    section="GMtrain" # not balanced
    run(section)
    section="GMtrainCoref"
    run(section)
    section="GMtestCoref"
    run(section)
     
     
     
    section="GCtest"
    run(section)
    section="GCtrain" # not balanced
    run(section)
    section="GCtrainCoref"
    run(section)
    section="GCtestCoref"
    run(section)
    section="ABtest"
    run(section)
    section="ABtrain" # not balanced
    run(section)
    section="ABtrainCoref"
    run(section)
    section="ABtestCoref"
    run(section)
    
    
