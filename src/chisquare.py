'''
Created on May 4, 2017

@author: amita
'''
import pandas as pd
import numpy as np,os
import logging
import file_utilities
import configparser
from sklearn.feature_selection import SelectKBest, chi2,f_classif
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

dupfeatures=["liwc_currentWord Count"]
def splifeatures_label(inputcsv,class_label,feature_list,param_dict):
    #print(inputcsv)
    df=pd.read_csv(inputcsv,na_filter=False)
    colnames=list(df.columns.values)
    feature_cols=[col for col in colnames if str(col).startswith(tuple(feature_list)) and str(col) not in dupfeatures]
    #print(feature_cols)
    param_dict["feature_cols_included"]=feature_cols
    Y_values= df[class_label].values
    X_df=  df[list(feature_cols)]
    return(X_df,Y_values,df)

def execute(param_dict,newdict):
    input_train_file= param_dict["input_train"]
    input_test_file= param_dict["input_test"]
    feature_list=param_dict["feature_list"]
    class_label=param_dict['class_label']
    num=int(param_dict["Num_features_selection"])
    X_train_df,Y_train,traindf=splifeatures_label(input_train_file,class_label,feature_list,param_dict)
    feature_names = list(X_train_df.columns.values)
    selector = SelectKBest(chi2, k=num)
    selector.fit(X_train_df, Y_train)
    mask = selector.get_support() #list of booleans
    new_features = [] # The list of your K best features
    for bool, feature in zip(mask, feature_names):
        if bool:
            new_features.append(feature)
    print(new_features) 
    newdict["train"]=input_train_file
    newdict["top_features"]=new_features
    newdict["input_feature_list"]=param_dict["feature_list"]
    newdict["feature_sel"]=selector
    
    
def run(section): 
    config = configparser.ConfigParser()
    regressioninput=file_utilities.get_absolutepath_data("config","chisquare_config.ini")
    config.read(regressioninput)
    input_file_config=config.get(section,"chi_square_input")
    outfile=config.get(section,"chi_square_output")
    input_file=file_utilities.get_absolutepath_data(input_file_config)
    outfile=file_utilities.get_absolutepath_data(outfile)
    inputdata_df=pd.read_csv(input_file)
    inputdata_df.fillna(0)
    
    all_list=[]
    for index, row in inputdata_df.iterrows():
        newdict={}
        param_dict={}
        param_dict["input_train"]=file_utilities.get_absolutepath_data(row["input_train"])
        param_dict["input_test"]=file_utilities.get_absolutepath_data(row["input_test"])
        outBaseDir=file_utilities.get_absolutepath_data(row["outputBaseDir"])
        param_dict["class_label"]=row["class_label"]
        param_dict["feature_list"]=list(row["feature_list"].split(","))
        #print(param_dict["feature_list"])
        if not os.path.exists(outBaseDir):
            os.mkdir(outBaseDir)
        param_dict["outputBaseDir"]= outBaseDir 
        param_dict["Num_features_selection"]=row["Num_features_selection"]
        execute(param_dict,newdict) 
        all_list.append(newdict)                       
    resultdf=pd.DataFrame(all_list)
    resultdf.to_csv(outfile)
         
if __name__ == '__main__':
    section="chi_square"
    run(section)