'''
Created on May 10, 2017

@author: amita
'''
import copy
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import numpy as np
import nltk
from nltk.corpus import stopwords
stop = set(stopwords.words('english'))

def cosinesimilarity(allsents,sent_baseline,featuredict):
    count_vectorizer = CountVectorizer()
    baselinelist=[]
    baselinelist.append(sent_baseline)
    base_copydf= baselinelist + allsents
    matrix=count_vectorizer.fit_transform(base_copydf) 
    res=(cosine_similarity(matrix[0:1], matrix))
    listcosine=res.tolist()[0][1:]
    value_index=[(v,i) for i,v in enumerate( listcosine)]
    value_index.sort(key=lambda tup: tup[0],reverse=True)
    return value_index
    
def addtupfeaturedict(value_index,featuredict):
    for value, ind in value_index:
        if featuredict[ind]==1:
            continue
        else:
            featuredict[ind]=1
            return
            
        
    
def preprocesssent(sent):
    words = [word.lower() for word in nltk.word_tokenize(sent) ]
    sent = " ".join(words)
    return sent    

def baselinefeaturedict(baseline_sent_set,copydialogdf,sentcol):
    baseline_sent_set_copy=copy.deepcopy(baseline_sent_set)
    allsents=list(copydialogdf[sentcol].values)
    sentsprocess=[]
    for sent in allsents:
            process_sent=preprocesssent(sent)
            sentsprocess.append(process_sent)
    featuredict={}
    for index, row in copydialogdf.iterrows():
        sentence=row[sentcol]
        sent_no=row["sent_no"]
        if sentence=="I have been with my partner for 15 years ." :
            print()
        
        if sentence in baseline_sent_set :
            featuredict[sent_no]=1
            if sentence in baseline_sent_set_copy:
                baseline_sent_set_copy.remove(sentence)
        else:
            featuredict[sent_no]=0
            
    if  baseline_sent_set_copy:
        for sent_base in   baseline_sent_set_copy:
            sent_base_process=preprocesssent(sent_base)
            value_index=cosinesimilarity(sentsprocess,sent_base_process,featuredict)
            addtupfeaturedict(value_index,featuredict)
                 
    return featuredict            
    
if __name__ == '__main__':
    pass