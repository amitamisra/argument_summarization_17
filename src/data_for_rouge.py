'''
Created on Jul 8, 2017

@author: amita
'''
import nltk,re
from evaluation import get_rouge_score,delete_data_from_dir
from file_handling import  writeTextFile
import pandas as pd,os,ast
import generate_baselines

def writesumm(filename,sents):
    with open(filename, mode='wt', encoding='utf-8') as myfile:
        myfile.write('\n'.join(sents))

def writerougescore(references, candidate , rouge_dir, data_dir):
    get_rouge_score(references, candidate , rouge_dir, data_dir)
    
    
def abstractsumm(inputjson,key,golddir,systemdir):
    df=pd.read_json(inputjson)
    delete_data_from_dir(systemdir)
    delete_data_from_dir(golddir)
   
    for index,row in df.iterrows():
        key_val=row[key]
        key_val=key_val.replace("_","*")
        nat_summ=row["Natural_Summary_Text"]
        nat_summ_dict=ast.literal_eval(nat_summ)
        i=1
        for summ_num_key,summ_text in  nat_summ_dict.items():
                sents=nltk.sent_tokenize(summ_text)
                words=nltk.word_tokenize(summ_text)
                wordcount=len(words)
                gold_file_file=os.path.join(golddir,"key"+key_val+"_reference"+ str(i)+".txt") 
                writesumm(gold_file_file,sents)
                i=i+1
        dialog=row["dialog_no_format_author"]
        dialog_text=re.sub("(Author[1-2]:[0-9]*-)\s*", ' ',dialog)
        lexranksumm= generate_baselines.run_lex_rank_wordcount(dialog_text,wordcount) 
        
        j=1 
        systemfile=os.path.join(systemdir,"key"+key_val+"_system"+ str(j)+".txt") 
        writesumm(systemfile,lexranksumm)
           
                
    

def  reference_system_summaries(inputcsv,key,sentcol,golddir,systemdir):
    df=pd.read_csv(inputcsv)
    dfgroup=df.groupby(key)
    delete_data_from_dir(systemdir)
    delete_data_from_dir(golddir)
    for key,df_key in dfgroup:
        #print(key)
        df_key.label = df_key.label.astype(int)
        df_key.lexscore=df_key.lexscore.astype(int)
        
        df_summ_sents=df_key.loc[(df_key['label']  >=3)]
        summ_sents_gold  = df_summ_sents[sentcol].tolist()
        
        lex_rank_summ_df=df_key.loc[(df_key['lexscore'] ==1)]
        lex_rank_sents=lex_rank_summ_df[sentcol].tolist()
        key=key.replace("_","*")
        
        lex_summ_file=os.path.join(systemdir,"key"+key+"_system.txt")   
        gold_file_file=os.path.join(golddir,"key"+key+"_reference.txt")   
        
        writesumm(lex_summ_file,lex_rank_sents)
        writesumm(gold_file_file,  summ_sents_gold)              
                          


def execute(inputcsv,key,sentcol,golddir,systemdir,rouge_dir,data_dir):
    reference_system_summaries(inputcsv,key,sentcol,golddir,systemdir)
    writerougescore(golddir, systemdir , rouge_dir, data_dir)
    
    

def execute_abstract(inputjson,key,sentcol,golddir,systemdir,rouge_dir,data_dir):
    abstractsumm(inputjson,key,golddir,systemdir)
    writerougescore(golddir, systemdir , rouge_dir, data_dir)              

if __name__ == '__main__':
    key="Key"
    sentcol="sent"
    rouge_dir="/Users/amita/software/rouge2.0-0.2-distribute"
    data_dir="/Users/amita/software/rouge2.0-0.2-distribute/afssummary"
    
    inputbasedir="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/abortion/Mturk/mturkcontributorresults2"
    filename="all_mtbatch_ABTest_features.csv"
    inputcsv=os.path.join(inputbasedir,filename)
    golddir="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/abortion/Mturk/mturkcontributorresults2/Rouge/golddir"
    systemdir="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/abortion/Mturk/mturkcontributorresults2/Rouge/lexdir"
    #execute(inputcsv,key,sentcol,golddir,systemdir,rouge_dir,data_dir)
    
    inputbasedir="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gun-control/Mturk/Features2/"
    filename="all_mtbatch_GC_Test_features.csv"
    inputcsv=os.path.join(inputbasedir,filename)
    golddir="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gun-control/Mturk/mturkcontributorresults2/Rouge/golddir"
    systemdir="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gun-control/Mturk/mturkcontributorresults2/Rouge/lexdir"
    #execute(inputcsv,key,sentcol,golddir,systemdir,rouge_dir,data_dir)
    
    inputjson="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gun-control/NaturalSummaryCSV_authorformat_all_pyramids_scus_author_corenlp.json"
    golddir="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gun-control/Rouge/NaturalSumm/"
    systemdir="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gun-control/Rouge/lex/"
  
    
    
    execute_abstract(inputjson,key,sentcol,golddir,systemdir,rouge_dir,data_dir)
    