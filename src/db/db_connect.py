import os
import json
import sqlalchemy
# from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from facet_quality_similarity_cluster.qual_sim_cluster_sharedutilities import file_utilities


def _load_connection_details(configuration_profile=None, filename=None):
    if filename is None:
        filename = file_utilities.load_resource("config/db_connection.json")
        if not os.path.exists(filename):
            raise "Database configuration does not exist. Follow the instructions in resources/config/db_connection.json.bak"

    connection_details_file = open(filename)
    connection_details_raw = json.load(connection_details_file)
    if configuration_profile is None:
        connection_details = connection_details_raw[connection_details_raw["default_configuration"]]
    else:
        connection_details = connection_details_raw[configuration_profile]
    return connection_details


def setup_connection(configuration_profile="local", connection_details=None):
    """
    Call this to obtain an engine and session from sqlalchemy
    :param configuration_profile: Profile name from the db config file to use
    :param connection_details: An alternative db config file other than the one in resources
    :return: sqlalchemy engine, sqlalchemy session
    """
    if connection_details is None:
        connection_details = _load_connection_details(configuration_profile)

    _connection_string = connection_details['dbapi']+'://'+connection_details['username']
    if connection_details['password'] != '':
        _connection_string += ':'+connection_details['password']
    _connection_string += '@'+connection_details['host']+':'+connection_details['port']+'/'+connection_details['database']

    sqlalchemy_engine = sqlalchemy.create_engine( _connection_string)
    session = sessionmaker(bind=sqlalchemy_engine)()
    return sqlalchemy_engine, session

# sql_engine, sql_session = _setup_connection()
# sql_metadata = sqlalchemy.MetaData(bind=sql_engine)
# SQL_Base = declarative_base()

if __name__ == "__main__":
    # test the db connection
    sql_engine, sql_session = setup_connection()
    results = sql_session.execute(
        "select post_id from posts where dataset_id = :dataset_id and discussion_id = :discussion_id",
        {"dataset_id": 1, "discussion_id": 1}
    )
    for row in results:
        print(row)
