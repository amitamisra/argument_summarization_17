from ._sql_connect import SQL_Base, sql_session, reconstructor, sql_metadata
from sqlalchemy.orm import relationship
import sqlalchemy


class Author(SQL_Base):
    __table__ = sqlalchemy.Table('authors', sql_metadata, autoload=True)

    def __init__(self, dataset_id: int, author_id: int, username: str):
        self.dataset_id = dataset_id
        self.author_id = author_id
        self.username = username
