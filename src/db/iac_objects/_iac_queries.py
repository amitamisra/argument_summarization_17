from ._sql_connect import sql_metadata
import sqlalchemy

def get_topic_id(topic: str) -> int:
    table = sqlalchemy.Table('topics', sql_metadata, autoload=True)
    s = sqlalchemy.select([table.c.topic_id]).where(
        (table.c.topic == topic))
    topic_id = s.execute().scalar()
    return topic_id
