import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
# Keep this unused import. Other files in iac_objects use it.
from sqlalchemy.orm import reconstructor

from qual_sim_cluster_sharedutilities.db.db_connect import setup_connection

sql_engine, sql_session = setup_connection()
sql_metadata = sqlalchemy.MetaData(bind=sql_engine)
SQL_Base = declarative_base()
