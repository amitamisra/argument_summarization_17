from collections import namedtuple

import sqlalchemy
from ._sql_connect import SQL_Base, sql_metadata

Topic_Stance_Full_ID = namedtuple("Discussion_Stance_Full_ID", \
        ["dataset_id", "discussion_id", "discussion_stance_id"])

class TopicStance(SQL_Base):
    __table__ = sqlalchemy.Table("topic_stances", sql_metadata, autoload=True)

    def __init__(self, topic_id, topic_stance_id, stance):
        self.topic_id = topic_id
        self.topic_stance_id = topic_stance_id
        self.stance = stance

