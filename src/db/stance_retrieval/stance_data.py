from qual_sim_cluster_sharedutilities.db.db_connect import setup_connection


def _get_self_sided_stance(sql_session, dataset_id, discussion_id, post_id):
    """
    Returns the user's self-annotated stance label and annotated topic stance label from createdebate or convinceme.
    :param sql_session: sqlalchemy session
    :param dataset_id:
    :param discussion_id:
    :param post_id:
    :return: A tuple of (self-annotated stance label, topic stance label)
    """
    results = list(sql_session.execute("""
        select discussion_stance, stance
        from posts
        natural join post_stances
        natural join discussion_stances
        natural join topic_stances
        where
            dataset_id = :dataset_id and
            discussion_id = :discussion_id and
            post_id = :post_id
    """, {"dataset_id": dataset_id, "discussion_id": discussion_id, "post_id": post_id}))

    if len(results) == 1:
        return results[0]
    elif len(results) > 1:
        raise "Query returned too many results"
    else:
        return None, None


def _get_4forums_stances(sql_session, dataset_id, discussion_id, post_id):
    """
    :param sql_session: sqlalchemy session
    :param dataset_id:
    :param discussion_id:
    :param post_id:
    :return: A tuple of (stance label 1, stance label 2, stance label 1 votes, stance label 2 votes, other votes)
    """
    results = list(sql_session.execute("""
        select
        ts_1.stance, ts_2.stance, topic_stance_votes_1, topic_stance_votes_2, topic_stance_votes_other
        from posts
        natural join mturk_author_stance as mt_stance
        join topic_stances as ts_1 on
            ts_1.topic_id = mt_stance.topic_id and
            ts_1.topic_stance_id = mt_stance.topic_stance_id_1
        join topic_stances as ts_2 on
            ts_2.topic_id = mt_stance.topic_id and
            ts_2.topic_stance_id = mt_stance.topic_stance_id_2
        where
            dataset_id = :dataset_id and
            discussion_id = :discussion_id and
            post_id = :post_id
    """, {"dataset_id": dataset_id, "discussion_id": discussion_id, "post_id": post_id}))

    if len(results) == 1:
        return results[0]
    elif len(results) > 1:
        raise "Query returned too many results"
    else:
        return None


def get_stance_info(sql_session, vote_percentage, dataset_id, discussion_id, post_id):
    """

    :param sql_session:
    :param vote_percentage: Percentage of votes that stance label needs to be greater than in order to be chosen
    :param dataset_id:
    :param discussion_id:
    :param post_id:
    :return: (self annotated stance label, topic stance label)
    """
    self_labeled_stance = None
    topic_stance = None
    if dataset_id == 1:
        label_results = _get_4forums_stances(sql_session, dataset_id, discussion_id, post_id)
        if label_results is not None:
            label1, label2, votes1, votes2, votes3 = label_results
            vote_total = votes1 + votes2 + votes3
            if votes1 / vote_total > vote_percentage:
                topic_stance = label1
            elif votes2 / vote_total > vote_percentage:
                topic_stance = label2
    elif dataset_id == 2 or dataset_id == 3:
        self_labeled_stance, topic_stance = _get_self_sided_stance(sql_session, dataset_id, discussion_id, post_id)

    return self_labeled_stance, topic_stance


if __name__ == "__main__":
    _, sql_session = setup_connection()
    print("4forums test: {0}".format(_get_4forums_stances(sql_session, 1, 200, 7)))
    print("cd/cv test: {0}".format(_get_self_sided_stance(sql_session, 2, 5313, 2)))

    print("combined test: {0}".format(_get_4forums_stances(sql_session, 1, 200, 7)))
    print("combined test: {0}".format(_get_self_sided_stance(sql_session, 2, 5313, 2)))
