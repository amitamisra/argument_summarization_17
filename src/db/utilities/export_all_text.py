"""
Exports all text in the database to flat files, one per post, named by {dataset_id}_{post_id}.txt
"""
import os

from qual_sim_cluster_sharedutilities import file_utilities
from qual_sim_cluster_sharedutilities.db.utilities.fast_text_iterator import IacTextIterator

text_iterator = IacTextIterator()

output_dir = file_utilities.load_resource("scratch", "all_text")
if not os.path.exists(output_dir):
    os.makedirs(output_dir)

for text_tup in text_iterator:
    file_name = os.path.join(output_dir, "{dataset_id}_{text_id}.txt".format(dataset_id=text_tup[0],
                                                                             text_id=text_tup[1]))
    with open(file_name, "w") as fh:
        fh.write(text_tup[2])

