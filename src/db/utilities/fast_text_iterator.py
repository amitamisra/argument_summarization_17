from qual_sim_cluster_sharedutilities.db.db_connect import setup_connection

page_query = """
SELECT  t.dataset_id, t.text_id, t.text
FROM    (
        SELECT  dataset_id, discussion_id
        FROM    discussions
        ORDER BY
                dataset_id, discussion_id LIMIT :offset, :page_length
        ) AS q
JOIN    posts p
ON
    p.dataset_id = q.dataset_id AND
    p.discussion_id = q.discussion_id
JOIN    texts t
ON
    t.dataset_id = p.dataset_id AND
    t.text_id = p.text_id
;"""

count_query = """
    select count(*) from discussions natural join posts natural join texts;
"""


class IacTextIterator(object):
    def __init__(self, page_length=10000):
        self.page_length = page_length
        _, self._sql_session = setup_connection()
        self._end = int(self._sql_session.execute(count_query).fetchone()[0])

    def __iter__(self):
        offset = 0
        while offset < self._end:
            try:

                for result in self._sql_session.execute(page_query,
                                                       {'offset': offset, 'page_length': self.page_length}).fetchall():
                    yield result
            except Exception as e:
                print("Query error: " + str(offset))
                print(str(e))

            offset += self.page_length


if __name__ == "__main__":
    iterator = IacTextIterator()
    count = 0
    for _ in iterator:
        count += 1

    assert(count == iterator._end)
