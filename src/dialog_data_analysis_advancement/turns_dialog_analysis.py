'''
Created on Apr 10, 2017

@author: amita
'''
import pandas as pd
from src import file_utilities
import configparser
import os
import matplotlib
matplotlib.use('PS')
import matplotlib.pyplot as plt
PROJECT_DIR=file_utilities.PROJECT_DIR
def plotauthor_dict(author_dict,plot_file):
    key_tuple = list(author_dict.keys())
    x=[tup[1] for tup in key_tuple]    
    y =list(author_dict.values())
    plt.plot(y)
    #plt.scatter(x,y)
    plt.ylabel("Number of posts")
    plt.xlabel("Authors")
    plt.legend(loc="upper right")
    plt.legend(frameon=False)
    plt.show()
    plt.savefig(plot_file)
    plt.close()

    
    

def plotdialog_turn(dialog_turn_dict,plotfile):
    x=NumberofturnsPerdialog = list(dialog_turn_dict.keys())
    y=NumOfdialogs =list(dialog_turn_dict.values())
    #plt.plot(y)
    plt.plot(x,y)
    plt.ylabel("Number of Dialogues")
    plt.xlabel("NumberofTurnsPerDialogue")
    plt.legend(loc="upper right")
    plt.legend(frameon=False)
    plt.show()
    plt.savefig(plotfile)
    plt.close()
        


def post_analysis(filewithpath):
    df=pd.read_csv(filewithpath)
    author_dict=df.groupby(["posts_dataset_id","posts_author_id"]).size().to_dict()
    return author_dict
    
    
    
def turn_analysis(dialog_dir):
    filelist=os.listdir(dialog_dir)
    all_dicts={}
    for filename in filelist:
        filewithpath=os.path.join(dialog_dir,filename)
        df=pd.read_csv(filewithpath)
        dialog_count=df.groupby("Dialog_Turn").count()
        dialog_frequency=df['Dialog_Turn'].value_counts().value_counts().to_dict()
        for key, value in dialog_frequency.items():
            if key in all_dicts.keys():
                old_value=all_dicts[key]
                newvalue=value+ old_value
                all_dicts[key]=newvalue
            else:
                all_dicts[key]=value    
    return all_dicts            
        
def run(section):
    config = configparser.ConfigParser()
    config_file=file_utilities.get_absolutepath_data("config","turns_dialog_analysis_config.ini")
    config.read(config_file) 
    
    dialog_dir=config.get(section,"dialog_directory") 
    all_discussions=config.get(section,"all_discussions")
    dirplots=os.path.join(file_utilities.PROJECT_DIR,"src","dialog_data_analysis_advancement")
    plotfile_turn=os.path.join(dirplots,config.get(section,"plotfile_turn"))
    plotfile_author=  os.path.join(dirplots,config.get(section,"plotfile_author"))
    
    dialog_turn_dict=turn_analysis(dialog_dir) 
    author_dict=post_analysis(all_discussions)
    
    plotauthor_dict( author_dict,plotfile_author)
    plotdialog_turn(dialog_turn_dict,plotfile_turn)
     
if __name__ == '__main__':
    section="GM"
    run(section)