import os
from src import file_utilities
# We need to store the project root for testing reason. Also, it need to be absolute path.
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))

# MAIN_DIR = "./"
MAIN_DIR = ROOT_DIR
RESOURCES = os.path.join(MAIN_DIR, "resources/")
TEMP = os.path.join(MAIN_DIR, "temp/")
DATA = os.path.join(MAIN_DIR, "data/")

#SWDA_DATA = "../../Speech-Act-Classification-with-Cross-training-And-Domain-adaptation/Data/SWDA/"
#SWDA_SINGLE_FILE = "../../data/swda/swda_combined_data.csv"

SWDA_DATA = file_utilities.get_absolutepath_data("dialogue_act_data","swda","swda_combined_data.csv")
SWDA_DAC_MODEL =  file_utilities.get_absolutepath_data("dialogue_act_data", "pickles","act_classification","swda_dac_model_special_features.pkl")

ACT_CLASSIFICATION = file_utilities.get_absolutepath_data("dialogue_act_data", "pickles","act_classification")
NLTK_DAC_MODEL = file_utilities.get_absolutepath_data("dialogue_act_data", "pickles","act_classification","nltk_dac_model_special_features.pkl")
#NLTK_DAC_MODEL = os.path.join(DATA, "pickles/act_classification/nltk_dac_model.pkl")

STANFORD_PARSER_HOME = os.path.join(MAIN_DIR, "stanford/stanford-corenlp-full-2016-10-31/")
BERKELEY_PARSER = os.path.join(RESOURCES, "berkeley-entity-master/berkeley-entity-1.0.jar")
TWEET_PARSER = os.path.join(RESOURCES, "twitter_barebones/")

SENTIMENT_DRACULA_DIR = os.path.join(RESOURCES, "dracula-sentiment-node-master/")
SENTIMENT_DRACULAR_RUN_SCRIPT = os.path.join(SENTIMENT_DRACULA_DIR, "run.js")

WIKI_SEARCHER = os.path.join(RESOURCES, "wikipedia-1.4.0")
DUCKDUCKGO_SEARCHER = os.path.join(RESOURCES, "duckduckgo2-0.242/")

GOOGLE_TREND_DIR = os.path.join(RESOURCES, "google-trend-api/")

NEWS_POOL_SQLITE_DB_DIR = os.path.join(MAIN_DIR, "memory/sqlite_db/news_pool.sqlite")
PROFANITY_FILE_DIR = os.path.join(MAIN_DIR, "resources/sensitive_info", "profanity.txt")
PORN_FILE_DIR = os.path.join(MAIN_DIR, "resources/sensitive_info", "porn_list.txt")

SCHEME_ROOTS = os.path.join(MAIN_DIR, "resources", "schema_roots.json")
SCHEME_ENTITIES = os.path.join(MAIN_DIR, "resources", "schema_entities.json")

M2D_PY_SCRIPTS_DIR = os.path.join(MAIN_DIR, "m2d", "py_scripts")

LOGGER_DIR = os.path.join(MAIN_DIR, "logs")
CHAT_HISTORY_SQLITE_DB_DIR = os.path.join(MAIN_DIR, "memory/sqlite_db/chat_history.sqlite")

DIALOG_INDEX_DIR = os.path.join(MAIN_DIR, "index/dialog_index_dir")
DIALOG_DATA_DIR = os.path.join(MAIN_DIR, "resources/dialog-data/")

if __name__ == "__main__":
    print(ROOT_DIR)
    print(RESOURCES)