'''
Created on Apr 11, 2017

@author: amita
'''
from nltk.corpus import nps_chat
from DialogueActTaggerNLTK import DialogueActTaggerNLTK

    
class  InitDialogue_Act:
        def  __init__(self,dialogueActTaggerNLTK=None):
        
            if dialogueActTaggerNLTK:
                self.DialogueActTaggerNLTK = dialogueActTaggerNLTK
            else:
                self.DialogueActTaggerNLTK = DialogueActTaggerNLTK(tagger_name="NLTK_DialogueActTagger",
                                                                   pickled_model=None)
                
                
                
                
def load_dialogue_act(): 
        dialogue_act_object=InitDialogue_Act()
        return dialogue_act_object
    
def classify_act(dialogue_act_object,text):  
        dialogue_act = dialogue_act_object.DialogueActTaggerNLTK.run_dialogue_act_tagger(text)
        return(dialogue_act)
           
def createactdictionary(acttype):
        act_type_statememt=["Statement","Emphasis","yAnswer","nAnswer","Reject","Continuer","Accept"]
        act_question=["whQuestion","ynQuestion","Clarify"]
        
        act_dict=dict()
        {"statement:0","question:0","other:0"}
        if acttype in act_type_statememt:
            act_dict['statement']=1
            act_dict['question']=0
            act_dict['other']=0
        else:
            if acttype in act_question:
                act_dict['question']=1
                act_dict['statement']=0
                act_dict['other']=0
            else:        
                act_dict['other']=1
                act_dict['statement']=0
                act_dict['question']=0
        return act_dict        
                    
def createactdictionary_2(acttype):
        act_type_statememt=["Statement","Emphasis","yAnswer","nAnswer","Reject","Continuer","Accept"]
        act_question=["whQuestion","ynQuestion","Clarify"]
        
        act_dict=dict()
        {"question":0,"other":0}
        
        if acttype in act_question:
                act_dict['question']=1
                act_dict['other']=0
        else:        
                act_dict['other']=1
                act_dict['question']=0
        return act_dict                   
                       
if __name__ == "__main__":
    actobject=load_dialogue_act()
    print(classify_act(actobject,"How can you say that"))
    
    
                