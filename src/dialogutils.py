import csv
import re
import random
import nltk
import pandas
import os
import codecs
import pickle

# DIR UTILS

def get_dir_contents(dir):
    return os.listdir(dir)

# READ/WRITE


def read_file_py2(filename):
    reader = codecs.open(filename, encoding='utf-8', mode='r')
    return reader.read()


def read_file_py3(filename):
    reader = open(filename, 'r', encoding="utf-8")
    return reader.read()


def read_file_lines(filename):
    reader = open(filename, 'r', encoding="utf-8")
    lines = reader.readlines()
    return lines


def read_csv(filename):
    with open(filename, 'r', encoding="utf-8") as csvfile:
        reader = csv.DictReader(csvfile)
        fieldnames = reader.fieldnames
        return list(reader), fieldnames


def write_csv_py2(filename, rows, header_fields=None):
    with open(filename, mode='w') as csvfile:
        writer = csv.writer(csvfile)
        if header_fields:
            writer.writerow(header_fields)
        for row in rows:
            writer.writerow(row)


def write_csv_py3(filename, rows, header_fields=None):
    with open(filename, 'w', encoding="utf-8") as csvfile:
        writer = csv.writer(csvfile)
        if header_fields:
            writer.writerow(header_fields)
        for row in rows:
            writer.writerow(row)


def write_unencoded_rows(filename, rows, header_fields=None):
    writer = csv.writer(open(filename, "w"), quoting=csv.QUOTE_NONE, escapechar='\\')
    if header_fields:
        writer.writerow(header_fields)
    for row in rows:
        writer.writerow(row)


def write_csv_from_dict(filename, rows, header_fields=None):
    with open(filename, 'w', encoding="utf-8") as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=header_fields)
        writer.writeheader()
        writer.writerows(rows)


def write_csv_from_list_of_dict(filename, rows, header_fields=None):
    to_write = []
    for row in rows:
        to_write += [[row[field] for field in header_fields]]
    with open(filename, 'w', encoding="utf-8") as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=header_fields)
        writer.writeheader()
        writer.writerows(to_write)


def write_text_to_file_py2(filepath, text):
    writer = codecs.open(filepath, encoding='utf-8', mode='w+')
    writer.write(text)
    writer.close()


def write_text_to_file_py3(filepath, text):
    writer = open(filepath, 'w', encoding="utf-8")
    writer.write(text)
    writer.close()


# MATCH IN CSV

def get_matching_sent(cue, post):
    sentence = ""
    post_sents = nltk.sent_tokenize(post)
    for sent in post_sents:
        if cue in sent:
            sentence = sent
            break
    return sentence


# HELPERS

def is_good_len(x):
    l = len(x.split())
    return 10 <= l <= 150


def is_good_len_tweet(x):
    l = len(x.split())
    return 4 <= l <= 150


def randomize_dict(d):
    random.seed(12345)
    items = d.items()
    d = random.shuffle(items)
    return d


def randomize_list(rows):
    random.seed(12345)
    rows = random.sample(rows, len(rows))
    return rows


def remove_nonascii(s):
    s = re.sub(r'[^\x00-\x7F]+', '', s)
    return s


def sort_dict_by_value(d):
    return sorted(d.items(), key=lambda x:x[1])


# PANDAS

def pprint_dataframe(df):
    with pandas.option_context('display.max_rows', 999, 'display.max_columns', 3):
        print(df)

# PICKLES

def save_model_to_pickle(model, pickle_file):
    f = open(pickle_file, 'wb')
    pickle.dump(model, f)
    f.close()


def load_model_from_pickle(pickle_file):
    f = open(pickle_file, 'rb')
    model = pickle.load(f)
    f.close()
    return model
