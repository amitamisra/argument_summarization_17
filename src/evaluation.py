import subprocess
from nltk.translate import bleu_score
from util import *
from shutil import copyfile

def get_bleu_score(references, candidate):
    candidate = [c.lower() for c in candidate]
    references = [r.lower() for r in references]
    print (bleu_score.sentence_bleu(references, candidate, smoothing_function=bleu_score.SmoothingFunction().method4))

def get_rouge_score(references, candidate ,  rouge_dir, data_dir):
    prev_dir = os.getcwd()
    os.chdir(rouge_dir)
    ref_dir = data_dir + '/reference/'
    sys_dir = data_dir + '/system/'

    delete_data_from_dir(sys_dir)
    delete_data_from_dir(ref_dir)
    for ref in os.listdir(references):
        ref_filename = os.path.join(ref_dir, ref)
        ref_path=os.path.join(references,ref)
        copyfile(ref_path, ref_filename)
    
    
    for can in os.listdir(candidate):
        can_filename = os.path.join(sys_dir, can)
        can_path=os.path.join(candidate,can)
        copyfile(can_path, can_filename)    
   
    subprocess.call(['java', '-jar', 'rouge2.0_0.2.jar'])
    os.chdir(prev_dir)


def example():
    ref = "this is ref summary. it is tough"
    can = "this is can summary. it is harsh"
    get_bleu_score(ref, can)
    get_rouge_score([ref], [can],  os.getcwd() + '/../rouge', os.getcwd() + '/../rouge')

#example()
