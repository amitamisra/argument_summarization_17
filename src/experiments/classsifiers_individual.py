'''
Created on Apr 7, 2017

@author: amita
'''
from sklearn.cross_validation import cross_val_score
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import Pipeline
from sklearn.svm import LinearSVC

from math import sqrt
import os, codecs

from sklearn import cross_validation 
from sklearn import preprocessing
from sklearn import svm
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.pipeline import Pipeline
import pandas as pd
import numpy as np
import logging
import configparser
from sklearn.grid_search import GridSearchCV
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import f1_score
from pprint import pprint
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class SVMclassification:
    
    def __init__(self,numFeatureSelection,gammastart,gammaend,gammacount,Cstart,Cend,Ccount):
        self.gammastart=float(gammastart)
        self.gammaend=float(gammaend)
        self.gammacount=float(gammacount)
        self.Cstart=float(Cstart)
        self.Cend=float(Cend)
        self.Ccount=float(Ccount)
        self.numFeatureSelection= [int(x) for x in numFeatureSelection if x.isdigit()] + ["all"]
        
    def setpipeline(self):
        self.pipeline=Pipeline([('scale', preprocessing.StandardScaler()),('filter', SelectKBest(chi2)),('svm', svm.SVC())])
        
    def setparam_grid(self):
        C_range = np.logspace(self.Cstart, self.Cend, self.Ccount)
        gamma_range = np.logspace(self.gammastart,self.gammaend,self.gammacount)
        #self.param_grid=[{'svr__kernel': ['linear'], 'svr__C': C_range,'filter__k': self.numFeatureSelection}]
        self.param_grid=[{'svm__kernel': ['rbf'], 'svm__gamma': gamma_range,'svm__C': C_range, 'filter__k': self.numFeatureSelection}]



def create_svm_pipeline(gammastart,gammaend,gammacount,Cstart,Cend,Ccount,param_dict_regression):
    num_features=param_dict_regression["Num_features_selection"]
    svmclassifier=SVMclassification(num_features,gammastart,gammaend,gammacount,Cstart,Cend,Ccount)
    svmclassifier.setparam_grid()
    svmclassifier.setpipeline()
   
    return svmclassifier

def create_result_dict(param_dict,Y_actual,Y_pred,result_dict,best_param,param_grid):
    result_dict["inputTrain"]=param_dict["inputTrain"]
    result_dict["inputTest"]=param_dict["inputTest"]
    result_dict["f1_macro"] =f1_score (Y_actual,Y_pred,average='macro')
    result_dict["f1_micro"] =f1_score(Y_actual,Y_pred,average='micro')
    result_dict["f1_weighted"] =f1_score(Y_actual,Y_pred,average='weighted')
    result_dict["best_param"]=best_param
    result_dict["param_grid"]=param_grid

def gridSearchTestSet(X_train,Y_train,X_test,Y_test,pipeline,param_grid,cvfolds,param_dict): 
        logger.info("Doing grid search for test set")
        result_dict={}
        grid_search = GridSearchCV(pipeline, param_grid=param_grid,cv=3)       
        grid_search.fit(X_train, Y_train)
        best_param=grid_search.best_params_
        Y_pred=grid_search.predict(X_test)
        pred_list= [{'Y_actual': v1, 'Y_predicted': v2} for v1, v2 in zip(Y_test, Y_pred)]
        create_result_dict(param_dict,Y_test, Y_pred,result_dict,best_param,param_grid)
        basedir=param_dict["outputBaseDir"]
        classifier=param_dict["classifier"]
        feature= "_".join(param_dict["featureList"])
        outputDir=os.path.join(basedir,feature)
        if not os.path.exists(outputDir):
            os.makedirs(outputDir)
        csvFile=os.path.join(outputDir,feature + classifier +"__test.csv")
        writeresultstoFile(csvFile,result_dict,pred_list,X_test,Y_test,param_dict)
        return result_dict     

def writeresultstoFile(csvFile,result_dict,pred_list,X_train,Y_train,param_dict_regression):
    logger.info("start writing file " + str(param_dict_regression["featureList"]) +" "+csvFile  )
    assert(len(pred_list)==len(X_train)==len(Y_train))
    
    df1=pd.DataFrame(pred_list)
    df2=pd.DataFrame(X_train)
    df = df2.join(df1)
    df.to_csv(csvFile,index=False)
    f = codecs.open(csvFile, "a",encoding='utf-8')
    pprint(result_dict, stream=f) 
    #pprint(param_dict_regression["feature_cols_included"], stream=f)
    f.close()
    logger.info("Done writing file " + str(param_dict_regression["featureList"]) +" "+csvFile  )              
if __name__ == '__main__':
    pass