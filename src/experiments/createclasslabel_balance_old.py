'''
Created on Apr 7, 2017

@author: amita
'''

randomseed=3
import pandas as pd
from src.stanford_parser_arg import filtersentence


def clean(uncleandf,textcol):
    allrows=[]
    for index, row in uncleandf.iterrows():
        sentence=row[textcol]
        add=filtersentence(sentence,3)
        if add:
            allrows.append(row)
    clean_df=pd.DataFrame(allrows)  
    return clean_df       
            
    
    
    

def classlabel(row,weightcol,wtvalue):
    if float(row[weightcol]) >= float(wtvalue):
        return 1
    else:
        return 0
        

def clean_coref(inputfile,outputcsv):
    df=pd.read_csv(inputfile)
    textcol="sent"
    clean_df=clean(df,textcol)
    clean_df.to_csv(outputcsv)
    

def balance_classlabel(dataframe,textcol,weightcol, wtvalue):
    """
    
    """
    grouped=dataframe.groupby(weightcol)
    df_good_list=[]
    alldf=[]
    df_bad_list=[]
    for wt, gp in grouped:
        if float(wt) >= float(wtvalue):
            df_good_list.append(gp)
        else:
            
            df_bad_list.append(gp)
    
    allgooddf=pd.concat(df_good_list)   
    allbaddf=pd.concat(df_bad_list)
    
    goodrowscount=allgooddf.shape[0]
    badrowscount=allbaddf.shape[0]
    if goodrowscount < badrowscount:
        samplebaddf=allbaddf.sample(goodrowscount,random_state=randomseed)
        alldf.append(allgooddf)
        alldf.append(samplebaddf)
    else:
        samplegooddf=allgooddf.sample(badrowscount,random_state=randomseed)
        alldf.append(allbaddf)
        alldf.append(samplegooddf)

    mergeddf=pd.concat(alldf)
    
    mergeddf['class'] = mergeddf.apply(classlabel,args=(weightcol,wtvalue),axis=1)     
    return mergeddf,"class" 
         
if __name__ == '__main__':
    inputfile="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gun-control/Mturk/mturkcontributorresults/all_mtbatch_result_SummaryCSV_authorformat_all_pyramids_scus_author_corenltrain_corefsent.csv"
    outputfile="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gun-control/Mturk/mturkcontributorresults/all_mtbatch_result_SummaryCSV_authorformat_all_pyramids_scus_author_corenltrain_cleancorefsent.csv"
    clean_coref(inputfile,outputfile)