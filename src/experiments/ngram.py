'''
Created on Apr 6, 2017

@author: amita
'''
'''
Created on Apr 5, 2017

@author: amita
'''
from sklearn.cross_validation import cross_val_score
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import Pipeline
from sklearn.svm import LinearSVC

from math import sqrt
import os, codecs

from sklearn import cross_validation 
from sklearn import preprocessing
from sklearn import svm
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.pipeline import Pipeline
import pandas as pd
import numpy as np
import logging
import configparser
from sklearn.grid_search import GridSearchCV
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import f1_score
from pprint import pprint
import classsifiers_individual
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.base import BaseEstimator, TransformerMixin
from src import createclasslabel_balance
from src import file_utilities
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


defaultgammastart=1
defaultgammaend=30
defaultCstart=1
defaultCend=60
defaultgammacount=10
defaultCcount=10
defaultfolds=10
cvfolds=5



logger = logging.getLogger()
handler = logging.StreamHandler()
formatter = logging.Formatter(
        '%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.INFO)
#import skip_thought_vectors_py3.addskipthought as addskipthought
logging.basicConfig(level=logging.CRITICAL)

logger = logging.getLogger(__name__)



class ColumnExtractor(BaseEstimator, TransformerMixin):
    def __init__(self,textcol):
        self.textcol=textcol

    def transform(self, df, y=None):
        return df[self.textcol].values

    def fit(self, df, y=None):
        return self
    
    
def createsvmclassifierpipeline(param_dict):
    if param_dict["gammastart"]:
        gammastart=param_dict["gammastart"]
    else:
        gammastart=defaultgammastart
        
    if param_dict["gammaend"]:
        gammaend=param_dict["gammaend"] 
    else:
        gammaend=defaultgammaend 
    if param_dict["gammacount"]:
        gammacount= param_dict["gammacount"]
    else:
        gammacount=defaultgammacount       
           
    if param_dict["Cstart"]:
        Cstart=param_dict["Cstart"]
    else:
        Cstart=defaultCstart
        
    if param_dict["Cend"]:
        Cend=param_dict["Cend"] 
    else:
        Cend=defaultCend 
        
    if param_dict["Ccount"]:
        Ccount=param_dict["Ccount"] 
    else:
        Ccount=defaultCcount   
        
    svm_withpipeline=classsifiers_individual.create_svm_pipeline(gammastart,gammaend,gammacount,Cstart,Cend,Ccount,param_dict)
    
    return svm_withpipeline



def create_feature_pipeline(param_dict,textcol):
    
    featureList=param_dict["featureList"]
    if "ngram" in featureList:
        features = ('features',
            FeatureUnion([
                ('ngrams_count', Pipeline([
                   ('column_extractor', ColumnExtractor(textcol)),
                   ('count_vectorizer', CountVectorizer(ngram_range=(1,2))),
                ])),
            ]))
        classifiers=(['linear_svc',LinearSVC()])


    else:
                
        if "ngram_tfidf" in featureList:
            features=    ('features',
            FeatureUnion([
                ('ngrams_tfidf', Pipeline([
                  ('column_extractor', ColumnExtractor(textcol)),
                  ('tfidf_vectorizer', TfidfVectorizer()),
                ])),
            ]))
        classifiers=(['linear_svc',LinearSVC()])   
    
    parameters = {
    'features__ngrams_count__count_vectorizer__ngram_range': [(1, 1), (1, 2)],
    'linear_svc__C': [10,100,]
}
    return features, classifiers, parameters

    

def execute(param_dict):
    input_train= param_dict["inputTrain"]
    input_test= param_dict["inputTest"]
    
    X_train_df=pd.read_csv(input_train)
    X_test_df=pd.read_csv(input_test)
    
    #X_train_df, class_label=createclasslabel_balance.balance_classlabel(X_train_df,"sent","label", 3)
    #X_test_df,class_label=createclasslabel_balance.balance_classlabel( X_test_df,"sent","label", 3)
    class_label=param_dict["class_label"]
    featureList=param_dict["featureList"]
    reg_type=param_dict["classifier"]
    eval_type=param_dict["eval_type"]
    num_features=param_dict["Num_features_selection"]
    
    
    Y_train= X_train_df[class_label].values
   
    Y_test= X_test_df[class_label].values
#     if  "SVM" == reg_type:
#         svm_withpipeline=createsvmclassifierpipeline(param_dict)
    features, classifiers, parameters=create_feature_pipeline(param_dict,param_dict["text_col"]) 
    
    pipeline=Pipeline([features, classifiers]) 
    
    if eval_type=="test":
        classsifiers_individual.gridSearchTestSet(X_train_df,Y_train,X_test_df,Y_test,pipeline,parameters,cvfolds,param_dict)
            
def run(section):
    
    config = configparser.ConfigParser()
    experiment_input=os.path.join(os.getcwd(),"experiments_config.ini")
    config.read(experiment_input)
    csv_file=config.get(section,"csv_with_file_list")
    input_file=os.path.join(os.getcwd(),csv_file)
    inputdata_df=pd.read_csv(input_file)
    inputdata_df.fillna(0)
    
    all_list=[]
    for index, row in inputdata_df.iterrows():
        param_dict={}
        done=row["done"]
        if int(done)==1:
            continue
        #addcosine(row["inputTrain"])
        param_dict["gammastart"]=row["gammastart"]
        param_dict["gammaend"]=row["gammaend"]
        param_dict["gammacount"]=row["gammacount"]
       
        param_dict["Cstart"]=row["Cstart"]
        param_dict["Cend"]=row["Cend"]
        param_dict["Ccount"]=row["Ccount"]
       
        param_dict["classifier"]=row["classifier"]
        param_dict["featureList"]=list(row["featureList"].split(","))
        param_dict["eval_type"]=row["eval_type"]
        param_dict["class_label"]=row["class_label"]
        param_dict["Num_features_selection"]=list(str(row["Featureselection"]).split(","))# A list of possible  feature count  for feature selection, by default no feature selection
        
        param_dict["inputTrain"]=file_utilities.get_absolutepath_data(row["inputTrain"])
        param_dict["inputTest"]=file_utilities.get_absolutepath_data(row["inputTest"])
        param_dict["sent_coref_col"]=row["sent_coref_col"]
        param_dict["writefeaturefile"]=row["writefeaturefile"]
        param_dict["text_col"]=row["textcol"]
        outBaseDir=file_utilities.get_absolutepath_data(row["outputBaseDir"])
        if not os.path.exists(outBaseDir):
            os.mkdir(outBaseDir)
        param_dict["outputBaseDir"]= outBaseDir 
        execute(param_dict)         
    
def runGM():
    pass
if __name__ == '__main__':
    section="GC"
    run(section) 
    