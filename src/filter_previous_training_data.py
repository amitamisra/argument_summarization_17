'''
Created on Mar 21, 2017

@author: amita
'''
import pandas as pd
import configparser
import os
import logging

logger = logging.getLogger()
handler = logging.StreamHandler()
formatter = logging.Formatter(
        '%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.INFO)

class Filter:
    def  __init__(self,post,text,quality,similarity):
        """
        These are boolean variables to determine the filter criteria
        """
        self.post= post
        self.text= text
        self.quality=quality
        self.similarity=similarity
    
    def  removepostsquality(self,df_filter_input, df_filter_apply):
          
        filtered_df=pd.merge(df_filter_apply,df_filter_input, how='outer', indicator=True,on=["Dataset_Id","Discussion_Id","Post_Id"])
        rows_in_df1_not_in_df2 = filtered_df[filtered_df['_merge']=='left_only'][df_filter_apply.columns]
        return rows_in_df1_not_in_df2 
        
    

def applyfilter(filterobj,filterinput,filterapply,filtered_output):
    if int(filterobj.post)==1 and int(filterobj.quality)==1:
        df_filter_input=pd.read_csv(filterinput)
        df_filter_apply=pd.read_csv(filterapply)
        filtered=filterobj.removepostsquality(df_filter_input, df_filter_apply)
        filtered.to_csv(filtered_output)
    
    
def run(section):
    config = configparser.ConfigParser()
    current_dir=os.path.abspath(os.path.dirname(__file__))
    config.read(os.path.abspath(os.path.join(current_dir,"filter_previous_training_data_config.ini")))
    post=config.get(section,"post")
    text=config.get(section,"text")
    quality=config.get(section,"quality")
    similarity=config.get(section,"similarity")
    filterinput=config.get(section,"filterinput")
    filterapply=config.get(section,"filterapply")
    filtered_output=config.get(section,"filtered_output")
    logger.info("applying filter to file {0}".format(filterinput))
    filterobj=Filter(post,text,quality,similarity)
    applyfilter(filterobj,filterinput,filterapply,filtered_output)
    
    
    if post and similarity:
        pass#TODO
    if post and similarity:
        pass#TODO
    if text and similarity:
        pass#TODO
        
if __name__ == '__main__':
    run("GM")
    run("GC")
    run("DP")
    run("EV")