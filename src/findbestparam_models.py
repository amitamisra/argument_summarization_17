'''
Created on Dec 14, 2017

@author: amita
'''


def create_base_network(input_shape,dropout,dim,filterratio,kernelsize,poollength,denselayer):
    
    input_1 = Input(shape=(input_shape,))
    embedded_layer = create_embedding_matrix(word_index,input_shape)
    encoded_sent = embedded_layer(input_1)
    encoded_sent = Dropout(dropout)(encoded_sent)
    encoded_sent= Conv1D(nb_filter=int(EMBEDDING_DIM/filterratio),kernel_size=kernelsize,padding='same', activation='relu')(encoded_sent)
    encoded_sent= MaxPooling1D(pool_length=poollength)(encoded_sent)
    encoded_sent=Dropout(dropout)( encoded_sent )
    encoded_sent= Conv1D(nb_filter=int(EMBEDDING_DIM/filterratio), kernel_size=kernelsize, padding='same', activation='relu')(encoded_sent)
    encoded_sent= MaxPooling1D(pool_length=poollength)(encoded_sent)
    encoded_sent=Dropout(dropout)( encoded_sent )
    encoded_sent=Flatten()(encoded_sent)
    encoded_sent=Dense({{choice([denselayer])}}, activation='relu')(encoded_sent)
    return Model(input_1,encoded_sent)

def create_model_noshared_baseline(train_data,features_count,add_features,dropout,dim,optim,denselayer,seq_length,act,filterratio,kernel_size,poollength):
    print("executing cnn no sharing")

    MAX_SEQUENCE_LENGTH=seq_length
    input_1 = Input(shape=(MAX_SEQUENCE_LENGTH,),name='sent1_input')
    input_2 = Input(shape=(MAX_SEQUENCE_LENGTH,),name='sent2_input')
    
    features_1 = Input(shape=(features_count,),name='features1_input')
    features_2 = Input(shape=(features_count,),name='features2_input')
    
    encoded_sent1 = create_embedding_matrix(word_index,MAX_SEQUENCE_LENGTH) (input_1)
    encoded_sent1 = Dropout(dropout)(encoded_sent1)
    encoded_sent1= Conv1D(nb_filter=int(EMBEDDING_DIM/filterratio),kernel_size=kernel_size, padding='same', activation='relu')(encoded_sent1)
    encoded_sent1=Dropout(dropout)( encoded_sent1 )
    encoded_sent1= Conv1D(nb_filter=int(EMBEDDING_DIM/filterratio), kernel_size=kernel_size, padding='same', activation='relu')(encoded_sent1)
    encoded_sent1= MaxPooling1D(pool_length=poollength)(encoded_sent1)
    encoded_sent1=Dropout(dropout)( encoded_sent1 )
    encoded_sent1=Flatten()(encoded_sent1)
    encoded_sent1=Dense(denselayer, activation='relu')(encoded_sent1)
    #encoded_sent21=GaussianNoise(noise)(encoded_sent21)
    
    encoded_sent2 = create_embedding_matrix(word_index,MAX_SEQUENCE_LENGTH) (input_2)
    encoded_sent2 = Dropout(dropout)(encoded_sent2)
    encoded_sent2= Conv1D(nb_filter=int(EMBEDDING_DIM/filterratio),kernel_size=kernel_size, padding='same', activation='relu')(encoded_sent2)
    encoded_sent2=Dropout(dropout)( encoded_sent2 )
    encoded_sent2= Conv1D(nb_filter=int(EMBEDDING_DIM/filterratio), kernel_size=kernel_size, padding='same', activation='relu')(encoded_sent2)
    encoded_sent2= MaxPooling1D(pool_length=poollength)(encoded_sent2)
    encoded_sent2=Dropout(dropout)( encoded_sent2 )
    encoded_sent2=Flatten()(encoded_sent2)
    encoded_sent2=Dense(denselayer, activation='relu')(encoded_sent2)
    

    merged= Concatenate()([encoded_sent1, encoded_sent2])
    merged =Dropout(dropout)(merged)
    preds=Dense(2, activation=act)(merged)
    model_merged = Model([input_1,input_2,features_1,features_2], preds)
    model_merged.compile(loss='categorical_crossentropy', optimizer=optim, metrics=['accuracy']) 
    model_merged.summary()
    return model_merged


def create_model_shared(train_data,features_count,add_features,dropout,dim,optim,denselayer,seq_length,act,filterratio,kernel_size,poollength):
    print("executing shared cnn")
    # network definition
    MAX_SEQUENCE_LENGTH=seq_length
    input_1 = Input(shape=(MAX_SEQUENCE_LENGTH,),name='sent1_input')
    input_2 = Input(shape=(MAX_SEQUENCE_LENGTH,),name='sent2_input')
    
    features_1 = Input(shape=(features_count,),name='features1_input')
    features_2 = Input(shape=(features_count,),name='features2_input')
    # network definition
    base_network = create_base_network(MAX_SEQUENCE_LENGTH,dropout,dim,filterratio,kernel_size,poollength,denselayer)
    encoded_sent1=base_network(input_1)
    encoded_sent2=base_network(input_2)
    
    subtracted = Subtract()([encoded_sent1, encoded_sent2])
    product= Multiply()([encoded_sent1, encoded_sent2])
    merged=concatenate([subtracted,product])
    merged =Dropout(dropout)(merged)
    
    preds=Dense(2, activation= act)(merged)
    model_merged = Model([input_1,input_2,features_1,features_2], preds)
    model_merged.compile(loss='categorical_crossentropy', optimizer=optim, metrics=['accuracy']) 
    model_merged.summary()
    return model_merged


def create_model_bilstm_baseline(train_data,features_count,dropout,dim,optim,denselayer,seq_length,act):
    print("executing lstm")

    MAX_SEQUENCE_LENGTH=seq_length
    input_1 = Input(shape=(MAX_SEQUENCE_LENGTH,),name='sent1_input')
    input_2 = Input(shape=(MAX_SEQUENCE_LENGTH,),name='sent2_input')
    
    features_1 = Input(shape=(features_count,),name='features1_input')
    features_2 = Input(shape=(features_count,),name='features2_input')
    
    encoded_sent1 = create_embedding_matrix(word_index,seq_length) (input_1)
    encoded_sent1 = Dropout(dropout)(encoded_sent1)
    encoded_sent1=Bidirectional(LSTM(denselayer))(encoded_sent1)

    encoded_sent2 =  create_embedding_matrix(word_index,seq_length)(input_2)
    encoded_sent2 = Dropout(dropout)(encoded_sent2)
    encoded_sent2=Bidirectional(LSTM(denselayer))(encoded_sent2)
    
    merged=Concatenate()([ encoded_sent1 , encoded_sent2])
    merged =Dropout(dropout)(merged)
    preds=Dense(2, activation=act)(merged)
    model_merged = Model([input_1,input_2,features_1,features_2], preds)
    model_merged.compile(loss='categorical_crossentropy', optimizer=optim, metrics=['accuracy']) 
    return model_merged
    
    

if __name__ == '__main__':
    pass