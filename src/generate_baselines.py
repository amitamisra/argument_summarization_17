
from gensim.summarization import  summarize
from sumy.summarizers.text_rank import TextRankSummarizer as textrank
from sumy.summarizers.lex_rank import LexRankSummarizer as lex
from sumy.parsers.plaintext import PlaintextParser
from sumy.summarizers.kl import KLSummarizer as klsum
from sumy.summarizers.sum_basic import SumBasicSummarizer as sbasic
from sumy.nlp.tokenizers import Tokenizer
from sumy.nlp.stemmers import Stemmer
from sumy.utils import get_stop_words
from run_pos_tagging import *
import subprocess
#from evaluation import *
from util import *
import shutil 
import tempfile


def run_textrank_text(text,num_sent):
    tmp = tempfile.mkdtemp()
    text_dir= os.path.join(tmp, "textrank_summ/")
    text_input=os.path.join(text_dir,"input")
    LANGUAGE = "english"
    if not os.path.exists(text_input):
            os.makedirs(text_input)
    filename = os.path.join( text_input, "textrank.txt")
    with open(filename,"w",encoding="utf-8") as f:
        f.write(text)
    parser =PlaintextParser.from_file(filename, Tokenizer(LANGUAGE))
    stemmer = Stemmer(LANGUAGE)
    summarizer = textrank(stemmer)
    summarizer.stop_words = get_stop_words(LANGUAGE)
    #print(num_sent)
    output = summarizer(parser.document ,num_sent)
    shutil.rmtree(tmp)
    return output    


def run_sumbasic_text(text,num_sent):  
    tmp = tempfile.mkdtemp()
    text_dir= os.path.join(tmp, "sumbasic/") 
    text_input=os.path.join(text_dir,"input")
    LANGUAGE = "english"
    if not os.path.exists(text_input):
            os.makedirs(text_input)
    filename = os.path.join( text_input,"sumbasic.txt")
    with open(filename,"w",encoding="utf-8") as f:
        f.write(text)
    parser =PlaintextParser.from_file(filename, Tokenizer(LANGUAGE))
    stemmer = Stemmer(LANGUAGE)
    summarizer=sbasic(stemmer)
    summarizer.stop_words = get_stop_words(LANGUAGE)
    #print(num_sent)
    output = summarizer(parser.document,num_sent)
    shutil.rmtree(tmp)
    return output    
     
def run_kl_text(text,num_sent):
    tmp = tempfile.mkdtemp()
    text_dir= os.path.join(tmp, "kl/") 
    text_input=os.path.join(text_dir,"input")
    LANGUAGE = "english"
    if not os.path.exists(text_input):
            os.makedirs(text_input)
    filename = os.path.join( text_input,"kl.txt")
    with open(filename,"w",encoding="utf-8") as f:
        f.write(text)
    parser =PlaintextParser.from_file(filename, Tokenizer(LANGUAGE))
    stemmer = Stemmer(LANGUAGE)
    summarizer=klsum(stemmer)
    summarizer.stop_words = get_stop_words(LANGUAGE)
    #print(num_sent)
    output = summarizer(parser.document,num_sent)
    shutil.rmtree(tmp)
    return output    
     
    
def run_text_rank(data_path):

    prev_dir = os.getcwd()
    text_rank_summaries =[]
    os.chdir(data_path)

    tex_dir =  os.getcwd() + "/../text_summ/"
    delete_data_from_dir(tex_dir)

    for filename in os.listdir(data_path) :
        if ".txt" in filename:
            f = open(filename, 'r')
            summ = tex_dir +"text" + filename
            text = f.read().replace('\n', '')
            f2 = open(summ, 'w')
            summary = summarize(text)
            f2.write(summary)
            text_rank_summaries.append(summary)
            #print summarize(text)
            f2.close()
            f.close()

    os.chdir(prev_dir)
    return text_rank_summaries

def run_lex_rank(data_path):
    prev_dir = os.getcwd()
    LANGUAGE = "english"
    lex_rank_summaries = []
    os.chdir(data_path)

    lex_dir = os.getcwd() + "/../lex_summ/"
    delete_data_from_dir(lex_dir)

    for filename in os.listdir(data_path) :
        if ".txt" in filename:
            parser =PlaintextParser.from_file(filename, Tokenizer(LANGUAGE))
            summ =  lex_dir + "lex" + filename
            stemmer = Stemmer(LANGUAGE)
            summarizer = lex(stemmer)
            summarizer.stop_words = get_stop_words(LANGUAGE)
            output = summarizer(parser.document , 5)
            lex_text = []
            f2 = open(summ, 'w')
            for sentence in output:
                #print sentence
                sent = str(sentence) + "\n"
                f2.write(sent)
                lex_text.append(sent)
            f2.close()
            lex_rank_summaries.append(''.join(lex_text))
    os.chdir(prev_dir)
    return lex_rank_summaries

def run_lex_rank_text(text,num_sent):
    #text = text.replace('\n', ' ')
    tmp = tempfile.mkdtemp()
    lex_dir= os.path.join(tmp, "lex_summ/")
    lex_input=os.path.join(lex_dir,"input")
    LANGUAGE = "english"
    if not os.path.exists(lex_input):
            os.makedirs(lex_input)
    filename = os.path.join( lex_input, "lex.txt")
    with open(filename,"w",encoding="utf-8") as f:
        f.write(text)
    parser =PlaintextParser.from_file(filename, Tokenizer(LANGUAGE))
    stemmer = Stemmer(LANGUAGE)
    summarizer = lex(stemmer)
    summarizer.stop_words = get_stop_words(LANGUAGE)
    #print(num_sent)
    output = summarizer(parser.document ,num_sent)
    shutil.rmtree(tmp)
    return output    
    

def run_lex_rank_wordcount(text,wc):
    #text = text.replace('\n', ' ')
    tmp = tempfile.mkdtemp()
    lex_dir= os.path.join(tmp, "lex_summ/")
    lex_input=os.path.join(lex_dir,"input")
    LANGUAGE = "english"
    if not os.path.exists(lex_input):
            os.makedirs(lex_input)
    filename = os.path.join( lex_input, "lex.txt")
    with open(filename,"w",encoding="utf-8") as f:
        f.write(text)
    parser =PlaintextParser.from_file(filename, Tokenizer(LANGUAGE))
    stemmer = Stemmer(LANGUAGE)
    summarizer = lex(stemmer)
    summarizer.stop_words = get_stop_words(LANGUAGE)
    #print(num_sent)
    output = summarizer(parser.document ,wc)
    shutil.rmtree(tmp)
    sents=[]
    for sentence in output:
                #print sentence
                sents.append(str(sentence))
    return sents       

def run_opinosis(data_path, opinosis_path):
    prev_dir = os.getcwd()
    os.chdir(data_path)
    input_opinosis = opinosis_path + "/work/input"
    work_opinosis = opinosis_path + "/work"
    run_pos(data_path, input_opinosis )
    os.chdir(opinosis_path)
    #java -jar opinosis.jar -b opinosis_sample/
    subprocess.call(['java', '-jar', 'opinosis.jar', '-b', work_opinosis ])
    output_dir = opinosis_path + "/work/output/run1"
    os.chdir(output_dir)
    opinosis_summaries = data_from_dir(output_dir, 'pos_1')
    os.chdir(prev_dir)
    return  opinosis_summaries




def example():
    data_dir = os.getcwd() + "/../data_dialogue"
    rouge_dir = os.getcwd() + '/../rouge'
    rouge_data_dir =os.getcwd() + '/../rouge'
    opinosis_dir =os.getcwd() + '/../opinosis'
    gold_dir = os.getcwd() + "/../gold_summ"

    #uncomment when we have gold summary
    #gold_summary = data_from_dir(gold_dir)

    lex_summary =  run_lex_rank(data_dir)
    tex_summary = run_text_rank(data_dir)
    print(lex_summary)
    #opinosis_summary = run_opinosis(data_dir,opinosis_dir)
    #gold_summary = lex_summary

    #get_rouge_score(gold_summary, tex_summary,rouge_dir, rouge_data_dir)
    #get_bleu_score(gold_summary, tex_summary)

    #get_rouge_score(gold_summary, lex_summary, rouge_dir, rouge_data_dir)
    #get_bleu_score(gold_summary, lex_summary)

    #get_rouge_score(gold_summary, opinosis_summary, rouge_dir, rouge_data_dir)
    #get_bleu_score(gold_summary, opinosis_summary)


if __name__ == '__main__':
    example()



