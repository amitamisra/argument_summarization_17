'''
Created on Mar 12, 2017

@author: amita
'''

import boto3
import configparser
import os
import urllib
from bs4 import BeautifulSoup
from boto.mturk.connection import MTurkConnection 
import urllib.request
import csv
import pandas
import ast
import requests
from requests import session
from selenium.webdriver.support import ui
from selenium.webdriver.common.keys import Keys
from selenium import webdriver

def page_is_loaded(driver):
    return driver.find_element_by_tag_name("body") != None
def get_all_HITS_qual(worker_qual,aws_iniconfig):
    allresponses=[]
    config = configparser.ConfigParser()
    config.read(aws_iniconfig)
    ACCESS_KEY = config.get("aws", 'ACCESS_KEY')
    SECRET_KEY = config.get("aws", 'SECRET_KEY')
    client = boto3.client('mturk',aws_access_key_id=ACCESS_KEY,aws_secret_access_key=SECRET_KEY)
    hits = client.list_hi_ts_for_qualification_type(QualificationTypeId=worker_qual)
    HITS=hits["HITs"]
    print (len(hits["HITs"]))
    hitids=[hit["HITId"] for hit in HITS]
    for hit_id in hitids:
        response = client.get_hit(HITId=hit_id)
        print(response)
        batchId=response["HIT"]["RequesterAnnotation"].split(";")[0].split(":")[1]
        web_page ="https://requester.mturk.com/batches/"+batchId+"/download"

        driver = webdriver.Firefox(executable_path="/Users/amita/software/geckodriver")
        driver.get("https://www.amazon.com/ap/signin?_encoding=UTF8&clientContext=2aa82e5b425d900334cd96550ceb0e&marketplaceId=A2LNHB43R2GSAT&openid.assoc_handle=amzn_mturk_requester&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.mode=checkid_setup&openid.ns.pape=http%3A%2F%2Fspecs.openid.net%2Fextensions%2Fpape%2F1.0&openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&openid.pape.max_auth_age=43200&openid.return_to=https%3A%2F%2Frequester.mturk.com%2Fmturk%2Fendsignin")
        wait = ui.WebDriverWait(driver, 10)
        wait.until(page_is_loaded)
        email_field = driver.find_element_by_id("ap_email")
        email_field.send_keys("subjectivelanguage@gmail.com")
        password_field = driver.find_element_by_id("ap_password")
        password_field.send_keys("am.n1DS.Uc5o392")
        driver.find_element_by_id("signInSubmit-input").click()
        #res=driver.get()
        print()
        allresponses.append(response)
    return( allresponses)
    
def getHITS_qualID(worker_qual,outputbase_dir,aws_iniconfig):
    for quali_id, dirname in worker_qual.items():
        responses=get_all_HITS_qual(quali_id,aws_iniconfig)
        outputfile=os.path.join(outputbase_dir,dirname)
        print()
        

if __name__ == '__main__':
    
    topic="gun-control"
    worker_qual={}
    worker_qual["3B69ZPW0W7OBQZXLGQ7REW8N3PIFVR"]= "AJP"
    worker_qual["38TZ8V8N0C5IV62FVM4DB1WXXT9IP0"]="A2LC"
    aws_iniconfig="/Users/amita/git/argument_summarization_17/src/aws.ini"
    outputbase_dir="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gun-control/Mturk/MTResults/"
    getHITS_qualID(worker_qual,outputbase_dir,aws_iniconfig)
