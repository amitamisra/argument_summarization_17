'''
Created on Mar 28, 2017

@author: amita
'''
import pandas as pd,os
from db import db_utilities
from db.stance_retrieval import add_stance_data_to_csv
from filter_previous_training_data import Filter

def grouppostsbyauthor():
    pass


def median_authorpost_discussion(grouped_discussion):
    discussionsbyauthor={}
    for discussion, group_disc in grouped_discussion:
        discussiondf=[]
        author_gp=group_disc.groupby("author_id")
        for author,author_df in author_gp:
            discussiondf.append(author_df)
        discussionsbyauthor[discussion]=discussiondf
    return discussionsbyauthor     

 
def groupbyauthor(df,authorcol,postcol):
    authordf=[]
    groupbyauthor_post=df.groupby(authorcol,postcol)
    grouped_discussion=df.groupby(authorcol) 
    for author, group in grouped_discussion:
        authordf[author]=group
    return authordf 
        
def groupbydiscussion(df,discussioncol):
    grouped_discussion=df.groupby(discussioncol) 
    return grouped_discussion

def addauthor(row,sql_session,datasetcol,discussioncol,postcol):
    dataset=int(row[datasetcol])
    discussion=int(row[discussioncol])
    post=int(row[postcol])
    author=db_utilities._get_author_post(sql_session, dataset, discussion, post)
    return author
    
def addauthordf(df,datasetcol,discussioncol,postcol):
    _, sql_session = db_utilities.setup_connection()
    df["author_id"]=df.apply(addauthor,args=(sql_session,datasetcol,discussioncol,postcol),axis=1)
    #stance_col_tuples = df.apply(add_stance_data_to_csv.get_stance,args=(sql_session,datasetcol,discussioncol,postcol), axis=1)
    #df["self_annotated_stance"] = pd.Series(map(lambda x: x[0], stance_col_tuples))
    #df["topic_stance"] = pd.Series(map(lambda x: x[1], stance_col_tuples))
    return df


def applyfilter( df_filter_apply,df_filter_input):
    post=1
    text=0
    quality=1
    similarity=0
    filterobj=Filter(post,text,quality,similarity)
    filtered_df=filterobj.removepostsquality(df_filter_input, df_filter_apply)
    return filtered_df
    


def run(input_csv,filter_input,datasetcol,discussioncol,postcol,outputfile,outputdir):
    df=pd.read_csv(input_csv)
    print(df.head())
    
    #df = df.reset_index()
    df_filter_input=pd.read_csv(filter_input)
    print(df_filter_input.head())
    #df_filter_input = df_filter_input.reset_index()
    df=applyfilter(df,df_filter_input)
    df_withauthor=addauthordf(df,datasetcol,discussioncol,postcol)
    df_withauthor.to_csv(outputfile) 
    
      

if __name__ == '__main__':
    
    datasetcol="Dataset_Id"
    discussioncol="Discussion_Id"
    postcol="Post_Id"
    
    filter_input="/Users/amita/git/facet_similarity_part3/facet_quality_similarity_cluster/resources/experiment_data/aq_SigDial2015/facet_quality/gm-slider-means.testing.pmi.csv"
    input_csv="/Users/amita/git/facet_similarity_part3/facet_quality_similarity_cluster/arg_quality/data/gaymarriage/db_exc2015train_pos/results/gm_db_exc2015train_post_Gw2vec_Doc2vecsvm_with_pred_mod.csv"
    outputdir="/Users/amita/git/facet_similarity_part3/facet_quality_similarity_cluster/arg_quality/data/gaymarriage/exc_train_test2015_pos"
    outputfile=os.path.join(outputdir,"gm_db_exc2015train_test_post_Gw2vec_Doc2vecsvm_pred_mod_author.csv")
    
    run(input_csv,filter_input,datasetcol,discussioncol,postcol,outputfile,outputdir)
        
    