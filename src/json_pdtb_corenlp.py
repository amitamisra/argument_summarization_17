'''
Created on Feb 22, 2017
corelp_author_dict": {
 first dialog       "0": {
            "corenlp_info": [
                {
                    "Author_turn": [],
                    "char_offset": [],
                    "deps_cc": [],
first sentence                    "lemmas": [],
                    "parse": "(ROOT (S (ADVP (RB So)) (NP (PRP you)) (VP (VBP acknowledge) (SBAR (IN that) (S (NP (PRP we)) (VP (VBP do) (RB n't) (VP (VB have) (NP (NP (DT any) (NN idea)) (SBAR (WHNP (WHNP (WDT what) (NN percentage)) (PP (IN of) (NP (DT the) (CD 29,000)))) (S (VP (VBP are) (PP (IN of) (NP (JJ non-US) (NN manufacture))) (PP (IN beyond) ('' '') (NP (DT some)))))))))))) ('' '') (. ?)))",
                    "pos":[],
                    "sentence_no": 0,
                    "tokens": []
                },
                {
                    "Author_turn": [],
                    "char_offset": [],
                    "deps_cc": [],
                    "lemmas": [],
second sentence                    "parse": "(ROOT (S (ADVP (RB So)) (NP (PRP you)) (VP (VBP acknowledge) (SBAR (IN that) (S (NP (PRP we)) (VP (VBP do) (RB n't) (VP (VB have) (NP (NP (DT any) (NN idea)) (SBAR (WHNP (WHNP (WDT what) (NN percentage)) (PP (IN of) (NP (DT the) (CD 29,000)))) (S (VP (VBP are) (PP (IN of) (NP (JJ non-US) (NN manufacture))) (PP (IN beyond) ('' '') (NP (DT some)))))))))))) ('' '') (. ?)))",
                    "pos":[],
                    "sentence_no": 1,
                    "tokens": []
                },
@author: amita
'''
import stanford_parser_arg
import file_utilities
import os,sys
import pandas as pd
import nltk
import re
import logging
import pdtb_parser
import copy
logging.basicConfig(level=logging.CRITICAL)
logger = logging.getLogger(__name__)
logger.setLevel(logging.CRITICAL)

def getallinfo_corenlp(text):
    """
    call rst_processor, parse text, returns corenlp and rst as java object
    """
    res=stanford_parser_arg.parse_text(text)
    return res


def add_corenlp_dict(sentence_dict,sentence,pdtb_input_list):
    sentence_dict["pos"]=list(sentence["pos"])
    sentence_dict["lemmas"]=list(sentence["lemmas"])
    sentence_dict["parse"]=str(sentence["parse"][0])
    numofdep=len(list(sentence["deps_cc"]))
    sentence_dict["deps_cc"]=list(list(dep) for dep in sentence["deps_cc"])
    sentence_dict["tokens"]=list(sentence["tokens"])
    sentence_dict["char_offset"]=list(list(charoffset) for charoffset in sentence["char_offset"] )
    pdtb_input=" " .join(list(sentence["tokens"][:-1]) )+ sentence["tokens"][-1] +"\n"
    pdtb_input_list.append(pdtb_input)



def add_corenlp_dict_token(sentence_dict,sentence,pdtb_input_list):
    sentence_dict["tokens"]=list(sentence["tokens"])
    pdtb_input=" " .join(list(sentence["tokens"][:-1]) )+ sentence["tokens"][-1] +"\n"
    pdtb_input_list.append(pdtb_input)    

def parse_response(res,key,inputdir_pdtb):
    """
    res " response object returned by stanford_parser_arg, uses py4j and calls the corenlp in Java
    """
    pdtb_input_list=[]
    allsentences_corenlpinfo=[]
    sentence_count=0
    for sentence in res:
        sentence_dict={}
        sentence_dict["sentence_no"]=sentence_count
        sentence_count=sentence_count+1
        if sentence["tokens"][0].startswith("Author"):
            if sentence["tokens"][2]== "-" :
                text=" ".join(sentence["tokens"][3:])
                new_response=getallinfo_corenlp(text)
                new_sentence=new_response[0]
                sentence_dict["Author_turn"]=list(sentence["tokens"][:3])
                add_corenlp_dict_token(sentence_dict,new_sentence,pdtb_input_list)
            else:
                print("error in splitting Autor text")
                sys.exit()  
        else:
            sentence_dict["Author_turn"]=[]    
            add_corenlp_dict_token(sentence_dict,sentence,pdtb_input_list)
        
        #sentence_dict["chunks"]=sentence["chunks"]
        allsentences_corenlpinfo.append(sentence_dict)
    
    doc_dict={}
    doc_dict["corenlp_info"]=  allsentences_corenlpinfo
    pdtb_file=os.path.join(inputdir_pdtb,key+".txt")
    with open(pdtb_file,"w",encoding="utf-8") as f:
        f.writelines(pdtb_input_list)
    return doc_dict

def applyparse(row,column,inputdir_pdtb):
    """
    apply the getallinforcorenlp to column row_wise
    """
    text=row[column]
    if "key" in row:
        key=row["key"]
    else:
        key=row["Key"]    
    nlp_info=getallinfo_corenlp(text)
    logger.info( "Parsing file {0}".format(key))
    parsed_info=parse_response(nlp_info,key,inputdir_pdtb)
    return parsed_info

def parse_pdtb_pipe(line):
    pipes=line.split("|")
    Num_cols=len(pipes)
    pdtb_dict={}
    if Num_cols != 48:
        print("error on parsing arguments")
        sys.exit()
    else:
        arg_dict={}
        i=0
        for pipe_text in pipes:
            arg_dict["col_"+ str(i)]=pipe_text
            i=i+1
    #pdtb_dict["pdtb"]=arg_dict
    return arg_dict

def extract_pdtb_rel_allsent(allpdtb_rels):
    """
    These numbers are fixed and based on https://github.com/WING-NUS/pdtb-parser
    """
    sentence_pdtbrel={}
    for arg_dict in allpdtb_rels:
        arg1_line=arg_dict["col_"+str(23)] 
        arg2_line=arg_dict["col_"+str(33)]
        rel_type= arg_dict["col_"+str(11)]
        sentence_pdtbrel[arg1_line]=1
        sentence_pdtbrel[arg2_line]=1
        if arg1_line+"_relation" in sentence_pdtbrel.keys():
            sentence_pdtbrel[arg1_line+"_relation"].append(rel_type)
        else:
            value=[]
            value.append(rel_type)
            sentence_pdtbrel[arg1_line+"_relation"]=value
        if arg2_line+"_relation" in sentence_pdtbrel.keys():
                sentence_pdtbrel[arg2_line+"_relation"].append(rel_type)
        else:
            value=[]
            value.append(rel_type)
            sentence_pdtbrel[arg2_line+"_relation"]=value
                
    return  sentence_pdtbrel    
           
         
def pdtbresult_tosent_dict(output_pdtb,colname_pdtb,corenlp_filename):
    df_corenlp=pd.read_json(corenlp_filename+".json")  
    corenlp_pdtb=os.path.join(os.path.dirname(corenlp_filename),os.path.basename(corenlp_filename+"_corenlp_pdtb")) 
    pdtb_filnames=os.listdir(output_pdtb)
    parser_dict_key={}
    for filename in pdtb_filnames:
        allrel=[]
        if str(filename).endswith(".pipe"):
            filewithpath=os.path.join(output_pdtb,filename)
            with open(filewithpath, "r", encoding="utf-8") as f :
                sentences=f.readlines()
                
            num_sentences=len(sentences)  
            key=filename.split(".txt.pipe")[0]
            for line in sentences:
                pdtb_rel=parse_pdtb_pipe(line)
                allrel.append(pdtb_rel)
                
            parser_dict_key[key]=extract_pdtb_rel_allsent(allrel)  
            
    df_corenlp["pdtb"]=df_corenlp.apply(merge_df_cornlp_pdtb,args=(parser_dict_key,),axis=1) 
    df_corenlp.to_csv(corenlp_pdtb+".csv")
    df_corenlp.to_json(corenlp_pdtb+".json")
    
    return  corenlp_pdtb 
      
def merge_df_cornlp_pdtb(row,parser_dict_key):
    if "key" in row:
        key=row["key"]
    else:
        key=row["Key"]  
    return parser_dict_key[key]
    

def run(topic,inputfile,column,pdtb_jar_location,output_dir):    
    input_base=os.path.basename(inputfile)
    output_corenlp=os.path.join(output_dir,input_base+"_corenlp")
    input_pdtb=os.path.join(output_dir,"pdtb_input")
    output_pdtb=os.path.join(output_dir,"pdtb_output")
    if not os.path.exists(input_pdtb):
        os.mkdir(input_pdtb)
    inputpickle=inputfile +".pkl"
    df=pd.read_pickle(inputpickle)
    #df.apply(checksentencelength, args=(column,),axis=1)
    df["corelp_author_dict"]=df.apply(applyparse,args=(column,input_pdtb),axis=1)
    pdtb_parser.run(input_pdtb,output_pdtb,pdtb_jar_location)
    df.to_csv(output_corenlp+".csv",index=False) 
    df.to_json(output_corenlp + ".json")
    
    return input_pdtb, output_pdtb,"corelp_author_dict", output_corenlp
    
if __name__ == '__main__':
    topic="gun-control"
    inputfile=""
    column="dialog_no_format_author"
    pdtb_jar_location="/Users/amita/software/pdtb-parser/"
    input_pdtb, output_pdtb, colname_pdtb,corenlp_filename=run(topic,inputfile,column,pdtb_jar_location)
    pdtbresult_tosent_dict(output_pdtb,colname_pdtb,corenlp_filename)

 #done for gun control