'''
Created on April 16, 2017

@author: sharath
'''
import pandas as pd
import sklearn.metrics
import math
def kappa(inputcsv, worker_tup):

    df=pd.read_csv(inputcsv)
    df[worker_tup[0]] = pd.to_numeric(df[worker_tup[0]], errors='coerce')
    df[worker_tup[1]] = pd.to_numeric(df[worker_tup[1]], errors='coerce')
    df = df.dropna(subset=[worker_tup[0], worker_tup[1]])
    workercol1_values=df[worker_tup[0]].tolist()
    workercol2_values=df[worker_tup[1]].tolist()
    return [sklearn.metrics.cohen_kappa_score(workercol1_values, workercol2_values), len(workercol1_values)]
                
if __name__ == '__main__':
    prefix="/home/sharath/Downloads/response_splitNSummaryCSV_authorformat_all_pyramids_scus_author_corenlp_pdtb_mt"
    suffix = "_results.csv"
        
    workeridcolumntuples=[("workerA3LZFA8OTAN21Dweight","workerAK5KE6MAYPM4Vweight"),
                        ("workerA2SDALQ5QDCM0weight", "workerA3AABYBB13R75Hweight"),
                        ("workerA2SDALQ5QDCM0weight", "workerA3AABYBB13R75Hweight"),
                        ("workerA3LZFA8OTAN21Dweight", "workerAK5KE6MAYPM4Vweight"),
                        ("workerA2SDALQ5QDCM0weight", "workerA3AABYBB13R75Hweight")]
    kappa_scores = {}

    for i in range(len(workeridcolumntuples)):
        
        score = (kappa(prefix + str(i+1) + suffix, workeridcolumntuples[i]))
        if workeridcolumntuples[i] in kappa_scores.keys():
            kappa_scores[workeridcolumntuples[i][0] + " and " +  workeridcolumntuples[i][1]] = (((kappa_scores[workeridcolumntuples[i][0] + " and " +  workeridcolumntuples[i][1]][0] * kappa_scores[workeridcolumntuples[i][0] + " and " +  workeridcolumntuples[i][1]][1]) + (score[0] * score[1])) / (kappa_scores[workeridcolumntuples[i][0] + " and " +  workeridcolumntuples[i][1]][1] + score[1]), kappa_scores[workeridcolumntuples[i][0] + " and " +  workeridcolumntuples[i][1]][1] + score[1])
            
        else: 
            kappa_scores[workeridcolumntuples[i][0] + " and " +  workeridcolumntuples[i][1]] = (score[0], score[1])
    print(kappa_scores)
