'''
Created on Apr 13, 2017

@author: amita
'''
import nltk
import string
punct = set(string.punctuation)
import generate_baselines
from sumy.utils import to_string
 
class LexRankExtractor():
    
    def preprocesssent(self,sent):
        """
        preprocess the sentences returned by lexrank to store in the set
        """
        words = [word for word in nltk.word_tokenize(sent) if  word not in punct]
        sent = " ".join(words)
        return sent
    
    def getlexranksummary_set(self,sentences,num_sent):
        text=" ".join(sentences)
        lexrank_sent_set=set()
        summary = generate_baselines.run_lex_rank_text(text,num_sent)
        for sent in summary:
            sent=to_string(sent)
            #print(type(sent))
            #sent=self.preprocesssent(sent)
            #print(sent)
            lexrank_sent_set.add(sent)
        return lexrank_sent_set

if __name__ == '__main__':
    pass