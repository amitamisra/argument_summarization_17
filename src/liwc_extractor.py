'''
Created on Apr 11, 2017

@author: amita
'''
import file_utilities
import word_category_counter
class LIWCextractor():
    def __init__(self):
        # Setup the dictionary
        word_category_counter.load_dictionary(file_utilities.get_absolutepath_data("liwc","LIWC2015_English_Flat.dic"))
     
    def score_text(self, text):
        return  word_category_counter.score_text(text)   
        

if __name__ == '__main__':
    pass