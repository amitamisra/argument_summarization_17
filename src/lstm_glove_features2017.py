'''
Created on Nov 15, 2016

@author: amita
'''
import os
import numpy as np
import random
seed=100
np.random.seed(seed)
random.seed(seed)
import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
from pprint import pprint
from keras import backend
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils.np_utils import to_categorical
from keras.layers import Dense, Input, Flatten
from keras.layers import Conv1D, MaxPooling1D, Embedding, Bidirectional
from keras.models import Model
from keras.models import Sequential
from keras.layers import LSTM
from keras.layers.embeddings import Embedding
from keras.preprocessing import sequence
from keras.layers.convolutional import Convolution1D
from keras.layers import Merge
import sys
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import f1_score
from keras.layers import Dropout
from sklearn.model_selection import GridSearchCV
from keras.wrappers.scikit_learn import KerasClassifier
import codecs
from pprint import pprint
from keras.callbacks import ModelCheckpoint
from keras.callbacks import History
from keras.optimizers import Adam
from sklearn.metrics import confusion_matrix
import keras.backend as K
import time
from src import file_utilities
import configparser
import os
from sklearn.metrics import classification_report
history = History()
GLOVE_DIR = "/Users/amita/software/" + '/glove.6B/'
MAX_SEQUENCE_LENGTH = 50
MAX_NB_WORDS = 20000
EMBEDDING_DIM = 100
VALIDATION_SPLIT = 0.2
TRUNC='post'
PADDING="post"
from keras.callbacks import ModelCheckpoint
word_index={}
embeddings_index = {}
train_length = 0
readabilty_features_count = 0
f = open(os.path.join(GLOVE_DIR, 'glove.6B.100d.txt'), "r", encoding='utf-8')
#f = open('glove.6B.100d.txt', 'r')
for line in f:
    values = line.split()
    word = values[0]
    coefs = np.asarray(values[1:], dtype='float32')
    embeddings_index[word] = coefs
f.close()

print('Found %s word vectors.' % len(embeddings_index))

# second, prepare text samples and their labels
print('Processing text dataset')

def vectorize_text (input_train,input_test,textcolumn,feature_list,labelcol):
    """ vectorize the text samples into a 2D integer tensor
    """
    global word_index, train_length
    df_train=pd.read_csv(input_train)
    train_texts=df_train[textcolumn].values
    labels_train=df_train[labelcol].values
    colnames=list(df_train.columns.values)
    features=[col for col in colnames if str(col).startswith(tuple(feature_list))]
    
    readabilty_features_train = df_train[features]
#    print readabilty_features_train
    readabilty_features_train = np.reshape(np.ravel(np.ravel(readabilty_features_train)), (len(readabilty_features_train), len(features), 1))
    tokenizer = Tokenizer(nb_words=MAX_NB_WORDS)
    tokenizer.fit_on_texts(train_texts)
    sequences_train = tokenizer.texts_to_sequences(train_texts)
    word_index = tokenizer.word_index
    print('Found %s unique tokens.' % len(word_index))

    # encode class values as integers
    encoder = LabelEncoder()
    encoder.fit(labels_train)
    encoded_labels_train = encoder.transform(labels_train)
    # convert integers to dummy variables (i.e. one hot encoded)
    labels_train= to_categorical(encoded_labels_train)
    data_train = pad_sequences(sequences_train, maxlen=MAX_SEQUENCE_LENGTH,truncating=TRUNC,padding=PADDING)
    
    df_test=pd.read_csv(input_test)
    test_texts=df_test[textcolumn].values
    labels_test=df_test[labelcol].values

    readabilty_features_test = df_test[features]
    readabilty_features_test = np.reshape(np.ravel(np.ravel(readabilty_features_test)), (len(readabilty_features_test), len(features), 1))

    sequences_test = tokenizer.texts_to_sequences(test_texts)
    data_test= pad_sequences(sequences_test, maxlen=MAX_SEQUENCE_LENGTH,truncating=TRUNC,padding=PADDING)
    labels_test = to_categorical(encoder.transform(labels_test))
    train_length = data_train.shape[0]
    print('Shape of data tensor:', data_train.shape)
    print('Shape of label tensor:', labels_train.shape)
    return(data_train,labels_train,data_test,labels_test,readabilty_features_train,readabilty_features_test,features)


def create_embedding_matrix(word_index):    
    nb_words = min(MAX_NB_WORDS, len(word_index))
    embedding_matrix = np.zeros((nb_words + 1, EMBEDDING_DIM))
    for word, i in word_index.items():
        if i > MAX_NB_WORDS:
            continue
        embedding_vector = embeddings_index.get(word)
        if embedding_vector is not None:
            # words not found in embedding index will be all-zeros.
            embedding_matrix[i] = embedding_vector

    # load pre-trained word embeddings into an Embedding layer
    # note that we set trainable = False so as to keep the embeddings fixed
    embedding_layer = Embedding(nb_words + 1,
                                EMBEDDING_DIM,
                                weights=[embedding_matrix],
                                input_length=MAX_SEQUENCE_LENGTH,
                                trainable=True)
    return embedding_layer 

def split_train_val(data,labels):
    indices = np.arange(data.shape[0])
    np.random.shuffle(indices)
    data = data[indices]
    labels = labels[indices]
    nb_validation_samples = int(VALIDATION_SPLIT * data.shape[0])
    x_train = data[:-nb_validation_samples]
    y_train = labels[:-nb_validation_samples]
    x_val = data[-nb_validation_samples:]
    y_val = labels[-nb_validation_samples:]
    return(x_train,y_train,x_val,y_val)
    

def create_model(dropout,dim,optimizer='rmsprop'):

    global train_length, readabilty_features_count
    model = Sequential()
    embedding_layer= create_embedding_matrix(word_index)
    model.add(embedding_layer)
    model.add(Dropout(dropout)) 

    model.add(Convolution1D(nb_filter=32, filter_length=3, border_mode='same', activation='relu'))
    model.add(MaxPooling1D(pool_length=2))
    model.add(Dropout(dropout)) 
    model.add(Bidirectional(LSTM(dim)))

    model_readabilty = Sequential()
    model_readabilty.add(Bidirectional(LSTM(readabilty_features_count), input_shape=(readabilty_features_count, 1)))
    
#   Merge

    merged = Merge([model, model_readabilty], mode='concat')
    model_merged = Sequential()
    model_merged.add(merged)
    model_merged.add(Dropout(dropout))
    model_merged.add(Dense(2, activation='sigmoid'))

    adam = Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
    model_merged.compile(loss='categorical_crossentropy', optimizer=adam, metrics=['accuracy']) 
    return model_merged
    
def create_result_dict(acurracy, f1weighted, epochs,batchsize,droput,dim,feature_list):
    result_dict={}
    result_dict["Acurracy"]=acurracy
    result_dict["f1weighted"]=f1weighted
    result_dict["epochs"]=epochs
    result_dict["batchsize"]=batchsize
    result_dict["droput"]=droput
    result_dict["dim"]=dim
    result_dict["feature_list"]=feature_list
    return result_dict


def writeresultstoFile(csvFile,result_dict,pred_list,Y_pred,Y_actual,input_test,features,f1weighted):
    df_test=pd.read_csv(input_test)
    assert(len(Y_pred)==len(Y_actual)==df_test.shape[0])
    type(Y_pred)
    df_test["Y_actual"]=Y_actual
    df_test["Y_predicted"]=Y_pred
    df_test.to_csv(csvFile,index=False)
    f = codecs.open(csvFile, "a",encoding='utf-8')
    result=str(result_dict) + str(features)+ str("\nf1weighted") +str(f1weighted)
    pprint(result, stream=f) 
#     print("classification report {0}".format(classification_report(Y_pred,Y_actual)))
#     print("confusion_matrix() {0}".format(confusion_matrix(Y_pred,Y_actual)))
#     print("fscore {0}".format(f1weighted))
    f.close()

def writeresultserver(f1weighted,outfile,features,dim,dropout,epochs,batchsize):
    outfile=outfile[:-4]+".txt"
    para="\ndrop\t"+str(dropout)+"\tdim\t"+str(dim)+"\tepoch\t"+str(epochs)+"\tbatch\t"+str(batchsize)
    result= str(para) + "\t"+ str(features)+ str("\t f1weighted") + str(f1weighted) +"\n"
    with open(outfile,"a") as f:
        f.write(result)

def runlstm(resultCSVFile,input_train,input_test,text_col,feature_list,label_col,dropoutlist,dimlist,outBaseDir,epochs,batchsize,all_list): 
    data_train,labels_train, data_test,labels_test,readabilty_features_train,readabilty_features_test,features=vectorize_text(input_train,input_test,text_col,feature_list,label_col) 
    x_train,y_train= data_train,labels_train
    x_test, y_test= data_test,labels_test
    filepath="weights.best.hdf5"
    checkpoint = ModelCheckpoint(filepath, monitor='val_acc', verbose=1, save_best_only=True, mode='max')
    callbacks_list = [history, checkpoint]
    global readabilty_features_count
    readabilty_features_count = len(features)
# Fit the model
    for droput in dropoutlist:
        for dim in dimlist:
            model=create_model(droput,dim)
            model.fit([x_train, readabilty_features_train], y_train, validation_split=0.2, epochs=epochs, batch_size=batchsize, callbacks=callbacks_list, verbose=0)
            scores = model.evaluate([x_test, readabilty_features_test], y_test, verbose=0)
            pred = model.predict([x_test, readabilty_features_test], batch_size=64)
            y_pred_values=pred.argmax(axis=1)
            y_test_values=y_test.argmax(axis=1)
            pred_list= [{'Y_actual': v1, 'Y_predicted': v2} for v1, v2 in zip( y_test_values,y_pred_values)]
            acurracy=scores[1]*100
            f1weighted=f1_score(y_test.argmax(axis=1), pred.argmax(axis=1), average='weighted')
            #confusion_mat=confusion_matrix(y_test.argmax(axis=1), pred.argmax(axis=1))
            result_dict=create_result_dict(acurracy, f1weighted, epochs,batchsize,droput,dim,feature_list)
            all_list.append(result_dict)
            if not os.path.exists(outBaseDir):
                os.mkdir(outBaseDir)
            filename="".join(feature_list)+"drop"+str(droput)+"dim"+str(dim)+"ep"+str(epochs)+"batch"+str(batchsize)+".csv"
            csvwithpath=os.path.join(outBaseDir,filename) 
            writeresultstoFile(csvwithpath,result_dict,pred_list,y_pred_values,y_test_values,input_test,features,f1weighted)
            writeresultserver(f1weighted,resultCSVFile,feature_list,dim,droput,epochs,batchsize)

def run(section):
    
    config = configparser.ConfigParser()
    lstminput=file_utilities.get_absolutepath_data("config","lstm_config.ini")
    config.read(lstminput)
    input_file_config=config.get(section,"csv_with_file_list")
    resultCSVFile_config=config.get(section,'resultCSVFile')
    input_file=file_utilities.get_absolutepath_data(input_file_config)
    resultCSVFile=file_utilities.get_absolutepath_data(resultCSVFile_config) 
    inputdata_df=pd.read_csv(input_file)
    inputdata_df.fillna(0)
    
    all_list=[]
    if os.path.exists(resultCSVFile):
            os.remove(resultCSVFile)
    for index, row in inputdata_df.iterrows():
        param_dict={}
        done=row["done"]
        if int(done)==1:
            continue
        input_train=file_utilities.get_absolutepath_data(row["input_train"])
        input_test=file_utilities.get_absolutepath_data(row["input_test"])
        outBaseDir=file_utilities.get_absolutepath_data(row["outputBaseDir"])
        text_col=row["textcol"]
        label_col=row["label_col"]
        dropoutlist=[0.1,0.2]
        dimlist=[200,300]
        epochs=5
        batch_size=64
        feature_list=list(row["feature_list"].split(","))
        
        if not os.path.exists(outBaseDir):
            os.mkdir(outBaseDir)
        outBaseDir= outBaseDir 
        runlstm(resultCSVFile,input_train,input_test,text_col,feature_list,label_col,dropoutlist,dimlist,outBaseDir,epochs,batch_size,all_list)  
 
        
    if all_list:
        df=pd.DataFrame(all_list)
        df.to_csv(resultCSVFile,index=False)         
        

if __name__ == '__main__':
    
    
    #section="GC"
    #run(section)
#     section="GC_Coref"
#     run(section)
    
    section="GM"
    run(section)
    section="GM_Coref"
    run(section)
#     
#     section="AB"
#     run(section)
#     section="AB_Coref"
#     run(section)
    
