'''
Created on Mar 16, 2017
merge mid and more 750 dialogs csv for visualization, rules for rst edu merging
@author: amita
'''
import pandas as pd
import np

def merge(input1,input2,output):
    df1=pd.read_csv(input1)
    df2=pd.read_csv(input2)
    dflist=[]
    dflist.append(df1)
    dflist.append(df2)
    df3=pd.concat(dflist)
    df3=df3.reindex(np.random.permutation(df3.index))
    df3 = df3.rename(columns={'Dialogtext': 'Dialog', })
    df3.to_csv(output,index=False)    
if __name__ == '__main__':
    input1="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/abortion/Dialog_File_midrange.csv"
    input2="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/abortion/Dialog_File_more750.csv"
    output="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/abortion/NaturalSummaryCSV.csv"
    merge(input2,input1,output)