'''
Created on Feb 27, 2017
merge pyramid labels (obtained by running scores_pyramid) and author_manipulation.py
@author: amita
'''
import pandas as pd,os
import file_utilities
import copy
from collections import OrderedDict
#------------------------------------------------------------------------------ 
#-------------------------------------------------------- class Pyramid_details:
    #------ def __init__(self,contributors,contrib,scuid,key_user,label,weight):
        #-------------------------------------------------- self.contrib=contrib
        #----------------------------------------------------- self.scuid =scuid
        #------------------------------------------------ self.key_user=key_user
        #------------------------------------------------- self.label=self.label
        #---------------------------------------------------- self.weight=weight
    #------------------------------------ def tupleid_contrib_label(self,list_):
        
            
        
def row_eachkey(dfpyramidlabels):
    """
    input:dataframe with all scus
    return dataframe, with one row per key/dialog
    """
    
    allrows=[]
    grouped_key=dfpyramidlabels.groupby("key_user")
    for key,group_key in grouped_key :
        rowdict={}
        count=1
        all_labels=group_key.T.to_dict().values()
        scu_row=OrderedDict()
        for row in all_labels:
            scu_row[count]=row
            count=count+1
        rowdict["scu_info"]=scu_row
        if "user" in key:
            rowdict["Key"]=key.split("user")[0][:-1]
        else:
             rowdict["Key"]=key.split("User")[0]  
        allrows.append(rowdict)     
        #contrib=group["contrib"]
    return pd.DataFrame(allrows) 
    
   
def run(topic,inputwithauthor): 
    authordialog=inputwithauthor+".pkl"
    df_author=pd.read_pickle(authordialog)
    basename=os.path.basename(inputwithauthor)
    pyramidlabels=file_utilities.get_absolutepath_data("data_originalsummaries",topic,"all_pyramids_scus","AllScus.pkl")
    dfpyramidlabels=pd.read_pickle(pyramidlabels)
    df_row_perkey=row_eachkey(dfpyramidlabels)
    df_row_perkey.to_csv(authordialog+"_scus_rowperkey.csv")
    keylist_rowper_key=df_row_perkey["Key"].tolist()
    keylist_author=df_author["Key"].tolist()
#     for keyrow in keylist_rowper_key:
#         if keyrow in keylist_author:
#            pass
#         else:
#             print("key not found{0}".format(keyrow))
#     
    author_scu=file_utilities.get_absolutepath_data("data_originalsummaries",topic, basename+"_all_pyramids_scus_author")
    mergeddf=pd.merge( df_author,df_row_perkey, on="Key",how="inner",suffixes=('_author_key', '_pyramid_key'))
    mergeddf.to_csv(author_scu+".csv",index=False)
    mergeddf.to_pickle(author_scu+".pkl")
    return author_scu
if __name__ == '__main__':
    
    topic="gun-control"
    input_withbaseline="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gun-control/NaturalSummaryCSV_authorformat"
    run(topic,input_withbaseline)