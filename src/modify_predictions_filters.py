'''
Created on Mar 28, 2017
modify predictions based on presence of certain filters such as verb, number of dict words
@author: amita
'''

import nltk
import re
import configparser
import enchant
import pandas as pd
import string,os
import numpy as np

def has_x_english_words(sent: list, en_dict, x, re_number):
        sent = [token for token in sent if not re.match(re_number, token) and not token == "."]
        return [en_dict.check(word.lower()) for word in sent].count(True) >= x

def has_a_verb(sent):
        verb_tags = {"VB", "VBD", "VBG", "VBN", "VBP", "VBZ"}
        pos_tags = [pair[1] in verb_tags for pair in nltk.pos_tag(sent)]
        return any(pos_tags)

def applyfilterdf(row,required_num_dict_words,re_number,en_dict,min_pred_score, predicted_scorecol):
    sent=row["sentence"]
    #print(sent)
    tokens = nltk.word_tokenize(sent)
    words = [x for x in tokens if x not in string.punctuation]

    # Make sure sentence has at least n english words
    if required_num_dict_words is not None and \
            not has_x_english_words(words, en_dict, required_num_dict_words, re_number):
        return min_pred_score
    if not has_a_verb(words):
        return min_pred_score
    return row[ predicted_scorecol]
        


def run(topic):
    config = configparser.ConfigParser()
    config.read("modify_predictions_filters_config.ini")
    inputwithpred=config.get(topic,"input_withpredic")
    output_base=os.path.basename(inputwithpred)[:-4]+"_mod.csv"
    output_csv=os.path.join(os.path.dirname(inputwithpred),output_base)
    required_num_dict_words=int(config.get(topic,"required_num_dict_words"))
    predicted_scorecol=config.get(topic,"predicted_col")
    re_number = re.compile("[0-9]+(\.[0-9]+)?")
    en_dict = enchant.Dict("en_US")
    df=pd.read_csv(inputwithpred)
    min_pred_score=min(df[predicted_scorecol].values)
    df = df.replace(np.nan, '', regex=True)
    df["modified_prediction"]=df.apply(applyfilterdf,args=(required_num_dict_words,re_number,en_dict, min_pred_score,predicted_scorecol),axis=1)
    df.to_csv(output_csv,index =False)
        
if __name__ == '__main__':
    #run("GC")
    #run("GM")
    run("DP")
    