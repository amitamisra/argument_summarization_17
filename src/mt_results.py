'''
Created on Mar 11, 2017
process the results from MT
The dialogs had exclsuive qualifications, Only 2 workers did it. There is a project for each worker.
At any time they do only one dialog
@author: amita
'''
import copy
import mturk_add_dialog_labeltext
import os
import file_utilities
import pandas as pd
import ast
from bs4 import BeautifulSoup
import operator
import itertools
import sys
from collections import defaultdict
import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
keys_ignore=["Answer.tmp2", "Title", "Description",  "Keywords", "Reward", "CreationTime",    "MaxAssignments",  "RequesterAnnotation" \
            ,"AssignmentDurationInSeconds" ,"AutoApprovalDelayInSeconds",  "Expiration" ,"AcceptTime" ,   "SubmitTime",    \
            "AutoApprovalTime",  "ApprovalTime","RejectionTime","RequesterFeedback", "WorkTimeInSeconds", "LifetimeApprovalRate", \
            "Last30DaysApprovalRate",    "Last7DaysApprovalRate" ,"Approve","Reject","Answer.tmp2"]

keys_hit_info=["HITId","HITTypeId","AssignmentId"]
keys_limited=["Dialog","inputable","dialog_no_format_author","edu_corelp", "dialog_text","label_text"]






def parse_inputtabe(input_table):
    """
       parse the inputable to get edus nos corresponding to phrasenos
       return a dictionary with phrasenos as keys and edusno as values
    """
    soup = BeautifulSoup(input_table, 'lxml') # Parse the HTML as a string
    #print(input_table)
    firstrow=True
    table = soup.find_all('table')[0] # Grab the first table
    mapping_phraseno={}
    for row in table.find_all('tr'):
        if firstrow:
            firstrow=False
            continue
        columns = row.find_all('td')
        try:
            for i in range(0,len(columns),3):
                edu_no=columns[i].text
                if columns[i+1].text:
                    text=text=columns[i+1].text
                    #print(text)
                    if not columns[i+2]:
                        print("stop")
                    phraseno=columns[i+2].textarea.attrs["name"]
                    new_dict={}
                    new_dict["edu_no"]=edu_no
                    new_dict["text_edu"]=text
                    mapping_phraseno[phraseno]=new_dict
        except IndexError:  
                logging.info("blank columns in table, ignore these")
            
                    
        
        
    return mapping_phraseno   

def merge_worker_responses(allrows):
    workercombined_rows=[]
    df= pd.DataFrame(allrows)
    grouped_key_edu=(df.groupby(['key', 'edu_no']))
    for name,group in grouped_key_edu:
        workercombineddict={}
        allworker_separate_row=group.to_dict('records')
        workercombineddict["key"]=name[0]
        workercombineddict["edu_no"]=name[1]
        for row in allworker_separate_row:
            for row_key, value in row.items():
                if row_key.startswith("worker"):
                    workerdict=value
                    worker_id=workerdict["worker"]
                    label_no=workerdict["label_no"]
                    workercombineddict["worker"+worker_id]=label_no
                else:
                    workercombineddict[row_key]=row[row_key]      
                        
        workercombined_rows.append( workercombineddict)    
    return workercombined_rows        

def addtruelabeledu_rst_info(df_dict, key_filemapping,outputdir):
    """
    add the truelabel of each edu to rst_info
    """
  
    allrows=[]
    for key, df_list in df_dict.items():
        df_concat=pd.concat(df_list)
        key_list=df_concat.to_dict('records')
        key_list.sort(key=operator.itemgetter("WorkerId"))
        for worker, workeritems in itertools.groupby(key_list, operator.itemgetter("WorkerId")):
            logger.info(key_filemapping)
            logger.info(key)
            logger.info(worker)
            worker_rows=list(workeritems)
            row_dict={}
            for worker_row in worker_rows:
                input_table=worker_row["Input____inputable"]
                phrasedict_table=parse_inputtabe(input_table)
                mappingdict=ast.literal_eval(worker_row["Input____mapping_edu_phrasenos"] )
                colnames=worker_row.keys()
                responses=[ans for ans in colnames if ans.startswith("Answer") and not str(ans)=="Answer.tmp2"]
                for col in colnames:
                        if col in keys_ignore:
                            continue
                        if str(col).startswith("Input"):
                            row_dict_key=str(col)[9:]
                            if col not in row_dict.keys() and not str(col)== "Input____mapping_edu_phrasenos":
                                row_dict[row_dict_key]=(worker_row[col])
                        
                for key_hit in keys_hit_info:
                    row_dict[key_hit]=worker_row[key_hit]
                
                for response in responses:  
                                if response.split("Answer____")[1] in phrasedict_table.keys():
                                    new_dict=copy.deepcopy(row_dict)
                                    phrase_no=phrasedict_table[response.split("Answer____")[1]]   
                                    #logger.info("phrase_no {0}".format(phrase_no) ) 
                                    #logger.info("response {0}".format(response) ) 
                                    #logger.info("worker_row response {0}".format(worker_row[response])) 
                                    worker_row[response]=str(worker_row[response])  
                                    if "," in   worker_row[response]:
                                         label_no=int((worker_row[response]).split(",")[0])
                                    else:     
                                        label_no=int(float(worker_row[response]))
                                    edu_no=phrase_no["edu_no"]
                                    text_edu=phrase_no["text_edu"]
                                    new_dict["worker"]= {"worker":worker,"label_no":label_no}
                                    new_dict["edu_no"]=edu_no
                                    new_dict["edu_text"]=text_edu
                                    new_dict["key"]=key
                                    allrows.append(new_dict)
    
    workercombined_rows=merge_worker_responses(allrows)                             
    return workercombined_rows              
def joinresultsperworker( inputdir_withpath_list):
    """
    """
    key_filemapping=defaultdict(list)
    df_dict=defaultdict(list)
    for inputdir_withpath in inputdir_withpath_list :
        filelist= os.listdir(inputdir_withpath)
        outputdir=os.path.join(os.path.dirname(inputdir_withpath),"mtresults_combined")
        if not os.path.exists(outputdir):
            os.mkdir(outputdir)
        for filename in filelist:
            filenamewithpath=os.path.join(inputdir_withpath,filename)
            if str(filenamewithpath).endswith(".csv"):
                df=pd.read_csv(filenamewithpath)
                
                df.columns = [c.replace('.', '____') for c in df.columns]
                keyvalue=df['Input____Key'].tolist()[0]
                df_dict[keyvalue].append(df)
                key_filemapping[keyvalue].append(filenamewithpath)
    allrows=addtruelabeledu_rst_info(df_dict,key_filemapping, outputdir)
    
    resultsdf=pd.DataFrame(allrows)
    df_limited = resultsdf.drop(keys_limited, axis=1)
    
    outputresultfile=os.path.join(outputdir,"mt_results")
    resultsdf.to_json(outputresultfile+".json")
    resultsdf.to_csv(outputresultfile+".csv")
    df_limited.to_csv(outputresultfile+"mt_resultslimited.csv")
            
    

def run(dirlist):
    inputdir_withpath_list=[]
    for mtresults_dir in dirlist:
        data_dir=file_utilities.DATA_DIR
        input_withdir=os.path.join(*mtresults_dir.split(","))
        inputdir_withpath=os.path.join(data_dir,input_withdir)
        inputdir_withpath_list.append(inputdir_withpath)
    joinresultsperworker( inputdir_withpath_list)
    
if __name__ == '__main__':
    #print("test")
    topic="gun-control"
    mtresults_dir1="data_originalsummaries,gun-control,Mturk,MTResults,A2LC_results"
    mtresults_dir2="data_originalsummaries,gun-control,Mturk,MTResults,AJP_results"
    mtresults_dir3="data_originalsummaries,gun-control,Mturk,MTResults,A1FBB_results"
    dirlist=[]
    dirlist.append(mtresults_dir1)
    dirlist.append(mtresults_dir2)
    dirlist.append(mtresults_dir3)
    run(dirlist)
    