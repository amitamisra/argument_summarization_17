'''
Created on Apr 6, 2017

@author: amita
'''
import pandas as pd
import nltk
import string

import nltk
import re
import configparser
import enchant
import pandas as pd
import string,os
import numpy as np
import random
randomseed=4
from src import file_utilities
def has_x_english_words(sent: list, en_dict, x, re_number):
        sent = [token for token in sent if not re.match(re_number, token) and not token == "."]
        return [en_dict.check(word.lower()) for word in sent].count(True) >= x

def has_a_verb(sent):
        verb_tags = {"VB", "VBD", "VBG", "VBN", "VBP", "VBZ"}
        pos_tags = [pair[1] in verb_tags for pair in nltk.pos_tag(sent)]
        return any(pos_tags)
    
def clean_df(df,textcol,required_num_dict_words):
    rowdicts=df.to_dict('records')
    allrows=[]
    for row in rowdicts:
        add=True
        re_number = re.compile("[0-9]+(\.[0-9]+)?")
        en_dict = enchant.Dict("en_US")
        sent=row[textcol]
        tokens = nltk.word_tokenize(sent)
        words = [x for x in tokens if x not in string.punctuation]
        if required_num_dict_words is not None and \
            not has_x_english_words(words, en_dict, required_num_dict_words, re_number):
            add=False
        else:
            if not has_a_verb(words):
                add= False
   
        if add:
            allrows.append(row)
    clean_df=pd.DataFrame(allrows)      
    return clean_df  


def balance(clean_df,dflist_tier_more3):
    listdf=[]
    num_rows_3= dflist_tier_more3.shape[0]
    df_reduced_lessthan3=clean_df.sample(num_rows_3,random_state=randomseed)
    listdf.append(dflist_tier_more3)
    listdf.append(df_reduced_lessthan3)
    merged_df=pd.concat(listdf)
    merged_df=random.shuffle(merged_df)
    return merged_df

def create_balanced_set(outputmt_split,outputmt_split_bal,weightcol):
    allkey_df=[]
    df=pd.read_csv(outputmt_split)
    dflist_tier_more3=[]
    df_list_tier_less_3=[]
    dialog_df_group=df.groupby("key")
    for key, dialog_key in  dialog_df_group:
        weightdf_groups=dialog_key.groupby(weightcol)
        for weight, weightdf in  weightdf_groups:
            if int(weight) >=3:
                dflist_tier_more3.add(weightdf)
            else:
                    df_list_tier_less_3.add(weightdf)
        
       
        clean_df=clean_df(df_list_tier_less_3)
        merged_df=balance(clean_df,dflist_tier_more3)
        allkey_df.append(merged_df)
    balanced_df=pd.concat(allkey_df)    
    balanced_df.tocsv(outputmt_split_bal)

def run(section):
    config = configparser.ConfigParser()
    regressioninput=file_utilities.get_absolutepath_data("config","mergecontributorMTresults_config.ini")
    config.read(regressioninput)
    outputmt_split=config.get(section,"outputmt_split")
    outputmt_split_bal=config.get(section,'balanced_split')
    weightcol=config.get(section,'weightcol')    
    create_balanced_set(outputmt_split,outputmt_split_bal,weightcol)
if __name__ == '__main__':
    
    run("GC_tier3_1")