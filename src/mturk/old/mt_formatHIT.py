'''
Created on Feb 28, 2017

@author: amita
'''
import pandas as pd
import copy,os

def testhititem(len_rst,noedusHIT):
    no_ofHITS=len_rst//noedusHIT
    rem=len_rst % noedusHIT
    start=0
    allindexlist=[]
    additems=rem // no_ofHITS
    additemslast=rem % no_ofHITS
    itemsHIT=noedusHIT+ additems
    for i in range(1,no_ofHITS+1 ) :
        if i <   no_ofHITS:
            end=start+ itemsHIT-1
            allindexlist.append((start,end))
            start=end+1
        else:
            if i==no_ofHITS:
                end= start + itemsHIT+ additemslast-1  
                allindexlist.append((start,end))

def  appendcolumnlastrow(newrow ,count,inputtable):
    
    rem= newrow %3
    if rem==0:
        return inputtable
    if rem==2:
            inputtable=inputtable+"""<td>""" + str(count)+"""</td><td> </td></tr>"""
    if rem==1:
            inputtable=inputtable+"""<td>"""+ str(count)+"""</td><td></td> <td>"""+ str(count+1)+"""</td><td></td></tr>"""
    return inputtable        

def HITdialog(row,noedusHIT,outputbasedir):
    
    allrows=[]
    rst_info=row["edu_corelp"]["rst_info"]
    key=row["Key"]
    #-------------------------------------- if key =="1-342_9_8__12_13_15_16_1":
        #--------------------------------------------------------------- print()
    len_rst=len(rst_info)
    no_ofHITS=len_rst // noedusHIT
    rem=len_rst % noedusHIT
    start=0
    allindexlist=[]
    allmapping=[]
    additems=rem // no_ofHITS
    additemslast=rem % no_ofHITS
    itemsHIT=noedusHIT+ additems
    for i in range(1,no_ofHITS+1 ) :
        if i <   no_ofHITS:
            end=start+ itemsHIT-1
            allindexlist.append((start,end))
            start=end+1
        else:
            if i==no_ofHITS:
                end=start+ itemsHIT+ additemslast-1  
                allindexlist.append((start,end))
    rowunique=0   
    for(start,end) in allindexlist:
            rowunique=rowunique+1
            inputtable=""" 
    <table>
    <tbody>
        <tr>
            <td>SNo</td>
            <td style="width: 500px; padding-right: 100px; background-color: #E6FFBF;">Text phrase</td>
            <td style="width: 50px; padding-right: 50px; background-color: #E6FFBF;">Label</td>
            <td>SNo</td>
            <td style="width: 500px; padding-right: 100px; background-color: #E6FFBF;">Text phrase</td>
            <td style="width: 50px; padding-right: 50px; background-color: #E6FFBF;">Label</td>
            <td>SNo</td>
            <td style="width: 500px; padding-right: 100px; background-color: #E6FFBF;">Text phrase</td>
            <td style="width: 50px; padding-right: 50px; background-color: #E6FFBF;">Label</td>
        </tr> """
            mtrow=copy.deepcopy(row)
            count=1
            newrow=3
            for i in range(start,end+1):
                if newrow % 3 == 0:
                    inputtable= inputtable +"""<tr>"""
                inputtable=inputtable + """<td style="text-align: right;">""" + str(i+1) + """</td>
                <td style="width: 500px; padding-right: 100px; background-color: #E6FFBF;">""" + rst_info[i]["rawtext"]+"""</td>
                <td><textarea name= phrase""" + str(count) + """ style="width: 100px; height: 50px;border-style:solid; border-color:#FF0000;" required ></textarea></td>"""
                mapping={"phrasecount":count,"edu_no":rst_info[i]["edu_no"], "rst_no":i}
                allmapping.append(mapping)
                count=count+1
                newrow=newrow+1
                if newrow %3 == 0:
                    inputtable=inputtable+"""</tr>"""                                
            #print(inputtable)   
            inputtable=appendcolumnlastrow(newrow ,count,inputtable)
            inputtable=inputtable+ """</tbody>
                                    </table>"""
            #print(inputtable)                        
            mtrow["inputable"]=inputtable
            mtrow["mapping_edu_phrasenos"] =allmapping
            mtrow["countphrases"]=count-1
            mtrow["rowuniqueno"]=rowunique
            allrows.append(mtrow)
            outputfile=os.path.join(outputbasedir,"inputtable",key +".html")
            with open(outputfile,"w", encoding="utf-8") as f:
                f.write(inputtable)   
            
            
    df= pd.DataFrame(allrows)
    outputfile=os.path.join(outputbasedir,"_mtinputformat"+key)
    df.to_csv(outputfile+".csv",index=False)
    
def run(inputfile, outputbasedir,noedusHIT):
    df=pd.read_json(inputfile+".json")
    df.apply(HITdialog,args=(noedusHIT,outputbasedir),axis=1)
    
    
if __name__ == '__main__':
    inputfile="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gun-control/mt_NaturalSummaryCSV_authorformat_all_pyramids_scus_author_edu_corenlp"
    noedusHIT=20
    outputbasedir="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gun-control/Mturk/formatteddialogs"
    run(inputfile,outputbasedir,noedusHIT)
    
    #===========================================================================
    # noedusHIT=20
    # len_rst=201
    # testhititem(len_rst,noedusHIT)
    #===========================================================================