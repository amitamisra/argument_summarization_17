'''
Created on Feb 28, 2017
create a HIT by splitting the edus for A DIALOG
@author: amita
'''
import pandas as pd 
import file_utilities
import os
import copy


def  appendblankcols(rst_info):
    
    len_rst_info=len(rst_info)
    rem= len_rst_info %3
    if rem==0:
        return rst_info
    if rem==1:
        rst_info.extend([{
                    "edu_no": len_rst_info+1,
                    "rawtext": ""
                },{
                    "edu_no": len_rst_info+2,
                    "rawtext": ""
                }])
    if rem==2:
            rst_info.extend([{
                        "edu_no": len_rst_info+1,
                        "rawtext": ""
                    }])
    return rst_info         

def creatmtdialogtext(row,directoryname):
    #dialog=row["dialog_no_format_author"]
    rst_info=list(row["edu_corelp"]["rst_info"])
    key=row["Key"]
    dialogtable=""" < table border="">
    <tbody>
        <tr>
            <td>
            <table border="1" cellpadding="1" cellspacing="0">
                <colgroup>
                    <col width="40/" />
                    <col width="300/" />
                    <col width="40/" />
                    <col width="320/" />
                    <col width="40/" />
                    <col width="300/" />                    
                </colgroup>
                <tbody>
                    <tr>
                        <th>SNo</th>
                        <th>Text</th>
                        <th>SNo</th>
                        <th>Text</th>
                        <th>SNo</th>
                        <th>Text</th>                        
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
        <tr>
            <td>
            <div style="width:1080px; height:150px; overflow:auto;">
            <table border="1" cellpadding="1" cellspacing="0" style="margin: 0px; font-size: 11px; line-height: normal; font-family: Menlo;" width="1060">
                <colgroup>
                    <col width="40/" />
                    <col width="250/" />
                    <col width="40/" />
                    <col width="270/" />
                    <col width="40/" />
                    <col width="270/" />
                </colgroup>
                <tbody>"""
    countcol=3
    
    newrst_info=appendblankcols(rst_info)
    #print(key)
    for rst in newrst_info:
        if countcol %3==0:
            dialogtable=dialogtable+"""<tr>"""
        
        dialogtable=dialogtable+"""<td>"""+ str(rst["edu_no"])+"""</td><td>"""+ str(rst["rawtext"])+"""</td>"""
        countcol=countcol+1
        
        if countcol %3==0:
            dialogtable=dialogtable+"""</tr>"""
    dialogtable=dialogtable+"""</tbody>
            </table>
            </div>
            </td>
        </tr>
    </tbody>
</table>     """    
    outputfile=os.path.join(directoryname,"dialoghtml",key +".html")
    with open(outputfile,"w", encoding="utf-8") as f:
        f.write(dialogtable)                    
    return dialogtable    
                           
def creatmtlabeltext(row,directoryname):
    dialog=row["dialog_no_format_author"]
    scu_info=copy.deepcopy(row["scu_info"])
    key=row["Key"]
    labeltable=""" <table border="1">
    <tbody>
        <tr>
            <td><span style="color:#0000FF;">Label NO</span></td>
            <td><span style="color:#0000FF;">LabelText</span></td>
            <td><span style="color:#0000FF;">Label NO</span></td>
            <td><span style="color:#0000FF;">LabelText</span></td>
        </tr>
         """
         
    #scu_info = sorted(scu_info, key=int(scu_info.get))
    #sorted(scu_info, key=lambda x: int(operator.itemgetter("pos")(x))) #sort on label numbers
    first=True
    second=False 
    len_scu=len(scu_info ) 
    if len_scu % 2 == 0 :
        pass
    else:
        scu_info[str(len_scu+1)]= {'label': ''}
    
    keylist = list(scu_info.keys())
    #print(keylist)
    keylist=[int(x) for x in  keylist]
    keylist.sort()     
    for scu_no in keylist:
        if first:
            labeltable=labeltable +""" <tr>
                <td><span style="color:#0000FF;">
                """
            labeltable=labeltable+ str(scu_no)
            labeltable=labeltable +   """</span></td>
                <td>
                <p style="margin: 0px; font-size: 11px; line-height: normal; font-family: Menlo;"><span style="color:#0000FF;"><span style="font-family: Menlo; font-size: 11px;">"""
            labeltable=labeltable+ scu_info[str(scu_no)]["label"] +"""</span></span></p> """
            labeltable=labeltable +  """</td>"""
            
            first=False
            second=True
            continue
        if second:
            labeltable=labeltable +"""<td><span style="color:#0000FF;">"""
            labeltable=labeltable + str(scu_no)+ """</span></td>
                <td>
                <p style="margin: 0px; font-size: 11px; line-height: normal; font-family: Menlo;"><span style="color:#0000FF;"><span style="font-family: Menlo; font-size: 11px;">"""
            labeltable=labeltable +   scu_info[str(scu_no)]["label"]+"""</span></span></p>"""
            labeltable=labeltable +    """</td> </tr>"""
            first=True
            second=False
    labeltable=labeltable+"""</tbody>
</table>  """       
    outputfile=os.path.join(directoryname,"labelhtm",key +".html")
    with open(outputfile,"w", encoding="utf-8") as f:
        f.write(labeltable)
        
    #mtrow["dialog_no_format_author"]=dialog
    #mtrow[key]=key
    return labeltable

def run(input_file):
    input_json=input_file +".json"
    df=pd.read_json(input_json)
    directoryname=os.path.dirname(input_json)
    df["label_text"]=df.apply(creatmtlabeltext,args=(directoryname,),axis=1)
    df["dialog_text"]=df.apply(creatmtdialogtext,args=(directoryname,),axis=1)
    basename=os.path.basename(input_json)
    dirname=os.path.dirname(input_json)
    output_file=os.path.join(dirname, "mt"+basename[:-5])
    df.to_json(output_file+".json") 
    df.to_csv(output_file+".csv", index=False)
    return output_file
    
    
if __name__ == '__main__':
    input_file="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gun-control/NaturalSummaryCSV_authorformat_all_pyramids_scus_author_edu_corenlp"
    run(input_file)
    