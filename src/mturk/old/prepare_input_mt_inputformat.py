'''
Created on Mar 2, 2017
create a MT input Format HIt. Takes the output result from running prepare_data_original_dialog_summary and calls
mt_formatHIT.py mt_inputformatHIT.py. ONE HIT is created per Dialog
@author: amita
'''
import mturk_add_dialog_labeltext
import os
import file_utilities
import mt_formatHIT

def run(input_file,no_edus_HIT,outputbasedir):
    """
    input file obtained after running  prepare_data_original_dialog_summary, give the path as comma separated directory and filename starting from
    data_originalsummaries
    outbasedir: The outpurdirectory where HITS for each dialog will be created
    Each dialog has it's own MT HIT CSV
    no_edu_HIT: I kept it 20. this varies depending upon the number of edus in a dialog
    It may be bewteen 20-39 for one HIT.
    """
    data_dir=file_utilities.DATA_DIR
    input_withdir=os.path.join(*input_file.split(","))
    input_withpath=os.path.join(data_dir,input_withdir)
    output_mturk_dialog= mturk_add_dialog_labeltext.run(input_withpath)
    output_withdir=os.path.join(*outputbasedir.split(","))
    outputwithpath=os.path.join(data_dir,output_withdir)
    mt_formatHIT.run(output_mturk_dialog,outputwithpath,no_edus_HIT)
    

if __name__ == '__main__':
    
    topic="gun-control"
    input_fordialog="data_originalsummaries,gun-control,NaturalSummaryCSV_authorformat_all_pyramids_scus_author_edu_corenlp"
    outputbasedir="data_originalsummaries,gun-control,Mturk,formatteddialogs"
    no_edus_HIT=20
    run(input_fordialog,no_edus_HIT,outputbasedir)