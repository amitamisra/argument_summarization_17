'''
Created on Mar 31, 2017

@author: amita
'''
import copy,os
import pandas as pd
import  configparser
from src import file_utilities

def splitcontib(allcontriblist,label):
    text="" +"<ul>"
    #contriblist=contribs[0]
    for contrib_list in allcontriblist:
        for contrib in contrib_list:
            text=text+"""<li>"""
            text=text+"""<font size ="2" color ="blue">"""+contrib+"." + "<br>" + "</font></li>"
    text=text+""" <ul>"""        
    text=text +"<br>"+ """<font size="2" color="red">"""+label +".</font>" 
    return text    

def createlabels(row,basedirectory,min_tier,mapping_count_scu_label_key):
    scu_info=copy.deepcopy(row["scu_info"])
    key=row["Key"]
    if key=="1-546_20_19__24_27_28_29_33_34_1":
        pass
    labeltable=""" <table border="1">
    <tbody>
        <tr>
            <td><span style="color:#0000FF;">Label NO</span></td>
            <td><span style="color:#0000FF;">LabelSet</span></td>
            <td><span style="color:#0000FF;">Label NO</span></td>
            <td><span style="color:#0000FF;">LabelSet</span></td>
        </tr>
         """
         
    mintier_scu={ k: v for k, v in scu_info.items() if int(v["weight"]) >= int(min_tier)}     
    first=True
    second=False 
    len_scu=len(mintier_scu ) 
    
    
    keylist = list(mintier_scu.keys())
    #print(keylist)
    keylist=[int(x) for x in  keylist]
    keylist.sort()     
    count=1
    if len_scu % 2 == 0 :
        pass
    else:
        mintier_scu[str(max(keylist)+1)]= {'label': '',"contrib":"","id":""}
    for scu_no in keylist:
            if first:
                labeltable=labeltable +""" <tr>"""
                labeltable=labeltable +""" 
                    <td><span style="color:#0000FF;">
                    """
                labeltable=labeltable+ str(count)
                labeltable=labeltable + """</span></td>
                    <td>
                    <p style="margin: 0px; font-size: 11px; line-height: normal; font-family: Menlo;"><span style="color:#0000FF;"><span style="font-family: Menlo; font-size: 11px;">"""
                labeltable=labeltable+ splitcontib(mintier_scu[str(scu_no)]["contrib"], mintier_scu[str(scu_no)]["label"] ) +"""</span></span></p> """
                labeltable=labeltable +  """</td>"""
                
                first=False
                second=True
                mapping_count_scu_label_key[count]=(mintier_scu[str(scu_no)],mintier_scu[str(scu_no)][str("id")])
                count=count+1
                continue
            if second:
                labeltable=labeltable +"""<td><span style="color:#0000FF;">"""
                labeltable=labeltable + str(count)+ """</span></td>
                    <td>
                    <p style="margin: 0px; font-size: 11px; line-height: normal; font-family: Menlo;"><span style="color:#0000FF;"><span style="font-family: Menlo; font-size: 11px;">"""
                labeltable=labeltable +  splitcontib( mintier_scu[str(scu_no)]["contrib"], mintier_scu[str(scu_no)]["label"])+"""</span></span></p>"""
                labeltable=labeltable +    """</td> """
                mapping_count_scu_label_key[count]=(mintier_scu[str(scu_no)],mintier_scu[str(scu_no)][str("id")])
                first=True
                second=False
                count=count+1
                labeltable=labeltable +""" </tr>"""
    labeltable=labeltable+"""</tbody>
    </table>  """       
    
    directoryname=os.path.join(basedirectory,"contrib_labeltable")
    if not os.path.exists(directoryname):
        os.mkdir(directoryname)
    outputfile=os.path.join(directoryname,"contribhtm_"+key +".html")
    with open(outputfile,"w", encoding="utf-8") as f:
        f.write(labeltable)
     
    num_labels=count-1    
    return labeltable,num_labels


def getauthor(sentence_df,sent_no):
    authorlist=sentence_df.iloc[sent_no]["Author_turn"]
    if authorlist:
        return(authorlist[0])
    else:
        return ""

def addradiobuttons(numoflabels,sent_no):
    radiotext=""
    for count in range(1,numoflabels+1):
        buttonname="sent" +str(sent_no)
        labelname="label_" + str(count)
        radiotext=radiotext+ """  <input type ="radio" name=""" + str(""" " """ + buttonname +""" " """).strip() + """ value="""  + """ " """ + labelname +""" " """ +""" > """ + labelname +"  "
        if count%4==0:
            radiotext=radiotext+"<br>"
    
    
    labelname="None"
    radiotext=radiotext+ """  <input type ="radio" name=""" + str(""" " """ + buttonname +""" " """).strip() + """ value="""  + """ " """ + labelname +""" " """ +""" > """ + labelname

        
    return radiotext
def createtable(newrow,start,end,basedirectory,sentence_df,prev_author,countlabels):
    key=newrow["Key"]
    inputtable=""" 
    <table border="1" cellpadding="1" cellspacing="0">
    <colgroup>
                    <col width="40/" />
                     <col width="60/" />
                    <col width="800/" />
                    <col width="300/" />
     </colgroup>               
    <tbody>
        <tr>
            
            <td>SNo</td>
            <td>Author</td>
            <td>Text</td>
            <td>Label</td>
            
        </tr> """
    for i in range(start,end):
            authorcol= getauthor(sentence_df,i)
            if authorcol:
                prev_author=authorcol
            
            if i==start and authorcol=="":
                authorcol=prev_author       
            inputtable= inputtable + """<tr>"""
            inputtable=inputtable + """ <td>""" + str(i) + """</td> <td>"""+ authorcol+"""</td>
            <td >""" + newrow["sentence"+str(i)]+"""</td><td>""" + addradiobuttons(countlabels,i)+"""</td> </tr>"""
    inputtable=inputtable+ """</tbody>
                                </table>"""
    
    directoryname=os.path.join(basedirectory,"inputtable")
    if not os.path.exists(directoryname):
        os.mkdir(directoryname)
    outputfile=os.path.join(directoryname,"inputtable"+key+str(start) +".html")  
    with open(outputfile,"w", encoding="utf-8") as f:
        f.write(inputtable)                    
    return inputtable,prev_author

def create_mtrows(newrow,sentence_df,directoryname,start,endrange,min_tier,prev_author):
    mapping_count_scu_label_key={}
    for i in range(start,endrange):
        sentence=" ".join(sentence_df.iloc[i]["tokens"])
        sentence_no=sentence_df.iloc[i]["sentence_no"]
        newrow["sentence"+str(sentence_no)]=sentence
    newrow["contribtable"], num_labels=createlabels(newrow,directoryname,min_tier,mapping_count_scu_label_key)
    newrow["inputtable"],prev_author=createtable(newrow,start,endrange,directoryname,sentence_df,prev_author, num_labels)
    newrow["countsentences"]=endrange-start
    return prev_author

def processdialog(row,allrows,directoryname,sentcols,min_tier):
    corelp_author_dict=sentcols[0]
    corenlp_info=sentcols[1]
    sentence_df=pd.DataFrame(row[corelp_author_dict][corenlp_info])
    num_sent=sentence_df.shape[0]
    
    newrow1=copy.deepcopy(row)
    num_sent_HIT=int(num_sent/3)
    prev_author=""
    prev_author=create_mtrows(newrow1,sentence_df,directoryname,0,num_sent_HIT,min_tier,prev_author)
    allrows.append(newrow1)
     
    newrow2=copy.deepcopy(row) 
    prev_author=create_mtrows(newrow2,sentence_df,directoryname,num_sent_HIT, 2*num_sent_HIT,min_tier,prev_author)   
    allrows.append(newrow2)
    
    newrow3=copy.deepcopy(row) 
    prev_author=create_mtrows(newrow3,sentence_df,directoryname,2*num_sent_HIT,num_sent,min_tier,prev_author)   
    allrows.append(newrow3)
        

def createHITtopic(inputfile,outputmtformatted,contrib_directory,sentcols,min_tier):
    df=pd.read_json(inputfile+".json")
    allrows=[]
    for index, row in df.iterrows():
        processdialog(row,allrows,contrib_directory,sentcols,min_tier)
    mtformatdf=pd.DataFrame(allrows)
    resetdf=mtformatdf.reset_index()
    #indexdf=resetdf.iloc[startindex:endindex]
    resetdf.to_json(outputmtformatted+".json")   
    resetdf.to_csv(outputmtformatted+".csv",index=False)


def run(topic):
    config = configparser.ConfigParser()
    regressioninput=file_utilities.get_absolutepath_data("config","prepareinputcontributorMT_config.ini")
    config.read(regressioninput)
    input_file=config.get(topic,"inputfile")
    contrib_directory=config.get(topic,"contrib_directory")
    sent_cols=config.get(topic,"sent_cols").split(",")
    min_tier=config.get(topic,"min_tier")
    #start_index=config.get(topic,"start_index)
    #end_index=config.get(topic,'end_index')
    outputmt_formatted=config.get(topic,'outputmtformatted')
    createHITtopic(input_file,outputmt_formatted,contrib_directory,sent_cols,min_tier)
    
if __name__ == '__main__':
    
    run("GC_tier3")

    



#===============================================================================
# <td style="width: 1000px; padding-right: 100px; background-color: #E6FFBF;">Text</td>
#             <td style="width: 1000px; padding-right: 100px; background-color: #E6FFBF;">Text</td>
#             <td style="width: 50px; padding-right: 50px; background-color: #E6FFBF;">Label</td>
#===============================================================================