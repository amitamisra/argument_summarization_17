'''
Created on Apr 4, 2017
merge the results from contribHITS acrosss various turkers
@author: amita
'''
import configparser
from src import file_utilities
import pandas as pd
import operator
import itertools
from collections import defaultdict
import ast
from bs4 import BeautifulSoup
import os
import copy

ignore_columns_list=" Title    Description    Keywords    Reward    CreationTime    MaxAssignments    RequesterAnnotation \
   AssignmentDurationInSeconds    AutoApprovalDelayInSeconds    Expiration    NumberOfSimilarHITs    LifetimeInSeconds \
   AcceptTime    SubmitTime    AutoApprovalTime    ApprovalTime    RejectionTime    RequesterFeedback    WorkTimeInSeconds \
   LifetimeApprovalRate    Last30DaysApprovalRate    Last7DaysApprovalRate Input.contribtable Input.inputtable Input.contribarray \
   Answer.tmp2    Approve    Reject  ".split()

def getlabel_tier_sentence(mapping_count_scu_label_key,ans,newdict,workerid,is_response_dict):
    mapping_count_scu_label_key=ast.literal_eval(mapping_count_scu_label_key)
    if is_response_dict==1:
        try:
            ans_info_dict=mapping_count_scu_label_key[ans][0]
            newdict["worker"+workerid+"weight"]=ans_info_dict["weight"]
            newdict["worker"+workerid+"label"]=ans_info_dict["label"]
        except KeyError:
                    newdict["worker"+workerid+"weight"]=2
                    newdict["worker"+workerid+"label"]="None"
        
    else:
        
    
            try:
                ans_info_dict=mapping_count_scu_label_key[ans][0]
                newdict["worker"+workerid+"weight"]=ans_info_dict["weight"]
                newdict["worker"+workerid+"label"]=ans_info_dict["label"]
                newdict["worker"+workerid+"id"]=ans_info_dict["id"]
                newdict["worker"+workerid+"contrib"]=ans_info_dict["contrib"]
                newdict["key_user"]=ans_info_dict["key_user"]
            except KeyError:
                newdict["worker"+workerid+"weight"]=2
                newdict["worker"+workerid+"label"]="None"
                newdict["worker"+workerid+"id"]=-1
                newdict["worker"+workerid+"contrib"]=-1
                newdict["worker"+workerid+"key_user"]=-1

def extract_sentnos(inputtable):
    soup = BeautifulSoup(inputtable)
    senttaglist=soup.table.table.find_all("tr")
    lentag=len(senttaglist)
    sent_nos=[]
    for i in range(1,lentag):
        trtag = senttaglist[i]
        sent_no=trtag.find("td").text
        sent_nos.append(sent_no)
    return(sent_nos)

def test_sentnumber(sent_no,row):
    """
    check if sentenxe number to be added to the dictioanry are correct
    """   
    sent_answers=extract_sentnos(row["Input.inputtable"])
    if str(sent_no) in sent_answers:
        return True
    else:
        False

def sort_sentenceno(df):
    df['sent_no'] = df['sent_no'].astype(int)
    grouped=df.groupby("Key")
    alldfs=[]
    for key, df in grouped:
        sorteddf=df.sort('sent_no')   
        alldfs.append(sorteddf)      
    sorteddf=pd.concat(alldfs)
    return sorteddf   
            
def calculate_avg_label(NewDict):
    totalscore=0
    count=0
    for key, value in NewDict.items():
        if key.startswith("worker") and key.endswith("weight"):
            totalscore=totalscore+ value
            count=count+1
    score=float(totalscore)/count         
    NewDict["label"]=score
    
def split_rows(rowdicts,outputmt_split):
    all_rows=list()
    response_rows=list()
    rowdicts.sort(key=operator.itemgetter("HITId"))
    for dictkey, items in itertools.groupby(rowdicts, operator.itemgetter("HITId")):
        hitids=list(items)
        hit0=hitids[0]
        ans_sents=[]
        for field in hit0.keys():
            if str(field).startswith("Answer.sent"):
                ans_sents.append(field)
            
        for ans_sent in ans_sents:
            sent_no= ans_sent.split("Answer.sent")[1] 
            checksent_no=test_sentnumber(sent_no,hit0)
            if checksent_no:
                    NewDict=defaultdict()  
                    response_dict={}  
                    for row in hitids:
                        NewDict["Id_"+ row["WorkerId"]]=row[ans_sent]
                        #response_dict["Id_"+ row["WorkerId"]]=row[ans_sent]
                        getlabel_tier_sentence(row["Input.mapping_count_scu_label_key"],row[ans_sent], NewDict,row["WorkerId"],0)
                        getlabel_tier_sentence(row["Input.mapping_count_scu_label_key"],row[ans_sent], response_dict,row["WorkerId"],1)
                        
                    NewDict["sent_no"]=  sent_no  
                    response_dict["sent_no"]=sent_no
                    NewDict["sent"]= row["Input.sentence"+  sent_no]
                    response_dict["sent"]=NewDict["sent"]
                    
                    for fieldname in row.keys():
                        if str(fieldname) in ignore_columns_list or str(fieldname).startswith("Input.sentence") or  str(fieldname).startswith("Answer.sent") :
                            continue
                        else:
                        #   
                            if str(fieldname).startswith("Input.") :
                                #print(fieldname)
                                newkey=fieldname.split("Input.")[1]
                                NewDict[newkey]=row[fieldname] 
                                #print(newkey)
                    
                    calculate_avg_label(NewDict)
                    calculate_avg_label(response_dict)
                    all_rows.append(NewDict)
                    response_dict["Key"]=NewDict["Key"]
                    response_rows.append(response_dict)
                                         
    newdf=sort_sentenceno(pd.DataFrame(all_rows))
    response_df=pd.DataFrame(response_rows)
    
    newdf.to_csv(outputmt_split+".csv")
    newdf.to_json(outputmt_split+".json")
    
    directory=os.path.dirname(outputmt_split)
    filename=os.path.basename(outputmt_split)
    
    responsefile=os.path.join(directory,"response_"+filename)
    
    response_df.to_csv(responsefile+".csv")
    response_df.to_json(responsefile+".json")                    
             
def run(topic):
    config = configparser.ConfigParser()
    regressioninput=file_utilities.get_absolutepath_data("config","mergecontributorMTresults2_config.ini")
    config.read(regressioninput)
    input_file=config.get(topic,"input_file")
    outputmt_split=config.get(topic,'outputmt_split')
    allmerged=config.get(topic,'allmerged')
    df= pd.read_csv(input_file)
    rowdicts=df.to_dict('records')
    split_rows(rowdicts,outputmt_split)
    return outputmt_split,allmerged



def mergebatches(outlist,mergedfile):
    alldf=[]
    for filename in outlist:
        df=pd.read_csv(filename+".csv") 
        alldf.append(df)
    merged_df=pd.concat(alldf)
    merged_df.to_csv(mergedfile+".csv",index=False)  
    
if __name__ == '__main__':
    
   
    out1,mergedfile=run("GC_tier3_1")
    out2,mergedfile=run('GC_tier3_2')
    out3,mergedfile=run('GC_tier3_3')
    out4,mergedfile=run('GC_tier3_4')         #DONE FOR SemDIAL 2017, do only once
    out5,mergedfile=run('GC_tier3_5')
    outlist=[]
    outlist.append(out1)
    outlist.append(out2)
    outlist.append(out3)
    outlist.append(out4)
    outlist.append(out5)
     
    mergebatches(outlist,mergedfile)


    out1,mergedfile=run("GM_tier3_1")
    out2,mergedfile=run('GM_tier3_2')
    out3,mergedfile=run('GM_tier3_3')
    out4,mergedfile=run('GM_tier3_4')         #DONE FOR SemDIAL 2017, do only once
    out5,mergedfile=run('GM_tier3_5')
    outlist=[]
    outlist.append(out1)
    outlist.append(out2)
    outlist.append(out3)
    outlist.append(out4)
    outlist.append(out5)
    mergebatches(outlist,mergedfile)
    
    
    out1,mergedfile=run("AB_tier3_1")
    out2,mergedfile=run('AB_tier3_2')
    out3,mergedfile=run('AB_tier3_3')
    out4,mergedfile=run('AB_tier3_4')         #DONE FOR SemDIAL 2017, do only once
    out5,mergedfile=run('AB_tier3_5')
    outlist=[]
    outlist.append(out1)
    outlist.append(out2)
    outlist.append(out3)
    outlist.append(out4)
    outlist.append(out5)
     
    mergebatches(outlist,mergedfile)
   
