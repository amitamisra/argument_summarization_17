'''
Created on Apr 11, 2017

@author: amita
'''
from nltk.corpus import nps_chat
from DialogueActTaggerNLTK import DialogueActTaggerNLTK
if __name__ == '__main__':
    
    class  InitDialogue_Act:
        def  __init__(self,dialogueActTaggerNLTK=None):
        
            if dialogueActTaggerNLTK:
                self.DialogueActTaggerNLTK = dialogueActTaggerNLTK
            else:
                self.DialogueActTaggerNLTK = DialogueActTaggerNLTK(tagger_name="NLTK_DialogueActTagger",
                                                                   pickled_model=None)
                
                
    
    
      
                    
                
    def load_dialogue_act(): 
        dialogue_act_object=InitDialogue_Act()
        return dialogue_act_object
    
    def classify_act(dialogue_act_object,text):  
        dialogue_act = dialogue_act_object.DialogueActTaggerNLTK.run_dialogue_act_tagger(text)
        return(dialogue_act)
           
            
if __name__ == "__main__":
    actobject=load_dialogue_act()
    print(classify_act(actobject,"do you want"))
    
    
                