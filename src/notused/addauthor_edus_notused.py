'''
Created on Feb 12, 2017
take the output from edus and add author information
@author: amita
'''
import re
def add_author_edu_list(sentence_list_edu,dialogtext_withauthor):
    """
    sentence_list_edu:List of sentences with edus from rst parser
    """
    
    posts=re.split("\s(?=[sS][12]:[0-9])",  dialogtext_withauthor)
    pass
if __name__ == '__main__':
    testdialog="""  S1:1-Care to cite the specific web address that you found that on? The only thing I found that confirmed that was a study that said the rates are the SAME, not higher among Christians than non-Christians. Another problem with the study is that most (or at least more) people in the country very well might identify as Christian and attending a denomination of SOME sort. If that's the case then the numbers would, by default, more likely favor Christians having a higher divorce rate.
 S2:1-Jyoshu, you don't know how they conducted the study. You don't know what questions they asked, or what they screening process was, or anything. You're just making up objections, and deciding without any factual evidence that they apply.
 S1:2-Yes, that's the point. I'd prefer to know the specifics. Questioning a study is a VERY appropriate thing to do in any study, particularly if it's used to vilify a group of people.
 S2:2-So then question, don't just state that the study has a problem. And "vilify"? Good grief! Where did you get that from? You seem to be taking this a bit too much to heart. A study was done, and done well from all evidence that I've seen, and the results reported. No one is vilifying anyone. If you can show an actuall problem with the study, and not something hypothetical that you came up with, than please do so. It seems quite professionally done to me, but you go ahead and dig all you want. But if you're simply uncomfortable with the results, well, that 's no reason to go around saying that people aren't doing their jobs or are out to vilify others.
 S1:13-Er . . . that's why I pointed out the specific things I saw as a problem. Are you in a bit of a reactionary mood today?  Yeah, because in my experience in these forums, here and elsewhere, an enormous amount of attention is given to trying to make Christianity look bad, through generalizations, citing irrelevant studies, and misrepresentations. See, the divorce rate of Christians has NO bearing whatsoever on the gay marriage amendment. Or did you forget that?
 S2:13-No more than usual. :) You, however, are stating that some is a problem, as opposed to possibly a problem. As if you wanted to simply dismiss the results, rather than actually check their validity. That's pretty reactionary. As long as people say that they're trying to protect the "sanctity" of marriage by keeping gays out it's extremely relevant, or at least a very good refutation. """
    
    sentence_list_edu=""
    add_author_edu_list(sentence_list_edu,testdialog.strip())