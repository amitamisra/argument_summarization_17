'''
Created on Apr 5, 2017

@author: amita
'''

from sklearn.cross_validation import cross_val_score
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.pipeline import Pipeline
from sklearn.svm import LinearSVC

from math import sqrt
import os, codecs

from sklearn import cross_validation 
from sklearn import preprocessing
from sklearn import svm
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from transformers import Word2VecTransformer
from transformers import ColumnExtractor
from sklearn.pipeline import Pipeline, FeatureUnion

def create_feature_pipeline(param_dict_regression,textcol):
    feature_transformer_list=[]
    
    featureList=param_dict_regression["featureList"]
    if "ngram" in featureList:
            ngram_tup=("ngram",Pipeline([
                  ('column_extractor', ColumnExtractor()),
                  ('ngram_vectorizer', CountVectorizer()),
                ]))
            feature_transformer_list.append(ngram_tup)
            
    if "ngram_tfidf" in featureList:
        ngram_tfidf=    ('ngrams_tfidf', Pipeline([
                  ('column_extractor', ColumnExtractor(textcol)),
                  ('tfidf_vectorizer', TfidfVectorizer()),
                ]))
        feature_transformer_list.append(ngram_tfidf)  
    if "w2vec" in   featureList:
        w2vec_loc=param_dict_regression["w2vec_loc"]
        w2vec_transformer=("w2vec",Word2VecTransformer(w2vec_loc,textcol))
        feature_transformer_list.append(w2vec_transformer)
    feature_pipeline=('features',FeatureUnion(feature_transformer_list))    
    return feature_pipeline

if __name__ == '__main__':
    pass