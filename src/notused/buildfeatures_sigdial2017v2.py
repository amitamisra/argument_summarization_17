'''
Created on Mar 20, 2017
build feature files and do clustering
Input : File obtained after running prepare_data_original_dialog_summary 
Corenlp column: Acces corenlp information

Before you run this uncomment  #df["sent_with_coref"]=addcorefsent(df,input_file) 
and create a file with column sent_with_coref. It is very slow otherwise
@author: amita
'''
import numpy as np
import copy
import random
np.random.seed(1)
random.seed(1)
import time
from collections import ChainMap
import os
import pandas as pd
import configparser, logging
#from collections import ChainMap
from word2vec_extractor import Word2vecExtractor
from timeit import default_timer as timer
from src import file_utilities
from pdtb_extractor import PDTBExtractor
from  word2vec_extractor import Word2vecExtractor
import createclasslabel_balance
import ast
import readability_metrics
import nltk
import dialogue_act_classifier
import liwc_extractor
from multiprocessing import Pool
logger = logging.getLogger()
handler = logging.StreamHandler()
formatter = logging.Formatter(
        '%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.INFO)
#import skip_thought_vectors_py3.addskipthought as addskipthought
logging.basicConfig(level=logging.CRITICAL)

logger = logging.getLogger(__name__)


features=["liwc","w2vec","readability"]
#features=["liwc"]

global w2vobject
global customdoc2vec

pdtb_loc="/Users/amita/software/pdtb-parser/"

def loadresourcesrequired(param_dict):
    pass
    
num_partitions = 10 #number of partitions to split dataframe
num_cores = 4 #number of cores on your machine


def addw2vecfeatures(sentence,w2vobject):
        feature_dictw2vec=w2vobject.extract(sentence)
        return feature_dictw2vec


def addpdtbfeatures(PdtbObject,row,sentence,sent_no):
    pdtb_feature_dict=PdtbObject.extract(row,sentence,sent_no)
    return pdtb_feature_dict

def addreadability(sentence):
    read_scores={'read_Kincaid':0,'read_ARI':0,'read_Coleman-Liau':0,'read_FleschReadingEase':0,"read_GunningFogIndex":0,"read_LIX":0,"read_SMOGIndex":0,"read_RIX":0}
    words=nltk.word_tokenize(sentence)
    if len(words) >1:
        read_scores=readability_metrics.getmeasures(sentence)
    return read_scores


def addLIWCscores(row,sent_col,dialog):
    liwc_features={}
    
    prev_sent_dict={}
    liwc_extractor_object=liwc_extractor.LIWCextractor()
    sent_no=int(row["sent_no"])
    sentence=row[sent_col]
    liwc_sent_dict=liwc_extractor_object.score_text(sentence)
    #logger.info(liwc_sent_dict)
    for key,value in liwc_sent_dict.items():
        liwc_features["liwc_current"+key]=value
    key=row["Key"]
    #logger.info("adding features for liwc for key {0}".format(key))
    #logger.info(sent_no)
    if sent_no > 0:
        prev_sent_no=sent_no-1
        
        #dialog=copydf.loc[copydf["Key"]==key]
        dialog.sent_no = dialog.sent_no.astype(int)
        prev_sentence=dialog[dialog["sent_no"]==int(prev_sent_no)][sent_col].values[0]
        prev_sent_dict=liwc_extractor_object.score_text(prev_sentence)
        for key,value in prev_sent_dict.items():
            liwc_features["liwc_prev"+key]=value
    return liwc_features
         
    
    
def adddialogscores(row,sent_col,dialog,actobject):
    #start = timer()
    dialogfeatures={}
    sent_no=int(row["sent_no"])
    key=row["Key"]
    
    logger.info("adding features for dialog act for key {0}".format(key))
    #logger.info(key)
    #logger.info(sent_no)
    if sent_no > 0:
        prev_sent_no=sent_no-1
        
        #dialog=copydf.loc[copydf["Key"]==key]
        #dialog.sent_no = dialog.sent_no.astype(int)
        sentence=dialog[dialog["sent_no"]==int(prev_sent_no)][sent_col].values[0]
        act_type=actobject.DialogueActTaggerNLTK.run_dialogue_act_tagger(sentence)
        dialogscore=dialogue_act_classifier.createactdictionary(act_type)
    else:
        dialogscore=dialogue_act_classifier.createactdictionary("none")  
    
    for key,value in dialogscore.items():
        dialogfeatures["dialog_act"+key]=value
        
    
    #end = timer()
    #print("feature generation for {0}took".format(str("dialogact")))
    return dialogfeatures     
    

def addauthor_initiate(row):
    sent_no=row["sent_no"]
    corenlp_author_dict= ast.literal_eval(row["corelp_author_dict"])
    author_initiate=ast.literal_eval(row["corelp_author_dict"])["corenlp_info"][sent_no]['Author_turn']
    sents=len(ast.literal_eval(row["corelp_author_dict"])["corenlp_info"])
    parts=sents/4.0
    pos=sent_no%parts            
    author_info={}
    if author_initiate:
        author_info["author_turn"]=1
        author_info["part_dialog"]=pos
    else:
        author_info["author_turn"]=0
        author_info["part_dialog"]=pos
    return author_info     

      
                 
def applyfeatures(row,W2vecobject,sent_col,copydf,actobject):
    liwc_scores={} 
    read_scores={}
    w2vec_scores={}
    dialog_scores={}
    
    sentence=row[sent_col]
    sent_no=row["sent_no"]
    if "w2vec" in features:
        w2vec_scores=addw2vecfeatures(sentence,W2vecobject)
        
    if "pdtb" in features:
        pdtb_dict=row["pdtb"]
        pdtb_sentence=row[sent_col]
        PdtbObject=PDTBExtractor(pdtb_loc)#pdtb_loc is the location of the jar
        pdtb_vec_scores=addpdtbfeatures(PdtbObject,row, pdtb_sentence,sent_no)
        
    if "authorinfo" in features:
        authorschange=addauthor_initiate(row)
    
    if 'readability' in features: 
        read_sentence=row[sent_col]            
        read_scores=addreadability(read_sentence)
    
    if "dialog_act" in features:
        dialog_scores=adddialogscores(row,sent_col,copydf,actobject)
    
    if "liwc" in features:
        liwc_scores=addLIWCscores(row,sent_col,copydf)   
           
        
        
    z= dict(ChainMap(w2vec_scores,read_scores,dialog_scores,liwc_scores))
    #z= dict(ChainMap(dialog_scores))

    #print (z)
    return pd.Series(z)
    
    
def parallelize_dataframe(df, func,W2vecObject,sentcolumn,copydf,actobject):
    df_split = np.array_split(df, num_partitions)
    pool = Pool(num_cores)
    df = pd.concat(pool.map(func(W2vecObject,sentcolumn,copydf,actobject), df_split))
    pool.close()
    pool.join()
    return df

def parallelfunc(df,W2vecObject,sentcolumn,copydf,actobject):
    newdf=df.apply(applyfeatures,args=(W2vecObject,sentcolumn,copydf,actobject),axis=1)
    return newdf
            
def extractfeatures(input_file,features_file,sentcolumn,w2vec_loc,tierlabelcol,mintierweight):
    alldf=[]
    """dialogdf : all sentences for one dialog
    "KEY: unique key for dialogue
    generate features for one dialogue, one at a time
    """
    if "w2vec" in features:
        W2vecObject=Word2vecExtractor(w2vec_loc)   
    else:
        W2vecObject=""    
    
    if "dialog_act" in features:
        actobject=dialogue_act_classifier.load_dialogue_act() 
        if actobject:
            print("succesfully loaded dialog act tagger")
    else:
        actobject=None                                  
    logger.info("inputfile with all the documents{0}".format(input_file))   
    df=pd.read_csv(input_file+".csv")
    #df["sent_with_coref"]=addcorefsent(df,input_file) 
    start = timer()
    
    dialog_gps=df.groupby("Key")
    for key,dialogdf in dialog_gps:
        copydialogdf=copy.deepcopy(dialogdf)
        newdf=dialogdf.apply(applyfeatures,args=(W2vecObject,sentcolumn,copydialogdf,actobject),axis=1)
        newdf.fillna(0,inplace=True)
        merged_df= df.join(newdf)
        alldf.append(merged_df)
        
    merged_df=pd.concat(alldf)    
    end = timer()
    print("feature generation for {0}took".format(str(features)))
    print(end - start)  
    balanced_df,classname=createclasslabel_balance.balance_classlabel(merged_df,sentcolumn,tierlabelcol,mintierweight)  
    dirname=os.path.dirname(features_file)
    bal_dir=  os.path.join(dirname,"balancedclean/")
    features_file_base=os.path.basename(features_file)
    if not  os.path.exists(bal_dir):
        os.makedirs(bal_dir)
    bal_file=os.path.join(bal_dir,features_file_base+"balance.csv")
    merged_df.to_csv(features_file+".csv")
    balanced_df.to_csv(bal_file)


    
def run(section):
    args= readCongigFile(section)
    inputfile=args[0]
    features_file=args[1]
    sentcolumn=args[2]
    w2vec_loc=args[3]
    tierlabelcol=args[4]
    mintierweight=args[5]
    extractfeatures(inputfile,features_file,sentcolumn,w2vec_loc,tierlabelcol,mintierweight)   
      

def readCongigFile(section):
    config = configparser.ConfigParser()
    config_file=file_utilities.get_absolutepath_data("config","buildfeatures_Config.ini")
    config.read(config_file)
    sentcolumn=config.get(section,'text')
    tierlabelcol=config.get(section,'tierlabelcol')
    mintierweight=config.get(section,'mintierweight')
    w2vec_loc=config.get(section,'w2vec_loc')
    input_file=config.get(section,"input_file")
    feature_file=config.get(section,'feature_file')
    
   
    #input_file=file_utilities.get_absolutepath_data(input_file_config)
    #feature_file=file_utilities.get_absolutepath_data(feature_file_config) 
    arguments=(input_file,feature_file,sentcolumn,w2vec_loc,tierlabelcol,mintierweight,pdtb_loc)

    return  (arguments)


if __name__ == '__main__':
    
#     section="test"
#     run(section)
    section="GCtrain" # not balanced
    run(section)
    section="GCtest"
    run(section)
    section="GCtrainCoref"
    run(section)
    section="GCtestCoref"
    run(section)