'''
Created on Mar 15, 2017

@author: amita
'''

import pandas as pd,os,sys
import subprocess


def clausie(file_with_path,outputdir,clausie_sh_location):
    arg1=clausie_sh_location
    arg2="-vf"
    
    #args.append(file_with_path)
    #args.append(outputfile)
    basename=os.path.basename(file_with_path)
    outputfile=os.path.join(outputdir,basename)
    cmd=str(clausie_sh_location+" "+"-f " + file_with_path + " -o " + outputfile)
    print(cmd)
    proc=subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    output, errors=proc.communicate()
    if proc.returncode != 0:
            print("clausie Failed {0} {1} {2}".format( proc.returncode, output,errors))
    

    
    
def run(inputdir,outputdir,clausie_sh_location):
    file_list=os.listdir(inputdir)
    for filename in file_list:
        if str(filename).endswith(".txt"):
            file_with_path=os.path.join(inputdir,filename)
            clausie(file_with_path,outputdir,clausie_sh_location)
        
            
if __name__ == '__main__':
    inputdir="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gun-control/namas"
    outputdir="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gun-control/clausie"
    clausie_sh_location="/Users/amita/software/clausie/clausie.sh"
    run(inputdir,outputdir,clausie_sh_location)
    
