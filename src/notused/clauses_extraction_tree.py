'''
Created on Mar 15, 2017

@author: amita
'''
import pandas as pd
import nltk
from nltk import Tree
import os

def sbar_extraction(row,columnname,filename):
    edu_core_nlp=row[columnname]
    core_nlp_info= edu_core_nlp["corenlp_info"]
    key=row["Key"]
    sent_count=0
    for sent in core_nlp_info:
        syntax_tree=sent["syntactictree"]
        tree= nltk.Tree.fromstring(syntax_tree)
        basename=os.path.basename(filename)
        dirname=os.path.dirname(filename)
        outbasedir=os.path.join(dirname,"syntaxtree",key)
        if not os.path.exists(outbasedir):
            os.makedirs(outbasedir)
        outputfile=os.path.join(outbasedir,"sent_"+str(sent_count))
        sent_count=sent_count+1
        with open(outputfile,"w",encoding="utf-8") as f:
            
            tree.pretty_print(stream=f)        
    
    
def run(inputjsonfile,columnname):
    df=pd.read_json(inputjsonfile)
    df.apply(sbar_extraction,args=(columnname,inputjsonfile),axis=1)
    
if __name__ == '__main__':
    inputjson="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gun-control/NaturalSummaryCSV_authorformat_all_pyramids_scus_author_edu_corenlp.json"
    columnname="edu_corelp"
    run(inputjson,columnname)