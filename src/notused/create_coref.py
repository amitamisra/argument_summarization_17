'''
Created on Apr 7, 2017

@author: amita
'''
import os
randomseed=4
import pandas as pd
from stanford_parser_arg import filtersentence
import stanford_nlp_features
import  configparser
import file_utilities


def testcorefworked(output_coref):
    df=pd.read_csv(output_coref)

def addcorefsent(input_file):
        df=pd.read_csv(input_file+".csv")
        all_dfs=[]
        dialog_groups=df.groupby("Key")
        for key, dialogdf in dialog_groups:
                  #---------------------------------------------------- continue
            sent_count=len(dialogdf["sent"].tolist())
            dialog= " ".join(dialogdf["sent"].tolist())
            res=stanford_nlp_features.parse_text(dialog, mode=stanford_nlp_features.Mode.SMART_COREF)
            doc_dict=stanford_nlp_features.parse_res_processor(res=res)
            allsentences_dialog_dict=doc_dict["corenlp_info"]
            allsents_dialog=[[" " .join(d['snlp_tokens']) for d in  allsentences_dialog_dict]][0]
            
            
            
            if str(key) == "1-9615_38_37__41_44_46_47_51_1":
                newallsents_dialog=allsents_dialog[:20]+ allsents_dialog[21:]
                allsents_dialog=newallsents_dialog
            else:
                if str(key)=="1-9235_99_98__100_101_102_103_4":
                    sent29=allsents_dialog[29][:-2] 
                    list29=[]
                    list29.append(sent29)
                    newallsents_dialog=allsents_dialog[:29] + list29 + allsents_dialog[31:]
                    allsents_dialog=newallsents_dialog
                else:
                    if str(key)=="1-10121_22_21__23_28_30_34_37_1":
                        newallsents_dialog=allsents_dialog[:3]+ allsents_dialog[6:]
                        allsents_dialog=newallsents_dialog    
                    else:
                            
            
                        if str(key)=="1-173_68_67__69_70_74_77_79_81_87_1":
                            sent16=allsents_dialog[16][:-2]
                            sent16=sent16+ " "+ allsents_dialog[17:18][0]
                            list16=[]
                            list16.append(sent16)
                            newallsents_dialog=allsents_dialog[:16] + list16 + allsents_dialog[18:]
                            allsents_dialog=newallsents_dialog
                            
                        else:
                            if str(key)=="1-10073_119_44__120_124_127_129_130_131_132_133_134_6":
                                newallsents_dialog=allsents_dialog[:36] + allsents_dialog[37:]
                                allsents_dialog=newallsents_dialog
                                print()
                            else:    
                                if str(key) == "1-6229_52_51__55_56_61_62_3":
                                    newallsents_dialog=allsents_dialog[:37] + allsents_dialog[38:]
                                    allsents_dialog=newallsents_dialog 
                                else:
                                    if str(key)=="1-6646_150_144_1_151_152_154_156_161_11":
                                        str12=allsents_dialog[12][:-1]+ allsents_dialog[13]
                                        str16=allsents_dialog[16][:-1]+ allsents_dialog[17]
                                        newlist12=[str12]
                                        newlist16=[str16]
                                        newallsents_dialog=allsents_dialog[:12]+ newlist12+ allsents_dialog[14:16]+ newlist16+allsents_dialog[18:]
                                        allsents_dialog=newallsents_dialog  
                                    
                                    else:
                                        if str(key)=="1-9391_125_113__126_136_140_142_154_2":
                                            list22=list()
                                            list22.append(".")
                                            list22.append("they are using you !")
                                            list22.append("Talk about pervy - am I to assume you have been peeping on my life ?")
                                            list22.append("And just who the hell are '' they '' ?")
                                            newallsents_dialog=allsents_dialog[:22]+list22+ allsents_dialog[25:]
                                            allsents_dialog=newallsents_dialog
                                            
                                            
            sent_countwithcoref=len(allsents_dialog)
            if sent_count ==sent_countwithcoref:
                dialogdf["sent_coref"]=allsents_dialog
                all_dfs.append(dialogdf)
            else:
                print(key)
                print("sent count does not match, error, terminating without creating coref file")
                os.sys.exit()
                
            
        dfwithcoref=pd.concat( all_dfs)   
        basefile=os.path.basename(input_file)
        dirname=os.path.dirname(input_file)
        coref_filename=os.path.join(dirname,basefile+"_corefsent.csv")
        dfwithcoref.to_csv(coref_filename)
        return dfwithcoref,coref_filename  


def clean(uncleandf,textcol):
    allrows=[]
    for index, row in uncleandf.iterrows():
        sentence=row[textcol]
        add=filtersentence(sentence,3)
        if add:
            allrows.append(row)
    clean_df=pd.DataFrame(allrows)  
    return clean_df       
            
    
    
    



def splittraintest(input_coref):
    merged_df=pd.read_csv(input_coref+".csv")
    df_dialogs=merged_df.groupby("Key")
    trainlist=[]
    testlist=[]
    count=0
    for key, df in df_dialogs:
        if count< 13:
            testlist.append(df)
        else:
            trainlist.append(df)    
        count=count+1    
        
    traindf=pd.concat(trainlist)
    testdf=pd.concat(testlist)
    traindf.to_csv(input_coref+"train.csv",index=False)
    testdf.to_csv(input_coref+"test.csv",index=False)

def run(section):
    if section=="GCcoref":
        config = configparser.ConfigParser()
        config_file=file_utilities.get_absolutepath_data("config","create_coref_cleanConfig.ini")
        config.read(config_file)
        inputfile=config.get(section,"inputfile")
        outputcoref=config.get(section,"outputcoref")
        if os.path.exists(outputcoref):
            os.remove(outputcoref)
        df_coref,output_coref=addcorefsent(inputfile)
    else:
        if section=="split":    
            config = configparser.ConfigParser()
            config_file=file_utilities.get_absolutepath_data("config","create_coref_cleanConfig.ini")
            config.read(config_file)
            output_coref=config.get(section,"outputcoref")
            splittraintest(output_coref)
    
    
if __name__ == '__main__':
    
    #section="GMCoref"
    #run(section)
    
    
    section="GCcoref"
    run(section)
    #section='split'
    #run(section)