'''
Created on Nov 10, 2016
writes name of one txt file per line in another file for rsr_peng input
 rsr_peng : expects input: each file name is on a separate line
@author: amita
'''
import file_utilities,file_handling
import os
def run(topic):
    dialog_dir=file_utilities.get_absolutepath_data("data_database",topic,"dialog_dir")
    fileinput_rst_feng=os.path.join(os.path.dirname(dialog_dir),topic+"_rst_feng_input.txt")
    filelist=os.listdir(dialog_dir)
    allfiles=[]
    for filename in filelist:
        #if str(filename).endswith("1-6124_36_32__38_42_51_55_1.txt"):
            #continue
        filewithpath=os.path.join(dialog_dir,filename)
        allfiles.append(filewithpath+"\n")
    file_handling.writeTextFile(fileinput_rst_feng,allfiles)
if __name__ == '__main__':
    topic="gay-rights-debates"
    #run(topic)
    topic="gun-control"
    run(topic)