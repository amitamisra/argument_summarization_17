'''
Created on Oct 3, 2016

@author: amita
'''
import sys,os,re
import pandas as pd
from db_connect import setup_connection
import db_functions
import file_utilities

ignorekey=["1-6646_268_261_1_272_274_289_290_291_293_294_13",]

def createdialogfiles(sql_session,inputcsv,outputcsv,dataset):
    """
    #input csv contains a key field, we parse that field to obtain dataset, discussion and postids for a dialog
    #structure of key datasetid_discussionid_1,postid2,postid.....dialognumber
    #there were several dialogs in a discussion
    """
    allrows=[]
    df=pd.read_csv(inputcsv)
    if "key" in df.columns:
        dialogkey="key"
    else:
        if "Key" in df.columns:
            dialogkey="Key"    
    keys=df[dialogkey].values
    for key in keys:
        rowdict=dict()
        keyseq=key.split("_")
        if len(keyseq[-1])>1:
            print(key)
            break
        (dataset,discussion)=keyseq[0].split("-")
        datasetid=dataset
        discussionid=int(discussion)
        if int(dataset)!= 1:
            #print("dataset not matching fourforums, exiting")
            sys.exit(-1)
        else:
            rowdict["datasetid"]=datasetid
            rowdict["discussonid"]= discussionid
            postidlist=[postid for postid in keyseq[1:-1]]
            postidlist=[int(x) for x in postidlist if x]
            postidlist=sorted(postidlist)           
            rowdict["postidlist"]=postidlist
            (S1,S2,alltext_noauthor, alltext_author_format,alltext_author_no_format)=addpostdetailsfromdb(sql_session,datasetid,discussionid,postidlist)
            rowdict["Dialog_from_database_author_format"]=re.sub(r"\bemoticon\w+", "", alltext_author_format)
            rowdict["Dialog_from_database_emoticon"]= alltext_author_no_format
            rowdict["Dialog_from_database_author_noformat"]=re.sub(r"\bemoticon\w+", "",alltext_author_no_format)
            rowdict["Author1"]=S1
            rowdict["Author2"]=S2
            rowdict["Dialog_from_database_noauthor"]=re.sub(r"\bemoticon\w+", "",alltext_noauthor)
            rowdict["key"]=key
            allrows.append(rowdict)
            
    dfwrite=pd.DataFrame(allrows)
    if "Summary" in df.columns:
        dfwrite["Summary"]=df["Summary"]
    else:
        if "Natural_Summary_Text" in df.columns:
            dfwrite["Natural_Summary_Text"]=df["Natural_Summary_Text"]
    if "Dialog" in df.columns:        
        dfwrite["Dialog_shownfor_summary"]      =df["Dialog"] 
    else:
        if "Dialogtext" in df.columns:
              dfwrite["Dialog_shownfor_summary"]      =df["Dialogtext"]    
    
    dfwrite.to_csv(outputcsv,index=False)
        


    

def addpostdetailsfromdb(sql_session,dataset_id,discussion_id,post_idseq):
    """
    # return author1,author2,
   
    alltext_noauthor:dialogtext without author
    alltext_author_format:dialogtext with author information and formatted
    alltext_author_no_format:dialogtext with author noformatting
    """
    count=1
    author=1
    
    alltext_noauthor=""
    alltext_author_no_format=""
    alltext_author_format=""
    
    for post_id in post_idseq:
        text=db_functions._get_post_text(sql_session, dataset_id, discussion_id, post_id)
        #text = os.linesep.join([s for s in text.splitlines() if s])#remove blank lines
        text=re.sub('\s+',' ',text)
        alltext_noauthor=alltext_noauthor + " "+ text +"\n"
        
        author_id=db_functions._get_author_post(sql_session, dataset_id, discussion_id, post_id)
        
        text_author_format="<br><b> Author{0}:{1}- </b>{2}".format(author,count,text)
        text_author_no_format=" Author{0}:{1}- {2} ".format(author,count,text)
        
        alltext_author_no_format=alltext_author_no_format+ text_author_no_format
        
        alltext_author_format= alltext_author_format+ " "+ text_author_format +"\n"
        
        if author==2:
            S2=author_id
            author=1
            count=count+1
        else:
            if author==1:    
                S1=author_id
                author=2
                
        
    return(S1,S2, alltext_noauthor, alltext_author_format,alltext_author_no_format)    
       
def createcsv(sql_session,inputcsv,output_database_csv):
    createdialogfiles(sql_session,inputcsv,output_database_csv,1)
    
def run(topic,input_withpath):  
    """
    return the name of output file created with dialogs, without extension
    """   
    data_dir=file_utilities.DATA_DIR  
    output_dir=os.path.join(data_dir,"data_database",topic)
    outputcsv_name=os.path.basename(input_withpath)
    outputcsv=os.path.join(output_dir, outputcsv_name +"_database.csv")
    if not os.path.exists(os.path.dirname(outputcsv)):
        os.makedirs(os.path.dirname(outputcsv))
    
    _, sql_session = setup_connection()
    inputcsv=input_withpath +".csv"
    createdialogfiles(sql_session,inputcsv,outputcsv,1)
    return outputcsv[:-4]
    
    
if __name__ == '__main__':
    data_dir=file_utilities.DATA_DIR 
    
    topic="gay-rights-debates" 
    inputfile="dialogdata,CSV,gay-rights-debates,Phase_1_2,AllDialogs_phase_1_2_Formatted"
    input_withdir=os.path.join(*inputfile.split(","))
    input_withpath=os.path.join(data_dir,input_withdir)
    #run(topic,input_withpath) #done for gay marriage
    
    topic="gun-control" 
    inputfile="dialogdata,CSV,gun-control,MTdata,MTSummary,AllMTSummary_50,NaturalSummaryCSV"
    input_withdir=os.path.join(*inputfile.split(","))
    input_withpath=os.path.join(data_dir,input_withdir)
    run(topic,input_withpath) #done for gun control
    
    topic="evolution"
    inputfile="dialogdata,CSV,evolution,MTdata,MTAll,Dialog_File_midrange"
    input_withdir=os.path.join(*inputfile.split(","))
    input_withpath=os.path.join(data_dir,input_withdir)
    run(topic,input_withpath)
    inputfile="dialogdata,CSV,evolution,MTdata,MTAll,Dialog_File_more750"
    input_withdir=os.path.join(*inputfile.split(","))
    input_withpath=os.path.join(data_dir,input_withdir)
    #run(topic,input_withpath)
    
    topic="abortion"
    inputfile="dialogdata,CSV,abortion,MTdata,MTAll,Dialog_File_midrange"
    input_withdir=os.path.join(*inputfile.split(","))
    input_withpath=os.path.join(data_dir,input_withdir)
    #run(topic,input_withpath)
    
    
    inputfile="dialogdata,CSV,abortion,MTdata,MTAll,Dialog_File_more750"
    input_withdir=os.path.join(*inputfile.split(","))
    input_withpath=os.path.join(data_dir,input_withdir)
    #run(topic,input_withpath)