'''
Created on Nov 5, 2016
merge pyramid labels obtained by running scores_pyramid and baseline dialog 
@author: amita
'''
import pandas as pd,os
import file_utilities
#------------------------------------------------------------------------------ 
#-------------------------------------------------------- class Pyramid_details:
    #------ def __init__(self,contributors,contrib,scuid,key_user,label,weight):
        #-------------------------------------------------- self.contrib=contrib
        #----------------------------------------------------- self.scuid =scuid
        #------------------------------------------------ self.key_user=key_user
        #------------------------------------------------- self.label=self.label
        #---------------------------------------------------- self.weight=weight
    #------------------------------------ def tupleid_contrib_label(self,list_):
        
            
        
def row_eachkey(dfpyramidlabels):
    """
    input:dataframe with all scus
    return dataframe, with one row per key/dialog
    """
    allrows=[]
    grouped_key=dfpyramidlabels.groupby("key_user")
    for key,group_key in grouped_key :
        rowdict={}
        grouped_weight=group_key.groupby("weight")
        for weight,dfvalues in grouped_weight:
            rowdict["labels with weights"+str(weight)]=dfvalues["label"].values
        rowdict["key"]=key.split("user")[0][:-1]
        allrows.append(rowdict)     
        #contrib=group["contrib"]
    return pd.DataFrame(allrows) 

   
def run(topic,input_withbaseline): 
    baselinedialog=input_withbaseline+".pkl"
    basename=os.path.basename(input_withbaseline)
    pyramidlabels=file_utilities.get_absolutepath_data("data_database",topic,"all_pyramids_scus","AllScus.pkl")
    baseline_scu=file_utilities.get_absolutepath_data("data_database",topic, basename+"_all_pyramids_scus_baseline")
    dfbaseline=pd.read_pickle(baselinedialog)
    dfpyramidlabels=pd.read_pickle(pyramidlabels)
    df_row_perkey=row_eachkey(dfpyramidlabels)
    mergeddf=pd.merge(dfbaseline,df_row_perkey, on="key",how="inner",suffixes=('_baseline_key', '_pyramid_key'))
    mergeddf.to_csv(baseline_scu+".csv")
    mergeddf.to_pickle(baseline_scu+".pkl")
    return baseline_scu
if __name__ == '__main__':
    topic="gay-rights-debates"
    input_withbaseline="/Users/amita/git/argument_summarization_17/data/data_database/gay-rights-debates/AllDialogs_phase_1_2_Formatted_database_baseline"
    run(topic,input_withbaseline)
    
    topic="gun-control"
    input_withbaseline="/Users/amita/git/argument_summarization_17/data/data_database/gun-control/NaturalSummaryCSV_database_baseline"
    run(topic,input_withbaseline)