'''
Created on Mar 15, 2017

@author: amita
'''
import pandas as pd
import nltk
from nltk import Tree
import os,itertools

def createpairs(row,columnname,filename):
    edu_core_nlp=row[columnname]
    rst_info_list=list(row["edu_corelp"]["rst_info"])
    edu_list=[rst_info["rawtext"] for rst_info in rst_info_list]
    key=row["Key"]
    scu_info=row["scu_info"]
    keylist = list(scu_info.keys())
    #print(keylist)
    keylist=[int(x) for x in  keylist]
    keylist.sort()     
    label_list=[scu_info[str(scu_no)]["label"] for scu_no in keylist]
    combinations=list(itertools.product(edu_list,label_list))
    allcoms=[]
    for  comb in combinations:
        comb_dict={}
        comb_dict["edu"]=comb[0]
        comb_dict["label"]=comb[1]
        allcoms.append(comb_dict)
        
    dirname=os.path.dirname(filename)
    outbasedir=os.path.join(dirname,"edu_label_sim")
    if not os.path.exists(outbasedir):
            os.makedirs(outbasedir)
    output_file=os.path.join(outbasedir,key+".csv")        
    df=pd.DataFrame(allcoms)       
    df.to_csv(output_file) 

                

def run(inputjsonfile,columnname):
    df=pd.read_json(inputjsonfile)
    df.apply(createpairs,args=(columnname,inputjsonfile),axis=1)
    
if __name__ == '__main__':
    inputjson="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gun-control/NaturalSummaryCSV_authorformat_all_pyramids_scus_author_edu_corenlp.json"
    columnname="edu_corelp"
    run(inputjson,columnname)