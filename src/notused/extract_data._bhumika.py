'''
Created on Nov 2, 2016

@author: bhumi
'''
import pandas
from nltk.tokenize import sent_tokenize
from string import punctuation
import configparser
import csv 
from fuzzywuzzy import fuzz

    

def readFile(section):
    config = configparser.ConfigParser()
    config.read('arg_summ_data_Config.ini')
    test_file=config.get(section,'test_file')
    training_2015=config.get(section,'training_2015')
    training_2016=config.get(section, 'training_2016')
    
    
    test_data = pandas.read_csv(test_file)
    training_data15 = pandas.read_csv(training_2015)
    training_data16 = pandas.read_csv(training_2016)
    
    
    (comparing(test_data, (list(training_data15['Phrase.x'])))).to_csv('after_removing15.csv')
    (comparing(test_data, (list(training_data16['sentence'])))).to_csv('after_removing16.csv')
    (comparing(test_data, (list(training_data15['Phrase.x'])) + (list(training_data16['sentence'])))).to_csv('after_removing_both')
    
    
    
        
def comparing(test_data, training_list):
    
    dialog_list = list(test_data['Dialog_noauthor'])

    this_index = 0
    for dialog in dialog_list:
        
        flag = 0
        prep_dialog = preprocess(dialog)
        for sent in prep_dialog:
            for train in training_list:
                train = preprocess(train)
                if(fuzz.ratio(sent,train)>90):
                    flag = 1
        
    
        if flag == 1:
            print 2
            test_data = test_data.drop(test_data.index[this_index])
        this_index = this_index + 1   
    return(test_data)
        
        
def preprocess(dialog_list):
    new_dialog = []
    tokenized_dialog = sent_tokenize(dialog_list)
    for dialog in tokenized_dialog:
        new_dialog.append(''.join(c for c in dialog if c not in punctuation))
    return(new_dialog)
    
    
section="new_data"
readFile(section)