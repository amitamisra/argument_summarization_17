'''
Created on Nov 7, 2016
change the dialog text to create an input for the discourse parser. It expects one sentence per 
line and with a prefix <s> and suffix </s>
Sample Input
This is sample input. It has to be changed.
Sample Output
<s> This is sample input. </s> 
<s> It has to be changed. </s>

@author: amita
'''
import file_utilities
import file_handling
import os
from  nltk import sent_tokenize
from nltk import word_tokenize

def modifydialogfile(dialogtextfile,dialog_parser_input):
    allsent=[]
    dialog= file_handling.readTextFile(dialogtextfile)
    assert len(dialog)==1
    sent_list=sent_tokenize(dialog[0])
    for sent in sent_list:
        words=word_tokenize(sent)
        newsent= '<s> ' + sent + ' </s>' +"\n"
        allsent.append(newsent)
    file_handling.writeTextFile(dialog_parser_input,allsent)    
    
    
def modifydialogdir(dialog_dir,dialog_dir_withparser_input):
    """
    input:dialog_dir: containing original dialogs without s1 s2
    output: dialogs as required by parser
    """
    filelist=os.listdir(dialog_dir)
    for dialogfile in filelist:
        if dialogfile.endswith("1-35_148_147__149_151_152_155_156_1.txt"):
            dialog_withparser_input=os.path.join(dialog_dir_withparser_input,dialogfile[:-4]+"parserinput.txt")
            modifydialogfile(os.path.join(dialog_dir,dialogfile) ,dialog_withparser_input)
        
        
    
if __name__ == '__main__':
    topic="gay-rights-debates"
    dialog_dir=file_utilities.get_absolutepath_data("data_database",topic,"dialog_dir")
    dialog_dir_withparser_input=file_utilities.get_absolutepath_data("data_database",topic,"dialog_dir_withparser_input")
    if not os.path.exists(dialog_dir_withparser_input):
        os.mkdir(dialog_dir_withparser_input)
    modifydialogdir(dialog_dir,dialog_dir_withparser_input)