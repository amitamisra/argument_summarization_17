'''
Created on Feb 22, 2017

@author: amita
'''
import rst_processor
import file_utilities
import os,sys
import pandas as pd
import nltk

def checksentencelength(row,col):
    doc=row[col]
    sentences=nltk.sent_tokenize(doc)
    for sent in sentences:
        words=nltk.word_tokenize(sent)
        if len(words) > 95:
            print(sent)
            print("terminating as sentence length is greater than 95, processor may skip the sentence. Check input")
            sys.exit(-1)
            

def getallinforst_processor(text):
    """
    call rst_processor, parse text, returns corenlp and rst as java object
    """
    res=rst_processor.parse_text(text)
    return res
def parse_res_processor(res,key,outputdir_namas):
    """
    res " response object returned by getallinforst_processor, uses py4j and calls the rst_processor in Java
    """
    sentences_info=res["sentences_info"]
    alledus=[]
    namas_input_list=[]
    allsentences_corenlpinfo=[]
    for sentence in sentences_info:
        namas_input=sentence["tokens"] +"\n"
        namas_input_list.append(namas_input)
        sentence_dict={}
        sentence_dict["syntactictree"]=str(sentence["syntactictree"].toString())
        sentence_dict["pos"]=sentence["pos"]
        sentence_dict["lemmas"]=sentence["lemmas"]
        sentence_dict["deplist"]=list(dict(javamap) for javamap in sentence["deplist"])
        sentence_dict["tokens"]=sentence["tokens"]
        sentence_dict["normalized_entities"]=sentence["normalized_entities"]
        sentence_dict["start_offset"]=sentence["start_offset"]
        sentence_dict["end_offset"]=sentence["end_offset"]
        sentence_dict["sentence_no"]=sentence["sentence_no"]
        sentence_dict["named_entities"]=sentence["named_entities"]
        #sentence_dict["chunks"]=sentence["chunks"]
        allsentences_corenlpinfo.append(sentence_dict)
    rst_info= res["rst_info"]
    for edu_info in rst_info:
        edudict={}
        edudict["edu_no"]=edu_info["edu_no"] 
        edudict["rawtext"]=edu_info["rawtext"]  
        edudict["lasttoken"]=edu_info["lasttoken"]
        edudict["firsttoken"]=edu_info ["firsttoken"] 
        edudict["firsttokensentence"]=  edu_info["firsttokensentence"]
        edudict["lasttokensentence"]=edu_info["lasttokensentence"]
        if not edudict["firsttokensentence"] == edudict["lasttokensentence"]:
            print("sentence numbers not matching in edus in the below given edu, terminating")
            print("sentence Nos")
            print(edudict["lasttokensentence"])
            print(edudict["lasttokensentence"])
            print(edudict["rawtext"])
            sys.exit(-1)
        alledus.append(edudict)
    doc_dict={}
    doc_dict["rst_info"]=alledus
    doc_dict["corenlp_info"]=  allsentences_corenlpinfo
    doc_dict["edu_count"]=int(res["edu_count"])
    namas_file=os.path.join(outputdir_namas,key+".txt")
    with open(namas_file,"w",encoding="utf-8") as f:
        f.writelines(namas_input_list)
    return doc_dict

def applyparse(row,column,outputdir_namas):
    """
    apply the getallinforst_processor to column row_wise
    """
    text=row[column]
    key=row["Key"]
    nlp_rst=getallinforst_processor(text)
    parsed_info=parse_res_processor(nlp_rst,key,outputdir_namas)
    return parsed_info
    
    

def run(topic,inputfile,column):    
    data_dir=file_utilities.DATA_DIR  
    output_dir=os.path.join(data_dir,"data_originalsummaries",topic)
    input_base=os.path.basename(inputfile)
    output=os.path.join(output_dir,input_base+"_edu_corenlp")
    outputdir_namas=os.path.join(output_dir,"namas")
    if not os.path.exists(outputdir_namas):
        os.mkdir(outputdir_namas)
    inputpickle=inputfile +".pkl"
    df=pd.read_pickle(inputpickle)
    
    #df.apply(checksentencelength, args=(column,),axis=1)
    df["edu_corelp"]=df.apply(applyparse,args=(column,outputdir_namas),axis=1)
    df.to_csv(output+".csv",index=False) 
    df.to_json(output+".json")
    #df.to_pickle(output+".pkl")  
    
if __name__ == '__main__':
    pass
 #done for gun control