'''
Created on Nov 16, 2015

@author: amita
'''

import file_utilities
from liwc import wcc2015
class LIWC_Stance:
    def __init__(self):
        self.liwc_ignore = [
            'Dictionary Words','dic',
            'Total Pronouns',
            'Other Punctuation',
            'Articles',
            'Auxiliary Verbs',
            'Comma',
            'Personal Pronouns',
            'Dash',
            'Parenthesis',
            # 'Space', # this does nothing, score_word(' ') = empty Counter()
            'prep',
            'Anxiety', 'anx',
            'Anger', 'anger',
            'Sadness', 'sad',
            'Past Focus', 'focuspast',
            'Present Focus', 'focuspresent',
            'Future Focus', 'focusfuture',
            'Informal', 'informal',
            'Netspeak', 'netspeak',
            'Assent', 'assent',
            'Nonfluencies', 'nonflr',
            'Filler', 'filler',
            'Work', 'work',
            'Leisure', 'leisure',
            'Home', 'home',
            'Money' 'money',
            'Religion', 'relig',
            'Death', 'death',
            'Periods', 'period',
            'Words Per Sentence','WPS','wps',
            'Word Count','WC','wc',
            'Six Letter Words','Sixltr','sixltr',
            'Regular Verbs','Verb','verb',
            'All Punctuation','Allpunc','allpunc',
        ]


        self.liwcFile_abs =  file_utilities.load_resource("LIWC2015_English_Flat.dic")
        self.liwc_dict =     wcc2015.Dictionary(self.liwcFile_abs)
    
    def add_category_token(self,rowdict):
    # get categories of individual tokens
        for token_dict in rowdict['tokens']:
            token_dict['category_list'] = \
                [cat for cat in self.liwc_dict.score_word(token_dict['corrected_token']) if cat not in self.liwc_ignore]
