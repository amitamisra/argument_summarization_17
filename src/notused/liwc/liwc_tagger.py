"""
Created on Nov 16, 2015

@author: Amita and Brian
"""
from stance import file_utilities
from stance.liwc import wcc2015


class LIWCTagger:
    def __init__(self):
        self.liwc_ignore = {}

        self.liwc_dict = wcc2015.Dictionary(file_utilities.load_resource("LIWC2015_English_Flat.dic"))

    def add_categories(self, token_dict, token_key):
        """
        Get the liwc categories for a token
        @param token_dict: A token dictionary
        @param token_key: The key to the string token in the token dictionary
        @return:
        """
        token_dict["category_list"] = [cat for cat in self.liwc_dict.score_word(token_dict[token_key])
                                       if cat not in self.liwc_ignore]
