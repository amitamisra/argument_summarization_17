#!/usr/local/bin/python2.7
# encoding: utf-8
'''
src.merge_training_data -- shortdesc


'''

import sys
import os,configparser
import pandas as pd

def run(section):
    args= readCongigFile(section)
    pairs_file= args[0]
    gold_std=args[1]
    dir_path = os.path.dirname(os.path.realpath(__file__))
    data_dir=os.path.join(os.path.dirname(dir_path),"data")
    outfile=os.path.join(data_dir,"input.deft-forum.csv")
    createpairs_gold_standard(pairs_file,gold_std,outfile)

def createpairs_gold_standard(pairs_file,gold_std,outfile):
    df_pairs=pd.read_table(pairs_file, sep='\t', lineterminator='\n',header=None)
    df_gs=pd.read_csv(gold_std, sep='\t', lineterminator='\n',header=None)
    result = pd.concat([df_pairs, df_gs], axis=1, join='inner')
    result.columns=["sentence_1","sentence_2","regression_label"]
    result.to_csv(outfile,index=False)
    
    

def readCongigFile(section):
    config = configparser.ConfigParser()
    config.read('merge_training_data_Config.ini')
    pairs_file=config.get(section,'pairs_file')
    gold_std=config.get(section,'gold_std')
    arguments=(pairs_file,gold_std)
    return  (arguments)

if __name__=="__main__":
    section="merge"
    
    run(section)
    
    