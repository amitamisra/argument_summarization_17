'''
Created on Nov 29, 2016

@author: amita
'''
import os,sys
import pandas as pd
import file_utilities
import file_handling

def table_edu(row,edulistcolname):
    edus=list(row[edulistcolname])
    no_edus=len(edus)
    table= """<table border="">
    <colgroup>
        <col width="" />
        <col width="350" />
        <col width="" />
        <col width="350" />
        <col width="" />
        <col width="" />
    </colgroup>
    <tbody>
        <tr>
            <th>SNo</th>
            <th>Text</th>
            <th>SNo</th>
            <th>Text</th>
            <th>SNo</th>
            <th>Text</th>
        </tr>
    """
    rem=no_edus % 3
    if rem == 1 :
        edus.append("")
        edus.append("")
    else:
        if rem == 2:
            edus.append("")
            
    #print("no_edu"+ str(no_edus))          
    for count in range(0,no_edus,3):
        table=table +"<tr><td>" + str(count+1) +"</td>" +"<td>" +edus[count]+ "</td>"+"<td>"+ str(count+2) +"</td>"+"<td>"+ edus[count+1]+"</td>" +"<td>"+str(count+3)+"</td>"+"<td>"+ edus[count+2]+"</td></tr>"
    table=table+"</tbody></table>"
        
    return table

def table_label(weight,row):
    labels=list(row["labels with weights:"+str(weight)])
    table= """<table border="">
    <colgroup>
        <col width="" />
        <col width="350" />
        <col width="" />
        <col width="350" />
        <col width="" />
        <col width="" />
    </colgroup>
    <tbody>
        <tr>
            <th>SNo</th>
            <th>Text</th>
            <th>SNo</th>
            <th>Text</th>
            <th>SNo</th>
            <th>Text</th>
        </tr>
    """

def createMTrow_label(row):
    labels=list(row["labels with weights5"])+ list(row["labels with weights4"]) + list(row["labels with weights3"] )
    for i in range(0,len(labels)):
        row["label_"+str(i)]=labels[i]
    return row   
    
def run(topic,educol):
    topic_dir= file_utilities.get_absolutepath_data("data_database",topic) 
    pyramids_edufile=os.path.join(topic_dir,"all_pyramids_scus_baseline_edu.pkl")  
    pyramids_edu_formatted=os.path.join(topic_dir,"all_pyramids_scus_baseline_edu_table")    
    df= pd.read_pickle(pyramids_edufile)
    df = df.fillna('')
    df["html_edu"]=df.apply(table_edu,args=(educol,), axis=1)
    df=df.apply(createMTrow_label,axis=1)
    df.to_csv( pyramids_edu_formatted+".csv",index=False)
    df.to_pickle(pyramids_edu_formatted+".pkl")
if __name__ == '__main__':
    topic="gay-rights-debates"
    educol="edus"
    run(topic,educol)