'''
Created on Nov 28, 2016
Parse the feng_rst_edu, add author information and dump them as a pickle.
@author: amita
'''
import os,sys
import pandas as pd
import file_utilities
import file_handling

ignore_keys=["1-6124_36_32__38_42_51_55_1"]

def splitEDU(key,edus_dir):
    """
    key: unique dialog key
    edu_dir:edus after running rst_feng
    """
    alledus=[]
    if key in ignore_keys:
        return alledus,len(alledus)
    else:
        filename=key+".txt.edus"
        edufile=os.path.join(edus_dir,filename)
        sentences=file_handling.readTextFile(edufile)
        for sentence in sentences:
            edus=str(sentence).split("EDU_BREAK")
            stripedus=map(str.strip, edus)
            alledus.extend(stripedus)
        return alledus,len(alledus)    
    
def addEdustopyramids_baseline_labels(df_pyramids,edus_dir):
    """
    add two new columns to the dataframe
    
    """
    df_pyramids["edus"],df_pyramids["edus_len"]=zip(*df_pyramids.key.apply(splitEDU,args=(edus_dir,)) )
    return df_pyramids        

def run(topic,edus_dir):
    topic_dir= file_utilities.get_absolutepath_data("data_database",topic) 
    pyramids_baseline=os.path.join(topic_dir,"all_pyramids_scus_baseline.pkl")
    df=pd.read_pickle(pyramids_baseline)
    df_edus=addEdustopyramids_baseline_labels(df,edus_dir)
    pyramids_edufile=os.path.join(topic_dir,"all_pyramids_scus_baseline_edu")
    df_edus.to_csv(pyramids_edufile+".csv",index=False)
    df.to_pickle(pyramids_edufile+".pkl")
    
    
    
if __name__ == '__main__':
    topic="gay-rights-debates"
    edus_dir="/Users/amita/git/feng-hirst-rst-parser-acl-2014/texts/results/rst_output_edu_withg"
    run(topic,edus_dir)