'''
Created on Feb 12, 2017

@author: amita
'''
from nltk.tree import *
import nltk
ROOT = 'ROOT'
from nltk.tree import ParentedTree
import os
def traverse(t):
    try:
        t.label()
    except AttributeError:
        return
    else:

        if t.height() == 2:   #child nodes
            print (t.leaves())
            return

        for child in t:
            traverse(child)


    
def getNodes(parent):
    for node in parent:
        if type(node) is nltk.Tree:
            if node.label() == ROOT:
                print ("======== Sentence =========")
                print( "Sentence:", " ".join(node.leaves()))
            else:
                continue
                #print( "Label:", node.label())
                #print ("Leaves:", node.leaves())

            getNodes(node)
        else:
            print ("Word:", node)
            
def traverseTree(tree):
    print("tree:", tree)
    for subtree in tree:
        if type(subtree) == nltk.tree.Tree:
            traverseTree(subtree)

def traverseleaves(ptree):
    leaf_values = ptree.leaves()

    for leaf in leaf_values:
        leaf_index = leaf_values.index(leaf)
        tree_location = ptree.leaf_treeposition(leaf_index)
        print (tree_location)
        print (ptree[tree_location])

def parentedtree(tree):
    newtree = ParentedTree.convert(tree)
    leaf_values = newtree.leaves()
    for leaf in leaf_values:
        leaf.parent()
        
#===============================================================================
# 
# def parsetree(input):
#     with open(input,'r') as myfile:
#         data=myfile.read()
# 
#     tree = Tree.fromstring(data)
#     ptree = ParentedTree.fromstring(data)
#     #parentedtree(tree)
#     traverseleaves(ptree)
#===============================================================================

def func_amita(parse_str):
    ptree = ParentedTree.fromstring(parse_str)
    #ptree.draw()
    #ptree.pprint()
    #helper_func_amita(ptree)


def helper_func_amita(ptree):
    children_list = []
    is_second_to_last_level = True
    for node in ptree:
        if type(node) is nltk.ParentedTree:
            children_list.append(node.label())
            helper_func_amita(node)
    for c in children_list:
        # if c has children, then put is_second_to_last_level to False
        for c1 in c:
            is_second_to_last_level = False
    if is_second_to_last_level:
        print(ptree.label())    
    #-------------------------------------------------- subtrees=tree.subtrees()
    #------------------------------------------- for subtree in tree.subtrees():
         #------------------------------------------------------- subtree.draw()
         #-------------------------------------------------- ht=subtree.height()
         #------------------------------------------------------------ print(ht)
         #-------------------------------------------------------------- print()
    #print(tree.leaves())
    #getNodes(tree)
if __name__ == '__main__':
    directory="/Users/amita/git/feng-hirst-rst-parser-acl-2014/texts/results/rst_feng_output_guncontrol"
    file_list=os.listdir(directory)
    for filename in file_list:
        if filename=="1-10145_4_3__5_9_21_29_1.txt.tree":
            continue
        if str(filename).endswith(".txt.tree"):
            
            filewithpath=os.path.join(directory,filename)
            #input="/Users/amita/git/feng-hirst-rst-parser-acl-2014/texts/results/rst_feng_output_guncontrol/1-690_2_1__3_4_5_7_1.txt.tree"
            with open(filewithpath,'r') as myfile:
                data=myfile.read()
            print(filename)    
            tree = Tree.fromstring(data)
            func_amita(data)