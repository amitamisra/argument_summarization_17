#!/bin/sh
'''
Created on Nov 10, 2016
execute scripts to get the data from database. This script actually just calls the scripts needed to create the input ready 
for rst parser
See this thread for dialog creation on gay marriage
http://www.4forums.com/political/showthread.php?t=9196&page=5
@author: amita
'''

import createdialogdata_database
import baseline_dialog
import scores_pyramid
import mergepyramidlabels_baseline
import file_utilities
import file_handling
import baselines
import os
import json_edus

def run(topic,input_fordialog):
    """
    createdialogdata_database: creates dialogs from database using key in postlist
    baseline_dialog: add text rank as baseline
    scores_pyramid: create a csv with labels and scores
    """
    
    input_withdir_fordialog=os.path.join(*input_fordialog.split(","))
    input_withpath_fordialog=os.path.join(data_dir,input_withdir_fordialog)
    dialogfilename=createdialogdata_database.run(topic,input_withpath_fordialog)
    
    #input_withdir_forbaseline=os.path.join(*input_forbaseline.split(","))
    #input_withpath_forbaseline=os.path.join(data_dir,input_withdir_forbaseline)
    baselinefile=baseline_dialog.run(topic,dialogfilename)
    
    scores_pyramid.run(topic)
    #input_formerge=input_withpath_forbaseline +"_baseline"
    outputmerged=mergepyramidlabels_baseline.run(topic,baselinefile)
   
    #inputfor_rst_corenlp=file_utilities.get_absolutepath_data("data_database",topic,"all_pyramids_scus_baseline")
    column="Dialog_from_database_author_noformat"
    json_edus.run(topic,outputmerged,column)
    

if __name__ == '__main__':
    data_dir=file_utilities.DATA_DIR
    
    topic="gay-rights-debates" 
    input_fordialog="dialogdata,CSV,gay-rights-debates,Phase_1_2,AllDialogs_phase_1_2_Formatted"
    #input_forbaseline="data_database,gay-rights-debates,AllDialogs_phase_1_2_Formatted_database"
    #edus_dir="/Users/amita/git/feng-hirst-rst-parser-acl-2014/texts/results/gay-rights-debates_rst_output_edu_withg"
    #run(topic,input_fordialog,input_forbaseline,edus_dir)
    
    
    topic="gun-control" 
    input_fordialog="dialogdata,CSV,gun-control,MTdata,MTSummary,AllMTSummary_50,NaturalSummaryCSV"
    #input_forbaseline="data_database,gun-control,NaturalSummaryCSV_database"
    #edus_dir="/Users/amita/git/feng-hirst-rst-parser-acl-2014/texts/results/gun-control_rst_output_edu_withoptiong"        
    
    run(topic,input_fordialog)
    
