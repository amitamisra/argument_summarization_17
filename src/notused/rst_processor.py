'''
Created on Feb 21, 2017
uses py4j to connect to a java program, java program should be running on the local host
at port 25330
run Callrstjava.java, in processors
@author: amita
'''
from py4j.java_gateway import JavaGateway, GatewayParameters
gateway=JavaGateway(gateway_parameters=GatewayParameters(port=25330))

def parse_text(text):
    res=gateway.entry_point.parse_text(text)
    return res
if __name__ == '__main__':
    text="John Smith went to China. He visited Beijing, on January 10th, 2013"
    res=parse_text(text)
    print(res)