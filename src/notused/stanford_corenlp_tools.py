'''
Created on Dec 16, 2016

@author: amita
'''

import os
import subprocess

def start_server():
    """
    Starts the server in a seperate terminal session. 
    :return:
    """
    os.chdir(os.path.join(os.getcwd(),"dialogue_systems/stanford","stanford-corenlp-full-2015-12-09"))
    corenlp_cmdline = 'start /wait java -mx1000m -cp "*" edu.stanford.nlp.pipeline.StanfordCoreNLPServer -timeout 10000'
    subprocess.call(corenlp_cmdline, shell=True)

        