'''
Created on Apr 14, 2016
Take a sequence of pair of files as input and run pairedTTest on predictedscores
@author: amita
'''

import configparser, os
import  pandas as pd
from scipy.stats import ttest_rel
from scipy.stats import ttest_ind
from src import file_utilities
from scipy import stats, math


def pairedTTest(param_dict):
    numrows= int(param_dict["numrows"])
    predict_col=param_dict["predict_col"]
    input1=param_dict["input1"]
    print(input1)
    df1=pd.read_csv(input1,na_filter=False,nrows=numrows)
    predicted1=df1[predict_col].values.astype(int)
    
    #===========================================================================
    # df1[predict_col]=df1[predict_col].astype(int)
    # predicted1=df1[predict_col].tolist()
    # #predicted1=[math.sqrt(abs(x)) for x in  predicted1 ]
    #df2.predict_col=df2.predict_col.astype(int)
    #predicted2=df2[predict_col].tolist()  
    #===========================================================================
    
    input2=param_dict["input2"]
    df2=pd.read_csv(input2,na_filter=False,nrows=numrows)
    predicted2=df2[predict_col].values.astype(int)
    
    #predicted2=[math.sqrt(abs(x)) for x in  predicted2 ]
    assert(len(predicted1)==len(predicted2))
    pairedTTest=ttest_rel(predicted1,predicted2) 
    return pairedTTest


def addresult(ttest,param_dict,all_list):
    result_dict={}
    input1=param_dict["input1"]
    input2=param_dict["input2"]
    result_dict["input1"]=input1
    result_dict["input2"]=input2
    result_dict["feature"]=os.path.basename(input1)+"_"+os.path.basename(input2)
    result_dict["dir"]=os.path.dirname(input1)+"_"+os.path.dirname(input2)
    result_dict["pairedTTest"+param_dict["predict_col"]]=ttest
    all_list.append(result_dict)

def run(section): 
    config = configparser.ConfigParser()
    regressioninput=file_utilities.get_absolutepath_data("config","pairedTTest.ini")
    config.read(regressioninput)
    inputdata=config.get(section,"csv_with_file_pairs_list")
    resultCSVFile=config.get(section,"resultCSVFile")
    resultCSVFile=file_utilities.get_absolutepath_data("config",resultCSVFile)
    inputdata=file_utilities.get_absolutepath_data("config",inputdata)
    inputdata_df=pd.read_csv(inputdata,na_filter=False)
    all_list=[]
    count=0
    if os.path.exists(resultCSVFile):
            os.remove(resultCSVFile)
            
    for index, row in inputdata_df.iterrows():
        param_dict={}
        count=count=count+1
        print("doing row {0}".format(count))
        param_dict["input1"]=row["input1"]
        param_dict["input2"]=row["input2"]
        param_dict["predict_col"]=row["predict_col"]
        param_dict["numrows"]=row["num_rows"]
        ttest= pairedTTest(param_dict)
        addresult(ttest,param_dict,all_list)
        
    df=pd.DataFrame(all_list)
    df.to_csv(resultCSVFile,index=False)     
if __name__ == '__main__':
    section="LSTM"
    run(section)
    section="GC"
    run(section)
    
   