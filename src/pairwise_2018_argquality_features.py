'''
Created on Nov 28, 2017

@author: amita
'''
import readability_metrics
import nltk
import string
import liwc_extractor
from collections import Counter

def addreadability(sentence):
    
    read_scores={'read_Kincaid':0,'read_ARI':0,'read_Coleman-Liau':0,'read_FleschReadingEase':0,"read_GunningFogIndex":0,"read_LIX":0,"read_SMOGIndex":0,"read_RIX":0}
    words=nltk.word_tokenize(sentence)
    if words[-1] in string.punctuation:
        words=words[:-1]
    if len(words) > 1:
        
        read_scores=readability_metrics.getmeasures(sentence)
    return read_scores


def create_featurename(feature_dict, prefix):
    newdict={}
    for key, value in feature_dict.items():
        newkey=key +"_"+ prefix
        newdict[newkey]=value
    return newdict     
    

def addliwc(sent):
    liwc_extractor_object=liwc_extractor.LIWCextractor()
    liwc_sent_dict=liwc_extractor_object.score_text(sent)
    new_dict={}
    for key,value in liwc_sent_dict.items():
        new_dict["liwc_"+ key]=value
    return new_dict    

def addkeyword(sentence):
    print()    

def generate_features(features,sent,topkeyword,sent_num):
        feature_dict={}
        if "readability" in features:
            read_score=addreadability(sent)
            read_dict=create_featurename(read_score, sent_num)
            feature_dict.update(read_dict)
            
        if "liwc" in features:
                liwc_score=addliwc(sent)
                liwc_dict=create_featurename(liwc_score, sent_num)
                feature_dict.update(liwc_dict)
        
            
        
        if "keyword_perc" in features:
            keyword_score=addkeyword(sent,topkeyword)
            keyword_dict=create_featurename(keyword_score, sent_num)
            feature_dict.update(keyword_dict)
            
        
        return  feature_dict
        
if __name__ == '__main__':
    pass