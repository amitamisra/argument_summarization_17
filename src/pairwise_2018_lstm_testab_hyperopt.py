'''
Created on Nov 15, 2017

@author: amita
'''
'''
code https://medium.com/mlreview/implementing-malstm-on-kaggles-quora-question-pairs-competition-8b31b0b16a07
https://arxiv.org/pdf/1508.01585v2.pdf
https://arxiv.org/pdf/1709.04348.pdf
https://github.com/fchollet/keras/blob/master/examples/pretrained_word_embeddings.py
https://github.com/fchollet/keras/blob/master/examples/mnist_siamese.py   shared network
https://stackoverflow.com/questions/43237124/role-of-flatten-in-keras
https://stackoverflow.com/questions/42445275/merge-outputs-of-different-models-with-different-input-shapes
http://freecontent.manning.com/introducing-keras-deep-learning-with-python/
http://freecontent.manning.com/deep-learning-for-text/
https://stackoverflow.com/questions/33229748/why-do-i-get-a-theano-typeerror-when-trying-to-update-a-shared-variable
https://machinelearningmastery.com/display-deep-learning-model-training-history-in-keras/
https://medium.com/mlreview/implementing-malstm-on-kaggles-quora-question-pairs-competition-8b31b0b16a07

Created on Oct 11, 2017
pairwise lstm
'''



import os
seed=100
import numpy as np
import random
np.random.seed(seed)
random.seed(seed)
import  pairwise_lstm_sharedfunctions_hyperopt as SF
import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


os.environ['KERAS_BACKEND']='theano'


rs=1000

if __name__ == '__main__':
    section="AB"
    SF.run(section,rs,seed)





# 
# import os,io
# seed=100
# import numpy as np
# import random
# np.random.seed(seed)
# random.seed(seed)
# from keras.preprocessing.text import Tokenizer
# from keras.preprocessing.sequence import pad_sequences
# from keras.utils.np_utils import to_categorical
# from keras.layers import Dense, Input, Flatten
# from keras.layers import Conv1D, MaxPooling1D,AveragePooling1D, Embedding, Bidirectional
# from keras.models import Model
# from keras.layers import LSTM
# from keras.layers.convolutional import Convolution1D
# import pandas as pd
# from sklearn.preprocessing import LabelEncoder
# from sklearn.metrics import f1_score
# from keras.layers import Dropout
# from pprint import pprint
# from keras.callbacks import ModelCheckpoint
# from keras.callbacks import History
# from keras.optimizers import Adam
# from sklearn.metrics import confusion_matrix
# import keras.backend as K
# #from sklearn.model_selection import train_test_split
# from sklearn.metrics import classification_report
# from keras.models import model_from_json
# import file_utilities
# from keras.layers import merge
# from keras.layers import Subtract, Add
# from keras.layers import Maximum
# from keras.layers import Concatenate
# from keras.layers import concatenate
# from keras.layers import Multiply
# from keras.layers import GaussianNoise
# import configparser
# import matplotlib
# from  keras.layers import TimeDistributed, Reshape
# matplotlib.use('PS')
# import matplotlib.pyplot as plt
# noise=0.01
# from keras.optimizers import RMSprop
# import  attentionlayer as F
# from keras.regularizers import l2
# #from keras import initializations
# 
# import logging
# logging.basicConfig(level=logging.INFO)
# logger = logging.getLogger(__name__)
# from pprint import pprint
# from keras.engine.topology import Layer
# 
# os.environ['KERAS_BACKEND']='theano'
# datadir=file_utilities.DATA_DIR
# 
# 
# GLOVE_DIR="/Users/amita/software/glove.6B"
# 
# 
# 
# MAX_NB_WORDS = 20000
# EMBEDDING_DIM = 100
# 
# 
# word_index={}
# embeddings_index = {}
# num_classes=2
# rs=1000
# testsize=0.30
# vs= 0.2
# batch=64
# kernelsize=3
# filterratio=4
# denselayer=256
# poollength=2
# f = open(os.path.join(GLOVE_DIR, 'glove.6B.100d.txt'), "r", encoding='utf-8')
# #f = open('glove.6B.100d.txt', 'r')
# for line in f:
#     values = line.split()
#     word = values[0]
#     coefs = np.asarray(values[1:], dtype='float32')
#     embeddings_index[word] = coefs
# f.close()
# 
# print('Found %s word vectors.' % len(embeddings_index))
# 
# # second, prepare text samples and their labels
# print('Processing text dataset')
# 
# 
# 
#       
# def  runlstm(resultCSVFile,all_list,param_dict):
#     """
#     inputtrain and input test files should be balanced and shuffled
#     feature_list: features to be included
#     """
#     
#     #K.clear_session()
#     input_train=file_utilities.get_absolutepath_data(param_dict["input_train"])
#     input_test=file_utilities.get_absolutepath_data(param_dict["input_test"])
#     input_val=file_utilities.get_absolutepath_data(param_dict["input_val"])
#     outBaseDir=file_utilities.get_absolutepath_data(param_dict["outputBaseDir"])
#     textcol1=param_dict["text_col1"]
#     textcol2=param_dict["text_col2"]
#     label_col=param_dict["label_col"]
#     shared=param_dict["shared"]
#     feature_list=param_dict["feature_list"]
#     row_unique=param_dict["row_unique"]
#     add_features= param_dict["add_features"]
#     embedd_shared=param_dict["embedd_shared"]
#     epochs=param_dict["epochs"]
#     dim=param_dict["dim"]
#     dropout=param_dict["dropout"]
#     trunc=param_dict["trunc"]
#     padding=param_dict["padding"]
#     seq_length=param_dict["seq_length"]
#     baseline=param_dict["baseline"]
#     
#     
#    
#     
#     if not os.path.exists(outBaseDir):
#             os.mkdir(outBaseDir)
#     outputfile=os.path.join(outBaseDir,"lstm_result.txt")
# 
#     df_train=pd.read_csv(input_train)
#     df_train=df_train.fillna(0)
#     train_column=set(df_train.columns.tolist())
#     
#     
#     df_test=pd.read_csv(input_test)
#     df_test=df_test.fillna(0)
#     test_column=set(df_test.columns.tolist())
#     
#     df_val=pd.read_csv(input_val)
#     df_val=df_val.fillna(0)
#     val_column=set(df_val.columns.tolist())
# 
#     
#     add_col_test=set.difference(train_column,test_column)
#     df_test=add_missing_featurecols(df_test, add_col_test)
#     
#     add_col_train=set.difference(test_column,train_column)
#     df_train=add_missing_featurecols(df_train, add_col_train)
#     
#     add_col_val=set.difference(train_column,val_column)
#     df_val=add_missing_featurecols(df_val, add_col_val)
#     
# 
#     
#     train_texts_S1,train_texts_S2,feature_df_train_S1,feature_df_train_S2,labels_train=get_data(df_train,feature_list,textcol1,textcol2,label_col)
#     test_texts_S1,test_texts_S2,feature_df_test_S1,feature_df_test_S2,labels_test=get_data(df_test,feature_list,textcol1,textcol2,label_col)
#     val_texts_S1,val_texts_S2,feature_df_val_S1,feature_df_val_S2,labels_val=get_data(df_val,feature_list,textcol1,textcol2,label_col)
# 
#     
#     features_train_S1=feature_df_train_S1.as_matrix()
#     features_train_S2=feature_df_train_S2.as_matrix()
# 
#     
#     features_test_S1=feature_df_test_S1.as_matrix()
#     features_test_S2=feature_df_test_S2.as_matrix()
#     
#     features_val_S1=feature_df_val_S1.as_matrix()
#     features_val_S2=feature_df_val_S2.as_matrix()
#     
#     
#     
#     data_train_S1,data_train_S2,labels_train,data_test_S1,data_test_S2,labels_test,data_val_S1,data_val_S2,labels_val=vectorize_text(train_texts_S1, train_texts_S2,test_texts_S1,test_texts_S2,val_texts_S1, val_texts_S2,labels_train,labels_test,labels_val,trunc,padding,seq_length)
#     
#     with open(outputfile, 'a') as out:
#                 pprint("starting for epochs {0}  and seed {1}, randomstate {2} , dim {3}, dropout {4}".format(epochs,seed,rs,dim,dropout),stream=out)  
#  
#     filepath=os.path.join(outBaseDir,"weights.best.hdf5_rowno"+ str(row_unique))
#     plot_file_acc=filepath+ "_acc.png"
#     plot_file_loss=filepath+"_loss.png"
#     if os.path.exists(filepath):
#         os.remove(filepath)
#     
#     history = History()    
#     checkpoint = ModelCheckpoint(filepath,  monitor='val_acc', verbose=1, save_best_only=True, mode='auto')
#     callbacks_list = [history, checkpoint]
#     feature_count=feature_df_train_S1.shape[1]
#     
#     lrate=param_dict["lr"]
#     optim= Adam(lr=lrate, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
#     #optim=RMSprop(lr=lrate, rho=0.9, epsilon=1e-08, decay=0.0)
#     
#     if baseline==1:
#          model=create_model_bilstm_baseline(feature_count,add_features,dropout,dim,param_dict,optim)
#     else:     
#         if shared==0:
#             model= create_model_noshared_baseline(feature_count,add_features,dropout,dim,param_dict,optim)
#         else:    
#                 model=create_model_shared(feature_count,add_features,dropout,dim,param_dict,optim)
#             
#     history_mod=model.fit({"sent1_input":data_train_S1, 'sent2_input': data_train_S2, "features1_input":features_train_S1,"features2_input":features_train_S2}, labels_train, validation_data=({"sent1_input":data_val_S1, 'sent2_input': data_val_S2, "features1_input":features_val_S1,"features2_input":features_val_S2},labels_val), epochs=epochs, batch_size=batch, callbacks=callbacks_list, verbose=1)
#     
#     model.load_weights( filepath)
#     model.compile(loss='categorical_crossentropy', optimizer=optim, metrics=['accuracy'])
#     scores = model.evaluate([data_test_S1,data_test_S2,features_test_S1,features_test_S2], labels_test, verbose=1)
#     print(history_mod.history.keys())
#     # summarize history for accuracy
#     plt.plot(history_mod.history['acc'])
#     plt.plot(history_mod.history['val_acc'])
#     plt.title('model accuracy')
#     plt.ylabel('accuracy')
#     plt.xlabel('epoch')
#     plt.legend(['train', 'test'], loc='upper left')
#     plt.savefig(plot_file_acc)
#     plt.close()
#     # summarize history for loss
#     plt.plot(history_mod.history['loss'])
#     plt.plot(history_mod.history['val_loss'])
#     plt.title('model loss')
#     plt.ylabel('loss')
#     plt.xlabel('epoch')
#     plt.legend(['train', 'test'], loc='upper left')
#     plt.savefig(plot_file_loss)
#     plt.close()
#     pred = model.predict([data_test_S1, data_test_S2, features_test_S1,features_test_S2], batch_size=64)
#     y_pred_values=pred.argmax(axis=1)
#     y_test_values=labels_test.argmax(axis=1)
#     pred_list= [{'Y_actual': v1, 'Y_predicted': v2} for v1, v2 in zip( y_test_values, y_pred_values)]
#     acurracy=scores[1]*100
#     f1weighted=f1_score(labels_test.argmax(axis=1), pred.argmax(axis=1),average="macro")
#     report=classification_report(labels_test.argmax(axis=1), pred.argmax(axis=1))
#     #confusion_mat=confusion_matrix(y_test.argmax(axis=1), pred.argmax(axis=1))
#     result_dict=create_result_dict(acurracy, f1weighted, epochs,batch,dropout,dim,param_dict)
#     
#     csvwithpath=outputfile[:-4]+"rowno "+str(row_unique)+".csv"
#     writeresult(outputfile,f1weighted,shared, embedd_shared,add_features,acurracy,param_dict,report)
#     writecsv(y_pred_values,y_test_values,df_test,  csvwithpath,f1weighted,shared, embedd_shared,add_features,acurracy,param_dict,report)
#     all_list.append(result_dict)
#     if not os.path.exists(outBaseDir):
#         os.mkdir(outBaseDir)
#     filename="".join(feature_list)+"drop"+str(dropout)+"dim"+str(dim)+"ep"+str(epochs)+"batch"+str(batch)+".csv"
#      
#     #writeresultserver(f1weighted,resultCSVFile,feature_list,dim,dropout,epochs,batch)
#     #writeresultstoFile(csvwithpath,result_dict,pred_list,y_pred_values,y_test_values,input_test,feature_list,f1weighted)
#     logger.info("Done for input {0} {1}".format(input_train,feature_list))
# 
# 
# def create_result_dict(acurracy, f1weighted, epochs,batch,dropout,dim,param_dict):
#     newdict={}
#     newdict["acurracy"]=acurracy
#     newdict["f1weighted"]=f1weighted
#     newdict["pochs"]= epochs
#     newdict["batch"]=batch
#     newdict["dropout"]=dropout
#     newdict["dim"]=dim
#     param_dict.update(newdict)
#     return param_dict
#          
#     
#     
# def run(section):
#     
#     config = configparser.ConfigParser()
#     lstminput=file_utilities.get_absolutepath_data("config","pairwiselstm_config.ini")
#     config.read(lstminput)
#     input_file_config=config.get(section,"csv_with_file_list")
#     resultCSVFile_config=config.get(section,'resultCSVFile')
#     input_file=file_utilities.get_absolutepath_data(input_file_config)
#     resultCSVFile=file_utilities.get_absolutepath_data(resultCSVFile_config) 
#     inputdata_df=pd.read_csv(input_file)
#     inputdata_df.fillna(0)
#     print(" LSTM for section {0}".format(section))
#     all_list=[]
#     if os.path.exists(resultCSVFile):
#             os.remove(resultCSVFile)
#     for index, row in inputdata_df.iterrows():
#         param_dict={}
#         done=row["done"]
#         if int(done)==1:
#             continue
#         
#        
#         
#         param_dict["input_train"]=row["input_train"]
#         param_dict["input_test"]= row["input_test"]
#         param_dict["input_val"]= row["input_val"]
#         param_dict["outputBaseDir"]= row["outputBaseDir"]
#         param_dict["text_col1"]=row["textcol1"]
#         param_dict["text_col2"]=row["textcol2"]
#         param_dict["label_col"]=row["label_col"]
#         param_dict["shared"]=int(row["shared"])
#         param_dict["feature_list"]=list(row["feature_list"].split(","))
#         param_dict["row_unique"]=row["row_unique"]
#         param_dict["add_features"]=int(row["add_features"])
#         param_dict["embedd_shared"]=int(row["embedd_shared"])
#         param_dict["epochs"]=int(row["epochs"])
#         param_dict["lr"]=float(row["lr"])
#         param_dict["dim"]=int(row["dim"])
#         param_dict["dropout"]=float(row["dropout"])
#         param_dict["trunc"] =row["trunc"]
#         param_dict["padding"]=row["padding"]
#         param_dict["activation_last"]=row["activation_last"]
#         param_dict["seq_length"]=int(row["seq_length"])
#         param_dict["baseline"]=int(row["baseline"])
#         runlstm(resultCSVFile, all_list,param_dict)  
#  
#         
#     if all_list:
#         df=pd.DataFrame(all_list)
#         df.to_csv(resultCSVFile,index=False)         
#         
