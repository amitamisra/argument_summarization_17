'''
Created on Nov 15, 2017

@author: amita
'''
'''
code https://medium.com/mlreview/implementing-malstm-on-kaggles-quora-question-pairs-competition-8b31b0b16a07
https://arxiv.org/pdf/1508.01585v2.pdf
https://arxiv.org/pdf/1709.04348.pdf
https://github.com/fchollet/keras/blob/master/examples/pretrained_word_embeddings.py
https://github.com/fchollet/keras/blob/master/examples/mnist_siamese.py   shared network
https://stackoverflow.com/questions/43237124/role-of-flatten-in-keras
https://stackoverflow.com/questions/42445275/merge-outputs-of-different-models-with-different-input-shapes
http://freecontent.manning.com/introducing-keras-deep-learning-with-python/
http://freecontent.manning.com/deep-learning-for-text/
https://stackoverflow.com/questions/33229748/why-do-i-get-a-theano-typeerror-when-trying-to-update-a-shared-variable
https://machinelearningmastery.com/display-deep-learning-model-training-history-in-keras/
https://medium.com/mlreview/implementing-malstm-on-kaggles-quora-question-pairs-competition-8b31b0b16a07
Created on Oct 11, 2017
pairwise lstm
'''

import os
seed=100
import numpy as np
import random
np.random.seed(seed)
random.seed(seed)
import  pairwise_lstm_sharedfunctions_hyperopt as SF
import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


os.environ['KERAS_BACKEND']='theano'


rs=1000



if __name__ == '__main__':
    section="Gay"
    SF.run(section,rs,seed)
