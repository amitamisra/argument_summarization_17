'''
Created on Nov 20, 2017

@author: amita
'''
import pandas as pd
#import itertools.product
import readability_metrics
from collections import ChainMap
import nltk
import os
import string
import configparser, logging
import file_utilities
import copy
from collections import OrderedDict
import file_handling
from itertools import product

features=["readability"]

def addreadability(sentence):
    
    read_scores={'read_Kincaid':0,'read_ARI':0,'read_Coleman-Liau':0,'read_FleschReadingEase':0,"read_GunningFogIndex":0,"read_LIX":0,"read_SMOGIndex":0,"read_RIX":0}
    words=nltk.word_tokenize(sentence)
    if words[-1] in string.punctuation:
        words=words[:-1]
    if len(words) > 1:
        
        read_scores=readability_metrics.getmeasures(sentence)
    return read_scores




def createfeatures(sentence):
    
    if 'readability' in features: 
        read_scores=addreadability(sentence)
    
    feature_dict= dict(ChainMap(read_scores))
    return feature_dict                   
                       

def pair_features(inputcsv,features_file,order_file,sent_column,w2vec_loc,tierlabelcol,keycolumn,lower_rank,upper_rank):
    df = pd.read_csv(inputcsv)
    dfkey=df.groupby(keycolumn)
    allrows=[]
    mappingrows=[]
    order_lines=[]
    rec_id=1
    for key_index, df_dialog in dfkey:
        lowerank_rows=df_dialog.loc[df_dialog['label'].isin(lower_rank)]
        upperrank_rows=df_dialog.loc[df_dialog['label'].isin(upper_rank)]
        lower_rank_sent_ids=lowerank_rows["sent_no"].tolist()
        upper_rank_sent_ids=upperrank_rows["sent_no"].tolist()
        sent_pairs=list(product(lower_rank_sent_ids,upper_rank_sent_ids))
        for sent_pair in sent_pairs:
            sentence_lower=df_dialog.loc[df_dialog['sent_no']==sent_pair[0]]["sent"].tolist()[0]
            sentence_upper=df_dialog.loc[df_dialog['sent_no']==sent_pair[1]]["sent"].tolist()[0]
            newdict=OrderedDict()
            mapping_dict={}
            lower_key=str(key_index)+"sent_no"+ str(sent_pair[0])
            id1=rec_id
            mapping_dict["id"]=id1
            mapping_dict["key"]=lower_key
            features=createfeatures(sentence_lower)
            newdict["id"]=id1
            newdict.update(features)
            allrows.append(newdict)
            mappingrows.append(mapping_dict)
            
            id2=rec_id+1
            newdict=OrderedDict()
            mapping_dict={}
            features=createfeatures(sentence_upper)
            upper_key=str(key_index)+ "sent_no"+str(sent_pair[1])
            newdict["id"]=id2
            newdict.update(features)
            mapping_dict["id"]=id2
            mapping_dict["key"]=upper_key
            allrows.append(newdict)
            mappingrows.append(mapping_dict)
            
            order=str(id2)+"," + str(id1)+"\n"
            order_lines.append(order)
            rec_id=rec_id+2
                     
    df_features=pd.DataFrame(allrows)        
    df_features.to_csv(features_file,index=False)  
    file_handling.writeTextFile(order_file,order_lines)      
            
            
            
            
            
def run(section):
    args= readCongigFile(section)
    inputfile=args[0]
    features_file=args[1]
    order_file=args[2]
    sent_column=args[3]
    w2vec_loc=args[4]
    tierlabelcol=args[5]
    keycolumn=args[6]
    lower_rank=args[7]
    upper_rank=args[8]
    pair_features(inputfile,features_file,order_file,sent_column,w2vec_loc,tierlabelcol,keycolumn,lower_rank,upper_rank)
 
      

def readCongigFile(section):
    config = configparser.ConfigParser()
    config_file=file_utilities.get_absolutepath_data("config","pairwise_data_Config.ini")
    config.read(config_file)
    tierlabelcol=config.get(section,'tierlabelcol')
    sent_column=config.get(section,'sent_column')
    w2vec_loc=config.get(section,'w2vec_loc')
    input_file=config.get(section,"input_file")
    features_file=config.get(section,'features_file')
    order_file=config.get(section,'order_file')
    keycolumn=config.get(section,'keycolumn')
    lower_rank=config.get(section,'lower_rank').split(",")
    upper_rank=config.get(section,'upper_rank').split(",")
    
    arguments=(input_file,features_file,order_file,sent_column,w2vec_loc,tierlabelcol,keycolumn,lower_rank,upper_rank)

    return  (arguments)
   
          
   
    
if __name__ == '__main__':
    section="GC"
    run(section)