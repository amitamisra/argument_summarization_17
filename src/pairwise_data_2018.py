'''
Created on Nov 20, 2017
creates data for pairwise argument comparison.
20 dialogues for testing, 10 for validation and others for training.

@author: amita
'''
import pandas as pd
#import itertools.product

from collections import ChainMap

import os

import configparser, logging
import file_utilities
import copy
from collections import OrderedDict
import file_handling
from itertools import product
from pairwise_2018_argquality_features import generate_features
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle

testsize=0.2
rs=1000
features=["readability","liwc"]
#,"liwc","keywordpercentage"]
dialogcount_test=21
dialogcount_val=31
def create_balanced(df,classlabel,outfile):
    """
    create a balanced dataset based on classlabel
    """
    g = df.groupby(classlabel)
    df=g.apply(lambda x: x.sample(g.size().min())).reset_index(drop=True)
    newdf=shuffle(df, random_state=rs )
    newdf.to_csv(outfile,index=False)


def pair_features(inputcsv,features_file,sent_column,w2vec_loc,tierlabelcol,keycolumn,lower_rank,upper_rank,valsplit,topic):
    df = pd.read_csv(inputcsv)
    df.insert(0,"topic",topic)
    df=shuffle(df,random_state =rs)
    dfkey=df.groupby(keycolumn)
    allrows=[]
    alltrainrows=[]
    alltestrows=[]
    allvalrows=[]
    dialogcount=0
    for key_index, df_dialog in dfkey:
        #print("dialogcount start "+ str(dialogcount))
        #print("key"+str(key_index))
        sents=" ".join(df_dialog['sent'].tolist())
        dialogcount=dialogcount+1
        lowerank_rows=df_dialog.loc[df_dialog['label'].isin(lower_rank)]
        upperrank_rows=df_dialog.loc[df_dialog['label'].isin(upper_rank)]
        lower_rank_sent_ids=lowerank_rows["sent_no"].tolist()
        upper_rank_sent_ids=upperrank_rows["sent_no"].tolist()
        sent_pairs=list(product(lower_rank_sent_ids,upper_rank_sent_ids))
        num_pairs=len(sent_pairs)
        topkeyword=""
        count=0
#         if len(sent_pairs) ==0:
#             print(key_index)
        for sent_pair in sent_pairs:
            sentence_lower=df_dialog.loc[df_dialog['sent_no']==sent_pair[0]]["sent"].tolist()[0]
            sentence_upper=df_dialog.loc[df_dialog['sent_no']==sent_pair[1]]["sent"].tolist()[0]
            newdict=OrderedDict()
            lower_key=str(key_index)+"sent_no"+ str(sent_pair[0])
            lower_label=df_dialog.loc[df_dialog['sent_no']==sent_pair[0]]["label"].tolist()[0]
            upper_label=df_dialog.loc[df_dialog['sent_no']==sent_pair[1]]["label"].tolist()[0]
            upper_key=str(key_index)+ "sent_no"+str(sent_pair[1])
            newdict["lower_key"]=lower_key
            newdict["upper_key"]=upper_key
            newdict["lower_label"]=lower_label
            newdict["upper_label"]=upper_label
            newdict["topic"]=topic
            newdict["sents_dialog"]=sents
            newdict["sents_no_lower"]=sent_pair[0]
            newdict["sents_no_upper"]=sent_pair[1]
            
            newdict["key"]=key_index
            newdict["dialog"]=df_dialog['dialog_no_format_author'].tolist()[0]
            if count < num_pairs/2:
                newdict["sent1"]=sentence_lower
                newdict["sent2"]=sentence_upper
                newdict["higher_rank"]="s2"  #s2 is better than s1
                feature_dict_1=generate_features(features,sentence_lower,topkeyword,"sent1") 
                feature_dict_2=generate_features(features,sentence_upper,topkeyword,"sent2") 
                
                 
            else:
                newdict["sent2"]=sentence_lower
                newdict["sent1"]=sentence_upper
                newdict["higher_rank"]="s1" # s1 is better than s2
                feature_dict_1=generate_features(features,sentence_lower,topkeyword,"sent2") 
                feature_dict_2=generate_features(features,sentence_upper,topkeyword,"sent1") 
            
            count=count+1
            newdict.update(feature_dict_1)
            newdict.update(feature_dict_2)
            allrows.append(newdict)
            
            if dialogcount < dialogcount_test:
                #print("adding dialogcount to test set " + str(key_index))
                #print("dialogcount"  +str(dialogcount))
                alltestrows.append(newdict)
            else:  
                if valsplit==1: 
                    if  dialogcount >= dialogcount_test and  dialogcount < dialogcount_val:
                        allvalrows.append(newdict)
                    else:
                        alltrainrows.append(newdict)
                else:
                    alltrainrows.append(newdict)
                            
    
    df_all=pd.DataFrame(allrows)
      
    df_train=pd.DataFrame(alltrainrows)
    df_test=pd.DataFrame(alltestrows) 
    
    df_train.to_csv(features_file[:-4]+"_train.csv",index=False) 
    df_test.to_csv(features_file[:-4]+"_test.csv",index=False) 
    
    balfile=features_file[:-4]+"_train_balanced.csv"
    create_balanced(df_train,'higher_rank',balfile)
    
    balfile=features_file[:-4]+"_test_balanced.csv"
    create_balanced(df_test,'higher_rank', balfile)
    
    if valsplit==1:
        df_val=pd.DataFrame(allvalrows) 
        balfile=features_file[:-4]+"_val_balanced.csv"
        create_balanced(df_val,'higher_rank', balfile)
    
    
def merge_topic_indep(train_gc,train_ab,train_gay,val_gc,val_ab,val_gay,topic_indep_dir) :
    
    train_gc_df=pd.read_csv(train_gc)
    train_ab_df=pd.read_csv(train_ab)
    train_gay_df=pd.read_csv(train_gay)

    
    val_gc_df=pd.read_csv(val_gc)
    val_ab_df=pd.read_csv(val_ab)
    val_gay_df=pd.read_csv(val_gay)
    
    train_gc_ab_df=pd.concat([train_gc_df,train_ab_df])
    train_gc_gay_df=pd.concat([train_gc_df,train_gay_df])
    train_ab_gay_df=pd.concat([train_ab_df,train_gay_df])
    
    val_gc_ab_df=pd.concat([val_gc_df,val_ab_df])
    val_gc_gay_df=pd.concat([val_gc_df,val_gay_df])
    val_ab_gay_df=pd.concat([val_ab_df,val_gay_df])
    
    testgc_dir=os.path.join(topic_indep_dir,"testgc")
    testab_dir=os.path.join(topic_indep_dir,"testab")
    testgay_dir=os.path.join(topic_indep_dir,"testgay")
    
    
    if not os.path.exists(testgc_dir):
        os.mkdir(testgc_dir)
    if not os.path.exists(testgay_dir):
        os.mkdir(testgay_dir) 
    if not os.path.exists(testab_dir):
        os.mkdir(testab_dir)
        
    outfile=os.path.join(testgay_dir,"train_gc_ab.csv")
    train_gc_ab_df=shuffle(train_gc_ab_df,random_state =rs)
    train_gc_ab_df.to_csv(outfile,index=False) 
    
    outfile=os.path.join(testab_dir,"train_gc_gay.csv")
    train_gc_gay_df=shuffle(train_gc_gay_df,random_state =rs)
    train_gc_gay_df.to_csv(outfile,index=False) 
    
    outfile=os.path.join(testgc_dir,"train_ab_gay.csv")
    train_ab_gay_df=shuffle(train_ab_gay_df,random_state =rs)
    train_ab_gay_df.to_csv(outfile,index=False)  
    
    valfile=os.path.join(testgay_dir,"val_gc_ab.csv")
    val_gc_ab_df=shuffle(val_gc_ab_df,random_state =rs)
    val_gc_ab_df.to_csv(valfile,index=False) 
    
    valfile=os.path.join(testab_dir,"val_gc_gay.csv")
    val_gc_gay_df=shuffle(val_gc_gay_df,random_state =rs)
    val_gc_gay_df.to_csv(valfile,index=False) 
    
    valfile=os.path.join(testgc_dir,"val_ab_gay.csv")
    val_ab_gay_df=shuffle(val_ab_gay_df,random_state =rs)
    val_ab_gay_df.to_csv(valfile,index=False)  
      
           
             
            
def run(section):
    if section=="topic_indep":
        args= readCongigFile_topic_indep(section)
        train_gc=args[0]
        train_ab=args[1]
        train_gay=args[2]
        val_gc=args[3]
        val_ab=args[4]
        val_gay=args[5]
        topic_indep_dir=args[6]
        merge_topic_indep(train_gc,train_ab,train_gay,val_gc,val_ab,val_gay,topic_indep_dir)
    else:
    
        args= readCongigFile(section)
        inputfile=args[0]
        features_file=args[1]
        sent_column=args[2]
        w2vec_loc=args[3]
        tierlabelcol=args[4]
        keycolumn=args[5]
        lower_rank=args[6]
        upper_rank=args[7]
        valsplit=int(args[8])
        topic=args[9]
        pair_features(inputfile,features_file,sent_column,w2vec_loc,tierlabelcol,keycolumn,lower_rank,upper_rank,valsplit,topic)
        
 
def readCongigFile_topic_indep(section):
    config = configparser.ConfigParser()
    config_file=file_utilities.get_absolutepath_data("config","pairwise_data_Config.ini")
    config.read(config_file)
    
    train_gc=file_utilities.get_absolutepath_data(config.get(section,"train_gc"))
    train_ab=file_utilities.get_absolutepath_data(config.get(section,"train_ab"))
    train_gay=file_utilities.get_absolutepath_data(config.get(section,"train_gay"))
#     
#     test_gc=config.get(section,"test_gc")
#     test_ab=config.get(section,"test_ab")
#     test_gay=config.get(section,"test_gay")
#     
    val_gc=file_utilities.get_absolutepath_data(config.get(section,"val_gc"))
    val_ab=file_utilities.get_absolutepath_data(config.get(section,"val_ab"))
    val_gay=file_utilities.get_absolutepath_data(config.get(section,"val_gay"))
    topic_indep_dir=file_utilities.get_absolutepath_data(config.get(section,"topic_indep_dir"))
    
    
    arguments=( train_gc, train_ab, train_gay, val_gc, val_ab, val_gay,topic_indep_dir)
    return arguments
    
          

def readCongigFile(section):
        
        config = configparser.ConfigParser()
        config_file=file_utilities.get_absolutepath_data("config","pairwise_data_Config.ini")
        config.read(config_file)
        tierlabelcol=config.get(section,'tierlabelcol')
        sent_column=config.get(section,'sent_column')
        w2vec_loc=config.get(section,'w2vec_loc')
        input_file=config.get(section,"input_file")
        features_file=config.get(section,'features_file')
        keycolumn=config.get(section,'keycolumn')
        lower_rank=config.get(section,'lower_rank').split(",")
        upper_rank=config.get(section,'upper_rank').split(",")
        valsplit=config.get(section,'valsplit')
        topic=config.get(section,'topic')
        arguments=(input_file,features_file,sent_column,w2vec_loc,tierlabelcol,keycolumn,lower_rank,upper_rank,valsplit,topic)
    
        return  (arguments)
   
          
def topic_indep_data():
    print()
       
    
if __name__ == '__main__':
    section="GC"
    run(section)
    section="GM"
    run(section)
    section="AB"
    run(section)
    section="topic_indep"
    run(section)