'''
Created on Dec 9, 2017
http://bicepjai.github.io/machine-learning/2017/11/10/text-class-part1.html#cnn-for-sentence-classification
http://cs231n.github.io/convolutional-networks/
http://xrds.acm.org/blog/2016/06/convolutional-neural-networks-cnns-illustrated-explanation/
https://ujjwalkarn.me/2016/08/11/intuitive-explanation-convnets/
http://blog.districtdatalabs.com/parameter-tuning-with-hyperopt
@author: amita
starting point is run method. It will be called from aanother program(pairwise_lstm_sharedfunctions_testab_hyperopt.py,
pairwise_lstm_sharedfunctions_testgm_hyperopt.py,pairwise_lstm_sharedfunctions_testgc_hyperopt.py)
'''


import os,io,sys
import numpy as np
from hyperas.distributions import choice, uniform
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils.np_utils import to_categorical
from keras.layers import Dense, Input, Flatten
from keras.layers import Conv1D, MaxPooling1D, Embedding, Bidirectional
from keras.models import Model
from keras.layers import LSTM
from sklearn.preprocessing import LabelEncoder
#from sklearn.model_selection import train_test_split
import file_utilities
from keras.layers import Subtract
from keras.layers import Concatenate
from keras.layers import concatenate
from keras.layers import Multiply
from keras.layers import Dropout
import configparser
import pandas as pd
import matplotlib
from keras.callbacks import ModelCheckpoint
from keras.callbacks import History
from keras.optimizers import Adam
from sklearn.metrics import classification_report
from sklearn.metrics import f1_score
matplotlib.use('PS')
import matplotlib.pyplot as plt
import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
from pprint import pprint
from keras.engine.topology import Layer
from hyperopt import fmin, tpe,anneal, hp, STATUS_OK, Trials
from concise.hyopt import CompileFN, CMongoTrials, test_fn
from keras.models import load_model
datadir=file_utilities.DATA_DIR
GLOVE_DIR="/Users/amita/software/glove.6B"

word_index={}
embeddings_index = {} 
MAX_NB_WORDS = 20000 
EMBEDDING_DIM=100

f = open(os.path.join(GLOVE_DIR, 'glove.6B.100d.txt'), "r", encoding='utf-8')
#f = open('glove.6B.100d.txt', 'r')
for line in f:
    values = line.split()
    word = values[0]
    coefs = np.asarray(values[1:], dtype='float32')
    embeddings_index[word] = coefs
f.close()

print('Found %s word vectors.' % len(embeddings_index))
print('Processing text dataset')

 
def get_data(df,feature_list,textcol1,textcol2,label_col):
    
    Sent1s=df[textcol1].values
    Sent1s=[text.lower() for text in Sent1s]
    
    Sent2s=df[textcol2].values
    Sent2s=[text.lower() for text in Sent2s]
    
    labels=df[label_col].values
    colnames=list(df.columns.values)
    
    feature_names=[col for col in colnames if str(col).startswith(tuple(feature_list))]
    
    feature_S1=[col for col in feature_names if str(col).endswith("_sent1")]
    feature_df_S1=df[feature_S1]
    
    feature_S2=[col for col in feature_names if str(col).endswith("_sent2")]
    feature_df_S2=df[feature_S2]
    return (Sent1s,Sent2s,feature_df_S1,feature_df_S2,labels)


def vectorize_text (train_texts_S1, train_texts_S2,test_texts_S1,test_texts_S2,val_texts_S1,val_texts_S2,labels_train,labels_test,labels_val,trunc,padding,seq_length):
    """ vectorize the text samples into a 2D integer tensor
    """
    global word_index, train_length
    MAX_SEQUENCE_LENGTH=seq_length
    TRUNC=trunc
    PADDING=padding
    tokenizer = Tokenizer(nb_words=MAX_NB_WORDS)
    train_texts=train_texts_S1+ train_texts_S2
    tokenizer.fit_on_texts(train_texts)
    
    sequences_train_S1 = tokenizer.texts_to_sequences(train_texts_S1)
    sequences_train_S2 = tokenizer.texts_to_sequences(train_texts_S2)
    word_index = tokenizer.word_index
    print('Found %s unique tokens.' % len(word_index))

    # encode class values as integers
    encoder = LabelEncoder()
    encoder.fit(labels_train)
    encoded_labels_train = encoder.transform(labels_train)
    # convert integers to dummy variables (i.e. one hot encoded)
    labels_train= to_categorical(encoded_labels_train)
    data_train_S1 = pad_sequences(sequences_train_S1, maxlen=MAX_SEQUENCE_LENGTH,truncating=TRUNC,padding=PADDING)
    data_train_S2 = pad_sequences(sequences_train_S2, maxlen=MAX_SEQUENCE_LENGTH,truncating=TRUNC,padding=PADDING)

    
    
    sequences_test_S1 = tokenizer.texts_to_sequences(test_texts_S1)
    sequences_test_S2 = tokenizer.texts_to_sequences(test_texts_S2)
    data_test_S1 = pad_sequences(sequences_test_S1, maxlen=MAX_SEQUENCE_LENGTH,truncating=TRUNC,padding=PADDING)
    data_test_S2 = pad_sequences(sequences_test_S2, maxlen=MAX_SEQUENCE_LENGTH,truncating=TRUNC,padding=PADDING)
    
    
    sequences_val_S1 = tokenizer.texts_to_sequences(val_texts_S1)
    sequences_val_S2 = tokenizer.texts_to_sequences(val_texts_S2)
    data_val_S1 = pad_sequences(sequences_val_S1, maxlen=MAX_SEQUENCE_LENGTH,truncating=TRUNC,padding=PADDING)
    data_val_S2 = pad_sequences(sequences_val_S2, maxlen=MAX_SEQUENCE_LENGTH,truncating=TRUNC,padding=PADDING)



    labels_test = to_categorical(encoder.transform(labels_test))
    labels_val = to_categorical(encoder.transform(labels_val))
    
    train_length = data_train_S1.shape[0]
    print('Shape of data tensor:', data_train_S1.shape)
    print('Shape of label tensor:', labels_train.shape)
    return(data_train_S1,data_train_S2,labels_train,data_test_S1,data_test_S2,labels_test,data_val_S1,data_val_S2,labels_val)


def writecsv(Y_pred,Y_actual,df,outputfile,f1weighted,shared, embedd_shared,add_features,acurracy,param_dict,report):
    for col in df.columns:
        if 'liwc' in col or "read" in col:
            del df[col]
    assert(len(Y_pred)==len(Y_actual)==df.shape[0])

    df["Y_actual"]=Y_actual
    df["Y_predicted"]=Y_pred
    df.to_csv(outputfile,index=False)
    f = io.open(outputfile, "a",encoding='utf-8')
    result1= str("\n shared  "+ str(shared)) + str(" \n embedd_shared "+ str(embedd_shared)) + str("epoch"+str(param_dict["epochs"]))+ str("\n acurracy  " +str(acurracy) )  + str("\n f1weighted"  +str(f1weighted)) + str("classification_report" +str(report))
    result2= "baseline: "+ str(param_dict["baseline"]) +"kernelsize1  "+str(param_dict["kernelsize"]) + "denselayer"+ str(param_dict["denselayer"]) +"poollength"+ str(param_dict["poollength"]) + "filterratio1: " + str(param_dict["filterratio"] )
    result=result1 + result2
    with open(outputfile, 'a') as f :
        pprint(result,stream=f)
   


   
def add_missing_featurecols(df,missing_cols): 
    for col in  missing_cols:
        df.insert(0,col,0)
    return df  

 
def create_embedding_matrix(word_index,seq_length):    
    nb_words = min(MAX_NB_WORDS, len(word_index))
    embedding_matrix = np.zeros((nb_words + 1, EMBEDDING_DIM))
    for word, i in word_index.items():
        if i > MAX_NB_WORDS:
            continue
        embedding_vector = embeddings_index.get(word)
        if embedding_vector is not None:
            # words not found in embedding index will be all-zeros.
            embedding_matrix[i] = embedding_vector

    embedding_layer = Embedding(nb_words + 1,
                                EMBEDDING_DIM,
                                weights=[embedding_matrix],
                                input_length=seq_length,
                                trainable=True)
    return embedding_layer 


def create_base_network(input_shape,dropout,dim,filterratio,kernelsize,poollength,denselayer):
    
    input_1 = Input(shape=(input_shape,))
    embedded_layer = create_embedding_matrix(word_index,input_shape)
    encoded_sent = embedded_layer(input_1)
    encoded_sent = Dropout(dropout)(encoded_sent)
    encoded_sent= Conv1D(nb_filter=int(filterratio),kernel_size=kernelsize,padding='same', activation='relu')(encoded_sent)
    encoded_sent= MaxPooling1D(pool_length=poollength)(encoded_sent)
    encoded_sent= Conv1D(nb_filter=int(filterratio), kernel_size=kernelsize, padding='same', activation='relu')(encoded_sent)
    encoded_sent= MaxPooling1D(pool_length=poollength)(encoded_sent)
    encoded_sent = Dropout(dropout)(encoded_sent)
    encoded_sent=Flatten()(encoded_sent)
    encoded_sent=Dense(denselayer, activation='relu')(encoded_sent)
  
    
    return Model(input_1,encoded_sent)

def create_model_noshared_baseline(train_data,features_count,add_features,dropout,dim,optim,filterratio,kernelsize,poollength,denselayer,seq_length,act):
    print("executing cnn no sharing")

    MAX_SEQUENCE_LENGTH=seq_length
    input_1 = Input(shape=(MAX_SEQUENCE_LENGTH,),name='sent1_input')
    input_2 = Input(shape=(MAX_SEQUENCE_LENGTH,),name='sent2_input')
    
    features_1 = Input(shape=(features_count,),name='features1_input')
    features_2 = Input(shape=(features_count,),name='features2_input')
    
    encoded_sent1 = create_embedding_matrix(word_index,MAX_SEQUENCE_LENGTH) (input_1)
    encoded_sent1 = Dropout(dropout)(encoded_sent1)
    encoded_sent1= Conv1D(nb_filter=int(filterratio),kernel_size=kernelsize, padding='same', activation='relu')(encoded_sent1)
    encoded_sent1= MaxPooling1D(pool_length=poollength)(encoded_sent1)
    encoded_sent1=Dropout(dropout)( encoded_sent1 )
    encoded_sent1=Flatten()(encoded_sent1)
    encoded_sent1=Dense(denselayer, activation='relu')(encoded_sent1)
    #encoded_sent21=GaussianNoise(noise)(encoded_sent21)
    
    encoded_sent2 = create_embedding_matrix(word_index,MAX_SEQUENCE_LENGTH) (input_2)
    encoded_sent2 = Dropout(dropout)(encoded_sent2)
    encoded_sent2= Conv1D(nb_filter=int(filterratio),kernel_size=kernelsize, padding='same', activation='relu')(encoded_sent2)
    encoded_sent2= MaxPooling1D(pool_length=poollength)(encoded_sent2)
    encoded_sent2=Dropout(dropout)( encoded_sent2 )
    encoded_sent2=Flatten()(encoded_sent2)
    encoded_sent2=Dense(denselayer, activation='relu')(encoded_sent2)
    
    merged= Concatenate()([encoded_sent1, encoded_sent2])
    merged =Dropout(dropout)(merged)
    preds=Dense(2, activation=act)(merged)
    model_merged = Model([input_1,input_2,features_1,features_2], preds)
    model_merged.compile(loss='categorical_crossentropy', optimizer=optim, metrics=['accuracy']) 
    model_merged.summary()
    return model_merged


def create_model_shared(train_data,features_count,add_features,dropout,dim,optim,filterratio,kernelsize,poollength,denselayer,seq_length,act):
    print("executing shared cnn")
    # network definition
    MAX_SEQUENCE_LENGTH=seq_length
    
    input_1 = Input(shape=(MAX_SEQUENCE_LENGTH,),name='sent1_input')
    input_2 = Input(shape=(MAX_SEQUENCE_LENGTH,),name='sent2_input')
    
    features_1 = Input(shape=(features_count,),name='features1_input')
    features_2 = Input(shape=(features_count,),name='features2_input')
    # network definition
    base_network = create_base_network(MAX_SEQUENCE_LENGTH,dropout,dim,filterratio,kernelsize,poollength,denselayer)
    encoded_sent1=base_network(input_1)
    encoded_sent2=base_network(input_2)
    
    subtracted = Subtract()([encoded_sent1, encoded_sent2])
    product= Multiply()([encoded_sent1, encoded_sent2])
    merged=concatenate([subtracted,product])
    merged =Dropout(dropout)(merged)
    
    preds=Dense(2, activation= act)(merged)
    model_merged = Model([input_1,input_2,features_1,features_2], preds)
    model_merged.compile(loss='categorical_crossentropy', optimizer=optim, metrics=['accuracy']) 
    model_merged.summary()
    return model_merged


def create_model_bilstm_baseline(train_data,features_count,add_features,dropout,dim,optim,filterratio,kernelsize,poollength,denselayer,seq_length,act):
    print("executing lstm")
    MAX_SEQUENCE_LENGTH=seq_length
    input_1 = Input(shape=(MAX_SEQUENCE_LENGTH,),name='sent1_input')
    input_2 = Input(shape=(MAX_SEQUENCE_LENGTH,),name='sent2_input')
    
    features_1 = Input(shape=(features_count,),name='features1_input')
    features_2 = Input(shape=(features_count,),name='features2_input')
    
    encoded_sent1 = create_embedding_matrix(word_index,MAX_SEQUENCE_LENGTH) (input_1)
    encoded_sent1 = Dropout(dropout)(encoded_sent1)
    encoded_sent1=Bidirectional(LSTM(denselayer))(encoded_sent1)

    encoded_sent2 =  create_embedding_matrix(word_index,MAX_SEQUENCE_LENGTH)(input_2)
    encoded_sent2 = Dropout(dropout)(encoded_sent2)
    encoded_sent2=Bidirectional(LSTM(denselayer))(encoded_sent2)
    
    merged=Concatenate()([ encoded_sent1 , encoded_sent2])
    merged =Dropout(dropout)(merged)
    preds=Dense(2, activation=act)(merged)
    model_merged = Model([input_1,input_2,features_1,features_2], preds)
    model_merged.compile(loss='categorical_crossentropy', optimizer=optim, metrics=['accuracy']) 
    return model_merged
    
    

def createdatamodel(data_train_S1,data_train_S2,features_train_S1, features_train_S2,labels_train,data_test_S1,data_test_S2,features_test_S1,features_test_S2,labels_test,data_val_S1,data_val_S2,features_val_S1,features_val_S2,labels_val):
    
    data=(({"sent1_input":data_train_S1,"sent2_input": data_train_S2,"features1_input":features_train_S1,"features2_input":features_train_S2}, labels_train), ({"sent1_input":data_val_S1, "sent2_input":data_val_S2, "features1_input":features_val_S1,"features2_input":features_val_S2},labels_val),({"sent1_input":data_test_S1,"sent2_input" :data_test_S2,"features1_input":features_test_S1,"features2_input":features_test_S2},labels_test))
    return data



def createhyperparam_dict(data_train_S1,data_train_S2,labels_train, features_train_S1, features_train_S2,data_test_S1,data_test_S2,features_test_S1,features_test_S2,labels_test,data_val_S1,data_val_S2,features_val_S1,features_val_S2,labels_val,param_dict):
    
    features_count=param_dict["feature_count"]
    seq_length=param_dict["seq_length"]
    act=param_dict["activation_last"]
    denselayer=param_dict["denselayer"]
    batch= param_dict["batch"]
    kernelsize=param_dict["kernelsize"]
    #kernelsize2=param_dict["kernelsize2"]
    filteratio=param_dict["filterratio"]
    poollength=param_dict["poollength"]
    dim=param_dict["dim"]
    dropout=param_dict["dropout"]
    add_features=param_dict["add_features"]
    epochs=param_dict["epochs"]
    
    
    lrate=param_dict["lr"]
    optim= Adam(lr=lrate, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
    
    
    hyper_params = {
                        
    "data":{"data_train_S1":data_train_S1,
            "data_train_S2":data_train_S2,
            "features_train_S1":features_train_S1,
            "features_train_S2":features_train_S2,
            "labels_train":labels_train,
            "data_test_S1":data_test_S1,
            "data_test_S2":data_test_S2,
            "features_test_S1":features_test_S1,
            "features_test_S2":features_test_S1,
            "labels_test":labels_test,
            "data_val_S1":data_val_S1,
            "data_val_S2":data_val_S2,
            "features_val_S1":features_val_S1,
            "features_val_S2":features_val_S2,
            "labels_val":labels_val
            },
    "model": {
         "features_count":features_count,
         "dropout":dropout,
         "dim":dim,
         "optim":optim,
        "denselayer": hp.choice("m_denselayer", tuple(denselayer)),
         "act":act,
         "seq_length":seq_length,
         "add_features":add_features,
        #"kernelsize1":hp.choice("m_kernelsize1",tuple(kernelsize1)),
        "kernelsize":hp.choice("m_kernelsize2",tuple(kernelsize)),
       "poollength":hp.choice("m_poollength", tuple(poollength)),
        "filterratio":hp.choice("m_filterratio",tuple(filteratio))
         }       
         ,
    "fit": {
               #"batch_size":hp.choice("m_batch",tuple(batch)),
        "epochs":  epochs,
        "batch_size": batch[0],
    }
}
    
    return hyper_params


    
def findbestparam(data_train_S1,data_train_S2,labels_train, features_train_S1, features_train_S2,data_test_S1,data_test_S2,features_test_S1,features_test_S2,labels_test,data_val_S1,data_val_S2,features_val_S1,features_val_S2,labels_val,param_dict):
    
    baseline=param_dict["baseline"]
    shared=param_dict["shared"]
    outputmodeldir=param_dict["outmodelDir"]
    maxeval=param_dict["maxeval"]
    if  not os.path.exists(outputmodeldir):  
        os.mkdir(outputmodeldir)
    
    if baseline==1:
        print("baseline is 1, executing lstm")
        model_name=create_model_bilstm_baseline
    else:
        if shared==0:
            model_name=create_model_noshared_baseline
        else:
            model_name=create_model_shared
    
    hyper_params=createhyperparam_dict(data_train_S1,data_train_S2,labels_train, features_train_S1, features_train_S2,data_test_S1,data_test_S2,features_test_S1,features_test_S2,labels_test,data_val_S1,data_val_S2,features_val_S1,features_val_S2,labels_val,param_dict)
    db_name="imdb"
    exp_name="myexp1"
    objective = CompileFN(db_name, exp_name,
                      data_fn=createdatamodel,
                      model_fn= model_name,
                      loss_metric="loss", # which metric to optimize for
                      loss_metric_mode="min",  # try to maximize the metric
                      valid_split=None, # use 20% of the training data for the validation set
                      save_model='best', # checkpoint the best model
                      save_results=False,
                      save_dir=outputmodeldir)  # place to store the models
        
        #test_fn(objective, hyper_params) 
    trials = Trials()
    best=fmin(objective, hyper_params,trials=trials, algo=tpe.suggest, max_evals=maxeval)
    besttrial=trials.best_trial
    result=besttrial["result"]
    storebestparam(result,param_dict, trials)
       

def storebestparam(result,param_dict,trials):
   
    resfile= param_dict["best_paramfile"]
    plot_file_loss=os.path.join(os.path.dirname(resfile), "bestresult_losscurve.png")
    historyfile=resfile[:-4] +"_history.csv"
    allrows=[]

# Best results stats        
    fit_param=result["param"]["fit"]
    model_param=result["param"]["model"]
    model_param.update(fit_param)
    
     
    rows=[]
    rows.append( model_param)
    df=pd.DataFrame(rows)
    df.to_csv(resfile,index=False)
    train_hist=result["history"]["loss"]
    plt.plot(train_hist["epoch"], train_hist["loss"], label="Training")
    plt.plot(train_hist["epoch"] , train_hist["val_loss"], label="Validation")
    plt.xlabel("Epoch")
    plt.ylabel("Cross-entropy loss")
    plt.legend()
    plt.savefig(plot_file_loss)
    plt.close()
    
    # information about all trials
    for trial in trials:
        newdict={}
        param=trial["result"]["param"]["fit"]
        model=trial["result"]["param"]["model"]
        allloss=trial["result"]["history"]["loss"]
        newdict["loss"]=trial["result"]["loss"]
        newdict.update(param)
        newdict.update(model)
        newdict.update(allloss)
        newdict["trial_id"]=trial["tid"]
        allrows.append(newdict)
    
    
    df=pd.DataFrame(allrows) 
    df.to_csv(historyfile,index=False)   
    
    

def train_bestparam(df_test,data_train_S1,data_train_S2,labels_train, features_train_S1, features_train_S2,data_test_S1,data_test_S2,features_test_S1,features_test_S2,labels_test,data_val_S1,data_val_S2,features_val_S1,features_val_S2,labels_val,param_dict):  
    shared=param_dict["shared"]
    feature_list=param_dict["feature_list"]
    row_unique=param_dict["row_unique"]
    add_features= param_dict["add_features"]
    embedd_shared=param_dict["embedd_shared"]
    epochs=param_dict["epochs"]
    dim=param_dict["dim"]
    dropout=param_dict["dropout"]
    seq_length=param_dict["seq_length"]
    baseline=param_dict["baseline"]
    seed=param_dict["seed"]
    rs=param_dict["rs"]
    filterratio=param_dict["filterratio"][0]
    kernelsize=param_dict["kernelsize"][0]
    #kernelsize2=param_dict["kernelsize2"][0]
    poollength=param_dict["poollength"][0]
    denselayer=param_dict["denselayer"][0]
    batch= param_dict["batch"][0]
    outputBaseDir= param_dict["outputBaseDir"]
    act=param_dict["activation_last"]
    topic=param_dict["test_topic"]

    if not os.path.exists( outputBaseDir):
        os.mkdir( outputBaseDir)
        
    outputfile=os.path.join(outputBaseDir, topic+"_cnnresults.txt")   
    
    with open(outputfile, 'a') as out:
                pprint("starting for epochs {0}  and seed {1}, randomstate {2} , dim {3}, dropout {4}".format(epochs,seed,rs,dim,dropout),stream=out)  
 
    filepath=os.path.join(outputBaseDir,"weights.best.hdf5_rowno"+ str(row_unique))
    plot_file_acc=filepath+ "_acc.png"
    plot_file_loss=filepath+"_loss.png"
    if os.path.exists(filepath):
        os.remove(filepath)
    
    history = History()    
    checkpoint = ModelCheckpoint(filepath,  monitor='val_loss', verbose=1, save_best_only=True, mode='auto')
    callbacks_list = [history, checkpoint]
    features_count=param_dict["feature_count"]
    
    lrate=param_dict["lr"]
    optim= Adam(lr=lrate, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
    datatrain=None
    
    
    if baseline==1:
        model=create_model_bilstm_baseline(datatrain,features_count,add_features,dropout,dim,optim,filterratio,kernelsize,poollength,denselayer,seq_length,act)
    else:     
        if shared==0:
            model= create_model_noshared_baseline(datatrain,features_count,add_features,dropout,dim,optim,filterratio,kernelsize,poollength,denselayer,seq_length,act)
        else:    
                model=create_model_shared(datatrain,features_count,add_features,dropout,dim,optim,filterratio,kernelsize,poollength,denselayer,seq_length,act)
            
    history_mod=model.fit({"sent1_input":data_train_S1, 'sent2_input': data_train_S2, "features1_input":features_train_S1,"features2_input":features_train_S2}, labels_train, validation_data=({"sent1_input":data_val_S1, 'sent2_input': data_val_S2, "features1_input":features_val_S1,"features2_input":features_val_S2},labels_val), epochs=epochs, batch_size=batch, callbacks=callbacks_list, verbose=1)
    
    model.load_weights( filepath)
    model.compile(loss='categorical_crossentropy', optimizer=optim, metrics=['accuracy'])
    scores = model.evaluate([data_test_S1,data_test_S2,features_test_S1,features_test_S2], labels_test, verbose=1)
    print(history_mod.history.keys())
    # summarize history for accuracy
    plt.plot(history_mod.history['acc'])
    plt.plot(history_mod.history['val_acc'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.savefig(plot_file_acc)
    plt.close()
    # summarize history for loss
    plt.plot(history_mod.history['loss'])
    plt.plot(history_mod.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.savefig(plot_file_loss)
    plt.close()
    pred = model.predict([data_test_S1, data_test_S2, features_test_S1,features_test_S2], batch_size=64)
    y_pred_values=pred.argmax(axis=1)
    y_test_values=labels_test.argmax(axis=1)
    #pred_list= [{'Y_actual': v1, 'Y_predicted': v2} for v1, v2 in zip( y_test_values, y_pred_values)]

    acurracy=scores[1]*100
    f1weighted=f1_score(labels_test.argmax(axis=1), pred.argmax(axis=1),average="macro")
    report=classification_report(labels_test.argmax(axis=1), pred.argmax(axis=1))

    csvwithpath=outputfile[:-4]+"rowno "+str(row_unique)+".csv"
    writeresult(outputfile,f1weighted,shared, embedd_shared,add_features,acurracy,param_dict,report)
    writecsv(y_pred_values,y_test_values,df_test,  csvwithpath,f1weighted,shared, embedd_shared,add_features,acurracy,param_dict,report)
    


def writeresult(outputfile,f1weighted,shared, embedd_shared,add_features,acurracy,param_dict,report):
    trunc=param_dict["trunc"]
    padding=param_dict["padding"]
    result1= str("\n shared  "+ str(shared)) + str(" \n embedd_shared "+ str(embedd_shared)) + str("epoch"+str(param_dict["epochs"]))+ str("lr"+str(param_dict["lr"])) +str( "\n addfetaures" + str(add_features)) + str("\n acurracy  " +str(acurracy) )  + str("\n f1weighted"  +str(f1weighted)) + str("classification_report" +str(report)+"TRUNC: " + trunc  + " padding"+ padding)
    result2= "baseline: "+ str(param_dict["baseline"]) + "kernelsize: "+ str(param_dict["kernelsize"]) +"denselayer: "+ str(param_dict["denselayer"]) +"poollength: "+ str(param_dict["poollength"]) +  "filterratio: " + str(param_dict["filterratio"])
    result=result1 + result2
    with open(outputfile, 'a') as f :
        pprint(result,stream=f)   

    
def  runlstm(resultCSVFile,all_list,param_dict):
    """
    inputtrain and input test files should be balanced and shuffled
    feature_list: features to be included
    """
    
    #K.clear_session()
    input_train=file_utilities.get_absolutepath_data(param_dict["input_train"])
    input_test=file_utilities.get_absolutepath_data(param_dict["input_test"])
    input_val=file_utilities.get_absolutepath_data(param_dict["input_val"])
    outBaseDir=file_utilities.get_absolutepath_data(param_dict["outputBaseDir"])
    param_dict["outputBaseDir"]= outBaseDir
    textcol1=param_dict["text_col1"]
    textcol2=param_dict["text_col2"]
    label_col=param_dict["label_col"]
    shared=param_dict["shared"]
    feature_list=param_dict["feature_list"]
    row_unique=param_dict["row_unique"]
    add_features= param_dict["add_features"]
    embedd_shared=param_dict["embedd_shared"]
    epochs=param_dict["epochs"]
    dim=param_dict["dim"]
    dropout=param_dict["dropout"]
    trunc=param_dict["trunc"]
    padding=param_dict["padding"]
    seq_length=param_dict["seq_length"]
    baseline=param_dict["baseline"]
    seed=param_dict["seed"]
    rs=param_dict["rs"]
    typelearn=param_dict["typelearn"]
    param_dict["outmodelDir"]=outBaseDir +"rowno" +str( row_unique) 
    best_paramfile=os.path.join(param_dict["outmodelDir"],"bestparam_hyperas.csv")
    param_dict["best_paramfile"]= best_paramfile
    
    

    df_train=pd.read_csv(input_train)
    df_train=df_train.fillna(0)
    train_column=set(df_train.columns.tolist())
    
    
    df_test=pd.read_csv(input_test)
    df_test=df_test.fillna(0)
    test_column=set(df_test.columns.tolist())
    
    df_val=pd.read_csv(input_val)
    df_val=df_val.fillna(0)
    val_column=set(df_val.columns.tolist())

    
    add_col_test=set.difference(train_column,test_column)
    df_test=add_missing_featurecols(df_test, add_col_test)
    
    add_col_train=set.difference(test_column,train_column)
    df_train=add_missing_featurecols(df_train, add_col_train)
    
    add_col_val=set.difference(train_column,val_column)
    df_val=add_missing_featurecols(df_val, add_col_val)
    

    
    train_texts_S1,train_texts_S2,feature_df_train_S1,feature_df_train_S2,labels_train=get_data(df_train,feature_list,textcol1,textcol2,label_col)
    test_texts_S1,test_texts_S2,feature_df_test_S1,feature_df_test_S2,labels_test=get_data(df_test,feature_list,textcol1,textcol2,label_col)
    val_texts_S1,val_texts_S2,feature_df_val_S1,feature_df_val_S2,labels_val=get_data(df_val,feature_list,textcol1,textcol2,label_col)

    
    features_train_S1=feature_df_train_S1.as_matrix()
    features_train_S2=feature_df_train_S2.as_matrix()

    
    features_test_S1=feature_df_test_S1.as_matrix()
    features_test_S2=feature_df_test_S2.as_matrix()
    
    features_val_S1=feature_df_val_S1.as_matrix()
    features_val_S2=feature_df_val_S2.as_matrix()
    
    
    
    data_train_S1,data_train_S2,labels_train,data_test_S1,data_test_S2,labels_test,data_val_S1,data_val_S2,labels_val=vectorize_text(train_texts_S1, train_texts_S2,test_texts_S1,test_texts_S2,val_texts_S1, val_texts_S2,labels_train,labels_test,labels_val,trunc,padding,seq_length)
        
    feature_count=feature_df_train_S1.shape[1]
    param_dict["feature_count"]=feature_count
    
    if typelearn=="findbestparam":
        findbestparam(data_train_S1,data_train_S2,labels_train, features_train_S1, features_train_S2,data_test_S1,data_test_S2,features_test_S1,features_test_S2,labels_test,data_val_S1,data_val_S2,features_val_S1,features_val_S2,labels_val, param_dict)
        #evaluate(param_dict,data_test_S1,data_test_S2,features_test_S1,features_test_S2,labels_test,df_test)
    
    
    else:
        if  typelearn=="train_knownbestparam":
            train_bestparam(df_test,data_train_S1,data_train_S2,labels_train, features_train_S1, features_train_S2,data_test_S1,data_test_S2,features_test_S1,features_test_S2,labels_test,data_val_S1,data_val_S2,features_val_S1,features_val_S2,labels_val, param_dict)
    
def run(section,rs,seed):
    
    config = configparser.ConfigParser()
    lstminput=file_utilities.get_absolutepath_data("config","pairwiselstm_config.ini")
    config.read(lstminput)
    input_file_config=config.get(section,"csv_with_file_list")
    resultCSVFile_config=config.get(section,'resultCSVFile')
    input_file=file_utilities.get_absolutepath_data(input_file_config)
    resultCSVFile=file_utilities.get_absolutepath_data(resultCSVFile_config) 
    inputdata_df=pd.read_csv(input_file)
    inputdata_df.fillna(0)
    print(" CNN for section {0}".format(section))
    all_list=[]
    if os.path.exists(resultCSVFile):
            os.remove(resultCSVFile)
    for index, row in inputdata_df.iterrows():
        param_dict={}
        done=row["done"]
        if int(done)==1:
            continue
        param_dict["input_train"]=row["input_train"]
        param_dict["input_test"]= row["input_test"]
        param_dict["input_val"]= row["input_val"]
        param_dict["outputBaseDir"]= row["outputBaseDir"]
        param_dict["text_col1"]=row["textcol1"]
        param_dict["text_col2"]=row["textcol2"]
        param_dict["label_col"]=row["label_col"]
        param_dict["shared"]=int(row["shared"])
        param_dict["feature_list"]=list(row["feature_list"].split(","))
        param_dict["row_unique"]=row["row_unique"]
        param_dict["add_features"]=int(row["add_features"])
        param_dict["embedd_shared"]=int(row["embedd_shared"])
        param_dict["epochs"]=int(row["epochs"])
        param_dict["lr"]=float(row["lr"])
        param_dict["dim"]=int(row["dim"])
        param_dict["dropout"]=float(row["dropout"])
        param_dict["trunc"] =row["trunc"]
        param_dict["padding"]=row["padding"]
        param_dict["activation_last"]=row["activation_last"]
        param_dict["seq_length"]=row["seq_length"]
        param_dict["baseline"]=int(row["baseline"])
        param_dict["kernelsize"]=[int(x) for x in row["kernelsize1"].split(",")]
        #param_dict["kernelsize2"]=[int(x) for x in row["kernelsize2"].split(",")]
        param_dict["filterratio"]=[int(x) for x in row["filterratio"].split(",")]
        param_dict["poollength"]=[int(x) for x in row["poollength"].split(",")]
        param_dict["batch"]=[int(x) for x in str(row["batch"]).split(",")]
        param_dict["seed"]=seed
        param_dict["denselayer"]=[int(x) for x in row["denselayer"].split(",")]
        param_dict["rs"]=rs 
        param_dict["typelearn"]=row["typelearn"]
        param_dict["test_topic"]=row["test_topic"]
        param_dict["maxeval"]=int(row["maxeval"])
        runlstm(resultCSVFile, all_list,param_dict)  
 
        
if __name__ == '__main__':
    pass

