'''
Created on May 8, 2017

@author: amita
'''
import configparser
import pandas as pd
import numpy as np
import os, codecs
import file_utilities
import itertools
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import nltk
from nltk.corpus import stopwords
stop = set(stopwords.words('english'))
from scipy import linalg, mat, dot


def preprocesssent(sent):
    words = [word for word in nltk.word_tokenize(sent) if  word not in stop]
    sent = " ".join(words)
    return sent

def cosinesimilarity(sents):
    count_vectorizer = CountVectorizer()
    matrix=count_vectorizer.fit_transform(sents) 
    similarity=cosine_similarity(matrix,matrix)
    triang=np.triu(similarity)
    num_docs=len(sents)
    #sum=np.matrix.sum(triang)
    mat=np.asmatrix(triang)
    print(mat)
    sum_docs=np.matrix.sum(mat) - num_docs
    average=sum_docs/num_docs
    #sim=cosine_similarity(matrix[0:1], matrix) 
    return average
    

def execute(df,sent_col,input_file,output_file):
    sents_imp_process=[]
    sents_nonimp_process=[]
    allsents_process=[]
    df['label'] = df['label'].astype(int)
    dialog_gps=df.groupby("Key")
    for key,dialogdf in dialog_gps:
        allsents=df[sent_col].values
        important_df=dialogdf[dialogdf['label']>=3]
        sents_imp=important_df[sent_col].values
        for sent_imp in sents_imp:
            process_sent_imp=preprocesssent(sent_imp)
            sents_imp_process.append(process_sent_imp)
            
        non_impdf=dialogdf[dialogdf['label']<3]
        sents_nonimp= non_impdf[sent_col].values
        
        for sent_nonimp in sents_nonimp:
            process_nonsent=preprocesssent(sent_nonimp)
            sents_nonimp_process.append(process_nonsent)   
        
        for sent in allsents:
            process_sent=preprocesssent(sent)
            allsents_process.append(process_sent)
        
        
        avg_sim_imp=cosinesimilarity(sents_imp_process)
        avg_sim_nonimp=cosinesimilarity(sents_nonimp_process)
        avg_sim_all=cosinesimilarity(allsents_process)
        #cosinesimilarity(sents_nonimp)
    
def run(section):
    config = configparser.ConfigParser()
    input_file=file_utilities.get_absolutepath_data("config","pairwise_cosine.config.ini")
    config.read(input_file)
    input_file_config=config.get(section,"input_file")
    output_file_config=config.get(section,'output_file')
    input_file=file_utilities.get_absolutepath_data(input_file_config)
    output_file=file_utilities.get_absolutepath_data(output_file_config)
    sent_col=config.get(section,'sent_col')
    inputdata_df=pd.read_csv(input_file)
    inputdata_df.fillna(0)
    execute(inputdata_df,sent_col,input_file,output_file)
if __name__ == '__main__':
      
#     section="GC"
#     run(section)
#      
#      
#     section ="GM"
#     run(section)
#     
    
    section ="AB"
    run(section)