'''
Created on Apr 7, 2017

@author: amita
'''
import  pdtb_helper
import os
randomseed=4
import pandas as pd
from stanford_parser_arg import filtersentence
import stanford_nlp_features
import  configparser
import file_utilities
import copy
from pdtb_extractor import PDTBExtractor
import multiprocessing as mp
from functools import partial

def testcorefworked(output_coref):
    df=pd.read_csv(output_coref)

def addcorefsent(input_file):
        df=pd.read_csv(input_file+".csv")
        all_dfs=[]
        dialog_groups=df.groupby("Key")
        for key, dialogdf in dialog_groups:
            
            sent_count=len(dialogdf["sent"].tolist())
            dialog= " ".join(dialogdf["sent"].tolist())
            
#             if str(key)!= "1-9137_112_100__114_117_121_122_130_2":
#                 continue
            
            if str(key) =="1-10121_22_21__23_28_30_34_37_1":
                sents=dialogdf["sent"].tolist()
                sents[1]=sents[1].replace(". ", "")
                sents[1]=sents[1].replace(". ,", "")
                sents[1]=sents[1]+". "
                dialog=" ".join(sents)
                
            if str(key)== "1-9137_112_100__114_117_121_122_130_2":
                sents=dialogdf["sent"].tolist()
                sents[63]=sents[63].replace(". ", "")
                sents[63]=sents[63].replace(". ,", "")
                dialog=" ".join(sents)
                
            res_nocoref=stanford_nlp_features.parse_text(dialog, mode=stanford_nlp_features.Mode.SIMPLE_PARSE)
            doc_dict_nocoref=stanford_nlp_features.parse_res_processor(res= res_nocoref)
            allsentences_dialog_dict_nocoref=doc_dict_nocoref["corenlp_info"]
            allsents_dialog_nocoref=[[" " .join(d['snlp_tokens']) for d in  allsentences_dialog_dict_nocoref]][0]
            sentiment_allsents_nocoref=[d['snlp_sentiment'][0] for d in  allsentences_dialog_dict_nocoref]
            ner_allsents_nocoref=[d['snlp_num_ner'][0] for d in  allsentences_dialog_dict_nocoref]
            adv_allsents_nocoref=[d['snlp_num_adv'][0] for d in  allsentences_dialog_dict_nocoref]
            adj_allsents_nocoref=[d['snlp_num_adj'][0] for d in  allsentences_dialog_dict_nocoref]
            tree_height_allsents_nocoref=[d['snlp_tree_height']for d in  allsentences_dialog_dict_nocoref]
            
            res=stanford_nlp_features.parse_text(dialog, mode=stanford_nlp_features.Mode.SMART_COREF)
            doc_dict=stanford_nlp_features.parse_res_processor(res=res)
            
            allsentences_dialog_dict=doc_dict["corenlp_info"]
            allsents_dialog=[[" " .join(d['snlp_tokens']) for d in  allsentences_dialog_dict]][0]
            
            
            sent_countwithcoref=len(allsents_dialog)
            if sent_count ==sent_countwithcoref:
                dialogdf["sent_coref"]=allsents_dialog
                all_dfs.append(dialogdf)
            else:
                print(key)
                print("sent count does not match, error, terminating without creating coref file")
                os.sys.exit()
                
        
            nocoref_count=len(allsents_dialog_nocoref)
            if  nocoref_count == len(allsents_dialog):
                dialogdf["SNLP_NoCoref_Sentiment"]=sentiment_allsents_nocoref
                dialogdf["SNLP_NoCoref_Ner"]=ner_allsents_nocoref
                dialogdf["SNLP_NoCoref_Adv"]=adv_allsents_nocoref
                dialogdf["SNLP_NoCoref_Adj"]=adj_allsents_nocoref
                dialogdf["SNLP_NoCoref_height"]=tree_height_allsents_nocoref
            else:
                print(key)
                print("sent count does not match in without coref features, error, terminating without creating coref file")
                os.sys.exit()
        
            
        dfwithcoref=pd.concat( all_dfs)   
        basefile=os.path.basename(input_file)
        dirname=os.path.dirname(input_file)
        coref_filename=os.path.join(dirname,basefile+"_corefsent.csv")
        dfwithcoref.to_csv(coref_filename)
        return dfwithcoref,coref_filename  


def clean(uncleandf,textcol):
    allrows=[]
    for index, row in uncleandf.iterrows():
        sentence=row[textcol]
        add=filtersentence(sentence,3)
        if add:
            allrows.append(row)
    clean_df=pd.DataFrame(allrows)  
    return clean_df       
            


def applypdtb(copydialogdf,pdtb_loc,pdtb_window,sent_col):
    copydialogdf['sent_no'] = copydialogdf['sent_no'].astype(int)
    copydialogdf=copydialogdf.sort("sent_no")
    PdtbObject=PDTBExtractor(pdtb_loc)#pdtb_loc is the location of the jar
    allnewrows=PdtbObject.extract(PdtbObject, pdtb_window, copydialogdf,sent_col)
    return allnewrows
    

def addpdtbsent(inputfile, outputfile,sent_col,pdtb_loc,pdtb_window):  
    num_cores = os.cpu_count()
    df=pd.read_csv(inputfile+".csv") 
    all_rows=[]
    dialogdf_list=[]
    outputfile= outputfile+".csv" 
    dialog_gps=df.groupby("Key")
    pdtb_helper.init_features_for_df(df)
    partialpdtb=partial(applypdtb,pdtb_loc=pdtb_loc,pdtb_window= pdtb_window,sent_col=sent_col)
    for key,dialogdf in dialog_gps:
        dialogdf_list.append(dialogdf)
    pool = mp.Pool(processes=num_cores)
    chunksize = len(dialogdf_list) // 3
    results = pool.map(partialpdtb, dialogdf_list)   
    pool.close() # no more tasks
    pool.join()    
    for result in results:
        all_rows.extend(result)   
    outdf=pd.DataFrame(all_rows)
    outdf.to_csv(outputfile)
     
def addpdtb(section):
    config = configparser.ConfigParser()
    config_file=file_utilities.get_absolutepath_data("config","coref_pdtb_config2.ini")
    config.read(config_file)
    inputfile=config.get(section,"inputfile")
    sent_col=config.get(section,"sent_col")
    window=config.get(section,"window")
    pdtb_loc=config.get(section,"pdtb_loc")
    outputfile=config.get(section,"outputfile")
    addpdtbsent(inputfile, outputfile,sent_col,pdtb_loc,window)
         



def splittraintest(input_coref,testsize):
    merged_df=pd.read_csv(input_coref+".csv")
    df_dialogs=merged_df.groupby("Key")
    trainlist=[]
    testlist=[]
    count=0
    for key, df in df_dialogs:
        if count< testsize:
            testlist.append(df)
        else:
            trainlist.append(df)    
        count=count+1    
        
    traindf=pd.concat(trainlist)
    testdf=pd.concat(testlist)
    traindf.to_csv(input_coref+"train.csv",index=False)
    testdf.to_csv(input_coref+"test.csv",index=False)

def run(section):
    if "Coref" in section:
        config = configparser.ConfigParser()
        config_file=file_utilities.get_absolutepath_data("config","coref_pdtb_config2.ini")
        config.read(config_file)
        inputfile=config.get(section,"inputfile")
        outputcoref=config.get(section,"outputcoref")
        if os.path.exists(outputcoref):
            os.remove(outputcoref)
        df_coref,output_coref=addcorefsent(inputfile)
    else:
        if "split" in section:    
            testsize=13
            config = configparser.ConfigParser()
            config_file=file_utilities.get_absolutepath_data("config","coref_pdtb_config2.ini")
            config.read(config_file)
            output_coref=config.get(section,"outputcoref")
            splittraintest(output_coref,testsize)
        else:
            if "pdtb" in section:
                addpdtb(section)    
    
    
if __name__ == '__main__':
    
#      section="smalltest_pdtb"
#      run(section)
    
    
#     section="pdtb_GM"
#     run(section)
#     section="pdtb_GC"
#     run(section)
#     section="pdtb_AB"
#     run(section)
    
    section="GMCoref"
    run(section)
    section="GMsplit"
    run(section)
    
    
    section="GCCoref"
    run(section)
    section='GCsplit'
    run(section)
    section="ABCoref"
    run(section)
    section="ABsplit"
    run(section)
