
'''
Created on Mar 23, 2017

@author: amita
'''
#import pdtbparser
import ast,sys,os
import shutil
import tempfile
import codecs
import pdtb_parser
import pdtb_helper

def writeTextFile(Filename, Lines):
    f = codecs.open(Filename, "w", encoding="utf-8")
    f.writelines(Lines)
    f.close()

class PDTBExtractor():
    
    def __init__(self,pdtb_loc):
        self.pdtb_jar_location=pdtb_loc
    
    #assumes sentence number starts with 0
    
    def parse_pdtb_pipe(self,line):
        pipes=line.split("|")
        Num_cols=len(pipes)

        if Num_cols != 48:
            print("error on parsing arguments")
            sys.exit()
        else:
            arg_dict={}
            i=0
            for pipe_text in pipes:
                arg_dict["col_"+ str(i)]=pipe_text
                i=i+1
        #pdtb_dict["pdtb"]=arg_dict
        return arg_dict
    
    def pdtb_todict(self,pipefile, mode, row,sent_col):
        with open(pipefile, "r") as f :
            sentences=f.readlines()
        for line in sentences:
                pdtb_rel=self.parse_pdtb_pipe(line)
                self.add_features_to_row(pdtb_rel, mode, row,sent_col )

    def add_features_to_row(self, arg_dict, mode, row,sent_col):
        ## adding relation . We may use this method to add more data later
        senses_data = str(arg_dict["col_" + str(pdtb_helper.senses_index)]).split('.')
        col1 = "pdtb_" + str(mode) + " _ " +  senses_data[0]
        col2 = "pdtb_" + str(mode) + " _ " +  senses_data[1]

        if col1 in pdtb_helper.senses_cols:
            if mode ==  'group':
                if   len(arg_dict['col_34'].split()) > 1 and set(arg_dict['col_34'].split()) < set(row[sent_col].split()) or len(arg_dict['col_24'].split()) > 1 and set(arg_dict['col_24'].split()) < set(row[sent_col].split()):
                    row[col1] += 1
            else:
                row[col1] += 1        
        if col2 in pdtb_helper.senses_cols:
            if mode ==  'group':
                if  len(arg_dict['col_34'].split()) > 1 and set(arg_dict['col_34'].split()) < set(row[sent_col].split()) or len(arg_dict['col_24'].split()) > 1 and set(arg_dict['col_24'].split()) < set(row[sent_col].split()):
                    row[col2]  += 1
            else:
                row[col2]  += 1
        pass
       
    
    
    def parsepdtb_results(self,dirname_output, filelist, dialogdf,sent_col):
        allnewrows=[]
        for file_tup in filelist:
            pipefile_ind=os.path.join(dirname_output,os.path.basename(file_tup[0]) +".pipe")
            pipefile_gp=os.path.join(dirname_output,os.path.basename(file_tup[1]) +".pipe")
            row=file_tup[2]
            mode="ind"
            self.pdtb_todict(pipefile_ind, mode, row,sent_col)
            mode="group"
            self.pdtb_todict(pipefile_gp, mode, row,sent_col)
            allnewrows.append(row)
        return allnewrows     
            


    def extract(self, PdtbObject, window, dialogdf,sent_col):
        #feature_dict={}
        #feature_dict["Relation type"] =""
        tmp = tempfile.mkdtemp()
        dirname_input = os.path.join(tmp, "pdtb/input/")
        dirname_output=os.path.join(dirname_input, "output/")
        if not os.path.exists(dirname_input):
            os.makedirs(dirname_input)
        filelist=[] 
        for ind,rowpdtb in  dialogdf.iterrows():
            sent_no=rowpdtb["sent_no"]
            sentnum_range = range(int(sent_no) - int(window), int(sent_no)+1 )
            small_df = dialogdf[dialogdf["sent_no"].isin( sentnum_range)]  
            text=rowpdtb[sent_col]
            grouped_text = ' '.join(small_df[sent_col].astype(str))
            
            file_gp = os.path.join( dirname_input, "gp_sent" + str(sent_no) + ".txt")
            LinesA = list()
            LinesA.append(grouped_text)
            writeTextFile(file_gp, LinesA)
            
            file_ind = os.path.join( dirname_input, "ind_sent" + str(sent_no) + ".txt")
            LinesA = list()
            LinesA.append(text)
            writeTextFile(file_ind, LinesA)
            
            filelist.append((file_ind,file_gp,rowpdtb))
       
 
        pdtb_parser.pdtb(dirname_input,self.pdtb_jar_location)
        allnewrows=self.parsepdtb_results(dirname_output, filelist, dialogdf,sent_col)     
        shutil.rmtree(tmp)
        return allnewrows
        
        
        
def example():
    import pandas as  pd
    import  os

    input_file = os.getcwd() + "/../pdtb.csv"  #test small csv
    input_file="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gun-control/Mturk/mturkcontributorresults/all_mtbatch_result_SummaryCSV_authorformat_all_pyramids_scus_author_corenl_corefsenttest.csv"
    df=pd.read_csv(input_file)
    pdtb_helper.init_features_for_df(df)
    sentcolumn ="sent"
    df_copy = df.copy()
    newdf=df.apply(applyfeatures,args=(sentcolumn, df_copy),axis=1)

def applyfeatures(row,sent_col, copy_df):
    sentence=row[sent_col]
    sent_no=row["sent_no"]
    pdtb_loc ="/Users/amita/software/pdtb-parser/"
    pdtb_dict=row["pdtb"]
    pdtb_sentence=row[sent_col]
    PdtbObject=PDTBExtractor(pdtb_loc)#pdtb_loc is the location of the jar
    pdtb_vec_scores=add_pdtb_features(PdtbObject,row, pdtb_sentence,sent_no,sent_col, copy_df)

def add_pdtb_features(PdtbObject,row,sentence,sent_no,  sent_col,  copy_df, window =2 ):
    pdtb_feature_dict=PdtbObject.extract( row=row,sentence=sentence,sent_no=sent_no,window=window, sent_col=sent_col, df=copy_df)
    return pdtb_feature_dict



if __name__ == '__main__':
    example()
