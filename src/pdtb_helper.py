senses =senses = [ 'Temporal', 'Asynchronous', 'Synchronous' ,'Contingency' , 'Cause', 'Pragmatic Cause', 'Condition','Comparision', 'Contrast', 'Pragmatic Contrast', 'Concession', 'Expansion', 'Instantiation' , 'Restatement', 'Alternative', 'Exception' , 'Conjunction', 'List']
senses_index = 11


from enum import Enum
from pandas import  *
import numpy as np

Senses_Mode = ['ind', 'group']

senses_cols = []

def init_features_for_df( df):
    for mode in Senses_Mode:
        for sense in senses :
            col = "pdtb_" + str(mode) + " _ " + sense
            df[col] = 0
            senses_cols.append(col)
    pass
