'''
Created on Mar 17, 2017

@author: amita
'''
import pandas as pd
import subprocess
import os,sys,shutil


def move(inputDir, outputDir):
        if not os.path.exists(outputDir):
            os.mkdir(outputDir)
        filelist=os.listdir(inputDir)
        for filename in filelist:
            if filename.startswith("."):
                continue
            src=os.path.join(inputDir,filename)
            dest=os.path.join(outputDir,filename)
            shutil.move(src,dest)
            

def pdtb(file_with_path,pdtb_jar_location):
    """
    give the complete location of pdtb jar
    file_with_path: text file to run pdtb
    output is written in the same directory as input file.
    
    """
    #args.append(file_with_path)
    #args.append(outputfile)
    currentdir=os.getcwd()
    parserloc=os.path.join(pdtb_jar_location,"parser.jar")
    os.chdir(pdtb_jar_location)
    cmd=str("java -jar "+  parserloc + " "+ file_with_path )
    #print(cmd)
   
    #print(pdtb_result_dir)
        #if os.path.exists(pdtb_result_dir()
    proc=subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    output, errors=proc.communicate()
    os.chdir(currentdir)
    if proc.returncode != 0:
            print("pdtb Failed {0} {1} {2}".format( proc.returncode, output,errors))
            sys.exit()


def callparser(inputdir,pdtb_jar_location):
    file_list=os.listdir(inputdir)
    for filename in file_list:
        if str(filename).endswith(".txt"):
            file_with_path=os.path.join(inputdir,filename)
            pdtb(file_with_path,pdtb_jar_location)

def run(inputdir,outputdir,pdtb_jar_location):
    """
    calls pdtb. pdtb writes all outputs to a folder named output in same directory
   We move all the files to the desired output_dir
    the script then moves this folder to desired outputdir
    """
    
    #basedir=os.path.dirname(inputdir)
    callparser(inputdir,pdtb_jar_location)
    default_pdtb_result_dir=os.path.join(inputdir,"output")        
    move( default_pdtb_result_dir,outputdir) 
            
if __name__ == '__main__':
    inputdir="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gun-control/pdtb_input/output"
    outputdir="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gun-control/pdtb_output"
    pdtb_jar="/Users/amita/software/pdtb-parser/"
    move(inputdir,outputdir)
    
    
