'''
Created on Apr 8, 2017

@author: amita
'''
import matplotlib
matplotlib.use('PS')
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from matplotlib import lines


styles =  [
    r'$\bowtie$',
    r'$\circlearrowleft$',
    r'$\checkmark$']

colors = ('b', 'g', 'r')

def plot_freq(inputfile,plot_file,topic):
    df=pd.read_csv(inputfile)
    values=df["label"].values
    plt.hist(values,alpha=0.5,label="sentence per tier",color="blue")
    plt.xlabel("Average Human Annotated Dialogue Sentence Tier Score ")
    plt.ylabel("Frequency distribution for {0}".format(topic))
    plt.legend(loc="upper right")
    plt.legend(frameon=False)
    plt.show()
    plt.savefig(plot_file)
    plt.close()
    
def plot_allthree(input_file1,input_file2,input_file3,topic1,topic2,topic3,allplotfile):
    count= 0
    df1=pd.read_csv(input_file1)
    values1=df1["label"].values
    
    df2=pd.read_csv(input_file2)
    values2=df2["label"].values
    
    df3=pd.read_csv(input_file3)
    values3=df3["label"].values
    color=colors[count]
    
    plt.hist(values1, histtype='step', linestyle='--',label=topic1)
    plt.hist(values3,histtype='step', linestyle='solid', label=topic3)
    plt.hist(values2, histtype='step', linestyle=':',color=color, label=topic2)
    plt.xlabel("Average human annotated dialogue sentence tier score ")
    plt.ylabel("Frequency distribution")
    
    plt.legend(loc="upper right")
    plt.legend(frameon=False)
    plt.show()
    plt.savefig(allplotfile)
    plt.close()
    
    
if __name__ == '__main__':
    input_file1="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gun-control/Mturk/mturkcontributorresults2/all_mtbatch_gun_corenlp_pdtb_corefsent.csv"
    plot_file1="/Users/amita/git/argument_summarization_17/src/plots/all_mtbatch_gc_histogm2.png"
    plot_freq(input_file1,plot_file1,"gun control")
    
    input_file2="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gay-rights-debates/Mturk/mturkcontributorresults2/all_mtbatch_gay_corenlp_pdtb_corefsent.csv"
    plot_file2="/Users/amita/git/argument_summarization_17/src/plots/all_mtbatch_gm_histogm2.png"
    plot_freq(input_file2,plot_file2,"gay-marriage")
    
    input_file3="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/abortion/Mturk/mturkcontributorresults2/all_mtbatch_abortion_corenlp_pdtb_corefsent.csv"
    plot_file3="/Users/amita/git/argument_summarization_17/src/plots/all_mtbatch_ab_histogm2.png"
    plot_freq(input_file3,plot_file3,"abortion")
    
    allplotfile="/Users/amita/git/argument_summarization_17/src/plots/alltopic_histogm2.png"
    
    plot_allthree(input_file1,input_file2,input_file3,"gun control","gay marriage","abortion",allplotfile)