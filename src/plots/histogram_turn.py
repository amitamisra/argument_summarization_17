'''
Created on Apr 16, 2017

@author: amita
'''

import pandas as pd
import matplotlib
matplotlib.use('PS')
import matplotlib.pyplot as plt


def plot(values, label,plotfile):
    plt.hist(values,alpha=0.5,color="blue")
    plt.ylabel("Frequency")
    plt.xlabel(label)
    plt.legend(loc="upper right")
    plt.legend(frameon=False)
    plt.show()
    plt.savefig(plotfile)
    plt.close()



def plot_rawcounts(newdf,plotfile_raw):
    values=newdf["raw_count_sent_perturn_class1"]
    plotfile=plotfile_raw+"_sents_perturn.png"
    label="Number of sentences per turn at pyramid level three and above"
    plot(values, label,plotfile)
    
    values=newdf["rawcount_words_perturn_class1"]
    plotfile=plotfile_raw+"_words_perturn.png"
    label="Number of words per turn at pyramid level three and above"
    plot(values, label,plotfile)
    
    values=newdf["raw_count_chars_perturn_class1"]
    plotfile=plotfile_raw+"_char_perturn.png"
    label="Number of characters per turn at pyramid level three and above"
    plot(values, label,plotfile)
    
    

def plot_density(df,plotfile):
    values=df["percentage_turns_class_1"]
    plt.hist(values,alpha=0.5,color="blue")
    plt.ylabel("Frequency")
    plt.xlabel("Percentage of a turn at pyramid level three and above")
    plt.legend(loc="upper right")
    plt.legend(frameon=False)
    plt.show()
    plt.savefig(plotfile+"_sent.png")
    plt.close()
    
    values=df["percentage_word_count_class_1"]
    plt.hist(values,alpha=0.5,color="blue")
    plt.ylabel("Frequency")
    plt.xlabel("Percentage of words at pyramid level three and above, per turn")
    plt.legend(loc="upper right")
    plt.legend(frameon=False)
    plt.show()
    plt.savefig(plotfile+"_word.png")
    plt.close()
    
    values=df["percentage_char_count_class_1"]
    plt.hist(values,alpha=0.5,color="blue")
    plt.ylabel("Frequency")
    plt.xlabel("Percentage of characters at pyramid level three and above, per turn")
    plt.legend(loc="upper right")
    plt.legend(frameon=False)
    plt.show()
    plt.savefig(plotfile+"_char.png")
    plt.close()
    


def turnhistogram(inputcsv,plotfile,plotfile_raw):
    df=pd.read_csv(inputcsv+".csv")
    allrows=[]
    dialog_turn_gp=df.groupby(["Key","turn_ids"])
    for key_turn, dialog_turn in  dialog_turn_gp:
        classdf_gp=dialog_turn.groupby("class")
        sents_one=0
        sents_zeros=0
        char_count_ones=0
        char_count_zeros=0
        word_count_ones=0
        word_count_zeros=0
        
        
        
        for classname, classdf in classdf_gp:
            if int(classname)==int(1):
                sents_one=classdf.shape[0]
                char_count_ones=sum(classdf["char_count"].values)
                word_count_ones=sum(classdf["word_count"].values)
            else:
                if int(classname)==int(0):
                    
                    sents_zeros=classdf.shape[0]
                    char_count_zeros=sum(classdf["char_count"].values)
                    word_count_zeros=sum(classdf["word_count"].values )  
    
        row={}      
        row["sent_perturn"] =sents_one+sents_zeros
        row["percentage_turns_class_1"]=float(float(sents_one)/float(sents_one+sents_zeros))  
        row["percentage_char_count_class_1"]=float(float(char_count_ones)/float(char_count_zeros+char_count_ones))  
        row["percentage_word_count_class_1"]=float(float(word_count_ones)/float(word_count_ones+ word_count_zeros))  
        row["key"]= key_turn  
        row["rawcount_words_perturn_class1"]=word_count_ones
        row["raw_count_chars_perturn_class1"]=char_count_ones
        row["raw_count_sent_perturn_class1"]=sents_one
        allrows.append(row) 
    
    
    
    newdf=pd.DataFrame(allrows)
    outfile=inputcsv+"_turnpercent.csv"
    plot_density(newdf,plotfile)
    plot_rawcounts(newdf,plotfile_raw)
    newdf.to_csv(outfile)
    
if __name__ == '__main__':
    gc="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gun-control/Mturk/mturkcontributorresults/all_mtbatch_result_SummaryCSV_authorformat_all_pyramids_scus_author_corenl_turnids"
    plotfile="/Users/amita/git/argument_summarization_17/src/plots/gc_turn_class_density"
    plotfile_raw="/Users/amita/git/argument_summarization_17/src/plots/gc_turn_class_rawcounts"
    turnhistogram(gc,plotfile,plotfile_raw)