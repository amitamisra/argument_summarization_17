'''
Created on Feb 26, 2017
run this script to prepare the data for corenlp
input:
@author: amita
'''

import scores_pyramid
import mergepyramidlabels_dialog
import file_utilities
import file_handling
import baselines
import os
import author_manipulation
import pdtb_parser
import json_pdtb_corenlp
import logging,sys
import pandas as  pd
import numpy as np
logging.basicConfig(level=logging.CRITICAL)
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def dividecorenlpforMT(corenlp_pdtb):
    corenlpdf=pd.read_json(corenlp_pdtb+".json")
    numberofrows=corenlpdf.shape[0]
    numrows_each=int(numberofrows/5)
    #print(numrows_each)
    #print(numberofrows)
    df1=corenlpdf.iloc[0:numrows_each]
    df2=corenlpdf.iloc[numrows_each: 2*numrows_each]
    df3=corenlpdf.iloc[2*numrows_each:3*numrows_each]
    df4=corenlpdf.iloc[3*numrows_each:4*numrows_each]
    df5=corenlpdf.iloc[4*numrows_each:numberofrows]
    
    outdf1=corenlp_pdtb +"_mt1"
    outdf2=corenlp_pdtb +"_mt2"
    outdf3=corenlp_pdtb +"_mt3"
    outdf4=corenlp_pdtb +"_mt4"
    outdf5=corenlp_pdtb +"_mt5"
    
    df1.to_csv(outdf1+".csv")
    df1.to_json(outdf1+".json")
    
    df2.to_csv(outdf2+".csv")
    df2.to_json(outdf2+".json")
    
    df3.to_csv(outdf3+".csv")
    df3.to_json(outdf3+".json")
    
    df4.to_csv(outdf4+".csv")
    df4.to_json(outdf4+".json")
    
    df5.to_csv(outdf5+".csv")
    df5.to_json(outdf5+".json")
    #print(df1.shape[0],df2.shape[0],df3.shape[0] ,df4.shape[0],df5.shape[0],corenlpdf.shape[0])
    if df1.shape[0] + df2.shape[0]+df3.shape[0] +df4.shape[0]+  df5.shape[0] == corenlpdf.shape[0]:
        print("succesfully didvided df")
    else:
        print("error in splitting df")
        sys.exit(-1)
            

def run(topic,input_fordialog,pdtb_loc,textcolumn):
    data_dir=file_utilities.DATA_DIR
    input_withdir_fordialog=os.path.join(*input_fordialog.split(","))
    input_withpath_fordialog=os.path.join(data_dir,input_withdir_fordialog)
    output_dir=os.path.join(data_dir,"data_originalsummaries",topic)
    outputwithauthor,columnname= author_manipulation.run(topic,input_withpath_fordialog,textcolumn,output_dir)
    scores_pyramid.run(topic)
    author_scu=mergepyramidlabels_dialog.run(topic,outputwithauthor) 
    #json_edus.run(topic,author_scu,columnname_edu) # done once for gun control, no more we do it
    input_pdtb, output_pdtb, colname_pdtb,corenlp_filename=json_pdtb_corenlp.run(topic,author_scu,columnname,pdtb_loc,output_dir)
    corenlp_pdtb =json_pdtb_corenlp.pdtbresult_tosent_dict(output_pdtb,colname_pdtb,corenlp_filename)
    dividecorenlpforMT(corenlp_pdtb)
    #clause_extraction.run(topic,author_scu,columnname_edu)

def test(output_pdtb,colname_pdtb,corenlp_filename):
    json_pdtb_corenlp.pdtbresult_tosent_dict(output_pdtb,colname_pdtb,corenlp_filename)
    
def testdiv():
    corenlp_pdtb="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gun-control/NaturalSummaryCSV_authorformat_all_pyramids_scus_author_corenlp_corenlp_pdtb"
    dividecorenlpforMT(corenlp_pdtb)
    
if __name__ == '__main__':
    topic="gun-control"
    #input_fordialog="dialogdata,CSV,gun-control,MTdata,MTAll,Dialog_File_midrange"
    input_fordialog="dialogdata,CSV,gun-control,MTdata,MTSummary,AllMTSummary_50,NaturalSummaryCSV"
    textcolumn="Dialog"
    pdtb_jar="/Users/amita/software/pdtb-parser/"
    output_pdtb="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gun-control/pdtb_output"
    colname_pdtb="corelp_author_dict"
    corenlp_filename="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gun-control/"
    #run(topic,input_fordialog,pdtb_jar,textcolumn) done for gc
    #test(output_pdtb,colname_pdtb,corenlp_filename)
    
    
    topic="gay-rights-debates"
    input_fordialog="dialogdata,CSV,gay-rights-debates,Phase_1_2,AllDialogs_phase_1_2_Formatted_nosum" 
    textcolumn="Dialog"
    pdtb_jar="/Users/amita/software/pdtb-parser/"
    output_pdtb="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gay-rights-debates/pdtb_output"
    colname_pdtb="corelp_author_dict"
    corenlp_filename="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gay-rights-debates/"
    #run(topic,input_fordialog,pdtb_jar,textcolumn)  done for gay rights
    
    
    
    
    topic="abortion"
    input_fordialog="dialogdata,CSV,abortion,abortion_NaturalSummaryCSV" 
    textcolumn="Dialog"
    pdtb_jar="/Users/amita/software/pdtb-parser/"
    output_pdtb="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/abortion/pdtb_output"
    colname_pdtb="corelp_author_dict"
    corenlp_filename="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/abortion"
    run(topic,input_fordialog,pdtb_jar,textcolumn)