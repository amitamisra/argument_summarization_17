"""Simple readability measures.
   Usage : getmeasure(list_of_tokenized_text)
"""

from __future__ import division, print_function, unicode_literals
import io
import os
try:
	import re2 as re
except ImportError:
	import re
import sys
import math
import getopt
import subprocess
import collections
import nltk
from readability.langdata import LANGDATA
if sys.version[0] >= '3':
	unicode = str  # pylint: disable=invalid-name,redefined-builtin

WORDRE = re.compile(r"\b[-\w]+\b", re.UNICODE)
PARARE = re.compile('\n\n+')
SENTRE = re.compile('[^\n]+(\n|$)')


def getmeasures(text, lang='en', merge=False):
	"""Collect surface characteristics of a tokenized text.

	>>> text = "A tokenized sentence .\\nAnother sentence ."
	>>> result = getmeasures(text)
	>>> result['sentence info']['words'] == 5
	True

	:param text: a single unicode string or an iterable of lines,
		one sentence per line of space separated tokens.
	:param lang: a language code to select the syllabification procedure and
		word types to count.
	:param merge: if ``True``, return a dictionary results into a single
		dictionary of key-value pairs.
	:returns: a two-level ordered dictionary with measurements."""
	characters = 0
	words = 0
	syllables = 0
	complex_words = 0
	complex_words_dc = 0
	long_words = 0
	paragraphs = 0
	sentences = 0
	vocabulary = set()
	syllcounter = LANGDATA[lang]['syllables']
	wordusageregexps = LANGDATA[lang]['words']
	beginningsregexps = LANGDATA[lang]['beginnings']
	basicwords = LANGDATA[lang].get('basicwords', frozenset())

	wordusage = collections.OrderedDict([(name, 0) for name, regexp
			in wordusageregexps.items()])
	beginnings = collections.OrderedDict([(name, 0) for name, regexp
			in beginningsregexps.items()])

	if isinstance(text, bytes):
		raise ValueError('Expected: unicode string or an iterable of lines')
	elif isinstance(text, unicode):
		# Collect surface characteristics from a string.
		# NB: only recognizes UNIX newlines.
		paragraphs = sum(1 for _ in PARARE.finditer(text)) + 1
		sentences = sum(1 for _ in SENTRE.finditer(text))
		# paragraphs = text.count('\n\n')
		# sentences = text.count('\n') - paragraphs
		for token in WORDRE.findall(text):
			vocabulary.add(token)
			words += 1
			characters += len(token)
			syll = syllcounter(token)
			syllables += syll
			if len(token) >= 7:
				long_words += 1

			if syll >= 3 and not token[0].isupper():  # ignore proper nouns
				complex_words += 1
			if token.lower() not in basicwords:
				complex_words_dc += 1

		for name, regexp in wordusageregexps.items():
			wordusage[name] += sum(1 for _ in regexp.finditer(text))
		for name, regexp in beginningsregexps.items():
			beginnings[name] += sum(1 for _ in regexp.finditer(text))
	else:  # Collect surface characteristics from an iterable.
		prevempty = True
		for sent in text:
			sent = sent.strip()

			if prevempty and sent:
				paragraphs += 1
			elif not sent:
				prevempty = True
				continue
			prevempty = False

			sentences += 1
			for token in WORDRE.findall(sent):
				vocabulary.add(token)
				words += 1
				characters += len(token)
				syll = syllcounter(token)
				syllables += syll
				if len(token) >= 7:
					long_words += 1

				if syll >= 3 and not token[0].isupper():  # ignore proper nouns
					complex_words += 1
				if token.lower() not in basicwords:
					complex_words_dc += 1

			for name, regexp in wordusageregexps.items():
				wordusage[name] += sum(1 for _ in regexp.finditer(sent))
			for name, regexp in beginningsregexps.items():
				beginnings[name] += regexp.match(sent) is not None

	if not words:
		raise ValueError("I can't do this, there's no words there!")

	readability = {'read_Kincaid':KincaidGradeLevel(syllables, words, sentences),
			'read_ARI':ARI(characters, words, sentences),
			'read_Coleman-Liau':ColemanLiauIndex(characters, words, sentences),
			'read_FleschReadingEase':FleschReadingEase(syllables, words, sentences),
			'read_GunningFogIndex':GunningFogIndex(words, complex_words, sentences),
			'read_LIX':LIX(words, long_words, sentences),
			'read_SMOGIndex':SMOGIndex(complex_words, sentences),
			'read_RIX':RIX(long_words, sentences)}
		
	return readability


def KincaidGradeLevel(syllables, words, sentences):
	return 11.8 * (syllables / words) + 0.39 * ((words / sentences)) - 15.59


def ARI(characters, words, sentences):
	return 4.71 * (characters / words) + 0.5 * (words / sentences) - 21.43


def ColemanLiauIndex(characters, words, sentences):
	return (5.879851 * characters / words - 29.587280 * sentences / words
			- 15.800804)

def FleschReadingEase(syllables, words, sentences):
	return 206.835 - 84.6 * (syllables / words) - 1.015 * (words / sentences)


def GunningFogIndex(words, complex_words, sentences):
	return 0.4 * (((words / sentences)) + (100 * (complex_words / words)))


def LIX(words, long_words, sentences):
	return words / sentences + (100 * long_words) / words


def SMOGIndex(complex_words, sentences):
	return math.sqrt(complex_words * (30 / sentences)) + 3


def RIX(long_words, sentences):
	return long_words / sentences


def DaleChallIndex(words, complex_words_dc, sentences):
	complex_prc = complex_words_dc / words * 100
	if complex_prc <= 5:
		return 0
	return 0.1579 * complex_prc + 0.0496 * words / sentences + 3.6365
