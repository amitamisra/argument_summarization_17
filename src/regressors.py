'''
Created on Apr 5, 2017

@author: amita
'''
from sklearn.cross_validation import cross_val_score
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import Pipeline
from sklearn.svm import LinearSVR
from sklearn.metrics import classification_report
from numpy.ma import corrcoef
from math import sqrt
import os, codecs
from sklearn import svm
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import f_regression
from sklearn import cross_validation 
from sklearn import preprocessing
from sklearn import svm
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.pipeline import Pipeline
import pandas as pd
import numpy as np
import logging
import configparser
from sklearn.grid_search import GridSearchCV
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import f1_score
from pprint import pprint
from sklearn.svm import LinearSVC
from sklearn.metrics import confusion_matrix
logging.basicConfig(level=logging.INFO)
from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score
logger = logging.getLogger(__name__)
randomseed=1
class SVMregression:
    
    def __init__(self,numFeatureSelection,gammastart,gammaend,gammacount,Cstart,Cend,Ccount,param_dict):
        self.gammastart=float(gammastart)
        self.gammaend=float(gammaend)
        self.gammacount=float(gammacount)
        self.Cstart=float(Cstart)
        self.Cend=float(Cend)
        self.Ccount=float(Ccount)
        #self.numFeatureSelection= [int(x) for x in numFeatureSelection if x.isdigit()] + ["all"]
        self.numFeatureSelection= ["all"]
        
    def setpipeline(self):
        self.pipeline=Pipeline([('scale', preprocessing.StandardScaler()),('svr', svm.LinearSVR(random_state=randomseed))])
                
    def setparam_grid(self):
        #C_range = np.logspace(self.Cstart, self.Cend, self.Ccount)
        C_range=[1,10, 100]
        gamma_range = np.logspace(self.gammastart,self.gammaend,self.gammacount)
        #self.param_grid=[{'svr__kernel': ['linear'], 'svr__C': C_range,'filter__k': self.numFeatureSelection}]
        #self.param_grid=[{'svr__kernel': ['rbf'], 'svr__gamma': gamma_range,'svr__C': C_range, 'filter__k': self.numFeatureSelection}]
        
        self.param_grid=[{ 'svr__C': C_range}]

def create_result_dict(param_dict,Y_actual,Y_pred,result_dict,best_param,param_grid):
    result_dict["inputTrain"]=param_dict["inputTrain"]
    result_dict["inputTest"]=param_dict["inputTest"]
    result_dict["r"] =corrcoef(Y_actual,Y_pred)[0, 1]
    rmse=result_dict["rmse"]=sqrt(mean_squared_error(Y_actual,Y_pred))
    rrse = rmse / np.sqrt(mean_squared_error(Y_actual, np.repeat(Y_actual.mean(), len(Y_actual))))
    result_dict["rrse"]=rrse
    result_dict["rsquare"]=r2_score(Y_actual,Y_pred)
    result_dict["best_param"]=best_param
    result_dict["param_grid"]=param_grid

def create_svm_pipeline(gammastart,gammaend,gammacount,Cstart,Cend,Ccount,param_dict):
    num_features=param_dict["Num_features_selection"]
    svmregressor=SVMregression(num_features,gammastart,gammaend,gammacount,Cstart,Cend,Ccount,param_dict)
    svmregressor.setparam_grid()
    svmregressor.setpipeline()
   
    return svmregressor


    
                 
def gridSearchTestSet(X_train,Y_train,X_test,Y_test,testdf,pipeline,param_grid,cvfolds,param_dict): 
        logger.info("Doing grid search for test set")
        result_dict={}
        grid_search = GridSearchCV(pipeline, param_grid=param_grid,cv=cvfolds)       
        grid_search.fit(X_train, Y_train)
        best_param=grid_search.best_params_
        Y_pred=grid_search.predict(X_test)
        pred_list= [{'Y_actual': v1, 'Y_predicted': v2} for v1, v2 in zip(Y_test, Y_pred)]
        create_result_dict(param_dict,Y_test, Y_pred,result_dict,best_param,param_grid)
        basedir=param_dict["outputBaseDir"]
        classifier=param_dict["classifier"]
        feature= "_".join(param_dict["featureList"])
        outputDir=os.path.join(basedir,feature)
        if not os.path.exists(outputDir):
            os.makedirs(outputDir)
        csvFile=os.path.join(outputDir,feature + classifier +"__test.csv")
        #print("classification report {0}".format(classification_report(Y_test,Y_pred)))
        writeresultstoFile(csvFile,result_dict,pred_list,X_test,Y_pred,Y_test,testdf,param_dict)
        return result_dict            


def gridsearchnestedCV(X_train,Y_train,traindf,pipeline, param_grid,cvfolds,param_dict):
        logger.info("Doing grid search for cv")
        grid_search = GridSearchCV(pipeline, param_grid=param_grid,cv=cvfolds)
        Y_pred=cross_validation.cross_val_predict(grid_search, X_train, Y_train,cv=cvfolds,n_jobs=-1)
        result_dict={}
        pred_list= [{'Y_actual': v1, 'Y_predicted': v2} for v1, v2 in zip(Y_train, Y_pred)]
        best_param=""
        create_result_dict(param_dict,Y_train, Y_pred,result_dict,best_param,param_grid)

        basedir=param_dict["outputBaseDir"]
        classifier=param_dict["classifier"]
        feature= "_".join(param_dict["featureList"])
        outputDir=os.path.join(basedir,feature)
        if not os.path.exists(outputDir):
            os.makedirs(outputDir)
            
        csvFile=os.path.join(outputDir,feature+classifier+"__CV.csv")
        writeresultstoFile(csvFile,result_dict, pred_list,X_train,Y_pred,Y_train,traindf,param_dict)
        return result_dict


#performs nested cross validation using grid search, pipeline contains the regressor and its parameters are evaluated using parameter grid    
#use it to report cross validation accuracy, get scores for each fold for pairdTTest
def gridsearchnestedCVfoldsTTest(X_train,Y_train,pipeline, param_grid,scoringcriteria,cvfolds,param_dict_regression):
        logger.info("Doing grid search for cvfolds") 
        grid_search = GridSearchCV(pipeline, param_grid=param_grid,scoring=scoringcriteria,cv=cvfolds,n_jobs=-1)
        scores = cross_validation.cross_val_score(grid_search, X_train, Y_train,cv=cvfolds)
        feature= "_".join(param_dict_regression["featureList"])
        colname= "scores:"+ scoringcriteria
        basedir=param_dict_regression["outputBaseDir"]
        classifier=param_dict_regression["reg_type"]
        score_list= [{colname: v1} for v1 in scores]
        outputDir=os.path.join(basedir,feature)
        if not os.path.exists(outputDir):
            os.makedirs(outputDir)
            
        csvFile=os.path.join(outputDir,feature+classifier+"__PairedTTestCV.csv")
        df1=pd.DataFrame(score_list)
        df1.to_csv(csvFile,index=False)
        f = codecs.open(csvFile, "a",encoding='utf-8')
        pprint(param_dict_regression["feature_cols_included"], stream=f)
        pprint(("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2)),stream=f)
        pprint("rmse: " +str(sqrt(scores.mean() * -1)),stream=f)
        pprint("scoringcriteria "+ scoringcriteria,stream=f)
        f.close()
      
    
def writeresultstoFile(csvFile,result_dict,pred_list,X_train,Y_pred,Y_train,df_original,param_dict):
    logger.info("start writing file " + str(param_dict["featureList"]) +" "+csvFile  )
    assert(len(pred_list)==len(X_train)==len(Y_train))
    
    df1=pd.DataFrame(pred_list)
    #df2=pd.DataFrame(X_train)
    df2=df_original
    df = df2.join(df1)
    df.to_csv(csvFile,index=False)
    f = codecs.open(csvFile, "a",encoding='utf-8')
    pprint(result_dict, stream=f) 
    pprint(param_dict["feature_cols_included"], stream=f)
    f.close()
    logger.info("Done writing file " + str(param_dict["featureList"]) +" "+csvFile  )        