'''
Created on Apr 5, 2017

@author: amita
'''
ignorekey=["1-6646_268_261_1_272_274_289_290_291_293_294_13",]

import sys,os,re
import pandas as pd
from db_connect import setup_connection
import db_functions
import file_utilities

def createdialogfiles(sql_session,inputcsv,outputcsv,dataset):
    """
    #input csv contains a key field, we parse that field to obtain dataset, discussion and postids for a dialog
    #structure of key datasetid_discussionid_1,postid2,postid.....dialognumber
    #there were several dialogs in a discussion
    """
    allrows=[]
    df=pd.read_csv(inputcsv)
    if "key" in df.columns:
        dialogkey="key"
    else:
        if "Key" in df.columns:
            dialogkey="Key"    
    keys=df[dialogkey].values
    for key in keys:
        rowdict=dict()
        keyseq=key.split("_")
        if len(keyseq[-1])>1:
            print(key)
            break
        (dataset,discussion)=keyseq[0].split("-")
        datasetid=dataset
        discussionid=int(discussion)
        if int(dataset)!= 1:
            #print("dataset not matching fourforums, exiting")
            sys.exit(-1)
        else:
            rowdict["datasetid"]=datasetid
            rowdict["discussonid"]= discussionid
            postidlist=[postid for postid in keyseq[1:-1]]
            postidlist=[int(x) for x in postidlist if x]
            postidlist=sorted(postidlist)
if __name__ == '__main__':
    pass