from stanford_nlp_features import *
import os

# path is input path for file with dialogue
# write_path is for pos_tagging
def run_pos(path, write_path):

    os.chdir(path)
    for filename in os.listdir(path) :
        if ".txt" not  in filename:
            continue
        f = open(filename, 'r')
        summ = write_path + "/pos_" + filename
        text = f.read().replace('\n', '')
        f2 = open(summ, 'w')
        pos_tagged_text = parse_pos(text)
        for sentence in pos_tagged_text:
            f2.write(sentence + '\n')

        f2.close()
        f.close()
    return write_path
