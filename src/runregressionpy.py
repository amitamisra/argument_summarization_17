'''
Created on Apr 5, 2017

@author: amita
'''
from sklearn.cross_validation import cross_val_score
from sklearn.pipeline import Pipeline

from math import sqrt
import os, codecs

from sklearn import cross_validation 
from sklearn import preprocessing
from sklearn import svm
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.pipeline import Pipeline
import pandas as pd
import numpy as np
import logging
import configparser
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
import regressors
import file_utilities
from numpy.ma import corrcoef

defaultgammastart=1
defaultgammaend=30
defaultCstart=1
defaultCend=60
defaultgammacount=10
defaultCcount=10
cvfolds=5


logger = logging.getLogger()
handler = logging.StreamHandler()
formatter = logging.Formatter(
        '%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.INFO)
#import skip_thought_vectors_py3.addskipthought as addskipthought
logging.basicConfig(level=logging.CRITICAL)

logger = logging.getLogger(__name__)


def splifeatures_label(inputcsv,class_label,feature_list,param_dict):
    df=pd.read_csv(inputcsv,na_filter=False)
    colnames=list(df.columns.values)
    feature_cols=[col for col in colnames if str(col).startswith(tuple(feature_list))]
    param_dict["feature_cols_included"]=feature_cols
    Y_values= df[class_label].values
    X_values= df[list(feature_cols)].values
    return(X_values,Y_values,df)

def createsvmclassifierpipeline(param_dict):
    if param_dict["gammastart"]:
        gammastart=param_dict["gammastart"]
    else:
        gammastart=defaultgammastart
        
    if param_dict["gammaend"]:
        gammaend=param_dict["gammaend"] 
    else:
        gammaend=defaultgammaend 
    if param_dict["gammacount"]:
        gammacount= param_dict["gammacount"]
    else:
        gammacount=defaultgammacount       
           
    if param_dict["Cstart"]:
        Cstart=param_dict["Cstart"]
    else:
        Cstart=defaultCstart
        
    if param_dict["Cend"]:
        Cend=param_dict["Cend"] 
    else:
        Cend=defaultCend 
        
    if param_dict["Ccount"]:
        Ccount=param_dict["Ccount"] 
    else:
        Ccount=defaultCcount  
    
    num_features=param_dict["Num_features_selection"]     
    svm_classifier=regressors.create_svm_pipeline(gammastart,gammaend,gammacount,Cstart,Cend,Ccount,param_dict)
    svm_classifier.setpipeline()
    svm_classifier.setparam_grid()
    return svm_classifier


def createresultdict(param_dict,result_dict):
    newdict={}
    newdict["inputTrain"]=param_dict["inputTrain"] 
    newdict["inputTest"]=param_dict["inputTest"]    
    newdict["reg_type"]=param_dict["classifier"]
    newdict["feature_cols_included"]=param_dict["feature_cols_included"]
    newdict["feature_list"]=str(param_dict["featureList"])
    newdict["rmse"]=result_dict["rmse"]
    newdict["rrse"]=result_dict["rrse"]
    newdict["rsquare"]=result_dict["rsquare"]
    newdict["r"]=result_dict["r"]
    newdict["Eval_type"]=param_dict["eval_type"]
    newdict["param_grid"]=result_dict["param_grid"]
    newdict["best_param"]=result_dict["best_param"]
    return newdict

def addresult(result_dict,param_dict,all_list):
    newdict=createresultdict(param_dict,result_dict)
    all_list.append(newdict)

def execute(param_dict):
    input_train_file= param_dict["inputTrain"]
    input_test_file= param_dict["inputTest"]
    
    feature_list=param_dict["featureList"]
    
    classifier_type=param_dict["classifier"]
    eval_type=param_dict["eval_type"]
    reg_label=param_dict["reg_label"]
    
    X_train_df,Y_train,traindf=splifeatures_label(input_train_file,reg_label,feature_list,param_dict)
    X_test_df,Y_test,testdf=splifeatures_label(input_test_file,reg_label,feature_list,param_dict)
    

    if  "SVM" == classifier_type:
        svm_regressor=createsvmclassifierpipeline(param_dict)
    
    if eval_type=="nestedCV":
        result_dict=regressors.gridsearchnestedCV(X_train_df,Y_train,traindf,svm_regressor.pipeline,svm_regressor.param_grid,cvfolds,param_dict)
    if eval_type=="test":
        result_dict=regressors.gridSearchTestSet(X_train_df,Y_train,X_test_df,Y_test,testdf,svm_regressor.pipeline,svm_regressor.param_grid,cvfolds,param_dict)
            
    return result_dict
            

def run(section): 
    config = configparser.ConfigParser()
    regressioninput=file_utilities.get_absolutepath_data("config","regression_config.ini")
    config.read(regressioninput)
    input_file_config=config.get(section,"csv_with_file_list")
    resultCSVFile_config=config.get(section,'resultCSVFile')
    input_file=file_utilities.get_absolutepath_data(input_file_config)
    resultCSVFile=file_utilities.get_absolutepath_data(resultCSVFile_config) 
    inputdata_df=pd.read_csv(input_file)
    inputdata_df.fillna(0)
    
    all_list=[]
    if os.path.exists(resultCSVFile):
            os.remove(resultCSVFile)
    for index, row in inputdata_df.iterrows():
        param_dict={}
        done=row["done"]
        if int(done)==1:
            continue
        #addcosine(row["inputTrain"])
        param_dict["gammastart"]=row["gammastart"]
        param_dict["gammaend"]=row["gammaend"]
        param_dict["gammacount"]=row["gammacount"]
       
        param_dict["Cstart"]=row["Cstart"]
        param_dict["Cend"]=row["Cend"]
        param_dict["Ccount"]=row["Ccount"]
       
        param_dict["classifier"]=row["classifier"]
        param_dict["featureList"]=list(row["featureList"].split(","))
        param_dict["eval_type"]=row["eval_type"]
        param_dict["reg_label"]=row["reg_label"]
        param_dict["Num_features_selection"]=list(str(row["Num_features_selection"]).split(","))# A list of possible  feature count  for feature selection, by default no feature selection
        
        param_dict["inputTrain"]=file_utilities.get_absolutepath_data(row["inputTrain"])
        param_dict["inputTest"]=file_utilities.get_absolutepath_data(row["inputTest"])
        outBaseDir=file_utilities.get_absolutepath_data(row["outputBaseDir"])
        if not os.path.exists(outBaseDir):
            os.mkdir(outBaseDir)
        param_dict["outputBaseDir"]= outBaseDir 
        result_dict=execute(param_dict)  
        addresult(result_dict,param_dict,all_list)  
        
    if all_list:
        df=pd.DataFrame(all_list)
        df.to_csv(resultCSVFile,index=False)         
        
if __name__ == '__main__':
    
    #section ="GC_coref"
    #run(section)
      
    section="GC"
    run(section)
     
    #section ="GM_coref"
    #run(section)
     
    section ="GM"
    run(section)
    
    #section ="AB_coref"
    #run(section)
    
    section ="AB"
    #run(section)
    