'''
Created on Apr 2, 2016
run various regression algorithms, 
parameter tuning and feature selection can be done by pipeline and parameter grid 
There are a few default parameter setting for each regressor. 
These can be changed using a csv input
The function run reads a csv containing a sequence of train,test and regression models to run . parameter can be set in this csv
The final output contains actual and predicted scores written to a csv, alongwith regression metrics.
Fields in Csv
ModelType,
Parameters,
data_length,
ratio,
train_file_name,
val_file_name,
test_file_name,
val_ratio,
embeddings,
type,
dropout,
savedModel,
done
        
( These parameters for the deepnet)
scoring: scoring critertia for nested CV
Featureselection: by default I am using auto for all the features, You can give a list of features like[10,20,40]
featurelist: is the specific feature names you want before any feature selection is done. They should be separated by comma.
you can just specify initial distinguishing characters for features. 
For example to keep unigram_i,bigram_i_am  as features, the feature list should be unigram, bigram
It then creates  a directory baseoutput/featurelist/classifier/featurelist_CV.csv for nested cross validation
It then creates  a directory baseoutput/featurelist/classifier/featurelist_test.csv for evaluation on nested test set
CV follds=10, can be changed in the script, See default settings below 
@author: sharath
'''
import configparser

import pandas as pd, os
#import regression
#import  file_utilities
import logging
import gensim
from scipy import spatial

##############################################################################################################################################

"""Libraries"""

import pandas as pd
import ast
import logging
from pandas import DataFrame
from keras.callbacks import History 
history = History()
import os
from nltk.corpus import stopwords
import re
#import enchant
from nltk.stem.porter import *
import numpy as np
import cPickle as pickle
from collections import Counter
from keras.models import model_from_json
import math
import signal
import h5py
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten, LSTM, Bidirectional
from keras.layers import Convolution1D, MaxPooling1D, Convolution2D, MaxPooling2D
from keras.optimizers import SGD
from keras.optimizers import Adam
from keras.constraints import maxnorm
from keras import backend as K
from scipy.sparse import csr_matrix
from sklearn.manifold import TSNE
import codecs
import pylab as plot
import random
from sklearn.utils import shuffle
import nltk
import gensim.models.word2vec as wv
from keras.layers import Merge
import copy
from collections import Counter
from sklearn.metrics import confusion_matrix
from sklearn.metrics import f1_score
#from normalize import *

###############################################################################################################################################

"""Default Constants and hyper parameters"""

K.set_image_dim_ordering('th')
default_max_words = 40 #number of word in text
default_depth = 100#300 #length of embedding of a word of Word2vec. Can be set to anything.
default_features = 20 # Number of additional features. Currently, this variable does nothing.
default_data_length = 12000 #Total number of samples
default_ratio = 0.8 #Percentage of training samples
default_train_length = int(default_ratio * default_data_length) #Number of training samples
default_filename_train = "train.csv"#gc_all_sentences_with_labels_filtered.csv" #Names of the file is the data folder
default_filename_val = "val.csv"
default_filename_test = "test.csv"
#Takes only regression now
default_model_flag = "reg" # reg or classification, currently takes in only reg.
default_validation_ratio = 0.05 #ratio of validation samples to training samples
###################################################################################################################################################


##logging.basicConfig(level=logging.INFO)
##logger = logging.getLogger(__name__)

##PROJECT_DIR = os.path.dirname(os.path.abspath((os.path.join(os.path.dirname(__file__), ".."))))
##RESOURCE_DIR = os.path.abspath(os.path.join(PROJECT_DIR, "resources"))
##defaultalphastart=1
##defaultalphaend=10
##defaultalphacount=10
##defaultgammastart=1
##defaultgammaend=30
##defaultCstart=1
##defaultCend=60
##defaultgammacount=10
##defaultCcount=10
##defaultscoring="mean_squared_error"
##defaultfolds=10

def handler(signum, frame):
    print 'Ctrl+Z pressed'
    assert False
signal.signal(signal.SIGTSTP, handler)

def readData(filename_train, filename_val, filename_test, data_length):
    
#    cwd = os.getcwd()
#    path = cwd + "/data/NNdata/" + filename_train
    
    """Training"""
    
    df = pd.read_csv(filename_train, sep=',', encoding='utf-8');
    df['label'] = pd.to_numeric(df['label'], errors='coerce')
    df['sent_no'] = pd.to_numeric(df['sent_no'], errors='coerce')

    """Testing"""
    df_test = pd.read_csv(filename_test, sep=',', encoding='utf-8')
    df_test['label'] = pd.to_numeric(df_test['label'], errors='coerce')
    df_test['sent_no'] = pd.to_numeric(df_test['sent_no'], errors='coerce')
    
    frame = [df, df_test]
    data = pd.concat(frame)

    data = data.reset_index()
    
    grouped_corenlp = data[['Key', 'corelp_author_dict']]#DataFrame(data.groupby('Key')['corelp_author_dict'].apply(list)).reset_index()
    grouped_corenlp = grouped_corenlp.drop_duplicates('Key')
    grouped_corenlp = grouped_corenlp.reset_index()
        
    grouped_sent_no = DataFrame(data.groupby('Key')['sent_no'].apply(list)).reset_index()
    
    for i in range(grouped_corenlp.shape[0]):
        for j in range(grouped_sent_no.shape[0]):
            
            if grouped_corenlp.ix[i, 'Key'] == grouped_sent_no.ix[j, 'Key']: 
            
                dictionary = ast.literal_eval(grouped_corenlp.ix[i, 'corelp_author_dict'])
#                print grouped_corenlp.ix[i, 'Key'], grouped_sent_no.ix[j, 'Key']
                print "corenlp ->", len(dictionary['corenlp_info']) - 1, "sentence count->", max(grouped_sent_no['sent_no'][j])
    
    print grouped_corenlp.shape[0]
    print grouped_sent_no.shape[0]
    

def execute(param_dict_regression, path):

    model_type = param_dict_regression["ModelType"]
    parameters = param_dict_regression["Parameters"]
    data_length = param_dict_regression["data_length"]
   
    ratio = param_dict_regression["ratio"]
    embeddings = param_dict_regression["embeddings"]
    type_ = param_dict_regression["type"]
   
    dropout = param_dict_regression["dropout"]
    saved_model = param_dict_regression["savedModel"]
    
    train_file_name = param_dict_regression["train_file_name"]
    val_file_name = param_dict_regression["val_file_name"]
    test_file_name = param_dict_regression["test_file_name"]
#        
    val_ratio = param_dict_regression["val_ratio"]
    depth = param_dict_regression["depth"]
    max_words = param_dict_regression["max_words"]
    no_features = param_dict_regression["no_features"]
    classification_threshold = param_dict_regression["classification_threshold"]
    train_length = int(ratio * data_length)
    
    
#    Train+val and test data    
    readData(path+train_file_name, path+val_file_name, path+test_file_name, data_length)
   
def run(section, input_file):
#    config = configparser.ConfigParser()
#    regressioninput=file_utilities.get_absolutepath_data("config","regression.ini")
#    config.read(regressioninput)
#    input_file_config=config.get(section,"csv_with_file_list")
#    resultCSVFile_config=config.get(section,'resultCSVFile')
#    input_file=file_utilities.get_absolutepath_data(input_file_config)
#    resultCSVFile=file_utilities.get_absolutepath_data(resultCSVFile_config) 
    cwd = os.getcwd()
    temp_path = cwd + "/data/NNdata/" 
    path = cwd + "/data/NNdata/" + input_file
    inputdata_df=pd.read_csv(path)
    inputdata_df.fillna(0)
    
#    all_list=[]
#    if os.path.exists(resultCSVFile):
#            os.remove(resultCSVFile)
    for index, row in inputdata_df.iterrows():
        param_dict_regression={}
        done=row["done"]
        if int(done)==1:
            continue
        #addcosine(row["inputTrain"])
        param_dict_regression["ModelType"]=row["ModelType"]
        param_dict_regression["Parameters"]=row["Parameters"]
        param_dict_regression["data_length"]=row["data_length"]
       
        param_dict_regression["ratio"]=row["ratio"]
        param_dict_regression["embeddings"]=row["embeddings"]
        param_dict_regression["type"]=row["type"]
       
        param_dict_regression["dropout"]=row["dropout"]
        param_dict_regression["savedModel"]=row["savedModel"]
        
        param_dict_regression["train_file_name"]=row["train_file_name"]
        param_dict_regression["val_file_name"]=row["val_file_name"]
        param_dict_regression["test_file_name"]=row["test_file_name"]
#        
        param_dict_regression["val_ratio"]=row["val_ratio"]
        param_dict_regression["depth"]=row["depth"]
        param_dict_regression["max_words"]=row["max_words"]
        param_dict_regression["no_features"]=row["no_features"]
        param_dict_regression["classification_threshold"]=row["classification_threshold"]
        
        execute(param_dict_regression, temp_path)
    
if __name__ == '__main__':
   
    section="afs"
    filename = "nn_config_sharath.csv"
    run(section, filename)
   
