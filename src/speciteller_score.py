''' assumes that you have speciteller and liblinear both checked in at the head level .
    if that is not the case, specifiy with spec_dir and change lib_linear path  in speciteller accordingly
'''
import subprocess
import os


def get_spec_score(text, spec_dir):

    prev_dir = os.getcwd()
    os.chdir(spec_dir)

    ref_filename ='sents_test'
    output_filename = 'test.probs'
    f = open(ref_filename, 'w')
    f.write(text)
    f.close()

    subprocess.call(['python' ,'speciteller.py' ,'--inputfile' ,ref_filename, '--outputfile', output_filename])

    f = open(output_filename, 'r')
    output = f.read().replace('\n', '')
    f.close()
    #print output

    os.remove(output_filename)
    os.remove(ref_filename)
    os.chdir(prev_dir)

    return output


def example():
    output = get_spec_score('lets test this',)
    print(output)

#example()
