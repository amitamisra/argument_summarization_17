'''
Created on Feb 21, 2017
uses py4j to connect to a java program, java program should be running on the local host
at port 25330
run SimplecoreNlp.java
@author: shubhangi
'''
import json
import os
from util import  tree_height
from speciteller_score import get_spec_score
import sys
from py4j.java_gateway import JavaGateway, GatewayParameters

from enum import Enum

class Mode(Enum):
    SIMPLE_PARSE = -1,
    BLIND_COREF = 1,
    SMART_COREF = 0

def parse_text(text, mode=Mode.SIMPLE_PARSE):

    gateway=JavaGateway(gateway_parameters=GatewayParameters(port=25340))
    if(mode == Mode.SIMPLE_PARSE):  #no coref
        res=gateway.entry_point.parsed_text(text)

    elif(mode == Mode.BLIND_COREF):   #replace coref in  every sentence
        res = gateway.entry_point.parse_text_with_coref(text, 1);

    elif(mode == Mode.SMART_COREF):    #do not replace coref in the same sentence
        res = gateway.entry_point.parse_text_with_coref(text, 0);
    return res

def parse_res_processor(res):
    allsentences_corenlpinfo=[]
    for sentence in res:
        sentence_dict={}
        sentence_dict["snlp_pos"]= list(sentence["pos"])
        sentence_dict["snlp_dep"] = list(list(dep) for dep in sentence["deps_cc"])
        sentence_dict['snlp_parse'] = list(sentence['parse'])
        sentence_dict["snlp_lemmas"]=list(sentence["lemmas"])
        sentence_dict["snlp_tokens"]=list(sentence["tokens"])
        sentence_dict["snlp_coref"]=list(sentence["coref"])
        sentence_dict["snlp_coref_mention"]=sentence["coref mention"]
        sentence_dict["snlp_sentiment"] = list(sentence["sentiment"])
        sentence_dict["snlp_tree_height"] = tree_height(sentence_dict["snlp_parse"][0])
        sentence_dict["snlp_ner"]=list(sentence["ner"])
        sentence_dict["snlp_num_adj"]=list(sentence["num_adj"])
        sentence_dict["snlp_num_adv"]=list(sentence["num_adv"])
        sentence_dict["snlp_num_ner"]=list(sentence["num_ner"])
        #sentence_dict["spec_score"] = get_spec_score(sentence_dict["snlp_tokens"][0])

        #print("sentence is ")
        #print(sentence_dict)
        allsentences_corenlpinfo.append(sentence_dict)

    doc_dict={}

    doc_dict["corenlp_info"]=  allsentences_corenlpinfo

    #print doc_dict
    return doc_dict

def parse_pos(text):
    gateway=JavaGateway(gateway_parameters=GatewayParameters(port=25340))
    res=gateway.entry_point.pos_tagger(text)

    return res
    #print res

def get_nlp_features(text):
    res=parse_text(text, mode=Mode.SMART_COREF)
    doc_dict= parse_res_processor(res=res)
    return doc_dict

if __name__ == '__main__':
    #text="John Smith went to China. He visited Beijing, on January 10th, 2013"
    text ="Barack Obama was born in Hawaii, he is the president. Obama was elected in 2008."
    res=parse_text(text, mode=Mode.SMART_COREF)
    print(res)
    parse_res_processor(res=res)

   # parsed = json.loads(res)
   #print json.dumps(res, indent=4, sort_keys=True)
