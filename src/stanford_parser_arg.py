'''
Created on Oct 7, 2016
uses py4j to connect to a java program, java program should be running on the local host
at port 25335.

@author: amita
'''
import nltk
import string
from src import modify_predictions_filters
import re 
import enchant
from py4j.java_gateway import JavaGateway, GatewayParameters
gateway=JavaGateway(gateway_parameters=GatewayParameters(port=25340))

def parse_text(text):
    res=gateway.entry_point.parsed_text(text)
    return res

def changeparsedtextformat_tokens(res):
    sentences=dict()
    all_sentences=[] 
    for sentence in res:
        newdict={}
        newdict["pos"]=sentence["pos"]
        all_sentences.append(newdict)
    sentences["sentences"]=all_sentences    
    return sentences

def filtersentence(sentence,required_num_dict_words):
    """
    filter a  sentence based on dictionary words and verb
    """
    sent=sentence
    re_number = re.compile("[0-9]+(\.[0-9]+)?")
    en_dict = enchant.Dict("en_US")
    # Make sure sentence has at least n english words
    add=True
    tokens = nltk.word_tokenize(sent)
    words = [x for x in tokens if x not in string.punctuation]
    if not  modify_predictions_filters.has_x_english_words(words, en_dict, required_num_dict_words, re_number):
        add=False
    if not modify_predictions_filters.has_a_verb:
        add =False
    return add       
def changeparsedtextformat(res):
    sentences=dict()
    all_sentences=[] 
    for sentence in res:
        newdict={}
        newdict["pos"]=sentence["pos"]
        newdict["lemmas"]=sentence["lemmas"]
        newdict["parse"]=str(sentence["parse"][0])
        newdict["deps_cc"]=sentence["deps_cc"]
        newdict["tokens"]=sentence["tokens"]
        newdict["char_offset"]=sentence["char_offset"]
        all_sentences.append(newdict)
    sentences["sentences"]=all_sentences    
    return sentences
if __name__ == '__main__':
    text="Law-abiding citizens are forbidden to carry guns. America is 1 in gun ownership it tanks only 107th in homicide rates."
    res=parse_text(text)
    print(res)


#Expected Output
# [{'deps_cc': [['amod', 2, 1], ['nsubjpass', 4, 2], ['auxpass', 4, 3], ['mark', 6, 5], ['xcomp', 4, 6],
#  ['dobj', 6, 7], ['punct', 4, 8], ['root', 0, 4]], 'pos': ['JJ', 'NNS', 'VBP', 'VBN', 'TO', 'VB', 'NNS', '.'], 
#  'lemmas': ['law-abiding', 'citizen', 'be', 'forbid', 'to', 'carry', 'gun', '.'], 
#  'tokens': ['Law-abiding', 'citizens', 'are', 'forbidden', 'to', 'carry', 'guns', '.'], 
#  'parse': ['(ROOT (S (NP (JJ Law-abiding) (NNS citizens)) (VP (VBP are) (VP (VBN forbidden) (S (VP (TO to) (VP (VB carry) (NP (NNS guns))))))) (. .)))'], 'char_offset': [[0, 11], [12, 20], [21, 24], [25, 34], [35, 37], [38, 43], [44, 48], [48, 49]]}, {'deps_cc': [['nsubj', 3, 1], ['cop', 3, 2], ['case', 6, 4], ['compound', 6, 5], ['nmod:in', 3, 6], ['dep', 8, 7], ['nsubj', 10, 8], ['advmod', 10, 9], ['acl:relcl', 3, 10], ['case', 13, 11], 
#['compound', 13, 12], ['nmod:in', 10, 13], ['punct', 3, 14], ['root', 0, 3]], 'pos': ['NNP', 'VBZ', 'CD', 'IN', 'NN', 'NN', 'PRP', 'NNS', 'RB', 'VBP', 'IN', 'NN', 'NNS', '.'], 'lemmas': ['America', 'be', '1', 'in', 'gun', 'ownership', 'it', 'tank', 'only', '107th', 'in', 'homicide', 'rate', '.'], 'tokens': ['America', 'is', '1', 'in', 'gun', 'ownership', 'it', 'tanks', 'only', '107th', 'in', 'homicide', 'rates', '.'],
# 'parse': ['(ROOT (S (NP (NNP America)) (VP (VBZ is) (NP (NP (CD 1)) (PP (IN in) (NP (NN gun) (NN ownership))) (SBAR (S (NP (PRP it) (NNS tanks)) (ADVP (RB only)) (VP (VBP 107th) (PP (IN in) (NP (NN homicide) (NNS rates)))))))) (. .)))'], 'char_offset': [[50, 57], [58, 60], [61, 62], [63, 65], [66, 69], [70, 79], [80, 82], [83, 88], [89, 93], [94, 99], [100, 102], [103, 111], [112, 117], [117, 118]]}]
# 

