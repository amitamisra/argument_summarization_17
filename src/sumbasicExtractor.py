'''
Created on May 9, 2017

@author: amita
'''

import nltk
import string
import generate_baselines
from sumy.utils import to_string


class SumBasicExtractor():
    def getsumbasicsummary_set(self,sentences,num_sent):
            text=" ".join(sentences)
            sumbasic_sent_set=set()
            summary = generate_baselines.run_sumbasic_text(text,num_sent)
            for sent in summary:
                sent=to_string(sent)
                sumbasic_sent_set.add(sent)
            return sumbasic_sent_set

if __name__ == '__main__':
    pass