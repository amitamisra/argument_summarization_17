'''
Created on Mar 15, 2017

@author: amita
'''
import pandas as pd
import nltk,sys
from nltk import Tree
import os,itertools,re

def remove_author(dialog_text):
    dialog_text=re.sub("(Author[1-2]:[0-9]*-)\s*", ' ',dialog_text)
    dialog_text= dialog_text.replace("\r\n"," ")
    #print(dialog_text)
    return (dialog_text)

def createpairs(row,dialog_text,reference_summary,key,filename):
    key=row[key]
    dialog_text=row[dialog_text]
    dialog_text=remove_author(dialog_text)
    reference_summary_list=re.split("Natural_Summary_Text_[1-5]:*",row["Natural_Summary_Text"])
    reference_summary=reference_summary_list[5]
    reference_summary=reference_summary.replace('\r'," ")
    #===========================================================================
    # for i in range(0,len(reference_summary)):
    #     print(reference_summary[i])
    # if len(reference_summary) !=6:
    #     print("errror in getting reference summaries")
    #     sys.exit("terminating")
    #===========================================================================
    dialog_sents=nltk.sent_tokenize(dialog_text)
    summ_sents=nltk.sent_tokenize(reference_summary)
    
    combinations=list(itertools.product(dialog_sents,summ_sents))
    allcoms=[]
    for  comb in combinations:
        comb_dict={}
        comb_dict["dialog_text"]=comb[0]
        comb_dict["summ_sent"]=comb[1]
        allcoms.append(comb_dict)
        
    dirname=os.path.dirname(filename)
    outbasedir=os.path.join(dirname,"dialog_summ_sim")
    if not os.path.exists(outbasedir):
            os.makedirs(outbasedir)
    output_file=os.path.join(outbasedir,key+".csv")        
    df=pd.DataFrame(allcoms)       
    df.to_csv(output_file) 

                

def run(filename,dialog_text,reference_summary,key):
    df=pd.read_json(filename)
    df.apply(createpairs,args=(dialog_text,reference_summary,key,filename),axis=1)
    
if __name__ == '__main__':
    filename="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gun-control/NaturalSummaryCSV_authorformat_all_pyramids_scus_author_edu_corenlp.json"
    dialog_text="dialog_no_format_author"
    reference_summary="Natural_Summary_Text"
    key="Key"
    run(filename,dialog_text,reference_summary,key)