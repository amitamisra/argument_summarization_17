'''
Created on Apr 10, 2017

@author: amita
'''
import pandas as pd
def testbalance( normalbalancefeaturefile, corebalabncefeaturefile):
    dfnormal=pd.read_csv(normalbalancefeaturefile)
    dfcoref=pd.read_csv(corebalabncefeaturefile)
    sentnos_normal=dfnormal["sent_no"].tolist()
    sentnos_coref=dfcoref["sent_no"].tolist()
    if sentnos_normal == sentnos_coref:
        print("test passes")
    else:
        print("test failed")    
    

def test( normalbalancefeaturefile, corebalabncefeaturefile):
    dfnormal=pd.read_csv(normalbalancefeaturefile)
    dfcoref=pd.read_csv(corebalabncefeaturefile)
    normalgps=dfnormal.groupby("Key")
    for key,df in  normalgps:
        coredf_key=dfcoref.loc[dfcoref['Key'] == key]
        sentnos_normal=df["sent_no"].tolist()
        sentnos_coref=coredf_key["sent_no"].tolist()
        if sentnos_normal == sentnos_coref:
            print("test passes")
        else:
            print("test failed")    
if __name__ == '__main__':
    normalbalancefeaturefile="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gun-control/Mturk/Features/balancedclean/all_mtbatch_GC_Test_featuresbalance.csv"
    corebalabncefeaturefile="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gun-control/Mturk/Features/balancedclean/all_mtbatch_GCTest_Coref_featuresbalance.csv"
    test( normalbalancefeaturefile, corebalabncefeaturefile)
    
    normalbalancefeaturefile="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gun-control/Mturk/Features/balancedclean/all_mtbatch_GC_Train_featuresbalance.csv"
    corebalabncefeaturefile="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gun-control/Mturk/Features/balancedclean/all_mtbatch_GCTrain_Coref_featuresbalance.csv"
    test( normalbalancefeaturefile, corebalabncefeaturefile)
    
    normalbalancefeaturefile="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gun-control/Mturk/mturkcontributorresults/all_mtbatch_result_SummaryCSV_authorformat_all_pyramids_scus_author_corenl.csv"
    corebalabncefeaturefile="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gun-control/Mturk/mturkcontributorresults/all_mtbatch_result_SummaryCSV_authorformat_all_pyramids_scus_author_corenl_corefsent.csv"
    test( normalbalancefeaturefile, corebalabncefeaturefile)
    