'''
Created on May 7, 2017

@author: amita
'''
import nltk
import string
import generate_baselines
from sumy.utils import to_string


class TextRankExtractor():
    def gettextranksummary_set(self,sentences,num_sent):
            text=" ".join(sentences)
            textrank_sent_set=set()
            summary = generate_baselines.run_textrank_text(text,num_sent)
            for sent in summary:
                sent=to_string(sent)
                #print(type(sent))
                #sent=self.preprocesssent(sent)
                #print(sent)
                textrank_sent_set.add(sent)
            return textrank_sent_set

if __name__ == '__main__':
    pass