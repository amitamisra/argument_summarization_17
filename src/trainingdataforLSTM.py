'''
Created on Mar 22, 2017

@author: amita
'''
import pandas as pd
import numpy as np
def bin_data(inputcsv,columnbin,numbins):
    df=pd.read_csv(inputcsv)
    df = df[np.isfinite(df[columnbin])]
    smallest = np.min(df[columnbin])
    largest = np.max(df[columnbin])
    num_edges = numbins
    # np.digitize(input_array, bin_edges)
    ind= np.digitize(df[columnbin], np.linspace(smallest, largest, num_edges))
    df['binned_Prediction'] = ind
    output_csv=inputcsv[:-4] +"_binned.csv"
    df.to_csv(output_csv)
    return df

def createtraining(dataframe,samples_bin,outputcsv):
    all_df=[]
    dfgrouped=dataframe.groupby("binned_Prediction")
    for key,group in dfgrouped:
        if group.shape[0] < samples_bin:
            dfgroup=group
        else:    
            dfgroup=group.sample(n=samples_bin,random_state=4)
        all_df.append(dfgroup)
        
    sampled_df=pd.concat(all_df)
    sampled_df=sampled_df.sample(frac=1, random_state=4)
    sampled_df.to_csv(outputcsv,index=False)
        
def run(inputcsv,columnbin,numbins,samples_bin,outputcsv):   
    dataframe=bin_data(inputcsv,columnbin,numbins)
    createtraining(dataframe,samples_bin,outputcsv)
         
if __name__ == '__main__':
    inputcsv="/Users/amita/sigdial_2016_data/data/guncontrol/gc_all_sentences_with_predictions_filtered.csv"
    columnbin="Prediction"
    numbins=10
    samples_bin=1500
    outputcsv="/Users/amita/git/argument_summarization_17/data/nn_data/gc_all_sentences_with_predictions_filtered_bin_sampled.csv"
    run(inputcsv,columnbin,numbins,samples_bin,outputcsv)
    
    