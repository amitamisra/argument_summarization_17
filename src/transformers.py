'''
Created on Apr 5, 2017

@author: amita
'''
import numpy as np
import pandas as pd
import gensim
import nltk
from sklearn.base import BaseEstimator, TransformerMixin
from collections import defaultdict
from config import classifier_config

class ColumnExtractor(BaseEstimator, TransformerMixin):
    def __init__(self,textcol):
        self.textcol=textcol

    def transform(self, df, y=None):
        return df[self.textcol].values

    def fit(self, df, y=None):
        return self
 
class Word2VecTransformer(BaseEstimator, TransformerMixin):
    def __init__(self, w2v_source):
        self.w2v_source = w2v_source
        self.w2v = None
        self.w2v_size = None

    def load_w2v(self):
        if self.w2v:
            return
        print("Loading w2v model...")
        source = classifier_config.w2v_models[self.w2v_source][0]
        is_bin = classifier_config.w2v_models[self.w2v_source][2]
        self.w2v = gensim.models.Word2Vec.load_word2vec_format(source, binary=is_bin, unicode_errors='ignore')
        print("Loaded model.")

    def sent2vec(self, sent):
        # if word not in stops and word not in punct]  ### BEST FOR GEN
        words = [word for word in nltk.word_tokenize(sent)]
        res = np.zeros(self.w2v.vector_size)
        count = 0
        for word in words:
            if word in self.w2v:
                count += 1
                res += self.w2v[word]
        if count != 0:
            res /= count
        return res

    def transform(self, df,textcol, y=None):
        self.load_w2v()
        d = df[textcol].apply(self.sent2vec)
        return np.array([d]).T

    def fit(self, df, y=None):
        return self





if __name__ == '__main__':
    pass