import os
import nltk

def data_from_dir(data_dir, extension = '.txt'):
    all_data = []
    if os.path.isdir(data_dir) == False:
        print("dir doesn't exist")
        return all_data
    prev_dir = os.getcwd()
    os.chdir(data_dir)
    for filename in os.listdir(data_dir):
        if extension in filename:
            f = open(filename, 'r')
            text = f.read().replace('\n', '')
            all_data.append(text)
            f.close()
    os.chdir(prev_dir)
    return all_data

def delete_data_from_dir(data_dir):
    if os.path.isdir(data_dir) == False:
        print("dir doesn't exist")
        return
    prev_dir = os.getcwd()
    os.chdir(data_dir)
    for filename in os.listdir(data_dir):
        os.remove(filename)
    os.chdir(prev_dir)
    return

def tree_height(parse_tree):
   # print parse_tree
    tree= nltk.Tree.fromstring(parse_tree)
   # tree.pretty_print()
    return tree.height()

def example():
    data_dir = os.getcwd() +  "/../rouge/system/"
    print(data_from_dir(data_dir))
    delete_data_from_dir(data_dir)
    data_dir = os.getcwd() +  "/../rouge/reference/"
    print(data_from_dir(data_dir))
    delete_data_from_dir(data_dir)

def tree_example():
    syntax_tree="(ROOT (FRAG (NP (NN Author1)) (SBAR (X (SYM :1)) (S (NP (PRN (: -) (NP (NN Nobody)))) (VP (VBZ has) (VP (VBN been) (ADJP (JJ able) (S (VP (TO to) (VP (VB prove) (SBAR (IN that) (S (NP (NN gun) (NNS owners)) (VP (VBP are) (ADJP (ADJP (JJR safer)) (PP (IN than) (NP (JJ non-gun) (NNS owners)))))))))))))))))"
    print(tree_height(parse_tree=syntax_tree))


#tree_example()
#example()
