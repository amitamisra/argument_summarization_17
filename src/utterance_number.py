import ast
import pandas as pd
def get_utterance_number(df):

    ids = []
    flag = True
    df['sent_no'] = df['sent_no'].astype(int)
    previous_key = df.ix[0, 'Key']
    dictionary = ast.literal_eval(df.ix[0, 'corelp_author_dict'])
    previous_author = dictionary['corenlp_info'][0]['Author_turn'] 
    for iter, row in df.iterrows():
        if flag:
            ids.append(previous_author)
            flag = False
        else:
            dictionary = ast.literal_eval(row['corelp_author_dict'])
            if dictionary['corenlp_info'][int(row['sent_no'])]['Author_turn'] != []:
                previous_author = dictionary['corenlp_info'][int(row['sent_no'])]['Author_turn']
            ids.append(previous_author)
        if row['Key'] != previous_key:
            flag = True
            previous_key = row['Key']
            
    return ids

if __name__ == '__main__':  
    testfile="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gun-control/Mturk/mturkcontributorresults/all_mtbatch_result_SummaryCSV_authorformat_all_pyramids_scus_author_corenl.csv"
    df=pd.read_csv(testfile)
    get_utterance_number(df)
    