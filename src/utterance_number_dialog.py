import ast
import pandas as pd
import copy
import createclasslabel_balance
import nltk
"""
adds utterance numbers to a dialogue
"""

def get_utterance_number(dfdialog):
    key=dfdialog["Key"].values[0]
    ids = []
    flag = True
    dfdialog['sent_no'] = dfdialog['sent_no'].astype(int)
    dfdialog=dfdialog.sort("sent_no")
    corenlpcolumn=dfdialog["corelp_author_dict"]
    dictionary=ast.literal_eval(corenlpcolumn.tolist()[0])
    #dictionary = ast.literal_eval(df.iloc[[0]]['corelp_author_dict'])
    previous_author = dictionary['corenlp_info'][0]['Author_turn'] 
    #counter=0
    for iter, row in dfdialog.iterrows():
        #counter=counter+1
        #print(counter)
        if flag:
            ids.append(previous_author)
            flag = False
        else:
            dictionary = ast.literal_eval(row['corelp_author_dict'])
            if dictionary['corenlp_info'][int(row['sent_no'])]['Author_turn'] != []:
                previous_author = dictionary['corenlp_info'][int(row['sent_no'])]['Author_turn']
            ids.append(previous_author)
        
            
    return ids

def addutterancenumber(inputcsv,wtvalue):
    allnewrows=[]
    df=pd.read_csv(inputcsv+".csv")
    outputcsv=inputcsv+"_turnids.csv"
    dfgrouped=df.groupby("Key")
    for key,dfdialog in dfgrouped:
        #if key != "1-10145_4_3__5_9_21_29_1":
            #continue
        #print(key)
        count=0
        dfdialog['sent_no'] = dfdialog['sent_no'].astype(int)
        dfdialog=dfdialog.sort("sent_no")
        ids=get_utterance_number(dfdialog)
        for  index, row in dfdialog.iterrows():
            row=row.to_dict()
            newrow={k: v for k, v in row.items() if not k.startswith("worker")}
            newrow["turn_ids"]=ids[count]
            newrow['class']=createclasslabel_balance.classlabel(row,"label",wtvalue)
            sent=row["sent"]
            words=nltk.word_tokenize(sent)
            count=count+1
            newrow["word_count"]=len(words)
            newrow["char_count"]=len(sent)
            allnewrows.append(newrow)
        #print(ids)
    
    newdf=pd.DataFrame(allnewrows)
    newdf.drop(['Dialog','dialog_no_format_author','pdtb'],inplace=True,axis=1)     
    newdf.to_csv(outputcsv)
    

if __name__ == '__main__':  
    gc="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gun-control/Mturk/mturkcontributorresults/all_mtbatch_result_SummaryCSV_authorformat_all_pyramids_scus_author_corenl"
    gm="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/gay-rights-debates/Mturk/mturkcontributorresults/all_mtbatch_result_AllDialogs_scu_corenlp"
    ab="/Users/amita/git/argument_summarization_17/data/data_originalsummaries/abortion/Mturk/mturkcontributorresults/all_mtbatch_abortion_result_AllDialogs_scu_corenlp"
    minweight=3
    addutterancenumber(gc,minweight)
    addutterancenumber(gm,minweight)
    addutterancenumber(gm,minweight)
    