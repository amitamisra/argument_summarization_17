'''
Created on Nov 28, 2016

@author: amita
'''
from nltk.tree import *
import file_handling
def  visualizetree(inputfile):
    stringtree=file_handling.readTextFile(inputfile)
    length=len(stringtree)
    print(stringtree)


def run(inputfile):
    visualizetree(inputfile)
    
if __name__ == '__main__':
    inputfile="/Users/amita/git/feng-hirst-rst-parser-acl-2014/texts/results/rst_feng_output/1-10073_26_23__27_28_29_79_85_1.txt.tree"
    run(inputfile)